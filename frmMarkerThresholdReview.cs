﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
namespace ArrayTubeAnalyzer
{
    public partial class FrmMarkerThresholdReview : Form
    {
        private readonly ListViewColumnSorter _lvwSpotsSorter = new ListViewColumnSorter();

        private readonly ListViewColumnSorter _lvwSpotInfoSorter = new ListViewColumnSorter();

        private SpotImageCollection _sic;

        private SpotMarkerArrayIndices _sga1;

        private readonly List<LabelWithColours> _lSpotH = new List<LabelWithColours>();

        private readonly SampleData _sd;

        public FrmMarkerThresholdReview(SampleData sampleData, double dMin, double dMax)
        {
            // This call is required by the designer.
            InitializeComponent();

            // Add any initialization after the InitializeComponent() call.
            _sd = sampleData;
            _sd.UpdateCalls();
            SetupChart(dMin, dMax);
            UpdateForm();

            lvwSpots.ListViewItemSorter = _lvwSpotsSorter;
            lvwSpotInfo.ListViewItemSorter = _lvwSpotInfoSorter;
            txtThreshold.LostFocus += TxtThresholdLostFocus;
        }

        public void UpdateForm()
        {
            UpdateSpotListview();
            lvwSpots.Items[0].Selected = true;
            lvwSpots.Items[0].Focused = true;
            UpdateChart(new SpotMarkerIndices(0, 0));
        }

        private void SetupChart(double dMin, double dMax)
        {
            chtSignals.ChartAreas[0].AxisX.IsLabelAutoFit = true;
            chtSignals.ChartAreas[0].AxisX.LabelStyle.Angle = 90;
            chtSignals.ChartAreas[0].AxisX.Interval = 1;
            chtSignals.ChartAreas[0].AxisY.Maximum = dMax;
            chtSignals.ChartAreas[0].AxisY.Minimum = dMin;
            chtSignals.ChartAreas[0].AxisY.Interval = 0.1;

            var sLine = new StripLine
                            {
                                BorderColor = Color.Red,
                                BorderWidth = 2,
                                BorderDashStyle = ChartDashStyle.Solid,
                                IntervalOffset = 0
                            };
            chtSignals.ChartAreas[0].AxisY.StripLines.Add(sLine);
        }

        private void UpdateChart(SpotMarkerIndices indices)
        {
            chtSignals.Titles[0].Text = "Signals for " + _sd.Markers[indices.Marker] + " - Threshold: " + _sd.Thresholds.Markers[indices.Marker].Threshold.ToString("0.0000");
            //+ " - Spot " + _lSpots(indices.Gene)(indices.Spot).ToString
            chtSignals.ChartAreas[0].AxisY.StripLines[0].IntervalOffset = _sd.Thresholds.Markers[indices.Marker].Threshold;
            chtSignals.Series["Signals"].Points.Clear();
            for (int i = 0; i < _sd.SampleStats.Count; i++)
            {
                double dSignal = _sd.SampleStats[i][indices.Marker].FinalAverageSignal;
                //Signal(indices.Spot)
                chtSignals.Series["Signals"].Points.Add(dSignal);
                chtSignals.Series["Signals"].Points[i].AxisLabel = _sd.SampleNames[i];
                var sga = new SpotMarkerArrayIndices(indices.Marker, indices.Spot, i);
                chtSignals.Series["Signals"].Points[i].Color = GetChartPointColor(sga);
                chtSignals.Series["Signals"].Points[i].Tag = sga;
            }
        }

        private Color GetChartPointColor(SpotMarkerArrayIndices sga)
        {
            Color colorPoint = Color.Red;
            Calls gCall = _sd.SampleCalls[sga.Array].Calls[sga.Marker];
            switch (gCall)
            {
                case Calls.Absent:
                    colorPoint = _sd.Settings.C.AbsentColour;
                    break;
                case Calls.AbsentAuto:
                    colorPoint = _sd.Settings.C.AbsentAutoColour;
                    break;
                case Calls.Present:
                    colorPoint = _sd.Settings.C.PresentColour;
                    break;
                case Calls.PresentAuto:
                    colorPoint = _sd.Settings.C.PresentAutoColour;
                    break;
                case Calls.AmbiguousAbsent:
                case Calls.AmbiguousManual:
                    colorPoint = _sd.Settings.C.AbsentFlaggedColour;
                    break;
                case Calls.AmbiguousPresent:
                    colorPoint = _sd.Settings.C.PresentFlaggedColour;
                    break;
            }
            return colorPoint;
        }

        private void UpdateSpotListview()
        {
            lvwSpots.Items.Clear();
            var lLvi = new List<ListViewItem>();
            for (int i = 0; i < _sd.Spots.Count; i++)
            {
                string sGene = _sd.Markers[i];
                string sThreshold = _sd.Thresholds.Markers[i].Threshold.ToString("0.0000");
                string sThresholdType = _sd.Thresholds.Markers[i].ThresholdType.ToString();
                var lvi = new ListViewItem(new[] { sGene, sThreshold, sThresholdType }) { Tag = new SpotMarkerIndices(i, 0) };
                lLvi.Add(lvi);
            }
            lvwSpots.Items.AddRange(lLvi.ToArray());
        }

        private void UpdateSpotSignalListview(SpotMarkerIndices indices)
        {
            lvwSpotInfo.Items.Clear();
            var lLvi = new List<ListViewItem>();
            for (int i = 0; i < _sd.SampleStats.Count; i++)
            {
                MarkerInfo at = _sd.SampleStats[i][indices.Marker];
                Calls gCall = _sd.SampleCalls[i].Calls[indices.Marker];
                Color bc = Color.LightGreen;
                switch (gCall)
                {
                    case Calls.Absent:
                        bc = _sd.Settings.C.AbsentListviewColour;
                        break;
                    case Calls.AbsentAuto:
                        bc = _sd.Settings.C.AbsentAutoListviewColour;
                        break;
                    case Calls.Present:
                        bc = _sd.Settings.C.PresentListviewColour;
                        break;
                    case Calls.PresentAuto:
                        bc = _sd.Settings.C.PresentAutoListviewColour;
                        break;
                    case Calls.AmbiguousAbsent:
                    case Calls.AmbiguousManual:
                        bc = _sd.Settings.C.AbsentFlaggedListviewColour;
                        break;
                    case Calls.AmbiguousPresent:
                        bc = _sd.Settings.C.PresentFlaggedListviewColour;
                        break;
                }
                var lvi = new ListViewItem(new[]
                                               {
                                                   _sd.SampleNames[i],
                                                   at.Signal[indices.Spot].ToString("0.0000"),
                                                   at.FinalAverageSignal.ToString("0.0000"),
                                                   gCall.ToString()
                                               }) { BackColor = bc, Tag = new SpotMarkerArrayIndices(indices.Marker, indices.Spot, i) };
                lLvi.Add(lvi);
            }
            lvwSpotInfo.Items.AddRange(lLvi.ToArray());
            lvwSpotInfo.Items[0].Selected = true;
        }

        private void LvwSpotsSelectedIndexChanged(Object sender, EventArgs e)
        {
            SpotMarkerIndices indices = GetSelectedSpot();
            if (indices == null)
                return;
            UpdateChart(indices);
            UpdateSpotSignalListview(indices);
            UpdateSpotImageCollection(indices);
        }

        private void UpdateSpotImageCollection(SpotMarkerIndices indices)
        {
            _sic = null;
            _sic = new SpotImageCollection(_sd, _sd.SampleStats, _sd.Images, _sd.Calibration, indices.Marker, _sd.Spots[indices.Marker].ToArray(), _sd.Spots);
        }

        private SpotMarkerIndices GetSelectedSpot()
        {
            if (lvwSpots.SelectedItems.Count == 0)
                return null;
            return (SpotMarkerIndices)lvwSpots.SelectedItems[0].Tag;
        }

        private SpotMarkerArrayIndices GetSelectedSpotInfo()
        {
            if (lvwSpotInfo.SelectedItems.Count == 0)
                return null;
            return (SpotMarkerArrayIndices)lvwSpotInfo.SelectedItems[0].Tag;
        }

        private void LvwSpotInfoItemSelectionChanged(Object sender, ListViewItemSelectionChangedEventArgs e)
        {
            SpotMarkerArrayIndices indices = GetSelectedSpotInfo();
            if (indices == null)
                return;
            ShowSpotsOnArrayImage(indices);
        }

        private void ShowSpotsOnArrayImage(SpotMarkerArrayIndices indices)
        {
            foreach (LabelWithColours t in _lSpotH)
            {
                t.Dispose();
            }
            _lSpotH.Clear();
            gbxPicArray.Text = _sd.SampleNames[indices.Array];
            picArray.Image = _sd.Images[indices.Array];
            int[] iSpots = _sd.Spots[indices.Marker].ToArray();
            int iI = 0;
            int iJ = 0;

            foreach (int iSpot in iSpots)
            {
                GetTopLeftIndices(iSpot, ref iI, ref iJ);
                double top;
                double left;
                double height;
                double width;
                GetTopLeftValues(_sd.GetCalibrationInfo(indices.Array), iI, iJ, out top, out left, out width, out height);
                var lbl = new LabelWithColours
                              {
                                  Parent = picArray,
                                  Location = new Point((int)left, (int)top),
                                  Size = new Size((int)width, (int)height),
                                  BorderColor = iSpot == indices.Spot ? Color.Aqua : Color.Black
                              };
                lbl.Show();
                _lSpotH.Add(lbl);
            }
        }

        private void GetTopLeftValues(CalibrationInfo ci, int iI, int iJ, out double dTop, out double dLeft, out double dWidth, out double dHeight)
        {
            double heightRatio = picArray.Height / 400d;
            double widthRatio = picArray.Width / 400d;
            double h1 = ci.TopLeftX * widthRatio;
            //lblTopLeft.Left
            double h2 = ci.TopRightX * widthRatio;
            //lblTopRight.Left
            double h4 = ci.BottomLeftX * widthRatio;
            //lblBottomLeft.Left
            double v1 = ci.TopLeftY * heightRatio;
            //lblTopLeft.Top
            double v2 = ci.TopRightY * heightRatio;
            //lblTopRight.Top
            double v4 = ci.BottomLeftY * heightRatio;
            //lblBottomLeft.Top

            double dVTop = v1 - v2;
            //change in vertical position between top two corner spots; positive means that v1 is lower than v2; negative means that v2 is lower than v1
            double dHLeft = h1 - h4;
            //change in horiz. position between left most corner spots; positive = h1 farther right than h4; neg. = h4 farther right than h1

            //Distances between top, bottom, right and left corner spots
            double distTop = h2 - h1 + (20d * widthRatio);
            //lblTopRight.Width 'horiz. distance between top two corner spots
            double distLeft = v4 - v1 + (20d * heightRatio);
            //lblBottomLeft.Height 'vert. distance between left most corner spots
            dHeight = distLeft / _sd.Layout.Rows;
            dWidth = distTop / _sd.Layout.Columns;
            dTop = v1 + ((iI + 1d) * distLeft / _sd.Layout.Rows) - (((iJ + 1d) / _sd.Layout.Columns) * dVTop) - (distLeft / _sd.Layout.Rows);
            dLeft = h1 + ((iJ + 1d) * distTop / _sd.Layout.Columns) - (((iI + 1d) / _sd.Layout.Rows) * dHLeft) - (distTop / _sd.Layout.Columns);
        }

        private void GetTopLeftIndices(int iSpot, ref int iI, ref int iJ)
        {
            for (int i = 0; i < _sd.Layout.Rows; i++)
            {
                for (int j = 0; j < _sd.Layout.Columns; j++)
                {
                    int spt = (_sd.Layout.Columns - (i + 1)) * _sd.Layout.Rows + (j + 1);
                    if (iSpot == spt)
                    {
                        iI = i;
                        iJ = j;
                        return;
                    }
                }
            }
        }

        private void LvwSpotsColumnClick(Object sender, ColumnClickEventArgs e)
        {
            // Determine if the clicked column is already the column that is being sorted.
            if ((e.Column == _lvwSpotsSorter.SortColumn))
            {
                // Reverse the current sort direction for this column.
                _lvwSpotsSorter.Order = (_lvwSpotsSorter.Order == SortOrder.Ascending) ? SortOrder.Descending : SortOrder.Ascending;
            }
            else
            {
                // Set the column number that is to be sorted; default to ascending.
                _lvwSpotsSorter.SortColumn = e.Column;
                _lvwSpotsSorter.Order = SortOrder.Ascending;
            }
            // Perform the sort with these new sort options.
            lvwSpots.Sort();
        }

        private void LvwSpotInfoColumnClick(Object sender, ColumnClickEventArgs e)
        {
            // Determine if the clicked column is already the column that is being sorted.
            if ((e.Column == _lvwSpotInfoSorter.SortColumn))
            {
                // Reverse the current sort direction for this column.
                _lvwSpotInfoSorter.Order = (_lvwSpotInfoSorter.Order == SortOrder.Ascending) ? SortOrder.Descending : SortOrder.Ascending;
            }
            else
            {
                // Set the column number that is to be sorted; default to ascending.
                _lvwSpotInfoSorter.SortColumn = e.Column;
                _lvwSpotInfoSorter.Order = SortOrder.Ascending;
            }
            // Perform the sort with these new sort options.
            lvwSpotInfo.Sort();
        }

        private void PicArraySizeChanged(Object sender, EventArgs e)
        {
            SpotMarkerArrayIndices indices = GetSelectedSpotInfo();
            if (indices == null)
                return;
            ShowSpotsOnArrayImage(indices);
        }

        private void ShowSpotsToolStripMenuItemClick(Object sender, EventArgs e)
        {
            SpotMarkerIndices indices = GetSelectedSpot();
            if (indices == null)
                return;
            var f = new FrmShowMarkerSpots(_sd, indices.Marker, 0d);
            f.Show();
        }

        private void ChtSignalsMouseUp(Object sender, MouseEventArgs e)
        {
            HitTestResult htr = chtSignals.HitTest(e.X, e.Y);
            if (htr == null)
                return;
            if (htr.PointIndex == -1)
                return;
            Point pointFrmSpot = System.Windows.Forms.Cursor.Position;
            var f = new FrmSpotPreview(_sd,
                                                    pointFrmSpot,
                                                    (SpotMarkerArrayIndices)chtSignals.Series[0].Points[htr.PointIndex].Tag,
                                                    _sic);
            f.Show();
        }

        private void LvwSpotInfoMouseClick(Object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                ListViewHitTestInfo ht = lvwSpotInfo.HitTest(e.Location);
                if (ht.Item == null)
                    return;
                if (ht.Item.Tag == null)
                    return;
                _sga1 = (SpotMarkerArrayIndices)ht.Item.Tag;

                Point pointFrmSpot = System.Windows.Forms.Cursor.Position;
                var f = new FrmSpotPreview(_sd, pointFrmSpot, _sga1, _sic);
                f.Show();
            }
        }

        private void SampleQCToolStripMenuItemClick(Object sender, EventArgs e)
        {
            _sd.Forms.SampleQC.Focus();
        }

        private void MarkerThresholdAnalysisToolStripMenuItemClick(Object sender, EventArgs e)
        {
            if (_sd.Forms.MarkerThresholdAnalysis == null)
            {
                _sd.Forms.MarkerThresholdAnalysis = new FrmMarkerThresholdAnalysis(_sd);
                _sd.Forms.MarkerThresholdAnalysis.Show();
            }
            _sd.Forms.MarkerThresholdAnalysis.Focus();
        }

        private void ReviewMarkerCallsToolStripMenuItemClick(Object sender, EventArgs e)
        {
            if (_sd.Forms.ReviewMarkerCalls == null)
            {
                _sd.Forms.ReviewMarkerCalls = new FrmReviewMarkerCalls(_sd);
                _sd.Forms.ReviewMarkerCalls.Show();
            }
            _sd.Forms.ReviewMarkerCalls.Focus();
        }

        private void ClusteringToolStripMenuItemClick(Object sender, EventArgs e)
        {
            _sd.UpdateCalls();
            if (_sd.Forms.Clustering == null)
            {
                _sd.Forms.Clustering = new FrmClustering(_sd);
                _sd.Forms.Clustering.Show();
            }
            _sd.Forms.Clustering.Focus();
        }

        private void SampleDiagnosticsToolStripMenuItemClick(Object sender, EventArgs e)
        {
            _sd.UpdateCalls();
            _sd.ClusterUPGMA();
            if (_sd.Forms.Diagnosis == null)
            {
                _sd.Forms.Diagnosis = new FrmDiagnosis(_sd);
                _sd.Forms.Diagnosis.Show();
            }
            _sd.Forms.Diagnosis.Focus();
        }

        private void SaveToolStripMenuItemClick(Object sender, EventArgs e)
        {
            _sd.WriteToMetaDataFile();
        }

        private void ExitToolStripMenuItemClick(Object sender, EventArgs e)
        {
            if (_sd.Forms.SampleQC.ExitApplication() == false)
            {
                Environment.Exit(0);
            }
        }

        private void AboutToolStripMenuItemClick(Object sender, EventArgs e)
        {
            var f = new AboutBox1(_sd.Forms);
            f.ShowDialog();
        }

        private void CloseMarkerThresholdReviewToolStripMenuItemClick(Object sender, EventArgs e)
        {
            Close();
        }

        /*
                private void ContextMenuStrip1Opening(System.Object sender, System.ComponentModel.CancelEventArgs e)
                {
                    SpotMarkerIndices sga = GetSelectedSpot();
                    txtThreshold.Text = _sd.Thresholds.Markers[sga.Marker].Threshold.ToString();
                }
        */

        private void TxtThresholdKeyPress(Object sender, KeyPressEventArgs e)
        {
            int i;
            if (int.TryParse(e.KeyChar.ToString(CultureInfo.InvariantCulture), out i) | e.KeyChar == '\b')
            {
                e.Handled = false;
            }
            else if (e.KeyChar == '.')
            {
                e.Handled = txtThreshold.Text.Contains(".");
            }
            else if (e.KeyChar == (char)(Keys.Enter) | e.KeyChar == (char)(Keys.Space) | e.KeyChar == '\t')
            {
                double d;
                if (double.TryParse(txtThreshold.Text, out d))
                {
                    SpotMarkerIndices indices = GetSelectedSpot();
                    const double epsilon = 0.00001;
                    if (Math.Abs(_sd.Thresholds.Markers[indices.Marker].Threshold - d) > epsilon)
                    {
                        MarkerThresholdInfo mInfo = _sd.Thresholds.Markers[indices.Marker];
                        mInfo.SetUserDefinedThreshold(d);
                        mInfo.ThresholdType = ThresholdType.UserDefined;
                        _sd.Thresholds.Markers[indices.Marker] = mInfo;

                        if (_sd.Forms.MarkerThresholdAnalysis != null)
                        {
                            _sd.Forms.MarkerThresholdAnalysis.ShowUserDefinedThreshold(indices.Marker);
                        }

                        _sd.UpdateCalls();
                        if (_sd.Forms.ReviewMarkerCalls != null)
                        {
                            _sd.Forms.ReviewMarkerCalls.UpdateForm(_sd.Forms.ReviewMarkerCalls.Indices.Array);
                        }
                        UpdateChart(indices);
                        UpdateSpotSignalListview(indices);
                        UpdateSpotImageCollection(indices);
                        UpdateListviewItem(indices);
                    }
                }
            }
            else
            {
                e.Handled = true;
            }
        }

        private void TxtThresholdLostFocus(object sender, EventArgs e)
        {
            var keyPressEvent = new KeyPressEventArgs((char)(Keys.Enter));
            TxtThresholdKeyPress(sender, keyPressEvent);
        }


        private void UpdateListviewItem(SpotMarkerIndices indices)
        {
            foreach (ListViewItem lvi in lvwSpots.Items)
            {
                var tagIndices = (SpotMarkerIndices)lvi.Tag;
                if (tagIndices.Marker == indices.Marker)
                {
                    lvi.SubItems[1].Text = _sd.Thresholds.Markers[indices.Marker].Threshold.ToString("0.0000");
                    lvi.SubItems[2].Text = _sd.Thresholds.Markers[indices.Marker].ThresholdType.ToString();
                    break;
                }
            }
        }

        private void FrmMarkerThresholdReviewFormClosing(object sender, FormClosingEventArgs e)
        {
            _sd.Forms.MarkerThresholdReview = null;
        }

        private void SetThresholdManuallyToolStripMenuItemClick(object sender, EventArgs e)
        {

        }
    }

}
