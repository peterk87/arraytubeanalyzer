using System;
using System.Collections.Generic;

namespace ArrayTubeAnalyzer
{
    [Serializable]
    public class ClusterGroup : IComparable
    {
        public List<int> NameIndices;
        public List<int> ClusterNumber;
        public List<double> Distances;

        public ClusterGroup(int iName, IEnumerable<double> lDistances)
        {
            NameIndices = new List<int> {iName};
            ClusterNumber = new List<int>();
            Distances = new List<double>();
            Distances.AddRange(lDistances);
        }

        public ClusterGroup(ClusterGroup cg)
        {
            NameIndices = new List<int>();
            ClusterNumber = new List<int>();
            Distances = new List<double>();
            NameIndices.AddRange(cg.NameIndices);
            ClusterNumber.AddRange(cg.ClusterNumber);
            Distances.AddRange(cg.Distances);
        }

        public ClusterGroup(ClusterGroup cg1, ClusterGroup cg2)
        {
            Distances = new List<double>();
            NameIndices = new List<int>();
            ClusterNumber = new List<int>();
            //Add the averages of each cluster comparison into the new Cluster variable
            for (int i = 0; i < cg1.Distances.Count; i++)
            {
                Distances.Add((cg1.Distances[i] + cg2.Distances[i]) / 2);
            }
            NameIndices.AddRange(cg1.NameIndices);
            NameIndices.AddRange(cg2.NameIndices);
        }

        public int CompareTo(object obj)
        {
            var cg = obj as ClusterGroup;

            if (cg != null && NameIndices.Count >= cg.NameIndices.Count)
            {
                return 1;
            }
            return -1;
        }
    }
}