﻿namespace ArrayTubeAnalyzer
{
    partial class FrmMarkerThresholdAnalysis
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.ListViewItem listViewItem1 = new System.Windows.Forms.ListViewItem(new string[] {
            "Marker",
            "1"}, -1);
            System.Windows.Forms.ListViewItem listViewItem2 = new System.Windows.Forms.ListViewItem(new string[] {
            "Threshold",
            "1"}, -1);
            System.Windows.Forms.ListViewItem listViewItem3 = new System.Windows.Forms.ListViewItem(new string[] {
            "Ambiguity Lower Bound",
            "1"}, -1);
            System.Windows.Forms.ListViewItem listViewItem4 = new System.Windows.Forms.ListViewItem(new string[] {
            "Ambiguity Upper Bound",
            "1"}, -1);
            System.Windows.Forms.ListViewItem listViewItem5 = new System.Windows.Forms.ListViewItem(new string[] {
            "Threshold Type",
            "1"}, -1);
            System.Windows.Forms.ListViewItem listViewItem6 = new System.Windows.Forms.ListViewItem(new string[] {
            "Global Background",
            "1"}, -1);
            System.Windows.Forms.ListViewItem listViewItem7 = new System.Windows.Forms.ListViewItem(new string[] {
            "Global Background Factor",
            "1"}, -1);
            System.Windows.Forms.ListViewItem listViewItem8 = new System.Windows.Forms.ListViewItem(new string[] {
            "Percent Positive Signal",
            "1"}, -1);
            System.Windows.Forms.ListViewItem listViewItem9 = new System.Windows.Forms.ListViewItem(new string[] {
            "Percent Positive Signal Factor",
            "1"}, -1);
            System.Windows.Forms.ListViewItem listViewItem10 = new System.Windows.Forms.ListViewItem(new string[] {
            "Global Error",
            "1"}, -1);
            System.Windows.Forms.ListViewItem listViewItem11 = new System.Windows.Forms.ListViewItem(new string[] {
            "Global Error SD",
            "1"}, -1);
            System.Windows.Forms.ListViewItem listViewItem12 = new System.Windows.Forms.ListViewItem(new string[] {
            "Global Error SD Factor",
            "1"}, -1);
            System.Windows.Forms.ListViewItem listViewItem13 = new System.Windows.Forms.ListViewItem(new string[] {
            "Threshold Flagging (GE SD x GE Factor)",
            "1"}, -1);
            System.Windows.Forms.ListViewItem listViewItem14 = new System.Windows.Forms.ListViewItem(new string[] {
            "No. of Flagged",
            "1"}, -1);
            System.Windows.Forms.ListViewItem listViewItem15 = new System.Windows.Forms.ListViewItem(new string[] {
            "Keep Marker?",
            "1"}, -1);
            System.Windows.Forms.ListViewItem listViewItem16 = new System.Windows.Forms.ListViewItem(new string[] {
            "Marker Comment",
            "1"}, -1);
            System.Windows.Forms.ListViewItem listViewItem17 = new System.Windows.Forms.ListViewItem(new string[] {
            "Max Signal",
            "1"}, -1);
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMarkerThresholdAnalysis));
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.StatusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tslStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.lvwMarkers = new ArrayTubeAnalyzer.ListViewFF();
            this.chMarker = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chThreshold = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chThresholdType = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chMaxSignal = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chAmbiguous = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chKeepMarker = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chMarkerComments = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.contextMenuMarkers = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cbxThresholdType = new System.Windows.Forms.ToolStripComboBox();
            this.showSpotStripToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showMarkerStripToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.changeMarkerCommentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.setMarkerThresholdToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.cht = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnDecrementWindowSize = new System.Windows.Forms.Button();
            this.btnIncrementWindowSize = new System.Windows.Forms.Button();
            this.chkFrequencies = new System.Windows.Forms.CheckBox();
            this.btnDecrementBinSize = new System.Windows.Forms.Button();
            this.btnIncrementBinSize = new System.Windows.Forms.Button();
            this.lblWindowSize = new System.Windows.Forms.Label();
            this.Label1 = new System.Windows.Forms.Label();
            this.txtBinSize = new System.Windows.Forms.TextBox();
            this.chkWindowedSTDev = new System.Windows.Forms.CheckBox();
            this.chkWindowedMedian = new System.Windows.Forms.CheckBox();
            this.txtWindowSize = new System.Windows.Forms.TextBox();
            this.gbxInfo = new System.Windows.Forms.GroupBox();
            this.lvwParam = new ArrayTubeAnalyzer.ListViewFF();
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.ddbFile = new System.Windows.Forms.ToolStripDropDownButton();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.closeMarkerThresholdAnalysisToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ddbView = new System.Windows.Forms.ToolStripDropDownButton();
            this.sampleQCToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.markerThresholdReviewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.markerCallReviewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clusteringToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.diagnosticsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ddbThresholdType = new System.Windows.Forms.ToolStripDropDownButton();
            this.globalBackgroundToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.percentPositiveSignalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.resetAllUserDefinedThresholdsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ddbThresholdFlagging = new System.Windows.Forms.ToolStripDropDownButton();
            this.globalErrorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.txtGlobalErrorSD = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.thresholdFactorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.txtThresholdFlaggingFactor = new System.Windows.Forms.ToolStripTextBox();
            this.ddbGlobalBackgroundThreshold = new System.Windows.Forms.ToolStripDropDownButton();
            this.globalBackgroundToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.lblGlobalBackground = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.thresholdToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lblGlobalBackgroundThreshold = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.thresholdCutoffToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.txtGlobalBackgroundCutoff = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.thresholdFactorToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.txtGlobalBackgroundFactor = new System.Windows.Forms.ToolStripTextBox();
            this.ddbPercentPositiveSignalThreshold = new System.Windows.Forms.ToolStripDropDownButton();
            this.thresholdCutoffToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.txtPercentPositiveCutoff = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.thresholdFactorToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.txtPercentPositiveFactor = new System.Windows.Forms.ToolStripTextBox();
            this.btnRecalculate = new System.Windows.Forms.ToolStripButton();
            this.ColumnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ColumnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ToolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.setUpperAndLowerThresholdsAcrossDatasetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripContainer1.BottomToolStripPanel.SuspendLayout();
            this.toolStripContainer1.ContentPanel.SuspendLayout();
            this.toolStripContainer1.TopToolStripPanel.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.StatusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.contextMenuMarkers.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cht)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.gbxInfo.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.BottomToolStripPanel
            // 
            this.toolStripContainer1.BottomToolStripPanel.Controls.Add(this.StatusStrip1);
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.Controls.Add(this.splitContainer1);
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(1008, 499);
            this.toolStripContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripContainer1.Location = new System.Drawing.Point(0, 0);
            this.toolStripContainer1.Name = "toolStripContainer1";
            this.toolStripContainer1.Size = new System.Drawing.Size(1008, 546);
            this.toolStripContainer1.TabIndex = 0;
            this.toolStripContainer1.Text = "toolStripContainer1";
            // 
            // toolStripContainer1.TopToolStripPanel
            // 
            this.toolStripContainer1.TopToolStripPanel.Controls.Add(this.toolStrip1);
            // 
            // StatusStrip1
            // 
            this.StatusStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.StatusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tslStatus});
            this.StatusStrip1.Location = new System.Drawing.Point(0, 0);
            this.StatusStrip1.Name = "StatusStrip1";
            this.StatusStrip1.Size = new System.Drawing.Size(1008, 22);
            this.StatusStrip1.TabIndex = 0;
            // 
            // tslStatus
            // 
            this.tslStatus.Name = "tslStatus";
            this.tslStatus.Size = new System.Drawing.Size(39, 17);
            this.tslStatus.Text = "Ready";
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.lvwMarkers);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(1008, 499);
            this.splitContainer1.SplitterDistance = 373;
            this.splitContainer1.TabIndex = 0;
            // 
            // lvwMarkers
            // 
            this.lvwMarkers.CheckBoxes = true;
            this.lvwMarkers.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.chMarker,
            this.chThreshold,
            this.chThresholdType,
            this.chMaxSignal,
            this.chAmbiguous,
            this.chKeepMarker,
            this.chMarkerComments});
            this.lvwMarkers.ContextMenuStrip = this.contextMenuMarkers;
            this.lvwMarkers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvwMarkers.FullRowSelect = true;
            this.lvwMarkers.GridLines = true;
            this.lvwMarkers.HideSelection = false;
            this.lvwMarkers.Location = new System.Drawing.Point(0, 0);
            this.lvwMarkers.MultiSelect = false;
            this.lvwMarkers.Name = "lvwMarkers";
            this.lvwMarkers.ShowItemToolTips = true;
            this.lvwMarkers.Size = new System.Drawing.Size(369, 495);
            this.lvwMarkers.TabIndex = 0;
            this.lvwMarkers.UseCompatibleStateImageBehavior = false;
            this.lvwMarkers.View = System.Windows.Forms.View.Details;
            this.lvwMarkers.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.LvwGenesColumnClick);
            this.lvwMarkers.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.LvwMarkersItemChecked);
            this.lvwMarkers.SelectedIndexChanged += new System.EventHandler(this.LvwGenesSelectedIndexChanged);
            // 
            // chMarker
            // 
            this.chMarker.Text = "Marker";
            // 
            // chThreshold
            // 
            this.chThreshold.Text = "Threshold";
            // 
            // chThresholdType
            // 
            this.chThresholdType.Text = "Threshold Type";
            // 
            // chMaxSignal
            // 
            this.chMaxSignal.Text = "Max Signal";
            // 
            // chAmbiguous
            // 
            this.chAmbiguous.Text = "Ambiguous";
            // 
            // chKeepMarker
            // 
            this.chKeepMarker.Text = "Keep Marker?";
            // 
            // chMarkerComments
            // 
            this.chMarkerComments.Text = "Marker Comments";
            // 
            // contextMenuMarkers
            // 
            this.contextMenuMarkers.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cbxThresholdType,
            this.showSpotStripToolStripMenuItem,
            this.showMarkerStripToolStripMenuItem,
            this.changeMarkerCommentToolStripMenuItem,
            this.setMarkerThresholdToolStripMenuItem});
            this.contextMenuMarkers.Name = "contextMenuGenes";
            this.contextMenuMarkers.Size = new System.Drawing.Size(213, 119);
            this.contextMenuMarkers.Opening += new System.ComponentModel.CancelEventHandler(this.ContextMenuGenesOpening);
            // 
            // cbxThresholdType
            // 
            this.cbxThresholdType.Name = "cbxThresholdType";
            this.cbxThresholdType.Size = new System.Drawing.Size(121, 23);
            this.cbxThresholdType.SelectedIndexChanged += new System.EventHandler(this.CbxThresholdTypeSelectedIndexChanged);
            // 
            // showSpotStripToolStripMenuItem
            // 
            this.showSpotStripToolStripMenuItem.Name = "showSpotStripToolStripMenuItem";
            this.showSpotStripToolStripMenuItem.Size = new System.Drawing.Size(212, 22);
            this.showSpotStripToolStripMenuItem.Text = "Show Spot Strip";
            this.showSpotStripToolStripMenuItem.Click += new System.EventHandler(this.ShowSpotDistributionToolStripMenuItemClick);
            // 
            // showMarkerStripToolStripMenuItem
            // 
            this.showMarkerStripToolStripMenuItem.Name = "showMarkerStripToolStripMenuItem";
            this.showMarkerStripToolStripMenuItem.Size = new System.Drawing.Size(212, 22);
            this.showMarkerStripToolStripMenuItem.Text = "Show Marker Strip";
            this.showMarkerStripToolStripMenuItem.Click += new System.EventHandler(this.ShowSpotDistributionClumpedToolStripMenuItemClick);
            // 
            // changeMarkerCommentToolStripMenuItem
            // 
            this.changeMarkerCommentToolStripMenuItem.Name = "changeMarkerCommentToolStripMenuItem";
            this.changeMarkerCommentToolStripMenuItem.Size = new System.Drawing.Size(212, 22);
            this.changeMarkerCommentToolStripMenuItem.Text = "Change Marker Comment";
            this.changeMarkerCommentToolStripMenuItem.Click += new System.EventHandler(this.ChangeMarkerCommentToolStripMenuItemClick);
            // 
            // setMarkerThresholdToolStripMenuItem
            // 
            this.setMarkerThresholdToolStripMenuItem.Name = "setMarkerThresholdToolStripMenuItem";
            this.setMarkerThresholdToolStripMenuItem.Size = new System.Drawing.Size(212, 22);
            this.setMarkerThresholdToolStripMenuItem.Text = "Set Marker Threshold";
            this.setMarkerThresholdToolStripMenuItem.Click += new System.EventHandler(this.SetMarkerThresholdToolStripMenuItemClick);
            // 
            // splitContainer2
            // 
            this.splitContainer2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.cht);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.splitContainer3);
            this.splitContainer2.Size = new System.Drawing.Size(631, 499);
            this.splitContainer2.SplitterDistance = 387;
            this.splitContainer2.TabIndex = 0;
            // 
            // cht
            // 
            chartArea1.Name = "ChartArea1";
            this.cht.ChartAreas.Add(chartArea1);
            this.cht.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cht.Location = new System.Drawing.Point(0, 0);
            this.cht.Name = "cht";
            this.cht.Size = new System.Drawing.Size(627, 383);
            this.cht.TabIndex = 0;
            this.cht.Text = "chart1";
            this.cht.CursorPositionChanging += new System.EventHandler<System.Windows.Forms.DataVisualization.Charting.CursorEventArgs>(this.ChtGeneCursorPositionChanging);
            // 
            // splitContainer3
            // 
            this.splitContainer3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.Location = new System.Drawing.Point(0, 0);
            this.splitContainer3.Name = "splitContainer3";
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.groupBox1);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.gbxInfo);
            this.splitContainer3.Size = new System.Drawing.Size(631, 108);
            this.splitContainer3.SplitterDistance = 320;
            this.splitContainer3.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnDecrementWindowSize);
            this.groupBox1.Controls.Add(this.btnIncrementWindowSize);
            this.groupBox1.Controls.Add(this.chkFrequencies);
            this.groupBox1.Controls.Add(this.btnDecrementBinSize);
            this.groupBox1.Controls.Add(this.btnIncrementBinSize);
            this.groupBox1.Controls.Add(this.lblWindowSize);
            this.groupBox1.Controls.Add(this.Label1);
            this.groupBox1.Controls.Add(this.txtBinSize);
            this.groupBox1.Controls.Add(this.chkWindowedSTDev);
            this.groupBox1.Controls.Add(this.chkWindowedMedian);
            this.groupBox1.Controls.Add(this.txtWindowSize);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(316, 104);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Chart Parameters";
            // 
            // btnDecrementWindowSize
            // 
            this.btnDecrementWindowSize.Location = new System.Drawing.Point(207, 34);
            this.btnDecrementWindowSize.Name = "btnDecrementWindowSize";
            this.btnDecrementWindowSize.Size = new System.Drawing.Size(18, 17);
            this.btnDecrementWindowSize.TabIndex = 29;
            this.btnDecrementWindowSize.Text = "-";
            this.btnDecrementWindowSize.UseVisualStyleBackColor = true;
            this.btnDecrementWindowSize.Click += new System.EventHandler(this.BtnDecrementWindowSizeClick);
            // 
            // btnIncrementWindowSize
            // 
            this.btnIncrementWindowSize.Location = new System.Drawing.Point(207, 17);
            this.btnIncrementWindowSize.Name = "btnIncrementWindowSize";
            this.btnIncrementWindowSize.Size = new System.Drawing.Size(18, 18);
            this.btnIncrementWindowSize.TabIndex = 28;
            this.btnIncrementWindowSize.Text = "+";
            this.btnIncrementWindowSize.UseVisualStyleBackColor = true;
            this.btnIncrementWindowSize.Click += new System.EventHandler(this.BtnIncrementWindowSizeClick);
            // 
            // chkFrequencies
            // 
            this.chkFrequencies.AutoSize = true;
            this.chkFrequencies.Checked = true;
            this.chkFrequencies.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkFrequencies.Location = new System.Drawing.Point(6, 52);
            this.chkFrequencies.Name = "chkFrequencies";
            this.chkFrequencies.Size = new System.Drawing.Size(73, 17);
            this.chkFrequencies.TabIndex = 23;
            this.chkFrequencies.Text = "Histogram";
            this.chkFrequencies.UseVisualStyleBackColor = true;
            this.chkFrequencies.CheckedChanged += new System.EventHandler(this.ChkFrequenciesCheckedChanged);
            // 
            // btnDecrementBinSize
            // 
            this.btnDecrementBinSize.Location = new System.Drawing.Point(296, 34);
            this.btnDecrementBinSize.Name = "btnDecrementBinSize";
            this.btnDecrementBinSize.Size = new System.Drawing.Size(18, 17);
            this.btnDecrementBinSize.TabIndex = 22;
            this.btnDecrementBinSize.Text = "-";
            this.btnDecrementBinSize.UseVisualStyleBackColor = true;
            this.btnDecrementBinSize.Click += new System.EventHandler(this.BtnDecrementBinSizeClick);
            // 
            // btnIncrementBinSize
            // 
            this.btnIncrementBinSize.Location = new System.Drawing.Point(296, 17);
            this.btnIncrementBinSize.Name = "btnIncrementBinSize";
            this.btnIncrementBinSize.Size = new System.Drawing.Size(18, 18);
            this.btnIncrementBinSize.TabIndex = 21;
            this.btnIncrementBinSize.Text = "+";
            this.btnIncrementBinSize.UseVisualStyleBackColor = true;
            this.btnIncrementBinSize.Click += new System.EventHandler(this.BtnIncrementBinSizeClick);
            // 
            // lblWindowSize
            // 
            this.lblWindowSize.AutoSize = true;
            this.lblWindowSize.Location = new System.Drawing.Point(129, 19);
            this.lblWindowSize.Name = "lblWindowSize";
            this.lblWindowSize.Size = new System.Drawing.Size(72, 13);
            this.lblWindowSize.TabIndex = 27;
            this.lblWindowSize.Text = "Window Size:";
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Location = new System.Drawing.Point(245, 19);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(45, 13);
            this.Label1.TabIndex = 19;
            this.Label1.Text = "Bin Size";
            // 
            // txtBinSize
            // 
            this.txtBinSize.Location = new System.Drawing.Point(248, 33);
            this.txtBinSize.Name = "txtBinSize";
            this.txtBinSize.Size = new System.Drawing.Size(45, 20);
            this.txtBinSize.TabIndex = 20;
            this.txtBinSize.Text = "0.02";
            this.txtBinSize.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtBinSizeKeyPress);
            // 
            // chkWindowedSTDev
            // 
            this.chkWindowedSTDev.AutoSize = true;
            this.chkWindowedSTDev.Location = new System.Drawing.Point(6, 19);
            this.chkWindowedSTDev.Name = "chkWindowedSTDev";
            this.chkWindowedSTDev.Size = new System.Drawing.Size(117, 17);
            this.chkWindowedSTDev.TabIndex = 24;
            this.chkWindowedSTDev.Text = "Standard Deviation";
            this.chkWindowedSTDev.UseVisualStyleBackColor = true;
            this.chkWindowedSTDev.CheckedChanged += new System.EventHandler(this.ChkWindowedSTDevCheckedChanged);
            // 
            // chkWindowedMedian
            // 
            this.chkWindowedMedian.AutoSize = true;
            this.chkWindowedMedian.Location = new System.Drawing.Point(6, 35);
            this.chkWindowedMedian.Name = "chkWindowedMedian";
            this.chkWindowedMedian.Size = new System.Drawing.Size(61, 17);
            this.chkWindowedMedian.TabIndex = 25;
            this.chkWindowedMedian.Text = "Median";
            this.chkWindowedMedian.UseVisualStyleBackColor = true;
            this.chkWindowedMedian.CheckedChanged += new System.EventHandler(this.ChkWindowedMedianCheckedChanged);
            // 
            // txtWindowSize
            // 
            this.txtWindowSize.Location = new System.Drawing.Point(175, 33);
            this.txtWindowSize.Name = "txtWindowSize";
            this.txtWindowSize.Size = new System.Drawing.Size(26, 20);
            this.txtWindowSize.TabIndex = 26;
            this.txtWindowSize.Text = "5";
            this.txtWindowSize.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtWindowSizeKeyPress);
            // 
            // gbxInfo
            // 
            this.gbxInfo.Controls.Add(this.lvwParam);
            this.gbxInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbxInfo.Location = new System.Drawing.Point(0, 0);
            this.gbxInfo.Name = "gbxInfo";
            this.gbxInfo.Size = new System.Drawing.Size(303, 104);
            this.gbxInfo.TabIndex = 18;
            this.gbxInfo.TabStop = false;
            this.gbxInfo.Text = "Threshold Parameters";
            // 
            // lvwParam
            // 
            this.lvwParam.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader7,
            this.columnHeader8});
            this.lvwParam.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvwParam.FullRowSelect = true;
            this.lvwParam.GridLines = true;
            this.lvwParam.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem1,
            listViewItem2,
            listViewItem3,
            listViewItem4,
            listViewItem5,
            listViewItem6,
            listViewItem7,
            listViewItem8,
            listViewItem9,
            listViewItem10,
            listViewItem11,
            listViewItem12,
            listViewItem13,
            listViewItem14,
            listViewItem15,
            listViewItem16,
            listViewItem17});
            this.lvwParam.Location = new System.Drawing.Point(3, 16);
            this.lvwParam.Name = "lvwParam";
            this.lvwParam.Size = new System.Drawing.Size(297, 85);
            this.lvwParam.TabIndex = 0;
            this.lvwParam.UseCompatibleStateImageBehavior = false;
            this.lvwParam.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Parameter";
            this.columnHeader7.Width = 188;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Value";
            this.columnHeader8.Width = 87;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ddbFile,
            this.ddbView,
            this.ddbThresholdType,
            this.ddbThresholdFlagging,
            this.ddbGlobalBackgroundThreshold,
            this.ddbPercentPositiveSignalThreshold,
            this.btnRecalculate});
            this.toolStrip1.Location = new System.Drawing.Point(3, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(886, 25);
            this.toolStrip1.TabIndex = 0;
            // 
            // ddbFile
            // 
            this.ddbFile.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.ddbFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveToolStripMenuItem,
            this.toolStripSeparator8,
            this.aboutToolStripMenuItem,
            this.toolStripSeparator7,
            this.closeMarkerThresholdAnalysisToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.ddbFile.Image = ((System.Drawing.Image)(resources.GetObject("ddbFile.Image")));
            this.ddbFile.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ddbFile.Name = "ddbFile";
            this.ddbFile.Size = new System.Drawing.Size(38, 22);
            this.ddbFile.Text = "File";
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Image = global::ArrayTubeAnalyzer.Properties.Resources.Save;
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(245, 22);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.SaveToolStripMenuItemClick);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(242, 6);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Image = global::ArrayTubeAnalyzer.Properties.Resources.info_16;
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(245, 22);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.TsbAboutClick);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(242, 6);
            // 
            // closeMarkerThresholdAnalysisToolStripMenuItem
            // 
            this.closeMarkerThresholdAnalysisToolStripMenuItem.Image = global::ArrayTubeAnalyzer.Properties.Resources.Delete;
            this.closeMarkerThresholdAnalysisToolStripMenuItem.Name = "closeMarkerThresholdAnalysisToolStripMenuItem";
            this.closeMarkerThresholdAnalysisToolStripMenuItem.Size = new System.Drawing.Size(245, 22);
            this.closeMarkerThresholdAnalysisToolStripMenuItem.Text = "Close Marker Threshold Analysis";
            this.closeMarkerThresholdAnalysisToolStripMenuItem.Click += new System.EventHandler(this.CloseGeneThresholdAnalysisToolStripMenuItemClick);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(245, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.ExitToolStripMenuItemClick);
            // 
            // ddbView
            // 
            this.ddbView.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.ddbView.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sampleQCToolStripMenuItem,
            this.markerThresholdReviewToolStripMenuItem,
            this.markerCallReviewToolStripMenuItem,
            this.clusteringToolStripMenuItem,
            this.diagnosticsToolStripMenuItem});
            this.ddbView.Image = ((System.Drawing.Image)(resources.GetObject("ddbView.Image")));
            this.ddbView.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ddbView.Name = "ddbView";
            this.ddbView.Size = new System.Drawing.Size(45, 22);
            this.ddbView.Text = "View";
            // 
            // sampleQCToolStripMenuItem
            // 
            this.sampleQCToolStripMenuItem.Image = global::ArrayTubeAnalyzer.Properties.Resources.warning_16;
            this.sampleQCToolStripMenuItem.Name = "sampleQCToolStripMenuItem";
            this.sampleQCToolStripMenuItem.Size = new System.Drawing.Size(207, 22);
            this.sampleQCToolStripMenuItem.Text = "Sample QC";
            this.sampleQCToolStripMenuItem.Click += new System.EventHandler(this.ArraySpotReviewToolStripMenuItemClick);
            // 
            // markerThresholdReviewToolStripMenuItem
            // 
            this.markerThresholdReviewToolStripMenuItem.Image = global::ArrayTubeAnalyzer.Properties.Resources.statistics_16;
            this.markerThresholdReviewToolStripMenuItem.Name = "markerThresholdReviewToolStripMenuItem";
            this.markerThresholdReviewToolStripMenuItem.Size = new System.Drawing.Size(207, 22);
            this.markerThresholdReviewToolStripMenuItem.Text = "Marker Threshold Review";
            this.markerThresholdReviewToolStripMenuItem.Click += new System.EventHandler(this.SpotSignalReviewToolStripMenuItemClick);
            // 
            // markerCallReviewToolStripMenuItem
            // 
            this.markerCallReviewToolStripMenuItem.Image = global::ArrayTubeAnalyzer.Properties.Resources.OK_All;
            this.markerCallReviewToolStripMenuItem.Name = "markerCallReviewToolStripMenuItem";
            this.markerCallReviewToolStripMenuItem.Size = new System.Drawing.Size(207, 22);
            this.markerCallReviewToolStripMenuItem.Text = "Marker Call Review";
            this.markerCallReviewToolStripMenuItem.Click += new System.EventHandler(this.GeneCallReviewToolStripMenuItemClick);
            // 
            // clusteringToolStripMenuItem
            // 
            this.clusteringToolStripMenuItem.Image = global::ArrayTubeAnalyzer.Properties.Resources.tree_16;
            this.clusteringToolStripMenuItem.Name = "clusteringToolStripMenuItem";
            this.clusteringToolStripMenuItem.Size = new System.Drawing.Size(207, 22);
            this.clusteringToolStripMenuItem.Text = "Clustering";
            this.clusteringToolStripMenuItem.Click += new System.EventHandler(this.ClusteringToolStripMenuItemClick);
            // 
            // diagnosticsToolStripMenuItem
            // 
            this.diagnosticsToolStripMenuItem.Image = global::ArrayTubeAnalyzer.Properties.Resources.search_16;
            this.diagnosticsToolStripMenuItem.Name = "diagnosticsToolStripMenuItem";
            this.diagnosticsToolStripMenuItem.Size = new System.Drawing.Size(207, 22);
            this.diagnosticsToolStripMenuItem.Text = "Diagnostics";
            this.diagnosticsToolStripMenuItem.Click += new System.EventHandler(this.DiagnosticsToolStripMenuItemClick);
            // 
            // ddbThresholdType
            // 
            this.ddbThresholdType.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.ddbThresholdType.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.globalBackgroundToolStripMenuItem,
            this.percentPositiveSignalToolStripMenuItem,
            this.toolStripSeparator1,
            this.resetAllUserDefinedThresholdsToolStripMenuItem,
            this.setUpperAndLowerThresholdsAcrossDatasetToolStripMenuItem});
            this.ddbThresholdType.Image = ((System.Drawing.Image)(resources.GetObject("ddbThresholdType.Image")));
            this.ddbThresholdType.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ddbThresholdType.Name = "ddbThresholdType";
            this.ddbThresholdType.Size = new System.Drawing.Size(102, 22);
            this.ddbThresholdType.Text = "Threshold Type";
            // 
            // globalBackgroundToolStripMenuItem
            // 
            this.globalBackgroundToolStripMenuItem.Checked = true;
            this.globalBackgroundToolStripMenuItem.CheckOnClick = true;
            this.globalBackgroundToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.globalBackgroundToolStripMenuItem.Name = "globalBackgroundToolStripMenuItem";
            this.globalBackgroundToolStripMenuItem.Size = new System.Drawing.Size(324, 22);
            this.globalBackgroundToolStripMenuItem.Text = "Global Background";
            this.globalBackgroundToolStripMenuItem.CheckedChanged += new System.EventHandler(this.NumberOfTimesAboveGlobalBackgrounddatasetspecificToolStripMenuItemCheckedChanged);
            // 
            // percentPositiveSignalToolStripMenuItem
            // 
            this.percentPositiveSignalToolStripMenuItem.CheckOnClick = true;
            this.percentPositiveSignalToolStripMenuItem.Name = "percentPositiveSignalToolStripMenuItem";
            this.percentPositiveSignalToolStripMenuItem.Size = new System.Drawing.Size(324, 22);
            this.percentPositiveSignalToolStripMenuItem.Text = "Percent Positive Signal";
            this.percentPositiveSignalToolStripMenuItem.CheckedChanged += new System.EventHandler(this.OfAveragePositiveSignalToolStripMenuItemCheckedChanged);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(321, 6);
            // 
            // resetAllUserDefinedThresholdsToolStripMenuItem
            // 
            this.resetAllUserDefinedThresholdsToolStripMenuItem.Name = "resetAllUserDefinedThresholdsToolStripMenuItem";
            this.resetAllUserDefinedThresholdsToolStripMenuItem.Size = new System.Drawing.Size(324, 22);
            this.resetAllUserDefinedThresholdsToolStripMenuItem.Text = "Reset All User-Defined Thresholds";
            this.resetAllUserDefinedThresholdsToolStripMenuItem.Click += new System.EventHandler(this.ResetAllUserDefinedThresholdsToolStripMenuItemClick);
            // 
            // ddbThresholdFlagging
            // 
            this.ddbThresholdFlagging.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.globalErrorToolStripMenuItem,
            this.txtGlobalErrorSD,
            this.toolStripSeparator6,
            this.thresholdFactorToolStripMenuItem,
            this.txtThresholdFlaggingFactor});
            this.ddbThresholdFlagging.Image = global::ArrayTubeAnalyzer.Properties.Resources.flag_16;
            this.ddbThresholdFlagging.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ddbThresholdFlagging.Name = "ddbThresholdFlagging";
            this.ddbThresholdFlagging.Size = new System.Drawing.Size(138, 22);
            this.ddbThresholdFlagging.Text = "Threshold Flagging";
            // 
            // globalErrorToolStripMenuItem
            // 
            this.globalErrorToolStripMenuItem.Name = "globalErrorToolStripMenuItem";
            this.globalErrorToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.globalErrorToolStripMenuItem.Text = "Global Error SD";
            // 
            // txtGlobalErrorSD
            // 
            this.txtGlobalErrorSD.Name = "txtGlobalErrorSD";
            this.txtGlobalErrorSD.Size = new System.Drawing.Size(163, 22);
            this.txtGlobalErrorSD.Text = "0.0";
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(160, 6);
            // 
            // thresholdFactorToolStripMenuItem
            // 
            this.thresholdFactorToolStripMenuItem.Name = "thresholdFactorToolStripMenuItem";
            this.thresholdFactorToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.thresholdFactorToolStripMenuItem.Text = "Threshold Factor";
            // 
            // txtThresholdFlaggingFactor
            // 
            this.txtThresholdFlaggingFactor.Name = "txtThresholdFlaggingFactor";
            this.txtThresholdFlaggingFactor.Size = new System.Drawing.Size(100, 23);
            this.txtThresholdFlaggingFactor.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtThresholdFlaggingFactorKeyPress);
            // 
            // ddbGlobalBackgroundThreshold
            // 
            this.ddbGlobalBackgroundThreshold.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.ddbGlobalBackgroundThreshold.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.globalBackgroundToolStripMenuItem1,
            this.lblGlobalBackground,
            this.toolStripSeparator4,
            this.thresholdToolStripMenuItem,
            this.lblGlobalBackgroundThreshold,
            this.toolStripSeparator3,
            this.thresholdCutoffToolStripMenuItem,
            this.txtGlobalBackgroundCutoff,
            this.toolStripSeparator2,
            this.thresholdFactorToolStripMenuItem1,
            this.txtGlobalBackgroundFactor});
            this.ddbGlobalBackgroundThreshold.Image = ((System.Drawing.Image)(resources.GetObject("ddbGlobalBackgroundThreshold.Image")));
            this.ddbGlobalBackgroundThreshold.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ddbGlobalBackgroundThreshold.Name = "ddbGlobalBackgroundThreshold";
            this.ddbGlobalBackgroundThreshold.Size = new System.Drawing.Size(177, 22);
            this.ddbGlobalBackgroundThreshold.Text = "Global Background Threshold";
            // 
            // globalBackgroundToolStripMenuItem1
            // 
            this.globalBackgroundToolStripMenuItem1.Name = "globalBackgroundToolStripMenuItem1";
            this.globalBackgroundToolStripMenuItem1.Size = new System.Drawing.Size(185, 22);
            this.globalBackgroundToolStripMenuItem1.Text = "Global Background";
            // 
            // lblGlobalBackground
            // 
            this.lblGlobalBackground.Name = "lblGlobalBackground";
            this.lblGlobalBackground.Size = new System.Drawing.Size(185, 22);
            this.lblGlobalBackground.Text = "toolStripMenuItem1";
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(182, 6);
            // 
            // thresholdToolStripMenuItem
            // 
            this.thresholdToolStripMenuItem.Name = "thresholdToolStripMenuItem";
            this.thresholdToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.thresholdToolStripMenuItem.Text = "Threshold";
            // 
            // lblGlobalBackgroundThreshold
            // 
            this.lblGlobalBackgroundThreshold.Name = "lblGlobalBackgroundThreshold";
            this.lblGlobalBackgroundThreshold.Size = new System.Drawing.Size(185, 22);
            this.lblGlobalBackgroundThreshold.Text = "toolStripMenuItem2";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(182, 6);
            // 
            // thresholdCutoffToolStripMenuItem
            // 
            this.thresholdCutoffToolStripMenuItem.Name = "thresholdCutoffToolStripMenuItem";
            this.thresholdCutoffToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.thresholdCutoffToolStripMenuItem.Text = "Threshold Cutoff (%)";
            // 
            // txtGlobalBackgroundCutoff
            // 
            this.txtGlobalBackgroundCutoff.Name = "txtGlobalBackgroundCutoff";
            this.txtGlobalBackgroundCutoff.Size = new System.Drawing.Size(100, 23);
            this.txtGlobalBackgroundCutoff.Text = "15";
            this.txtGlobalBackgroundCutoff.ToolTipText = "The percentage of the lowest positive signal values in the current dataset to use" +
                " in the calculation of the global background.";
            this.txtGlobalBackgroundCutoff.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtBackgroundCutoffKeyPress);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(182, 6);
            // 
            // thresholdFactorToolStripMenuItem1
            // 
            this.thresholdFactorToolStripMenuItem1.Name = "thresholdFactorToolStripMenuItem1";
            this.thresholdFactorToolStripMenuItem1.Size = new System.Drawing.Size(185, 22);
            this.thresholdFactorToolStripMenuItem1.Text = "Threshold Factor";
            // 
            // txtGlobalBackgroundFactor
            // 
            this.txtGlobalBackgroundFactor.Name = "txtGlobalBackgroundFactor";
            this.txtGlobalBackgroundFactor.Size = new System.Drawing.Size(100, 23);
            this.txtGlobalBackgroundFactor.Text = "2";
            this.txtGlobalBackgroundFactor.ToolTipText = "How many times above the global background the global background threshold will b" +
                "e.";
            this.txtGlobalBackgroundFactor.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtGlobalBackgroundFactorKeyPress);
            // 
            // ddbPercentPositiveSignalThreshold
            // 
            this.ddbPercentPositiveSignalThreshold.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.ddbPercentPositiveSignalThreshold.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.thresholdCutoffToolStripMenuItem1,
            this.txtPercentPositiveCutoff,
            this.toolStripSeparator5,
            this.thresholdFactorToolStripMenuItem2,
            this.txtPercentPositiveFactor});
            this.ddbPercentPositiveSignalThreshold.Image = ((System.Drawing.Image)(resources.GetObject("ddbPercentPositiveSignalThreshold.Image")));
            this.ddbPercentPositiveSignalThreshold.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ddbPercentPositiveSignalThreshold.Name = "ddbPercentPositiveSignalThreshold";
            this.ddbPercentPositiveSignalThreshold.Size = new System.Drawing.Size(195, 22);
            this.ddbPercentPositiveSignalThreshold.Text = "Percent Positive Signal Threshold";
            // 
            // thresholdCutoffToolStripMenuItem1
            // 
            this.thresholdCutoffToolStripMenuItem1.Name = "thresholdCutoffToolStripMenuItem1";
            this.thresholdCutoffToolStripMenuItem1.Size = new System.Drawing.Size(185, 22);
            this.thresholdCutoffToolStripMenuItem1.Text = "Threshold Cutoff (%)";
            // 
            // txtPercentPositiveCutoff
            // 
            this.txtPercentPositiveCutoff.Name = "txtPercentPositiveCutoff";
            this.txtPercentPositiveCutoff.Size = new System.Drawing.Size(100, 23);
            this.txtPercentPositiveCutoff.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtPercentPositiveCutOffKeyPress);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(182, 6);
            // 
            // thresholdFactorToolStripMenuItem2
            // 
            this.thresholdFactorToolStripMenuItem2.Name = "thresholdFactorToolStripMenuItem2";
            this.thresholdFactorToolStripMenuItem2.Size = new System.Drawing.Size(185, 22);
            this.thresholdFactorToolStripMenuItem2.Text = "Threshold Factor (%)";
            // 
            // txtPercentPositiveFactor
            // 
            this.txtPercentPositiveFactor.Name = "txtPercentPositiveFactor";
            this.txtPercentPositiveFactor.Size = new System.Drawing.Size(100, 23);
            this.txtPercentPositiveFactor.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtPercentPositiveFactorKeyPress);
            // 
            // btnRecalculate
            // 
            this.btnRecalculate.Image = global::ArrayTubeAnalyzer.Properties.Resources.gear_16;
            this.btnRecalculate.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnRecalculate.Name = "btnRecalculate";
            this.btnRecalculate.Size = new System.Drawing.Size(148, 22);
            this.btnRecalculate.Text = "Recalculate Thresholds";
            this.btnRecalculate.Click += new System.EventHandler(this.BtnRecalculateClick);
            // 
            // ColumnHeader5
            // 
            this.ColumnHeader5.Text = "Parameter";
            this.ColumnHeader5.Width = 186;
            // 
            // ColumnHeader6
            // 
            this.ColumnHeader6.Text = "Value";
            this.ColumnHeader6.Width = 150;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Parameter";
            this.columnHeader1.Width = 186;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Value";
            this.columnHeader2.Width = 150;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Parameter";
            this.columnHeader3.Width = 186;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Value";
            this.columnHeader4.Width = 150;
            // 
            // setUpperAndLowerThresholdsAcrossDatasetToolStripMenuItem
            // 
            this.setUpperAndLowerThresholdsAcrossDatasetToolStripMenuItem.Name = "setUpperAndLowerThresholdsAcrossDatasetToolStripMenuItem";
            this.setUpperAndLowerThresholdsAcrossDatasetToolStripMenuItem.Size = new System.Drawing.Size(324, 22);
            this.setUpperAndLowerThresholdsAcrossDatasetToolStripMenuItem.Text = "Set Upper and Lower Thresholds Across Dataset";
            this.setUpperAndLowerThresholdsAcrossDatasetToolStripMenuItem.Click += new System.EventHandler(this.SetUpperAndLowerThresholdsAcrossDatasetToolStripMenuItemClick);
            // 
            // FrmMarkerThresholdAnalysis
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 546);
            this.Controls.Add(this.toolStripContainer1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmMarkerThresholdAnalysis";
            this.Text = "Array Tube Analyzer - Marker Threshold Analysis";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmMarkerThresholdAnalysisFormClosing);
            this.toolStripContainer1.BottomToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.BottomToolStripPanel.PerformLayout();
            this.toolStripContainer1.ContentPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.PerformLayout();
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.StatusStrip1.ResumeLayout(false);
            this.StatusStrip1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.contextMenuMarkers.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cht)).EndInit();
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.gbxInfo.ResumeLayout(false);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStripContainer toolStripContainer1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private ListViewFF lvwMarkers;
        private System.Windows.Forms.ColumnHeader chMarker;
        private System.Windows.Forms.ColumnHeader chThreshold;
        private System.Windows.Forms.ColumnHeader chThresholdType;
        private System.Windows.Forms.ColumnHeader chAmbiguous;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.DataVisualization.Charting.Chart cht;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.GroupBox groupBox1;
        internal System.Windows.Forms.Button btnDecrementWindowSize;
        internal System.Windows.Forms.Button btnIncrementWindowSize;
        internal System.Windows.Forms.CheckBox chkFrequencies;
        internal System.Windows.Forms.Button btnDecrementBinSize;
        internal System.Windows.Forms.Button btnIncrementBinSize;
        internal System.Windows.Forms.Label lblWindowSize;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.TextBox txtBinSize;
        internal System.Windows.Forms.CheckBox chkWindowedSTDev;
        internal System.Windows.Forms.CheckBox chkWindowedMedian;
        internal System.Windows.Forms.TextBox txtWindowSize;
        internal System.Windows.Forms.GroupBox gbxInfo;
        private ListViewFF lvwParam;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripDropDownButton ddbFile;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeMarkerThresholdAnalysisToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripDropDownButton ddbView;
        private System.Windows.Forms.ToolStripMenuItem sampleQCToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem markerThresholdReviewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem markerCallReviewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clusteringToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem diagnosticsToolStripMenuItem;
        private System.Windows.Forms.ToolStripDropDownButton ddbThresholdType;
        private System.Windows.Forms.ToolStripMenuItem globalBackgroundToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem percentPositiveSignalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resetAllUserDefinedThresholdsToolStripMenuItem;
        private System.Windows.Forms.ToolStripDropDownButton ddbThresholdFlagging;
        private System.Windows.Forms.ToolStripMenuItem globalErrorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem txtGlobalErrorSD;
        private System.Windows.Forms.ToolStripMenuItem thresholdFactorToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox txtThresholdFlaggingFactor;
        private System.Windows.Forms.ToolStripDropDownButton ddbGlobalBackgroundThreshold;
        private System.Windows.Forms.ToolStripMenuItem globalBackgroundToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem lblGlobalBackground;
        private System.Windows.Forms.ToolStripMenuItem thresholdToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lblGlobalBackgroundThreshold;
        private System.Windows.Forms.ToolStripMenuItem thresholdCutoffToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox txtGlobalBackgroundCutoff;
        private System.Windows.Forms.ToolStripMenuItem thresholdFactorToolStripMenuItem1;
        private System.Windows.Forms.ToolStripTextBox txtGlobalBackgroundFactor;
        private System.Windows.Forms.ToolStripDropDownButton ddbPercentPositiveSignalThreshold;
        private System.Windows.Forms.ToolStripMenuItem thresholdCutoffToolStripMenuItem1;
        private System.Windows.Forms.ToolStripTextBox txtPercentPositiveCutoff;
        private System.Windows.Forms.ToolStripMenuItem thresholdFactorToolStripMenuItem2;
        private System.Windows.Forms.ToolStripTextBox txtPercentPositiveFactor;
        private System.Windows.Forms.ToolStripButton btnRecalculate;
        internal System.Windows.Forms.ColumnHeader ColumnHeader5;
        internal System.Windows.Forms.ColumnHeader ColumnHeader6;
        internal System.Windows.Forms.ColumnHeader columnHeader1;
        internal System.Windows.Forms.ColumnHeader columnHeader2;
        internal System.Windows.Forms.ColumnHeader columnHeader3;
        internal System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.StatusStrip StatusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tslStatus;
        private System.Windows.Forms.ContextMenuStrip contextMenuMarkers;
        private System.Windows.Forms.ToolStripComboBox cbxThresholdType;
        private System.Windows.Forms.ToolStripMenuItem showSpotStripToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showMarkerStripToolStripMenuItem;
        private System.Windows.Forms.ToolTip ToolTip1;
        private System.Windows.Forms.ColumnHeader chMarkerComments;
        private System.Windows.Forms.ToolStripMenuItem changeMarkerCommentToolStripMenuItem;
        private System.Windows.Forms.ColumnHeader chMaxSignal;
        private System.Windows.Forms.ColumnHeader chKeepMarker;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem setMarkerThresholdToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem setUpperAndLowerThresholdsAcrossDatasetToolStripMenuItem;
    }
}