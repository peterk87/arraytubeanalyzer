﻿namespace ArrayTubeAnalyzer
{
    partial class FrmDiagnosis
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmDiagnosis));
            this.ImportProfilesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SaveSampleInfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ExportProfilesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ddbFile = new System.Windows.Forms.ToolStripDropDownButton();
            this.CloseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.RemoveDiagnosisProfileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.AddDiagnosisProfileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStrip1 = new System.Windows.Forms.ToolStrip();
            this.ddbEdit = new System.Windows.Forms.ToolStripDropDownButton();
            this.ChangeDiagnosisProfileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.treatAmbiguousSpotsAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cbxAmbiguous = new System.Windows.Forms.ToolStripComboBox();
            this.AddDiagnosisProfileToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.SaveSampleInfoToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.ContextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.RemoveDiagnosisProfileToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.ChangeDiagnosisProfileToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.ImportProfilesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.ExportProfilesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.lvwProfiles = new ArrayTubeAnalyzer.ListViewFF();
            this.ToolStrip1.SuspendLayout();
            this.ContextMenuStrip1.SuspendLayout();
            this.ToolStripContainer1.ContentPanel.SuspendLayout();
            this.ToolStripContainer1.TopToolStripPanel.SuspendLayout();
            this.ToolStripContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ImportProfilesToolStripMenuItem
            // 
            this.ImportProfilesToolStripMenuItem.Name = "ImportProfilesToolStripMenuItem";
            this.ImportProfilesToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.ImportProfilesToolStripMenuItem.Text = "Import Profile(s)";
            this.ImportProfilesToolStripMenuItem.Click += new System.EventHandler(this.ImportProfilesToolStripMenuItemClick);
            // 
            // SaveSampleInfoToolStripMenuItem
            // 
            this.SaveSampleInfoToolStripMenuItem.Name = "SaveSampleInfoToolStripMenuItem";
            this.SaveSampleInfoToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.SaveSampleInfoToolStripMenuItem.Text = "Save Sample Info";
            this.SaveSampleInfoToolStripMenuItem.Click += new System.EventHandler(this.SaveSampleInfoToolStripMenuItemClick);
            // 
            // ExportProfilesToolStripMenuItem
            // 
            this.ExportProfilesToolStripMenuItem.Name = "ExportProfilesToolStripMenuItem";
            this.ExportProfilesToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.ExportProfilesToolStripMenuItem.Text = "Export Profile(s)";
            this.ExportProfilesToolStripMenuItem.Click += new System.EventHandler(this.ExportProfilesToolStripMenuItemClick);
            // 
            // ddbFile
            // 
            this.ddbFile.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.ddbFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.SaveSampleInfoToolStripMenuItem,
            this.ImportProfilesToolStripMenuItem,
            this.ExportProfilesToolStripMenuItem,
            this.CloseToolStripMenuItem});
            this.ddbFile.Image = ((System.Drawing.Image)(resources.GetObject("ddbFile.Image")));
            this.ddbFile.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ddbFile.Name = "ddbFile";
            this.ddbFile.Size = new System.Drawing.Size(38, 22);
            this.ddbFile.Text = "File";
            // 
            // CloseToolStripMenuItem
            // 
            this.CloseToolStripMenuItem.Name = "CloseToolStripMenuItem";
            this.CloseToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.CloseToolStripMenuItem.Text = "Close";
            // 
            // RemoveDiagnosisProfileToolStripMenuItem
            // 
            this.RemoveDiagnosisProfileToolStripMenuItem.Name = "RemoveDiagnosisProfileToolStripMenuItem";
            this.RemoveDiagnosisProfileToolStripMenuItem.Size = new System.Drawing.Size(214, 22);
            this.RemoveDiagnosisProfileToolStripMenuItem.Text = "Remove Diagnosis Profile";
            this.RemoveDiagnosisProfileToolStripMenuItem.Click += new System.EventHandler(this.RemoveDiagnosisProfileToolStripMenuItemClick);
            // 
            // AddDiagnosisProfileToolStripMenuItem
            // 
            this.AddDiagnosisProfileToolStripMenuItem.Name = "AddDiagnosisProfileToolStripMenuItem";
            this.AddDiagnosisProfileToolStripMenuItem.Size = new System.Drawing.Size(214, 22);
            this.AddDiagnosisProfileToolStripMenuItem.Text = "Add Diagnosis Profile";
            this.AddDiagnosisProfileToolStripMenuItem.Click += new System.EventHandler(this.AddDiagnosisProfileToolStripMenuItemClick);
            // 
            // ToolStrip1
            // 
            this.ToolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.ToolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ddbFile,
            this.ddbEdit});
            this.ToolStrip1.Location = new System.Drawing.Point(3, 0);
            this.ToolStrip1.Name = "ToolStrip1";
            this.ToolStrip1.Size = new System.Drawing.Size(90, 25);
            this.ToolStrip1.TabIndex = 0;
            // 
            // ddbEdit
            // 
            this.ddbEdit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.ddbEdit.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.AddDiagnosisProfileToolStripMenuItem,
            this.RemoveDiagnosisProfileToolStripMenuItem,
            this.ChangeDiagnosisProfileToolStripMenuItem,
            this.toolStripSeparator2,
            this.treatAmbiguousSpotsAsToolStripMenuItem,
            this.cbxAmbiguous});
            this.ddbEdit.Image = ((System.Drawing.Image)(resources.GetObject("ddbEdit.Image")));
            this.ddbEdit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ddbEdit.Name = "ddbEdit";
            this.ddbEdit.Size = new System.Drawing.Size(40, 22);
            this.ddbEdit.Text = "Edit";
            // 
            // ChangeDiagnosisProfileToolStripMenuItem
            // 
            this.ChangeDiagnosisProfileToolStripMenuItem.Name = "ChangeDiagnosisProfileToolStripMenuItem";
            this.ChangeDiagnosisProfileToolStripMenuItem.Size = new System.Drawing.Size(214, 22);
            this.ChangeDiagnosisProfileToolStripMenuItem.Text = "Change Diagnosis Profile";
            this.ChangeDiagnosisProfileToolStripMenuItem.Click += new System.EventHandler(this.ChangeDiagnosisProfileToolStripMenuItemClick);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(211, 6);
            // 
            // treatAmbiguousSpotsAsToolStripMenuItem
            // 
            this.treatAmbiguousSpotsAsToolStripMenuItem.Name = "treatAmbiguousSpotsAsToolStripMenuItem";
            this.treatAmbiguousSpotsAsToolStripMenuItem.Size = new System.Drawing.Size(214, 22);
            this.treatAmbiguousSpotsAsToolStripMenuItem.Text = "Treat Ambiguous Spots As";
            // 
            // cbxAmbiguous
            // 
            this.cbxAmbiguous.Items.AddRange(new object[] {
            "Threshold Defined",
            "Positive",
            "Negative"});
            this.cbxAmbiguous.Name = "cbxAmbiguous";
            this.cbxAmbiguous.Size = new System.Drawing.Size(121, 23);
            this.cbxAmbiguous.SelectedIndexChanged += new System.EventHandler(this.CbxAmbiguousSelectedIndexChanged);
            this.cbxAmbiguous.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CbxAmbiguousMarkersKeyPress);
            // 
            // AddDiagnosisProfileToolStripMenuItem1
            // 
            this.AddDiagnosisProfileToolStripMenuItem1.Name = "AddDiagnosisProfileToolStripMenuItem1";
            this.AddDiagnosisProfileToolStripMenuItem1.Size = new System.Drawing.Size(208, 22);
            this.AddDiagnosisProfileToolStripMenuItem1.Text = "Add Diagnosis Profile";
            this.AddDiagnosisProfileToolStripMenuItem1.Click += new System.EventHandler(this.AddDiagnosisProfileToolStripMenuItemClick);
            // 
            // SaveSampleInfoToolStripMenuItem1
            // 
            this.SaveSampleInfoToolStripMenuItem1.Name = "SaveSampleInfoToolStripMenuItem1";
            this.SaveSampleInfoToolStripMenuItem1.Size = new System.Drawing.Size(208, 22);
            this.SaveSampleInfoToolStripMenuItem1.Text = "Save Sample Info";
            this.SaveSampleInfoToolStripMenuItem1.Click += new System.EventHandler(this.SaveSampleInfoToolStripMenuItemClick);
            // 
            // ContextMenuStrip1
            // 
            this.ContextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.AddDiagnosisProfileToolStripMenuItem1,
            this.RemoveDiagnosisProfileToolStripMenuItem1,
            this.ChangeDiagnosisProfileToolStripMenuItem1,
            this.ToolStripSeparator1,
            this.ImportProfilesToolStripMenuItem1,
            this.ExportProfilesToolStripMenuItem1,
            this.SaveSampleInfoToolStripMenuItem1});
            this.ContextMenuStrip1.Name = "ContextMenuStrip1";
            this.ContextMenuStrip1.Size = new System.Drawing.Size(209, 142);
            // 
            // RemoveDiagnosisProfileToolStripMenuItem1
            // 
            this.RemoveDiagnosisProfileToolStripMenuItem1.Name = "RemoveDiagnosisProfileToolStripMenuItem1";
            this.RemoveDiagnosisProfileToolStripMenuItem1.Size = new System.Drawing.Size(208, 22);
            this.RemoveDiagnosisProfileToolStripMenuItem1.Text = "Remove Diagnosis Profile";
            this.RemoveDiagnosisProfileToolStripMenuItem1.Click += new System.EventHandler(this.RemoveDiagnosisProfileToolStripMenuItemClick);
            // 
            // ChangeDiagnosisProfileToolStripMenuItem1
            // 
            this.ChangeDiagnosisProfileToolStripMenuItem1.Name = "ChangeDiagnosisProfileToolStripMenuItem1";
            this.ChangeDiagnosisProfileToolStripMenuItem1.Size = new System.Drawing.Size(208, 22);
            this.ChangeDiagnosisProfileToolStripMenuItem1.Text = "Change Diagnosis Profile";
            this.ChangeDiagnosisProfileToolStripMenuItem1.Click += new System.EventHandler(this.ChangeDiagnosisProfileToolStripMenuItemClick);
            // 
            // ToolStripSeparator1
            // 
            this.ToolStripSeparator1.Name = "ToolStripSeparator1";
            this.ToolStripSeparator1.Size = new System.Drawing.Size(205, 6);
            // 
            // ImportProfilesToolStripMenuItem1
            // 
            this.ImportProfilesToolStripMenuItem1.Name = "ImportProfilesToolStripMenuItem1";
            this.ImportProfilesToolStripMenuItem1.Size = new System.Drawing.Size(208, 22);
            this.ImportProfilesToolStripMenuItem1.Text = "Import Profile(s)";
            this.ImportProfilesToolStripMenuItem1.Click += new System.EventHandler(this.ImportProfilesToolStripMenuItemClick);
            // 
            // ExportProfilesToolStripMenuItem1
            // 
            this.ExportProfilesToolStripMenuItem1.Name = "ExportProfilesToolStripMenuItem1";
            this.ExportProfilesToolStripMenuItem1.Size = new System.Drawing.Size(208, 22);
            this.ExportProfilesToolStripMenuItem1.Text = "Export Profile(s)";
            this.ExportProfilesToolStripMenuItem1.Click += new System.EventHandler(this.ExportProfilesToolStripMenuItemClick);
            // 
            // ToolStripContainer1
            // 
            // 
            // ToolStripContainer1.ContentPanel
            // 
            this.ToolStripContainer1.ContentPanel.Controls.Add(this.lvwProfiles);
            this.ToolStripContainer1.ContentPanel.Size = new System.Drawing.Size(1025, 638);
            this.ToolStripContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ToolStripContainer1.Location = new System.Drawing.Point(0, 0);
            this.ToolStripContainer1.Name = "ToolStripContainer1";
            this.ToolStripContainer1.Size = new System.Drawing.Size(1025, 663);
            this.ToolStripContainer1.TabIndex = 1;
            this.ToolStripContainer1.Text = "ToolStripContainer1";
            // 
            // ToolStripContainer1.TopToolStripPanel
            // 
            this.ToolStripContainer1.TopToolStripPanel.Controls.Add(this.ToolStrip1);
            // 
            // lvwProfiles
            // 
            this.lvwProfiles.ContextMenuStrip = this.ContextMenuStrip1;
            this.lvwProfiles.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvwProfiles.FullRowSelect = true;
            this.lvwProfiles.GridLines = true;
            this.lvwProfiles.Location = new System.Drawing.Point(0, 0);
            this.lvwProfiles.Name = "lvwProfiles";
            this.lvwProfiles.Size = new System.Drawing.Size(1025, 638);
            this.lvwProfiles.TabIndex = 0;
            this.lvwProfiles.UseCompatibleStateImageBehavior = false;
            this.lvwProfiles.View = System.Windows.Forms.View.Details;
            // 
            // frmDiagnosis
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1025, 663);
            this.Controls.Add(this.ToolStripContainer1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmDiagnosis";
            this.Text = "Array Tube Analyzer - Sample Diagnostics";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmDiagnosisFormClosing);
            this.ToolStrip1.ResumeLayout(false);
            this.ToolStrip1.PerformLayout();
            this.ContextMenuStrip1.ResumeLayout(false);
            this.ToolStripContainer1.ContentPanel.ResumeLayout(false);
            this.ToolStripContainer1.TopToolStripPanel.ResumeLayout(false);
            this.ToolStripContainer1.TopToolStripPanel.PerformLayout();
            this.ToolStripContainer1.ResumeLayout(false);
            this.ToolStripContainer1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.ToolStripMenuItem ImportProfilesToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem SaveSampleInfoToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem ExportProfilesToolStripMenuItem;
        internal System.Windows.Forms.ToolStripDropDownButton ddbFile;
        internal System.Windows.Forms.ToolStripMenuItem CloseToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem RemoveDiagnosisProfileToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem AddDiagnosisProfileToolStripMenuItem;
        internal System.Windows.Forms.ToolStrip ToolStrip1;
        internal System.Windows.Forms.ToolStripDropDownButton ddbEdit;
        internal System.Windows.Forms.ToolStripMenuItem ChangeDiagnosisProfileToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem AddDiagnosisProfileToolStripMenuItem1;
        internal System.Windows.Forms.ToolStripMenuItem SaveSampleInfoToolStripMenuItem1;
        internal System.Windows.Forms.ContextMenuStrip ContextMenuStrip1;
        internal System.Windows.Forms.ToolStripMenuItem RemoveDiagnosisProfileToolStripMenuItem1;
        internal System.Windows.Forms.ToolStripMenuItem ChangeDiagnosisProfileToolStripMenuItem1;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator1;
        internal System.Windows.Forms.ToolStripMenuItem ImportProfilesToolStripMenuItem1;
        internal System.Windows.Forms.ToolStripMenuItem ExportProfilesToolStripMenuItem1;
        internal System.Windows.Forms.ToolStripContainer ToolStripContainer1;
        private ListViewFF lvwProfiles;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem treatAmbiguousSpotsAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripComboBox cbxAmbiguous;
    }
}