﻿using System.Collections.Generic;
using System.Drawing;

namespace ArrayTubeAnalyzer
{
    public class SpotImageCollection
    {
        private readonly List<SpotImageCrop> _sic;

        private readonly SampleData _sd;
        public List<SpotImageCrop> SpotImages
        {
            get { return _sic; }
        }

        public SpotImageCollection(SampleData sampleData, List<List<MarkerInfo>> lATinfo, List<Image> lImage, List<CalibrationInfo> lCalibration, int iGene, int[] iSpots, List<List<int>> spot)
        {
            _sd = sampleData;
            
            _sic = new List<SpotImageCrop>();

            int iI = 0;
            int iJ = 0;

            for (int i = 0; i <= lImage.Count - 1; i++)
            {
                foreach (int iSpot in iSpots)
                {
                    GetTopLeftIndices(iSpot, ref iI, ref iJ);
                    Image img = lImage[i];
                    double dTop;
                    double dLeft;
                    GetTopLeftValues(img, getSpotCalibration(lCalibration, i), iI, iJ, out dTop, out dLeft);
                    MarkerInfo at = lATinfo[i][iGene];
                    int iIndex = -1;
                    for (int j = 0; j <= spot[iGene].Count - 1; j++)
                    {
                        if (spot[iGene][j] == iSpot)
                        {
                            iIndex = j;
                            break;
                        }
                    }
                    _sic.Add(new SpotImageCrop(CropBitmap((Bitmap)img, (int)dLeft + _sd.Settings.SpotXOffset, (int)dTop + _sd.Settings.SpotYOffset, _sd.Settings.SpotWidth, _sd.Settings.SpotHeight), iSpot, iGene, i, at.Signal[iIndex], at.AverageSignal, at.FinalAverageSignal, at.Flag[iIndex]));
                }
            }
        }

        /// <summary>Spot image collection of all the spots in the dataset.</summary>
        /// <param name="sampleData"> </param>
        /// <param name="lATinfo"></param>
        /// <param name="lImage"></param>
        /// <param name="lCalibration"></param>
        /// <param name="spot"></param>
        /// <remarks></remarks>
        public SpotImageCollection(SampleData sampleData, List<List<MarkerInfo>> lATinfo, List<Image> lImage, List<CalibrationInfo> lCalibration, List<List<int>> spot)
        {
            _sd = sampleData;
            _sic = new List<SpotImageCrop>();

            int iI = 0;
            int iJ = 0;

            for (int i = 0; i <= lATinfo.Count - 1; i++)
            {
                List<MarkerInfo> lat = lATinfo[i];
                for (int j = 0; j <= lat.Count - 1; j++)
                {
                    MarkerInfo at = lat[j];
                    for (int k = 0; k <= at.Signal.Count - 1; k++)
                    {
                        int iSpot = spot[j][k];
                        GetTopLeftIndices(iSpot, ref iI, ref iJ);
                        Image img = lImage[i];
                        double dLeft;
                        double dTop;
                        GetTopLeftValues(img, getSpotCalibration(lCalibration, i), iI, iJ, out dTop, out dLeft);
                        _sic.Add(new SpotImageCrop(CropBitmap((Bitmap)img, (int)dLeft + _sd.Settings.SpotXOffset, (int)dTop + _sd.Settings.SpotYOffset, _sd.Settings.SpotWidth, _sd.Settings.SpotHeight), iSpot, j, i, at.Signal[k], at.AverageSignal, at.FinalAverageSignal, at.Flag[k]));
                    }
                }
            }
        }

        public SpotImageCollection(SampleData sampleData, int iArray, List<MarkerInfo> ati, Image img, CalibrationInfo cali, List<List<int>> spot, SampleCalls gc)
        {
            _sd = sampleData;
            _sic = new List<SpotImageCrop>();

            int iI = 0;
            int iJ = 0;

            foreach (List<int> liSpot in spot)
            {
                int iGene = spot.IndexOf(liSpot);
                MarkerInfo at = ati[iGene];
                foreach (int iSpot in liSpot)
                {
                    GetTopLeftIndices(iSpot, ref iI, ref iJ);
                    double dTop;
                    double dLeft;
                    GetTopLeftValues(img, cali, iI, iJ, out dTop, out dLeft);
                    int iIndex = liSpot.IndexOf(iSpot);
                    bool bPresent = true;
                    switch (gc.Calls[iGene])
                    {
                        case Calls.Absent:
                        case Calls.AbsentAuto:
                        case Calls.AmbiguousAbsent:
                            bPresent = false;
                            break;
                    }
                    _sic.Add(new SpotImageCrop(CropBitmap((Bitmap)img, (int)dLeft + _sd.Settings.SpotXOffset, (int)dTop + _sd.Settings.SpotYOffset, _sd.Settings.SpotWidth, _sd.Settings.SpotHeight), iSpot, iGene, iArray, at.Signal[iIndex], at.AverageSignal, at.FinalAverageSignal, at.Flag[iIndex], bPresent));
                }
            }
        }

        public void SortBySignal()
        {
            //by default set to sort by signal
            _sic.Sort();
        }

        public void SortByFinalAverageSignal()
        {
            foreach (SpotImageCrop s in _sic)
            {
                s.SortBy = SpotImageCrop.Sorting.ByFinalAverageSignal;
            }
            _sic.Sort();
        }

        public void SortBySpot()
        {
            foreach (SpotImageCrop s in _sic)
            {
                s.SortBy = SpotImageCrop.Sorting.BySpot;
            }
            _sic.Sort();
        }

        private CalibrationInfo getSpotCalibration(List<CalibrationInfo> lCalibration, int index)
        {
            return lCalibration[index];
        }

        private void GetTopLeftValues(Image img, CalibrationInfo ci, int iI, int iJ, out double dTop, out double dLeft)
        {
            double heightRatio = img.Height / 400d;
            double widthRatio = img.Width / 400d;
            double h1 = ci.TopLeftX * widthRatio;
            //lblTopLeft.Left
            double h2 = ci.TopRightX * widthRatio;
            //lblTopRight.Left
            double h4 = ci.BottomLeftX * widthRatio;
            //lblBottomLeft.Left
            double v1 = ci.TopLeftY * heightRatio;
            //lblTopLeft.Top
            double v2 = ci.TopRightY * heightRatio;
            //lblTopRight.Top
            double v4 = ci.BottomLeftY * heightRatio;
            //lblBottomLeft.Top

            double dVTop = v1 - v2;
            //change in vertical position between top two corner spots; positive means that v1 is lower than v2; negative means that v2 is lower than v1
            double dHLeft = h1 - h4;
            //change in horiz. position between left most corner spots; positive = h1 farther right than h4; neg. = h4 farther right than h1
            //TODO: Find out if the offsets and new sizes are necessary for getting the proper top and left location values for each spot in the grid
            //Distances between top, bottom, right and left corner spots
            double distTop = h2 - h1 + 30d; // (_sd.Settings.SpotHeight + _sd.Settings.SpotYOffset);
            //lblTopRight.Width 'horiz. distance between top two corner spots
            double distLeft = v4 - v1 + 30d; // (_sd.Settings.SpotWidth + _sd.Settings.SpotXOffset);
            //lblBottomLeft.Height 'vert. distance between left most corner spots

            dTop = v1 + ((iI + 1d) * distLeft / _sd.Layout.Rows) - (((iJ + 1d) / _sd.Layout.Columns) * dVTop) - (distLeft / _sd.Layout.Rows);
            dLeft = h1 + ((iJ + 1d) * distTop / _sd.Layout.Columns) - (((iI + 1d) / _sd.Layout.Rows) * dHLeft) - (distTop / _sd.Layout.Columns);
        }

        private void GetTopLeftIndices(int iSpot, ref int iI, ref int iJ)
        {
            for (int i = 0; i < _sd.Layout.Rows; i++)
            {
                for (int j = 0; j < _sd.Layout.Columns; j++)
                {
                    int spt = (_sd.Layout.Columns - (i + 1)) * _sd.Layout.Rows + (j + 1);
                    if (iSpot == spt)
                    {
                        iI = i;
                        iJ = j;
                        return;
                    }
                }
            }
        }

        public static Bitmap CropBitmap(Bitmap srcBitmap, int cropX, int cropY, int cropWidth, int cropHeight)
        {

            // Create the new bitmap and associated graphics object
            if (cropHeight == 0) cropHeight = 30;
            if (cropWidth == 0) cropWidth = 30;
            var bmp = new Bitmap(cropWidth, cropHeight);

            Graphics g = Graphics.FromImage(bmp);
            // Draw the specified section of the source bitmap to the new one
            g.DrawImage(srcBitmap, new Rectangle(0, 0, cropWidth, cropHeight), cropX, cropY, cropWidth, cropHeight, GraphicsUnit.Pixel);
            // Clean up
            g.Dispose();

            // Return the bitmap
            return bmp;

        }

    }

}
