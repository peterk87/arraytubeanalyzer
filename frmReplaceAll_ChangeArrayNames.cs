﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

namespace ArrayTubeAnalyzer
{
    public partial class frmReplaceAll_ChangeArrayNames : Form
    {
	private List<string> newSampleNames;

	private List<string> originalSampleNames;

	private FindReplace _fr;

	public frmReplaceAll_ChangeArrayNames(object objNames, object objOriginalNames, object fr)
	{
		// This call is required by the designer.
		InitializeComponent();

		// Add any initialization after the InitializeComponent() call.
		_fr = fr as FindReplace;
		//find and replace string values kept in FindReplace class
		txtFind.Text = _fr.Find;
		txtReplace.Text = _fr.Replace;
		this.Location = new Point(_fr.X, _fr.Y);
		//change position of form to where the cursor is but up and to the left a little
		newSampleNames = objNames as List<string>;
		//new AT name list copied by reference to class private variable
		originalSampleNames = objOriginalNames as List<string>;
		//backup of the new AT name list
	}

	private void btnReplace_Click(System.Object sender, System.EventArgs e)
	{
		//replace the 'Find' string with the 'Replace' string
		for (int i = 0; i <= newSampleNames.Count - 1; i++) {
			newSampleNames[i] = newSampleNames[i].Replace(txtFind.Text, txtReplace.Text);
		}
		//save the 'Find' and 'Replace' strings in the FindReplace object
		_fr.Find = txtFind.Text;
		_fr.Replace = txtReplace.Text;
		_fr.X = this.Location.X;
		_fr.Y = this.Location.Y;
		//return DialogResult.Retry which refreshes the frmChangeArrayNames listview containing AT names and reopen the Replace All form
	}

	private void btnCancel_Click(System.Object sender, System.EventArgs e)
	{
		//restore the AT names to what they were prior to changes
		newSampleNames.Clear();
		newSampleNames.AddRange(originalSampleNames);
		//return DialogResult.Cancel 
	}

    private void btnOK_Click(object sender, EventArgs e)
    {

    }

}

public class FindReplace
{
	public string Find { get; set; }
	public string Replace { get; set; }
	public int X { get; set; }
	public int Y { get; set; }
	public FindReplace(string sFind, string sReplace, int iX, int iY)
	{
		Find = sFind;
		Replace = sReplace;
		X = iX;
		Y = iY;
	}
}

}
