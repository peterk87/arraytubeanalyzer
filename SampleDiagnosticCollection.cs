﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.IO;

namespace ArrayTubeAnalyzer
{
    [Serializable]
    public class SampleDiagnosticCollection
    {
        private readonly DiagnosisProfileCollection _dpc;
        public DiagnosisProfileCollection Profiles
        {
            get { return _dpc; }
        }

        public List<SampleDiagnosis> Samples { get; private set; }

        private readonly SampleData _sd;

        public SampleDiagnosticCollection(object objSampleData)
        {
            _sd = objSampleData as SampleData;
            _dpc = new DiagnosisProfileCollection();
            Samples = new List<SampleDiagnosis>();
            SetupSampleDiagnosis();
        }

        public SampleDiagnosticCollection(object objSampleData, IEnumerable<DiagnosisProfile> lDiagnosticProfile)
        {
            _sd = objSampleData as SampleData;
            _dpc = new DiagnosisProfileCollection();
            foreach (DiagnosisProfile dp in lDiagnosticProfile)
            {
                _dpc.Add(dp);
            }
            Samples = new List<SampleDiagnosis>();
            UpdateSampleDiagnosis();
        }

        public void AddDiagnosticProfile(DiagnosisProfile dp)
        {
            _dpc.Add(dp);
            UpdateSampleDiagnosis();
        }

        public void RemoveDiagnosticProfiles(List<int> indices)
        {
            indices.Sort();
            indices.Reverse();
            foreach (int index in indices)
            {
                _dpc.Remove(index);
            }
            UpdateSampleDiagnosis();
        }

        public void ChangeDiagnosticProfile(int index, DiagnosisProfile dp)
        {
            _dpc.Change(index, dp);
            UpdateSampleDiagnosis();
        }

        public void WriteDiagnosticProfiles(List<string> lGenes, List<int> indices, string filename)
        {
            using (var sw = new StreamWriter(filename))
            {
                var sb = new StringBuilder();
                foreach (string gene in lGenes)
                {
                    sb.Append('\t');
                    sb.Append(gene);
                }
                sw.WriteLine(sb.ToString());
                foreach (int index in indices)
                {
                    _dpc.Write(index, sw);
                }
                sw.Close();
            }
        }

        public bool ReadDiagnosticProfiles(string filename, int iNumGenes)
        {
            try
            {
                using (var sr = new StreamReader(filename))
                {
                    sr.ReadLine();
                    while (!sr.EndOfStream)
                    {
                        bool bError = false;
                        var newDP = new DiagnosisProfile(sr.ReadLine(), iNumGenes, ref bError);
                        if (bError)
                        {
                            throw new Exception();
                        }
                        _dpc.Add(newDP);
                    }
                    sr.Close();
                }
                UpdateSampleDiagnosis();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private void SetupSampleDiagnosis()
        {
            for (int i = 0; i < _sd.SampleCount; i++)
            {
                Samples.Add(new SampleDiagnosis(_sd.SampleNames[i], i, _sd.SampleCalls[i], _sd.Ambiguous));
            }
        }

        public void UpdateSampleDiagnosis()
        {
            Samples = new List<SampleDiagnosis>();
            SetupSampleDiagnosis();
            foreach (SampleDiagnosis t in Samples)
            {
                t.UpdateDiagnosis(_dpc.DiagnosisProfileList);
            }
        }

        public void WriteSampleInfo(string filename)
        {
            using (var sw = new StreamWriter(filename))
            {
                var list = new List<string> {"Sample", "Cluster"};
                foreach (DiagnosisProfile dp in _dpc.DiagnosisProfileList)
                {
                    list.Add(dp.Name);
                }
                sw.WriteLine(string.Join("\t", list));

                for (int i = 0; i < _sd.SampleCount; i++)
                {
                    string sample = _sd.SampleNames[_sd.Cluster.ClusterGroup.NameIndices[i]];
                    string num = _sd.Cluster.ClusterGroup.ClusterNumber[i].ToString(CultureInfo.InvariantCulture);

                    var ls = new List<string> {sample, num};
                    //Samples
                    foreach (SampleDiagnosis sd in Samples)
                    {
                        if (sd.Name != sample) continue;
                        foreach (DiagnosisProfile dp in Profiles.DiagnosisProfileList)
                        {
                            ls.Add(sd.DiagnosticProfiles.Contains(dp) ? "1" : "0");
                        }
                        break;
                    }
                    sw.WriteLine(string.Join("\t", ls));
                }
                sw.Close();
            }
        }
    }
}
