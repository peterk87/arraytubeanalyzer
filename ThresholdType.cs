namespace ArrayTubeAnalyzer
{
    public enum ThresholdType
    {
        UserDefined,
        GlobalBackground,
        PercentOfPositiveSignal
    }
}