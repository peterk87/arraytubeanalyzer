﻿using System.Drawing;
using System.Windows.Forms;

namespace ArrayTubeAnalyzer
{
    public partial class FrmColourProfile : Form
    {

	private readonly INISettings _cp;

	public FrmColourProfile(INISettings colourProfile)
	{
		FormClosed += FrmColourProfileFormClosed;
		// This call is required by the designer.
		InitializeComponent();

		// Add any initialization after the InitializeComponent() call.
		_cp = colourProfile;
		SetupButtons();
	}

	private void SetupButtons()
	{
		//Main Window - Spot Highlighters
		btnFlagged.BackColor = _cp.C.FlaggedColour;
		btnKeep.BackColor = _cp.C.KeepColour;
		btnKeepAuto.BackColor = _cp.C.KeepAutoColour;
		btnDiscard.BackColor = _cp.C.DiscardColour;

		//Main Window - Listview Items
		btnFlaggedListview.BackColor = _cp.C.FlaggedListviewColour;
		btnKeepListview.BackColor = _cp.C.KeepListviewColour;
		btnKeepAutoListview.BackColor = _cp.C.KeepAutoListviewColour;
		btnDiscardListview.BackColor = _cp.C.DiscardListviewColour;

		//Gene Call Review Window - Spot Highlighters
		btnPresent.BackColor = _cp.C.PresentColour;
		btnAbsent.BackColor = _cp.C.AbsentColour;
		btnPresentAuto.BackColor = _cp.C.PresentAutoColour;
		btnAbsentAuto.BackColor = _cp.C.AbsentAutoColour;
		btnFlaggedPresent.BackColor = _cp.C.PresentFlaggedColour;
		btnFlaggedAbsent.BackColor = _cp.C.AbsentFlaggedColour;

		//Gene Call Review Window - Listview Items
		btnPresentListview.BackColor = _cp.C.PresentListviewColour;
		btnAbsentListview.BackColor = _cp.C.AbsentListviewColour;
		btnPresentAutoListview.BackColor = _cp.C.PresentAutoListviewColour;
		btnAbsentAutoListview.BackColor = _cp.C.AbsentAutoListviewColour;
		btnFlaggedPresentListview.BackColor = _cp.C.PresentFlaggedListviewColour;
		btnFlaggedAbsentListview.BackColor = _cp.C.AbsentFlaggedListviewColour;

		//Clustering Window - Odd Clusters
		btnPresentOdd.BackColor = _cp.C.PresentClusteringOddColour;
		btnAbsentOdd.BackColor = _cp.C.AbsentClusteringOddColour;
		btnSampleOdd.BackColor = _cp.C.StrainOddColour;

		//Clustering Window - Even Clusters
		btnPresentEven.BackColor = _cp.C.PresentClusteringEvenColour;
		btnAbsentEven.BackColor = _cp.C.AbsentClusteringEvenColour;
		btnSampleEven.BackColor = _cp.C.StrainEvenColour;

	}

	private Color changeColour(Color c)
	{
		using (var cd = new ColorDialog()) {
			if (cd.ShowDialog() == DialogResult.OK) {
				c = cd.Color;
			}
		}
		return c;
	}


	#region "Main Window - Spot Highlighters"
	private void BtnFlaggedClick(System.Object sender, System.EventArgs e)
	{
		var btn = sender as Button;
        _cp.C.FlaggedColour = changeColour(_cp.C.FlaggedColour);
	    if (btn != null) btn.BackColor = _cp.C.FlaggedColour;
	}

	private void BtnKeepColourClick(System.Object sender, System.EventArgs e)
	{
        var btn = sender as Button;
        _cp.C.KeepColour = changeColour(_cp.C.KeepColour);
	    if (btn != null) btn.BackColor = _cp.C.KeepColour;
	}

	private void BtnKeepAutoClick(System.Object sender, System.EventArgs e)
	{
        var btn = sender as Button;
        _cp.C.KeepAutoColour= changeColour(_cp.C.KeepAutoColour);
	    if (btn != null) btn.BackColor = _cp.C.KeepAutoColour;
	}

	private void BtnDiscardClick(System.Object sender, System.EventArgs e)
	{
        var btn = sender as Button;
        _cp.C.DiscardColour= changeColour(_cp.C.DiscardColour);
	    if (btn != null) btn.BackColor = _cp.C.DiscardColour;
	}
	#endregion
	#region "Main Window - Listview Items"
	private void BtnFlaggedListviewClick(System.Object sender, System.EventArgs e)
	{
        var btn = sender as Button;
        _cp.C.FlaggedListviewColour = changeColour(_cp.C.FlaggedListviewColour);
	    if (btn != null) btn.BackColor = _cp.C.FlaggedListviewColour;
	}

	private void BtnKeepListviewClick(System.Object sender, System.EventArgs e)
	{
        var btn = sender as Button;
        _cp.C.KeepListviewColour  = changeColour(_cp.C.KeepListviewColour);
	    if (btn != null) btn.BackColor = _cp.C.KeepListviewColour;
	}

	private void BtnKeepAutoListviewClick(System.Object sender, System.EventArgs e)
	{
        var btn = sender as Button;
        _cp.C.KeepAutoListviewColour= changeColour( _cp.C.KeepAutoListviewColour);
	    if (btn != null) btn.BackColor = _cp.C.KeepAutoListviewColour;
	}

	private void BtnDiscardListviewClick(System.Object sender, System.EventArgs e)
	{
        var btn = sender as Button;
        _cp.C.DiscardListviewColour= changeColour(_cp.C.DiscardListviewColour);
	    if (btn != null) btn.BackColor = _cp.C.DiscardListviewColour;
	}
	#endregion
	#region "Marker Call Review Window - Spot Highlighters"
	private void BtnPresentClick(System.Object sender, System.EventArgs e)
	{
        var btn = sender as Button;
		_cp.C.PresentColour = changeColour(_cp.C.PresentColour);
	    if (btn != null) btn.BackColor = _cp.C.PresentColour;
	}

	private void BtnAbsentClick(System.Object sender, System.EventArgs e)
	{
        var btn = sender as Button;
        _cp.C.AbsentColour= changeColour(_cp.C.AbsentColour);
	    if (btn != null) btn.BackColor = _cp.C.AbsentColour;
	}

	private void BtnPresentAutoClick(System.Object sender, System.EventArgs e)
	{
        var btn = sender as Button;
        _cp.C.PresentAutoColour= changeColour(_cp.C.PresentAutoColour);
	    if (btn != null) btn.BackColor = _cp.C.PresentAutoColour;
	}

	private void BtnAbsentAutoClick(System.Object sender, System.EventArgs e)
	{
        var btn = sender as Button;
        _cp.C.AbsentAutoColour= changeColour(_cp.C.AbsentAutoColour);
	    if (btn != null) btn.BackColor = _cp.C.AbsentAutoColour;
	}

	private void BtnFlaggedPresentClick(System.Object sender, System.EventArgs e)
	{
        var btn = sender as Button;
        _cp.C.PresentFlaggedColour = changeColour(_cp.C.PresentFlaggedColour);
	    if (btn != null) btn.BackColor = _cp.C.PresentFlaggedColour;
	}

	private void BtnFlaggedAbsentClick(System.Object sender, System.EventArgs e)
	{
	    var btn = sender as Button;
	    if (btn != null) btn.BackColor = changeColour(_cp.C.AbsentFlaggedColour);
	}

        #endregion
	#region "Marker Call Review Window - Listview Items"
	private void BtnPresentListviewClick(System.Object sender, System.EventArgs e)
	{
        var btn = sender as Button;
        _cp.C.PresentListviewColour= changeColour(_cp.C.PresentListviewColour);
	    if (btn != null) btn.BackColor = _cp.C.PresentListviewColour;
	}

	private void BtnAbsentListviewClick(System.Object sender, System.EventArgs e)
	{
        var btn = sender as Button;
        _cp.C.AbsentListviewColour= changeColour(_cp.C.AbsentListviewColour);
	    if (btn != null) btn.BackColor = _cp.C.AbsentListviewColour;
	}

	private void BtnPresentAutoListviewClick(System.Object sender, System.EventArgs e)
	{
        var btn = sender as Button;
        _cp.C.PresentAutoListviewColour= changeColour(_cp.C.PresentAutoListviewColour);
	    if (btn != null) btn.BackColor = _cp.C.PresentAutoListviewColour;
	}

	private void BtnAbsentAutoListviewClick(System.Object sender, System.EventArgs e)
	{
        var btn = sender as Button;
        _cp.C.AbsentAutoListviewColour = changeColour(_cp.C.AbsentAutoListviewColour);
	    if (btn != null) btn.BackColor = _cp.C.AbsentAutoListviewColour;
	}

	private void BtnFlaggedPresentListviewClick(System.Object sender, System.EventArgs e)
	{
        var btn = sender as Button;
        _cp.C.PresentFlaggedListviewColour= changeColour(_cp.C.PresentFlaggedListviewColour);
	    if (btn != null) btn.BackColor = _cp.C.PresentFlaggedListviewColour;
	}

	private void BtnFlaggedAbsentListviewClick(System.Object sender, System.EventArgs e)
	{
        var btn = sender as Button;
        _cp.C.AbsentFlaggedListviewColour = changeColour(_cp.C.AbsentFlaggedListviewColour);
	    if (btn != null) btn.BackColor = _cp.C.AbsentFlaggedListviewColour;
	}
	#endregion

	private void FrmColourProfileFormClosed(System.Object sender, FormClosedEventArgs e)
	{
		_cp.Write();
	}
	#region "Clustering Window - Odd Clusters"

	private void BtnPresentOddClick(System.Object sender, System.EventArgs e)
	{
		var btn = sender as Button;
        _cp.C.PresentClusteringOddColour= changeColour(_cp.C.PresentClusteringOddColour);
	    if (btn != null) btn.BackColor = _cp.C.PresentClusteringOddColour;
	}

	private void BtnAbsentOddClick(System.Object sender, System.EventArgs e)
	{
		var btn = sender as Button;
        _cp.C.AbsentClusteringOddColour= changeColour(_cp.C.AbsentClusteringOddColour);
	    if (btn != null) btn.BackColor = _cp.C.AbsentClusteringOddColour;
	}

	private void BtnSampleOddClick(System.Object sender, System.EventArgs e)
	{
		var btn = sender as Button;
        _cp.C.StrainOddColour = changeColour(_cp.C.StrainOddColour);
	    if (btn != null) btn.BackColor = _cp.C.StrainOddColour;
	}
	#endregion
	#region "Clustering Window - Even Clusters"
	private void BtnPresentEvenClick(System.Object sender, System.EventArgs e)
	{
		var btn = sender as Button;
        _cp.C.PresentClusteringEvenColour = changeColour(_cp.C.PresentClusteringEvenColour);
	    if (btn != null) btn.BackColor = _cp.C.PresentClusteringEvenColour;
	}

	private void BtnAbsentEvenClick(System.Object sender, System.EventArgs e)
	{
		var btn = sender as Button;
        _cp.C.AbsentClusteringEvenColour = changeColour(_cp.C.AbsentClusteringEvenColour);
	    if (btn != null) btn.BackColor = _cp.C.AbsentClusteringEvenColour;
	}

	private void BtnSampleEvenClick(System.Object sender, System.EventArgs e)
	{
		var btn = sender as Button;
        _cp.C.StrainEvenColour  = changeColour(_cp.C.StrainEvenColour);
	    if (btn != null) btn.BackColor = _cp.C.StrainEvenColour;
	}
	#endregion
}

}
