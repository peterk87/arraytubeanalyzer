﻿using System;
using System.Collections.Generic;

namespace ArrayTubeAnalyzer
{
    [Serializable]
    public class SampleDiagnosis
    {
        private readonly List<bool> _calls;

        /// <summary>Sample name.</summary>
        public string Name { get; private set; }

        /// <summary>Index of the sample.</summary>
        public int Index { get; private set; }

        private readonly List<DiagnosisProfile> _diagnosticProfiles;
        public List<DiagnosisProfile> DiagnosticProfiles
        {
            get { return _diagnosticProfiles; }
        }

        public SampleDiagnosis(string sampleName, int sampleIndex, SampleCalls atgc, TreatAmbiguousAs ambiguous)
        {
            _diagnosticProfiles = new List<DiagnosisProfile>();
            _calls = new List<bool>();

            Index = sampleIndex;
            Name = sampleName;
            GetCalls(atgc, ambiguous);
        }

        private void GetCalls(SampleCalls atgc, TreatAmbiguousAs ambiguous)
        {
            foreach (Calls geneCall in atgc.Calls)
            {
                switch (ambiguous)
                {
                    case TreatAmbiguousAs.Negative:
                        switch (geneCall)
                        {
                            case Calls.Absent:
                            case Calls.AbsentAuto:
                            case Calls.AmbiguousPresent:
                            case Calls.AmbiguousAbsent:
                                _calls.Add(false);
                                break;
                            case Calls.Present:
                            case Calls.PresentAuto:
                                _calls.Add(true);
                                break;
                        }
                        break;
                    case TreatAmbiguousAs.Positive:
                        switch (geneCall)
                        {
                            case Calls.Absent:
                            case Calls.AbsentAuto:
                                _calls.Add(false);
                                break;
                            case Calls.AmbiguousPresent:
                            case Calls.Present:
                            case Calls.PresentAuto:
                            case Calls.AmbiguousAbsent:
                                _calls.Add(true);
                                break;
                        }
                        break;
                    case TreatAmbiguousAs.ThresholdDetermined:
                        switch (geneCall)
                        {
                            case Calls.Absent:
                            case Calls.AbsentAuto:
                            case Calls.AmbiguousAbsent:
                                _calls.Add(false);
                                break;
                            case Calls.AmbiguousPresent:
                            case Calls.Present:
                            case Calls.PresentAuto:
                                _calls.Add(true);
                                break;
                        }
                        break;
                }
            }
        }

        public void UpdateDiagnosis(List<DiagnosisProfile> allDP)
        {
            foreach (DiagnosisProfile dp in allDP)
            {
                bool bMatch = true;
                for (int i = 0; i < dp.DiagnosisList.Count; i++)
                {
                    Diagnosis diag = dp.DiagnosisList[i];
                    if ((diag == Diagnosis.Ignore | diag == Diagnosis.MaybeAbsent | diag == Diagnosis.MaybePresent))
                        continue;
                    if (diag == Diagnosis.Absent & _calls[i])
                    {
                        bMatch = false;
                        break;
                    }
                    if (!(diag == Diagnosis.Present & !_calls[i])) continue;
                    bMatch = false;
                    break;
                }
                if (bMatch)
                {
                    _diagnosticProfiles.Add(dp);
                }
            }
        }
    }

}
