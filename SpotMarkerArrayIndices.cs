﻿using System;

namespace ArrayTubeAnalyzer
{
    /// <summary>
    /// Object to store indices for the spot, gene and sample array tube.
    /// </summary>
    /// <remarks></remarks>
    [Serializable]
    public class SpotMarkerArrayIndices : SpotMarkerIndices
    {
        public bool EqualsTo(SpotMarkerArrayIndices value2)
        {
            if (value2 == null)
            {
                return false;
            }
            return (Array == value2.Array & Marker == value2.Marker & Spot == value2.Spot);
        }

        public bool NotEqualsTo(SpotMarkerArrayIndices value2)
        {
            return !(EqualsTo(value2));
        }

        private readonly int _iArray;
        public int Array
        {
            get { return _iArray; }
        }

        public SpotMarkerArrayIndices(int geneIndex, int spotIndex, int arrayIndex)
            : base(geneIndex, spotIndex)
        {
            _iArray = arrayIndex;
        }

    }

    

}
