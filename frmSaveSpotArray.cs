﻿using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;

namespace ArrayTubeAnalyzer
{
    public partial class FrmSaveSpotArray : Form
    {

        private readonly SpotArrayImageParameters _saip;


        private readonly SampleData _sd;

        public FrmSaveSpotArray(object objSampleData)
        {
            // This call is required by the designer.
            InitializeComponent();

            // Add any initialization after the InitializeComponent() call.
            _sd = objSampleData as SampleData;
            _saip = new SpotArrayImageParameters();
            btnFont.Font = _saip.HeaderFont;
            btnSpotSignalFont.Font = _saip.SignalFont;
            btnPresent.BackColor = _saip.Present;
            btnAbsent.BackColor = _saip.Absent;

            PopulateGenesToShowListview();
            PopulateSamplesToShowListview();
            GetSelectedGenes();
            GetSelectedSamples();
        }

        private void PopulateGenesToShowListview()
        {
            List<string> markers = _sd.Markers;

            var lviList = new List<ListViewItem>();
            foreach (string marker in markers)
            {
                var lvi = new ListViewItem(marker) { BackColor = Color.DodgerBlue, ForeColor = Color.White, Selected = true };
                lviList.Add(lvi);
            }
            lvwGenes.Items.AddRange(lviList.ToArray());
        }

        private void PopulateSamplesToShowListview()
        {
            List<string> samples = _sd.SampleNames;

            var lviList = new List<ListViewItem>();
            foreach (string sample in samples)
            {
                var lvi = new ListViewItem(sample) { BackColor = Color.DodgerBlue, ForeColor = Color.White, Selected = true };
                lviList.Add(lvi);
            }
            lvwSamples.Items.AddRange(lviList.ToArray());
        }

        private void GetSelectedGenes()
        {
            var lGene = new List<int>();
            foreach (int i in lvwGenes.SelectedIndices)
            {
                lGene.Add(i);
            }
            _saip.SetGenes(lGene);
            lblGenes.Text = string.Format("{0} Genes To Show", _saip.Genes.Count);
        }

        private void GetSelectedSamples()
        {
            var lSamples = new List<int>();
            foreach (int i in lvwSamples.SelectedIndices)
            {
                lSamples.Add(i);
            }
            _saip.SetSamples(lSamples);
            lblSamples.Text = string.Format("{0} Samples To Show", _saip.Samples.Count);
        }

        private void GetSelectedGenes(int index, bool bSelected)
        {
            List<int> lGene = _saip.Genes;

            if (lGene.Contains(index))
            {
                if (!bSelected)
                {
                    lGene.Remove(index);
                }
            }
            else
            {
                if (bSelected)
                {
                    lGene.Add(index);
                }
            }
            _saip.SetGenes(lGene);

            lblGenes.Text = string.Format("{0} Genes To Show", _saip.Genes.Count);
        }

        private void GetSelectedSamples(int index, bool bSelected)
        {
            List<int> lSamples = _saip.Samples;

            if (lSamples.Contains(index))
            {
                if (!bSelected)
                {
                    lSamples.Remove(index);
                }
            }
            else
            {
                if (bSelected)
                {
                    lSamples.Add(index);
                }
            }
            _saip.SetSamples(lSamples);

            lblSamples.Text = string.Format("{0} Samples To Show", _saip.Samples.Count);
        }

        private void LvwGenesItemSelectionChanged(System.Object sender, ListViewItemSelectionChangedEventArgs e)
        {
            if (e.IsSelected)
            {
                e.Item.BackColor = Color.DodgerBlue;
                e.Item.ForeColor = Color.White;
            }
            else
            {
                e.Item.BackColor = Color.White;
                e.Item.ForeColor = Color.Black;
            }
            GetSelectedGenes(e.ItemIndex, e.IsSelected);

        }

        private void LvwSamplesItemSelectionChanged(System.Object sender, ListViewItemSelectionChangedEventArgs e)
        {
            if (e.IsSelected)
            {
                e.Item.BackColor = Color.DodgerBlue;
                e.Item.ForeColor = Color.White;
            }
            else
            {
                e.Item.BackColor = Color.White;
                e.Item.ForeColor = Color.Black;
            }
            GetSelectedSamples(e.ItemIndex, e.IsSelected);
        }
        private void BtnAbsentClick(System.Object sender, System.EventArgs e)
        {
            using (var cd = new ColorDialog())
            {
                if (cd.ShowDialog() == DialogResult.OK)
                {
                    _saip.Absent = cd.Color;
                    btnAbsent.BackColor = _saip.Absent;
                }
            }
        }

        private void BtnPresentClick(System.Object sender, System.EventArgs e)
        {
            using (var cd = new ColorDialog())
            {
                if (cd.ShowDialog() == DialogResult.OK)
                {
                    _saip.Present = cd.Color;
                    btnPresent.BackColor = _saip.Present;
                }
            }
        }

        private void BtnAmbiguousClick(System.Object sender, System.EventArgs e)
        {
            using (var cd = new ColorDialog())
            {
                if (cd.ShowDialog() == DialogResult.OK)
                {
                    _saip.Ambiguous = cd.Color;
                    btnAmbiguous.BackColor = _saip.Ambiguous;
                }
            }
        }
        private void GetSaveFileForSpotArray()
        {
            using (var sfd = new SaveFileDialog())
            {
                sfd.FileName = "Spot Array.jpeg";
                sfd.Filter = "JPEG File (*.jpeg)|*.jpeg|PNG File (*.png)|*.png|Bitmap File (*.bmp)|*.bmp|GIF File (*.gif)|*.gif|TIFF File (*.tiff)|*.tiff";
                sfd.Title = "Specify save filename for spot array picture";
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    switch (sfd.FilterIndex)
                    {
                        case 1:
                            _saip.ImgFormat = ImageFormat.Jpeg;
                            break;
                        case 2:
                            _saip.ImgFormat = ImageFormat.Png;
                            break;
                        case 3:
                            _saip.ImgFormat = ImageFormat.Bmp;
                            break;
                        case 4:
                            _saip.ImgFormat = ImageFormat.Gif;
                            break;
                        case 5:
                            _saip.ImgFormat = ImageFormat.Tiff;
                            break;
                    }
                    //tslStatus.Text = "Creating spot array picture"
                    _saip.SavePath = sfd.FileName;
                    txtSave.Text = _saip.SavePath;
                    btnBuildSavePic.Enabled = true;
                }
            }
        }

        private void BtnBuildSavePicClick(System.Object sender, System.EventArgs e)
        {
            SaveClusteredSpotArrayImage();
        }

        private const int Sizexy = 85;

        private void SaveClusteredSpotArrayImage()
        {
            tslStatus.Text = "Creating spot array image...";
            StatusStrip1.Refresh();
            List<int> iArrays = _sd.Cluster.ClusterGroup.NameIndices;

            var lSic = new List<SpotImageCollection>();
            for (int i = 0; i < iArrays.Count; i++)
            {
                int index = iArrays[i];
                if (_saip.Samples.Contains(index))
                {
                    lSic.Add(new SpotImageCollection(_sd, index, _sd.SampleStats[index], _sd.Images[index], _sd.Calibration[index], _sd.Spots, _sd.SampleCalls[index]));
                }
            }

            int iWidth = GetSelectedNumberOfGeneSpots(lSic[0]) * Sizexy + 100;
            int iHeight = lSic.Count * Sizexy + 100;
            var bm = new Bitmap(iWidth, iHeight);
            Graphics g = Graphics.FromImage(bm);
            g.FillRectangle(Brushes.White, new Rectangle(0, 0, iWidth, iHeight));
            if (chkOneSpotPerMarker.Checked)
            {
                CreateColumnHeadersForSpotArray(ref g);
            }
            else
            {
                CreateColumnHeadersForSpotArray(lSic[0], ref g);
            }
            CreateRowHeadersForSpotArray(lSic, ref g);
            int y = 101;

            
            ProgressBar1.Visible = true;
            ProgressBar1.Step = 1;
            ProgressBar1.Maximum = lSic.Count;
            ProgressBar1.Value = 0;

            foreach (SpotImageCollection s in lSic)
            {
                int x;
                CreateSpotPictures(s, ref y, out x, ref g);

                y += Sizexy;
                ProgressBar1.PerformStep();
            }
            bm.Save(_saip.SavePath, _saip.ImgFormat);
            bm.Dispose();
            ProgressBar1.Visible = false;
            tslStatus.Text = "Saved to '" + _saip.SavePath + "'";

        }

        private int GetSelectedNumberOfGeneSpots(SpotImageCollection sic)
        {
            int i = 0;
            var lGenes = new List<int>();
            lGenes.AddRange(_saip.Genes);
            foreach (SpotImageCrop s in sic.SpotImages)
            {
                if (lGenes.Contains(s.Gene))
                {
                    if (chkOneSpotPerMarker.Checked)
                    {
                        lGenes.Remove(s.Gene);
                    }
                    i += 1;
                }
            }
            return i;
        }



        private void CreateSpotPictures(SpotImageCollection sic, ref int iY, out int iX, ref Graphics g)
        {
            iX = 101;
            const int minus5 = Sizexy - 5;
            const int minus1 = Sizexy - 1;
            var lGenes = new List<int>();
            lGenes.AddRange(_saip.Genes);
            foreach (SpotImageCrop s in sic.SpotImages)
            {
                if (lGenes.Contains(s.Gene))//& ((s.Flagging != Flagging.Discard & chkOneSpotPerMarker.Checked) | !chkOneSpotPerMarker.Checked))
                {
                    bool createSpotPicture = false;
                    if (chkOneSpotPerMarker.Checked)
                    {
                        bool allReplicatesDiscarded = true;
                        //check if all replicates are discarded
                        MarkerInfo marker = _sd.SampleStats[s.Array][s.Gene];
                        foreach (Flagging flag in marker.Flag)
                        {
                            if (flag != Flagging.Discard)
                            {
                                allReplicatesDiscarded = false;
                            }
                        }
                        if (allReplicatesDiscarded || s.Flagging != Flagging.Discard)
                        {
                            lGenes.Remove(s.Gene);
                            createSpotPicture = true;
                        }
                    }
                    if (!chkOneSpotPerMarker.Checked || (chkOneSpotPerMarker.Checked && createSpotPicture))
                    {
                        var rf = new Rectangle(iX, iY, minus1, minus1);
                        var colorSpotBorder = new Pen(_saip.Absent);
                        Brush colorFillSpotRectangle = new SolidBrush(_saip.Absent);
                        if (s.GeneCall)
                        {
                            colorSpotBorder = new Pen(_saip.Present);
                            colorFillSpotRectangle = new SolidBrush(_saip.Present);
                        }
                        if (_saip.ShowAmbiguous)
                        {
                            Calls gCall = _sd.SampleCalls[s.Array].Calls[s.Gene];
                            if (gCall == Calls.AmbiguousAbsent |
                                gCall == Calls.AmbiguousPresent |
                                gCall == Calls.AmbiguousManual)
                            {
                                colorSpotBorder = new Pen(_saip.Ambiguous);
                                colorFillSpotRectangle = new SolidBrush(_saip.Ambiguous);
                            }
                        }
                        if (s.Flagging == Flagging.Discard)
                        {
                            colorSpotBorder = new Pen(_sd.Settings.C.DiscardColour);
                            colorFillSpotRectangle = new SolidBrush(_sd.Settings.C.DiscardColour);
                        }

                        g.DrawRectangle(colorSpotBorder, rf);
                        g.FillRectangle(colorFillSpotRectangle, rf);
                        g.DrawImage(s.ImageAT, iX + 2, iY + 2, minus5, minus5);
                        if (_saip.ShowSignal)
                        {
                            string sSignal = s.Signal.ToString("0.0000");
                            SizeF sizeSignal = g.MeasureString(sSignal, _saip.SignalFont);
                            g.DrawString(sSignal, _saip.SignalFont, Brushes.Black, iX + 2, iY + 2 + minus5 - sizeSignal.Height);
                        }
                        if (s.Flagging == Flagging.Discard)
                        {
                            const string discarded = "Discarded";
                            //SizeF sizeDiscarded = g.MeasureString(discarded, _saip.SignalFont);
                            g.DrawString(discarded, _saip.SignalFont, Brushes.Red, iX + 2, iY + 2);
                        }
                        iX += Sizexy;
                    }
                }
            }
        }

        /// <summary>Create only one header per marker.</summary>
        /// <param name="g"></param>
        private void CreateColumnHeadersForSpotArray(ref Graphics g)
        {
            int x = 101;
            const int y = 1;
            const int minus1 = Sizexy - 1;
            foreach (int iGene in _saip.Genes)
            {
                string sGene = _sd.Markers[iGene];
                Font f = _saip.HeaderFont;
                var rf = new Rectangle(x, y, minus1, 100);
                g.DrawRectangle(Pens.Black, rf);
                g.FillRectangle(Brushes.White, rf);
                g.DrawString(sGene, f, Brushes.Black, rf);
                x += Sizexy;
            }
        }

        /// <summary>Create a header for each spot - multiple headers per marker depending on number of replicates for marker.</summary>
        /// <param name="sic"></param>
        /// <param name="g"></param>
        private void CreateColumnHeadersForSpotArray(SpotImageCollection sic, ref Graphics g)
        {
            int x = 101;
            const int y = 1;
            const int plus50 = 51;
            const int minus1 = Sizexy - 1;
            foreach (SpotImageCrop s in sic.SpotImages)
            {
                if (_saip.Genes.Contains(s.Gene))
                {
                    string sGene = _sd.Markers[s.Gene];
                    Font f = _saip.HeaderFont;
                    var rf = new Rectangle(x, y, minus1, 50);
                    g.DrawRectangle(Pens.Black, rf);
                    g.FillRectangle(Brushes.White, rf);
                    g.DrawString(sGene, f, Brushes.Black, rf);

                    string sSpot = string.Format("Spot - {0}", s.Spot);
                    var rf2 = new Rectangle(x, plus50, minus1, 49);
                    g.DrawRectangle(Pens.Black, rf2);
                    g.FillRectangle(Brushes.White, rf2);
                    g.DrawString(sSpot, f, Brushes.Black, rf2);

                    x += Sizexy;
                }
            }
        }

        private void CreateRowHeadersForSpotArray(IEnumerable<SpotImageCollection> lSic, ref Graphics g)
        {
            const int halfSize = Sizexy / 2;
            const int x = 1;
            int y = 101;
            foreach (SpotImageCollection s in lSic)
            {
                string sArray = _sd.SampleNames[s.SpotImages[0].Array];
                Font f = _saip.HeaderFont;
                var rf = new Rectangle(x, y, 99, halfSize);
                g.DrawRectangle(Pens.Black, rf);
                g.FillRectangle(Brushes.White, rf);
                g.DrawString(sArray, f, Brushes.Black, rf);
                int arrayIndex = _sd.SampleNames.IndexOf(sArray);
                int clusterIndex = _sd.Cluster.ClusterGroup.NameIndices.IndexOf(arrayIndex);
                string sCluster = string.Format("Cluster: {0}", _sd.Cluster.ClusterGroup.ClusterNumber[clusterIndex]);
                //+ clusterNumber(lSic.IndexOf(s)).ToString
                var rf2 = new Rectangle(x, y + halfSize, 99, halfSize - 1);
                g.DrawRectangle(Pens.Black, rf2);
                g.FillRectangle(Brushes.White, rf2);
                g.DrawString(sCluster, f, Brushes.Black, rf2);

                y += Sizexy;
            }
        }

        private void BtnFontClick(System.Object sender, System.EventArgs e)
        {
            using (var fd = new FontDialog())
            {
                if (fd.ShowDialog() == DialogResult.OK)
                {
                    _saip.HeaderFont = fd.Font;
                    btnFont.Font = _saip.HeaderFont;
                }
            }
        }

        private void BtnSaveClick(System.Object sender, System.EventArgs e)
        {
            GetSaveFileForSpotArray();
        }

        private void ChkShowSignalCheckedChanged(System.Object sender, System.EventArgs e)
        {
            _saip.ShowSignal = chkShowSignal.Checked;
            btnSpotSignalFont.Enabled = chkShowSignal.Checked;
            lblSignalFont.Enabled = chkShowSignal.Checked;
        }

        private void BtnSpotSignalFontClick(System.Object sender, System.EventArgs e)
        {
            using (var fd = new FontDialog())
            {
                if (fd.ShowDialog() == DialogResult.OK)
                {
                    _saip.SignalFont = fd.Font;
                    btnSpotSignalFont.Font = _saip.SignalFont;
                }
            }
        }

        private void BtnLoadGeneSelectionClick(System.Object sender, System.EventArgs e)
        {
            using (var ofd = new OpenFileDialog())
            {
                ofd.Title = "Load gene selection from file";
                ofd.FileName = "Gene Selection.ata";
                ofd.Filter = "ATA Gene Selection (*.ata)|*.ata|All Files (*.*)|*.*";
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    using (var sr = new System.IO.StreamReader(ofd.FileName))
                    {
                        var li = new List<int>();
                        while (!sr.EndOfStream)
                        {
                            int i;
                            if (int.TryParse(sr.ReadLine(), out i))
                            {
                                li.Add(i);
                            }
                            else
                            {
                                break;
                            }
                        }
                        sr.Close();
                        for (int i = 0; i < lvwGenes.Items.Count; i++)
                        {
                            if (li.Contains(i))
                            {
                                lvwGenes.Items[i].BackColor = Color.DodgerBlue;
                                lvwGenes.Items[i].ForeColor = Color.White;
                                lvwGenes.Items[i].Selected = true;
                            }
                            else
                            {
                                lvwGenes.Items[i].BackColor = Color.White;
                                lvwGenes.Items[i].ForeColor = Color.Black;
                                lvwGenes.Items[i].Selected = false;
                            }
                        }
                    }
                }
            }
        }

        private void BtnSaveGeneSelectionClick(System.Object sender, System.EventArgs e)
        {
            using (var sfd = new SaveFileDialog())
            {
                sfd.Title = "Save gene selection to file";
                sfd.FileName = "Gene Selection";
                sfd.Filter = "ATA Gene Selection (*.ata)|*.ata|All Files (*.*)|*.*";
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    using (var sw = new System.IO.StreamWriter(sfd.FileName))
                    {
                        foreach (int i in lvwGenes.SelectedIndices)
                        {
                            sw.WriteLine(i);
                        }
                        sw.Close();
                    }
                    tslStatus.Text = "Gene Selection Saved - '" + sfd.FileName + "'";
                }
            }
        }

        private void BtnLoadSampleSelectionClick(System.Object sender, System.EventArgs e)
        {
            using (var ofd = new OpenFileDialog())
            {
                ofd.Title = "Load sample selection from file";
                ofd.FileName = "Sample Selection.ata";
                ofd.Filter = "ATA Sample Selection (*.ata)|*.ata|All Files (*.*)|*.*";
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    using (var sr = new System.IO.StreamReader(ofd.FileName))
                    {
                        var li = new List<int>();
                        while (!sr.EndOfStream)
                        {
                            int i;
                            if (int.TryParse(sr.ReadLine(), out i))
                            {
                                li.Add(i);
                            }
                            else
                            {
                                break;
                            }
                        }
                        sr.Close();
                        for (int i = 0; i < lvwSamples.Items.Count; i++)
                        {
                            if (li.Contains(i))
                            {
                                lvwSamples.Items[i].BackColor = Color.DodgerBlue;
                                lvwSamples.Items[i].ForeColor = Color.White;
                                lvwSamples.Items[i].Selected = true;
                            }
                            else
                            {
                                lvwSamples.Items[i].BackColor = Color.White;
                                lvwSamples.Items[i].ForeColor = Color.Black;
                                lvwSamples.Items[i].Selected = false;
                            }
                        }
                    }
                }
            }
        }

        private void BtnSaveSampleSelectionClick(System.Object sender, System.EventArgs e)
        {
            using (var sfd = new SaveFileDialog())
            {
                sfd.Title = "Save sample selection to file";
                sfd.FileName = "Sample Selection";
                sfd.Filter = "ATA Sample Selection (*.ata)|*.ata|All Files (*.*)|*.*";
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    using (var sw = new System.IO.StreamWriter(sfd.FileName))
                    {
                        foreach (int i in lvwSamples.SelectedIndices)
                        {
                            sw.WriteLine(i);
                        }
                        sw.Close();
                    }
                    tslStatus.Text = "Sample Selection Saved - '" + sfd.FileName + "'";
                }
            }
        }

        private void ChkAmbiguousCheckedChanged(System.Object sender, System.EventArgs e)
        {
            bool b = chkAmbiguous.Checked;
            lblAmbiguous.Enabled = b;
            btnAmbiguous.Enabled = b;
            _saip.ShowAmbiguous = b;
            _saip.Ambiguous = btnAmbiguous.BackColor;
        }

    }

    public class SpotArrayImageParameters
    {
        public Color Absent { get; set; }
        public Color Present { get; set; }
        public Color Ambiguous { get; set; }
        public bool ShowAmbiguous { get; set; }
        public Font HeaderFont { get; set; }
        public string SavePath { get; set; }
        public ImageFormat ImgFormat { get; set; }
        private List<int> _geneIndices;
        private List<int> _sampleIndices;
        public bool ShowSignal { get; set; }
        public Font SignalFont { get; set; }

        public List<int> Genes
        {
            get { return _geneIndices; }
        }
        public void SetGenes(List<int> geneList)
        {
            _geneIndices = geneList;
        }
        public List<int> Samples
        {
            get { return _sampleIndices; }
        }
        public void SetSamples(List<int> sampleList)
        {
            _sampleIndices = sampleList;
        }

        public SpotArrayImageParameters()
        {
            Absent = Color.Red;
            Present = Color.Green;
            Ambiguous = Color.Yellow;
            ShowAmbiguous = false;
            HeaderFont = new Font(FontFamily.GenericSansSerif, 8, FontStyle.Bold);
            SignalFont = new Font(FontFamily.GenericSansSerif, 8);
            SavePath = "";
            ImgFormat = ImageFormat.Jpeg;
            ShowSignal = false;
        }

        public SpotArrayImageParameters(Color cAbsent, Color cPresent, Color cAmbiguous, bool bShowAmbiguous, Font fHeaderFont, string sSavePath)
        {
            Absent = cAbsent;
            Present = cPresent;
            Ambiguous = cAmbiguous;
            ShowAmbiguous = bShowAmbiguous;
            HeaderFont = fHeaderFont;
            SavePath = sSavePath;
        }
    }

}
