﻿using System;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;

namespace ArrayTubeAnalyzer
{
    public partial class FrmSpotPreview : Form
    {
        private SpotMarkerArrayIndices _indices;
        private bool _chkDiscardTriggerCheckedEvent;
        private readonly SpotImageCollection _spotImages;
        private readonly SampleData _sd;

        public FrmSpotPreview(SampleData sampleData, Point location, SpotMarkerArrayIndices spotMarkerArrayIndices, SpotImageCollection spotImages)
        {
            // This call is required by the designer.
            InitializeComponent();
            _sd = sampleData;
            _indices = spotMarkerArrayIndices;
            _spotImages = spotImages;
            // Add any initialization after the InitializeComponent() call.
            Location = location;
            cbxGeneCall.Items.AddRange(new string[]{
			"Auto",
			"Present",
			"Absent",
			"Ambiguous"
		});
            CreateSpotImages();
            Text = _sd.SampleNames[_indices.Array] + " - " + _sd.Markers[_indices.Marker];
        }

        public override sealed string Text
        {
            get { return base.Text; }
            set { base.Text = value; }
        }

        private void FrmSpotPreviewFormClosing(object sender, FormClosingEventArgs e)
        {
            _sd.Forms.SpotPreviewWindowClosed(this);
        }

        private void CbxGeneCallSelectedIndexChanged(object sender, EventArgs e)
        {
            int index = cbxGeneCall.SelectedIndex;
            Calls markerCall = default(Calls);
            switch (index)
            {
                case 0:
                    MarkerThresholdInfo thresholdInfo = _sd.Thresholds.Markers[_indices.Marker];
                    double finalAverageSignal = _sd.SampleStats[_indices.Array][_indices.Marker].FinalAverageSignal;
                    if (finalAverageSignal >= thresholdInfo.UpperThreshold)
                    {
                        markerCall = Calls.PresentAuto;
                    }
                    else if (finalAverageSignal >= thresholdInfo.Threshold)
                    {
                        markerCall = Calls.AmbiguousPresent;
                    }
                    else if (finalAverageSignal >= thresholdInfo.LowerThreshold)
                    {
                        markerCall = Calls.AmbiguousAbsent;
                    }
                    else
                    {
                        markerCall = Calls.AbsentAuto;
                    }
                    break;
                case 1:
                    markerCall = Calls.Present;
                    break;
                case 2:
                    markerCall = Calls.Absent;
                    break;
                case 3:
                    markerCall = Calls.AmbiguousManual;
                    break;
            }
            _sd.SampleCalls[_indices.Array].Calls[_indices.Marker] = markerCall;
            ClearPictures();
            CreateSpotImages();

            UpdateForms();
        }

        private void ChkDiscardMarkerCheckedChanged(object sender, EventArgs e)
        {
            if (_chkDiscardTriggerCheckedEvent)
            {
                _sd.DiscardMarker(_indices.Marker, chkDiscardMarker.Checked);
                UpdateForms();
            }
        }

        private void ChkDiscardCheckedChanged(object sender, EventArgs e)
        {
            if (_chkDiscardTriggerCheckedEvent)
            {
                Calls markerCall = _sd.SampleCalls[_indices.Array].Calls[_indices.Marker];
                if (chkDiscardReplicate.Checked)
                {
                    _sd.SampleStats[_indices.Array][_indices.Marker].Flag[_indices.Spot] = Flagging.Discard;
                }
                else
                {
                    _sd.SampleStats[_indices.Array][_indices.Marker].Flag[_indices.Spot] = Flagging.KeepManual;
                }

                _sd.RecalculateFinalAverageSignal(_indices);

                if (markerCall != Calls.Absent | markerCall != Calls.Present | markerCall != Calls.AmbiguousManual)
                {
                    Calls newMarkerCall;

                    MarkerThresholdInfo thresholdInfo = _sd.Thresholds.Markers[_indices.Marker];
                    double finalAverageSignal = _sd.SampleStats[_indices.Array][_indices.Marker].FinalAverageSignal;
                    if (finalAverageSignal >= thresholdInfo.UpperThreshold)
                    {
                        newMarkerCall = Calls.PresentAuto;
                    }
                    else if (finalAverageSignal >= thresholdInfo.Threshold)
                    {
                        newMarkerCall = Calls.AmbiguousPresent;
                    }
                    else if (finalAverageSignal >= thresholdInfo.LowerThreshold)
                    {
                        newMarkerCall = Calls.AmbiguousAbsent;
                    }
                    else
                    {
                        newMarkerCall = Calls.AbsentAuto;
                    }

                    if (markerCall != newMarkerCall)
                    {
                        _sd.SampleCalls[_indices.Array].Calls[_indices.Marker] = newMarkerCall;
                    }
                }

                UpdateForms();
                ClearPictures();
                CreateSpotImages();
            }
        }

        private void ClearPictures()
        {
            bool bDisposed = true;
            while (bDisposed)
            {
                bDisposed = false;
                foreach (Control c in Controls)
                {
                    c.Dispose();
                    bDisposed = true;
                }
            }

        }

        private void ContextMenuStrip1Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            var p = (PictureBox)ContextMenuStrip1.SourceControl;
            _indices = (SpotMarkerArrayIndices)p.Tag;
            _chkDiscardTriggerCheckedEvent = false;
            chkDiscardReplicate.Checked = (_sd.SampleStats[_indices.Array][_indices.Marker].Flag[_indices.Spot] == Flagging.Discard);
            chkDiscardMarker.Checked = _sd.DiscardedMarkers[_indices.Marker];
            _chkDiscardTriggerCheckedEvent = true;
        }

        private void CreateSpotImages()
        {
            int iX = 0;
            foreach (SpotImageCrop s in _spotImages.SpotImages)
            {
                if (s.Array == _indices.Array)
                {
                    MarkerInfo at = _sd.SampleStats[_indices.Array][_indices.Marker];
                    var p = new PictureBox
                                {
                                    Parent = this,
                                    BorderStyle = BorderStyle.FixedSingle,
                                    Location = new Point(iX, 0),
                                    Size = new Size(80, 80)
                                };
                    _indices = new SpotMarkerArrayIndices(s.Gene, _sd.Spots[s.Gene].IndexOf(s.Spot), s.Array);
                    p.Tag = _indices;
                    p.ContextMenuStrip = ContextMenuStrip1;
                    var bmp = new Bitmap(80, 80);
                    Graphics g = Graphics.FromImage(bmp);
                    var f = new Font(FontFamily.GenericSansSerif, 8);
                    Brush b = Brushes.Black;
                    Color colorBorder = Color.Red;
                    switch (_sd.SampleCalls[_indices.Array].Calls[_indices.Marker])
                    {
                        case Calls.Present:
                            colorBorder = _sd.Settings.C.PresentColour;
                            break;
                        case Calls.Absent:
                            colorBorder = _sd.Settings.C.AbsentColour;
                            break;
                        case Calls.AmbiguousAbsent:
                        case Calls.AmbiguousManual:
                            colorBorder = _sd.Settings.C.AbsentFlaggedColour;
                            break;
                        case Calls.AbsentAuto:
                            colorBorder = _sd.Settings.C.AbsentAutoColour;
                            break;
                        case Calls.AmbiguousPresent:
                            colorBorder = _sd.Settings.C.PresentFlaggedColour;
                            break;
                        case Calls.PresentAuto:
                            colorBorder = _sd.Settings.C.PresentAutoColour;
                            break;
                    }
                    g.FillRectangle(new SolidBrush(colorBorder), 0, 0, 80, 80);
                    g.DrawImage(s.ImageAT, 1, 1, 78, 78);
                    g.DrawString(s.Spot.ToString(CultureInfo.InvariantCulture), f, b, new PointF(1, 1));


                    if (at.Flag[_indices.Spot] == Flagging.Discard)
                    {
                        var topRight = new PointF(79 - g.MeasureString("Discarded", f).Width, 1);
                        g.DrawString("Discarded", f, Brushes.Red, topRight);
                    }

                    var pBottomLeft = new PointF(1, 79 - g.MeasureString(s.Signal.ToString(CultureInfo.InvariantCulture), f).Height);
                    g.DrawString(s.Signal.ToString(CultureInfo.InvariantCulture), f, b, pBottomLeft);

                    p.Image = bmp;
                    p.SizeMode = PictureBoxSizeMode.StretchImage;

                    string sToolTipText = string.Format("Array: {0}\nMarker: {1}\nSpot: {2}\nSignal: {3}\nAverage Signal: {4}\nFinal Average Signal: {5}\nFlagging: {6}",
                        _sd.SampleNames[s.Array],
                        _sd.Markers[s.Gene],
                        s.Spot,
                        at.Signal[_indices.Spot].ToString("0.0000"),
                        at.AverageSignal.ToString("0.0000"),
                        at.FinalAverageSignal.ToString("0.0000"),
                        at.Flag[_indices.Spot]);
                    ToolTip1.SetToolTip(p, sToolTipText);
                    p.Show();
                    iX += 80;
                }
            }

            var rtb = new RichTextBox
                          {
                              Parent = this,
                              Multiline = true,
                              BorderStyle = BorderStyle.None,
                              Location = new Point(0, 80),
                              Size = new Size(iX, 80),
                              Text = GetRichTextBoxText()
                          };

            rtb.TextChanged += RtbTextChanged;
            rtb.KeyPress += RtbKeyPress;
            rtb.Show();

            Size = new Size(iX + _sd.Layout.Rows - 1, 160 + 35);
        }

        private string GetRichTextBoxText()
        {
            MarkerInfo ati = _sd.SampleStats[_indices.Array][_indices.Marker];

            Calls markerCall = _sd.SampleCalls[_indices.Array].Calls[_indices.Marker];
            double finalAverageSignal = ati.FinalAverageSignal;
            MarkerThresholdInfo thresholdInfo = _sd.Thresholds.Markers[_indices.Marker];
            if (markerCall == Calls.PresentAuto | markerCall == Calls.AbsentAuto | markerCall == Calls.AmbiguousAbsent | markerCall == Calls.AmbiguousPresent)
            {
                if (finalAverageSignal >= thresholdInfo.UpperThreshold)
                {
                    markerCall = Calls.PresentAuto;
                }
                else if (finalAverageSignal >= thresholdInfo.Threshold)
                {
                    markerCall = Calls.AmbiguousPresent;
                }
                else if (finalAverageSignal >= thresholdInfo.LowerThreshold)
                {
                    markerCall = Calls.AmbiguousAbsent;
                }
                else
                {
                    markerCall = Calls.AbsentAuto;
                }
            }
            return string.Format("Array: {0}\nMarker: {1}\nAverage Signal: {2}\nFinal Average Signal: {3}\nGene Call: {4}",
                _sd.SampleNames[_indices.Array],
                _sd.Markers[_indices.Marker],
                ati.AverageSignal.ToString("0.0000"),
                ati.FinalAverageSignal.ToString("0.0000"),
                markerCall);
        }

        private void RtbTextChanged(object sender, EventArgs e)
        {
            var rtb = sender as RichTextBox;
            if (rtb != null) rtb.Text = GetRichTextBoxText();
        }

        private static void RtbKeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void UpdateForms()
        {
            _sd.UpdateCalls();
            _sd.ClusterUPGMA();
            _sd.Forms.SampleQC.UpdatePicAndListview(0);

            if (_sd.Forms.MarkerThresholdAnalysis != null)
            {
                _sd.Forms.MarkerThresholdAnalysis.UpdateForm();
            }
            if (_sd.Forms.MarkerThresholdReview != null)
            {
                _sd.Forms.MarkerThresholdReview.UpdateForm();
            }
            if (_sd.Forms.ReviewMarkerCalls != null)
            {
                _sd.Forms.ReviewMarkerCalls.UpdateForm(_indices.Array);
            }
            if (_sd.Forms.Clustering != null)
            {
                _sd.Forms.Clustering.UpdateForm();
            }
            if (_sd.Forms.Diagnosis != null)
            {
                _sd.Forms.Diagnosis.UpdateListview();
            }
        }

    }
}
