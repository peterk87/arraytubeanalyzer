﻿using System;

namespace ArrayTubeAnalyzer
{

    /// <summary>
    /// Calibration information for spot highlighting grid overlay. 
    /// </summary>
    /// <remarks></remarks>
    [Serializable]
    public class CalibrationInfo
    {
        /// <summary>Top left hand calibrator "Top" value.</summary>
        public double TopLeftY;
        /// <summary>Top left hand calibrator "Left" value.</summary>
        public double TopLeftX;
        /// <summary>Top right hand calibrator "Top" value.</summary>
        public double TopRightY;
        /// <summary>Top right hand calibrator "Left" value.</summary>
        public double TopRightX;
        /// <summary>Bottom right hand calibrator "Top" value.</summary>
        public double BottomRightY;
        /// <summary>Bottom right hand calibrator "Left" value.</summary>
        public double BottomRightX;
        /// <summary>Bottom left hand calibrator "Top" value.</summary>
        public double BottomLeftY;
        /// <summary>Bottom left hand calibrator "Left" value.</summary>
        public double BottomLeftX;

        /// <summary>Default values for calibration (-1)</summary>
        public CalibrationInfo()
        {
            TopLeftY = -1;
            TopLeftX = -1;
            TopRightY = -1;
            TopRightX = -1;
            BottomRightY = -1;
            BottomRightX = -1;
            BottomLeftY = -1;
            BottomLeftX = -1;
        }
        public CalibrationInfo(double topLeftY, double topLeftX, double topRightY, double topRightX, double bottomRightY, double bottomRightX, double bottomLeftY, double bottomLeftX)
        {
            TopLeftY = topLeftY;
            TopLeftX = topLeftX;
            TopRightY = topRightY;
            TopRightX = topRightX;
            BottomLeftX = bottomLeftX;
            BottomLeftY = bottomLeftY;
            BottomRightX = bottomRightX;
            BottomRightY = bottomRightY;

        }
    }
}
