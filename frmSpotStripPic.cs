﻿using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;

namespace ArrayTubeAnalyzer
{
    public partial class FrmSpotStripPic : Form
    {
        private readonly SpotImageCollection _sic;
        private readonly SampleData _sd;
        const int SizeXy = 85;

        public FrmSpotStripPic(object objSampleData, List<List<MarkerInfo>> lsInfo, List<Image> lImage, List<CalibrationInfo> lCalibration, List<List<int>> lspots)
        {
            _sd = objSampleData as SampleData;
            InitializeComponent();
            _sic = new SpotImageCollection(_sd, lsInfo, lImage, lCalibration, lspots);
            _sic.SortBySignal();
        }

        private void BtnBrowseClick(System.Object sender, System.EventArgs e)
        {
            using (var sfd = new SaveFileDialog())
            {
                sfd.Title = "Select save path for spot strip picture";
                sfd.FileName = "SpotStripPicture.jpeg";
                sfd.Filter = "JPEG (*.jpeg)|*.jpeg|All Files (*.*)|*.*";
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    txtSave.Text = sfd.FileName;
                    btnSave.Enabled = true;
                }
            }
        }

        private void BtnSaveClick(System.Object sender, System.EventArgs e)
        {
            CreatePictures();
        }

        private void CreatePictures()
        {
            if (_sic == null) return;
            int iWidth;
            int iHeight;
            if (_sic.SpotImages.Count < 300)
            {
                iWidth = _sic.SpotImages.Count * SizeXy;
                iHeight = SizeXy;
            }
            else
            {
                iWidth = 300 * SizeXy;
                iHeight = (_sic.SpotImages.Count / 300) * SizeXy;
            }

            var bm = new Bitmap(iWidth, iHeight);
            Graphics g = Graphics.FromImage(bm);
            g.FillRectangle(Brushes.White, new Rectangle(0, 0, iWidth, iHeight));
            int x = 0;
            CreateSpotPictures(ref x, ref g);

            bm.Save(txtSave.Text, ImageFormat.Jpeg);
            bm.Dispose();
        }

        private void CreateSpotPictures(ref int iX, ref Graphics g)
        {
            const int minus5 = SizeXy - 5;
            const int minus1 = SizeXy - 1;
            var font = new Font(FontFamily.GenericSansSerif, 8);
            dynamic count = 1;
            int iY = 0;
            foreach (SpotImageCrop s in _sic.SpotImages)
            {
                var rf = new Rectangle(iX, iY, minus1, minus1);
                var colorSpotBorder = new Pen(Color.Red);
                Brush colorFillSpotRectangle = new SolidBrush(Color.Red);
                Calls mCall = _sd.SampleCalls[s.Array].Calls[s.Gene];
                switch (mCall)
                {
                    case Calls.AmbiguousAbsent:
                    case Calls.AmbiguousManual:
                    case Calls.AmbiguousPresent:
                        colorSpotBorder = new Pen(Color.Yellow);
                        colorFillSpotRectangle = new SolidBrush(Color.Yellow);
                        break;
                    case Calls.Absent:
                    case Calls.AbsentAuto:
                        colorSpotBorder = new Pen(Color.Red);
                        colorFillSpotRectangle = new SolidBrush(Color.Red);
                        break;
                    case Calls.Present:
                    case Calls.PresentAuto:
                        colorSpotBorder = new Pen(Color.Green);
                        colorFillSpotRectangle = new SolidBrush(Color.Green);
                        break;
                }
                g.DrawRectangle(colorSpotBorder, rf);
                g.FillRectangle(colorFillSpotRectangle, rf);
                g.DrawImage(s.ImageAT, iX + 2, iY + 2, minus5, minus5);
                string sSignal = s.Signal.ToString("0.0000");
                SizeF sizeSignal = g.MeasureString(sSignal, font);
                g.DrawString(s.Signal.ToString("0.0000"), font, Brushes.Black, iX + 2, iY + 2 + minus5 - sizeSignal.Height);
                if (count == 300)
                {
                    iX = 0;
                    iY += SizeXy;
                    count = 1;
                }
                else
                {
                    iX += SizeXy;
                    count += 1;
                }
            }
        }

    }
}
