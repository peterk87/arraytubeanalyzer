﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ArrayTubeAnalyzer
{
    class ListViewFF : ListView
    {
        public ListViewFF()
        {
            DoubleBuffered = true;
            View = View.Details;
            FullRowSelect = true;
            GridLines = true;
        }
    }
}
