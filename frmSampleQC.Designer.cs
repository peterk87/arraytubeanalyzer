﻿namespace ArrayTubeAnalyzer
{
    partial class FrmSampleQC
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSampleQC));
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.tslStatus = new System.Windows.Forms.StatusStrip();
            this.lblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.ProgressBar1 = new System.Windows.Forms.ToolStripProgressBar();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.lvwArrays = new ArrayTubeAnalyzer.ListViewFF();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ContextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.changeArrayTubeSampleNamesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showMarkerStripToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnSaveSpotStripPic = new System.Windows.Forms.ToolStripMenuItem();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.lblArrayName = new System.Windows.Forms.GroupBox();
            this.lblBottomRight = new ArrayTubeAnalyzer.LabelWithColours();
            this.lblBottomLeft = new ArrayTubeAnalyzer.LabelWithColours();
            this.lblTopRight = new ArrayTubeAnalyzer.LabelWithColours();
            this.lblTopLeft = new ArrayTubeAnalyzer.LabelWithColours();
            this.tbrContrast = new System.Windows.Forms.TrackBar();
            this.pbx = new System.Windows.Forms.PictureBox();
            this.splitContainer4 = new System.Windows.Forms.SplitContainer();
            this.gbxSettings = new System.Windows.Forms.GroupBox();
            this.chkCalibrate = new System.Windows.Forms.CheckBox();
            this.chkAdjustContrast = new System.Windows.Forms.CheckBox();
            this.chkSpotNumbers = new System.Windows.Forms.CheckBox();
            this.chkShowGrid = new System.Windows.Forms.CheckBox();
            this.pbxReplicates = new System.Windows.Forms.PictureBox();
            this.lvwATinfo = new ArrayTubeAnalyzer.ListViewFF();
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader11 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader12 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.ddbFile = new System.Windows.Forms.ToolStripDropDownButton();
            this.newATAnalysisFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnOpenZippedAnalysis = new System.Windows.Forms.ToolStripMenuItem();
            this.btnSaveZippedAnalysis = new System.Windows.Forms.ToolStripMenuItem();
            this.btnOpenATAnalysisFile = new System.Windows.Forms.ToolStripMenuItem();
            this.btnSaveMeta = new System.Windows.Forms.ToolStripMenuItem();
            this.btnSaveAsMeta = new System.Windows.Forms.ToolStripMenuItem();
            this.btnExportSignalData = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnExit = new System.Windows.Forms.ToolStripMenuItem();
            this.ddbView = new System.Windows.Forms.ToolStripDropDownButton();
            this.arrayStatisticsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.markerThresholdAnalysisToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.markerThresholdReviewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reviewMarkerCallsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clusteringToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.diagnosticsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ddbSettings = new System.Windows.Forms.ToolStripDropDownButton();
            this.changeColourProfileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.arrayLayoutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.numberOfRowsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.txtNumRows = new System.Windows.Forms.ToolStripTextBox();
            this.numberOfColumnsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.txtNumColumns = new System.Windows.Forms.ToolStripTextBox();
            this.applyNewGridToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.spotImageSizeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.widthToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.txtSpotX = new System.Windows.Forms.ToolStripTextBox();
            this.heightToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.txtSpotY = new System.Windows.Forms.ToolStripTextBox();
            this.xLocationOffsetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.txtXOffset = new System.Windows.Forms.ToolStripTextBox();
            this.yLocationOffsetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.txtYOffset = new System.Windows.Forms.ToolStripTextBox();
            this.ddbSamples = new System.Windows.Forms.ToolStripDropDownButton();
            this.btnAddSample = new System.Windows.Forms.ToolStripMenuItem();
            this.btnRemoveSelectedSample = new System.Windows.Forms.ToolStripMenuItem();
            this.removeMultipleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnFinalizeSample = new System.Windows.Forms.ToolStripMenuItem();
            this.btnFinalizeAllSamples = new System.Windows.Forms.ToolStripMenuItem();
            this.ddbFlagging = new System.Windows.Forms.ToolStripDropDownButton();
            this.sDThresholdLowSignalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.txtSDThresholdLow = new System.Windows.Forms.ToolStripTextBox();
            this.sDThresholdHighSignalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.txtSDThresholdHigh = new System.Windows.Forms.ToolStripTextBox();
            this.highSignalThresholdToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.txtHighSignalThreshold = new System.Windows.Forms.ToolStripTextBox();
            this.globalErrorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.globalErrorSDToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.globalErrorThresholdLowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.globalErrorThresholdHighToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.resetFlaggingForSelectedSampleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resetFlaggingForEntireDatasetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnMarkerThresholdAnalysis = new System.Windows.Forms.ToolStripButton();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.toolStripContainer1.BottomToolStripPanel.SuspendLayout();
            this.toolStripContainer1.ContentPanel.SuspendLayout();
            this.toolStripContainer1.TopToolStripPanel.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.tslStatus.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.ContextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            this.lblArrayName.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbrContrast)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).BeginInit();
            this.splitContainer4.Panel1.SuspendLayout();
            this.splitContainer4.Panel2.SuspendLayout();
            this.splitContainer4.SuspendLayout();
            this.gbxSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxReplicates)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.BottomToolStripPanel
            // 
            this.toolStripContainer1.BottomToolStripPanel.Controls.Add(this.tslStatus);
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.Controls.Add(this.splitContainer1);
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(980, 561);
            this.toolStripContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripContainer1.Location = new System.Drawing.Point(0, 0);
            this.toolStripContainer1.Name = "toolStripContainer1";
            this.toolStripContainer1.Size = new System.Drawing.Size(980, 608);
            this.toolStripContainer1.TabIndex = 0;
            this.toolStripContainer1.Text = "toolStripContainer1";
            // 
            // toolStripContainer1.TopToolStripPanel
            // 
            this.toolStripContainer1.TopToolStripPanel.Controls.Add(this.toolStrip1);
            // 
            // tslStatus
            // 
            this.tslStatus.Dock = System.Windows.Forms.DockStyle.None;
            this.tslStatus.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblStatus,
            this.ProgressBar1});
            this.tslStatus.Location = new System.Drawing.Point(0, 0);
            this.tslStatus.Name = "tslStatus";
            this.tslStatus.Size = new System.Drawing.Size(980, 22);
            this.tslStatus.TabIndex = 0;
            // 
            // lblStatus
            // 
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(39, 17);
            this.lblStatus.Text = "Ready";
            // 
            // ProgressBar1
            // 
            this.ProgressBar1.Name = "ProgressBar1";
            this.ProgressBar1.Size = new System.Drawing.Size(100, 16);
            this.ProgressBar1.Step = 1;
            this.ProgressBar1.Visible = false;
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.lvwArrays);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(980, 561);
            this.splitContainer1.SplitterDistance = 196;
            this.splitContainer1.TabIndex = 0;
            // 
            // lvwArrays
            // 
            this.lvwArrays.CheckBoxes = true;
            this.lvwArrays.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4});
            this.lvwArrays.ContextMenuStrip = this.ContextMenuStrip1;
            this.lvwArrays.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvwArrays.FullRowSelect = true;
            this.lvwArrays.GridLines = true;
            this.lvwArrays.HideSelection = false;
            this.lvwArrays.Location = new System.Drawing.Point(0, 0);
            this.lvwArrays.Name = "lvwArrays";
            this.lvwArrays.ShowItemToolTips = true;
            this.lvwArrays.Size = new System.Drawing.Size(192, 557);
            this.lvwArrays.TabIndex = 0;
            this.lvwArrays.UseCompatibleStateImageBehavior = false;
            this.lvwArrays.View = System.Windows.Forms.View.Details;
            this.lvwArrays.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.OnArraysItemChecked);
            this.lvwArrays.SelectedIndexChanged += new System.EventHandler(this.OnArraysSelectedIndexChanged);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Sample";
            this.columnHeader1.Width = 56;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Calibrated";
            this.columnHeader2.Width = 40;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Finalized";
            this.columnHeader3.Width = 44;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Flagged";
            this.columnHeader4.Width = 44;
            // 
            // ContextMenuStrip1
            // 
            this.ContextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.changeArrayTubeSampleNamesToolStripMenuItem,
            this.showMarkerStripToolStripMenuItem,
            this.btnSaveSpotStripPic});
            this.ContextMenuStrip1.Name = "contextMenuArrayInfo";
            this.ContextMenuStrip1.Size = new System.Drawing.Size(259, 70);
            this.ContextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.OnContextMenuOpening);
            // 
            // changeArrayTubeSampleNamesToolStripMenuItem
            // 
            this.changeArrayTubeSampleNamesToolStripMenuItem.Name = "changeArrayTubeSampleNamesToolStripMenuItem";
            this.changeArrayTubeSampleNamesToolStripMenuItem.Size = new System.Drawing.Size(258, 22);
            this.changeArrayTubeSampleNamesToolStripMenuItem.Text = "Change Array Tube Sample Names";
            // 
            // showMarkerStripToolStripMenuItem
            // 
            this.showMarkerStripToolStripMenuItem.Name = "showMarkerStripToolStripMenuItem";
            this.showMarkerStripToolStripMenuItem.Size = new System.Drawing.Size(258, 22);
            this.showMarkerStripToolStripMenuItem.Text = "Show Marker Strip";
            this.showMarkerStripToolStripMenuItem.Click += new System.EventHandler(this.OnShowMarkerStrip);
            // 
            // btnSaveSpotStripPic
            // 
            this.btnSaveSpotStripPic.Name = "btnSaveSpotStripPic";
            this.btnSaveSpotStripPic.Size = new System.Drawing.Size(258, 22);
            this.btnSaveSpotStripPic.Text = "Save Spot Strip Pic";
            // 
            // splitContainer2
            // 
            this.splitContainer2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.splitContainer3);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.lvwATinfo);
            this.splitContainer2.Size = new System.Drawing.Size(780, 561);
            this.splitContainer2.SplitterDistance = 410;
            this.splitContainer2.TabIndex = 0;
            // 
            // splitContainer3
            // 
            this.splitContainer3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.Location = new System.Drawing.Point(0, 0);
            this.splitContainer3.Name = "splitContainer3";
            this.splitContainer3.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.lblArrayName);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.splitContainer4);
            this.splitContainer3.Size = new System.Drawing.Size(410, 561);
            this.splitContainer3.SplitterDistance = 432;
            this.splitContainer3.TabIndex = 0;
            // 
            // lblArrayName
            // 
            this.lblArrayName.Controls.Add(this.lblBottomRight);
            this.lblArrayName.Controls.Add(this.lblBottomLeft);
            this.lblArrayName.Controls.Add(this.lblTopRight);
            this.lblArrayName.Controls.Add(this.lblTopLeft);
            this.lblArrayName.Controls.Add(this.tbrContrast);
            this.lblArrayName.Controls.Add(this.pbx);
            this.lblArrayName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblArrayName.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblArrayName.Location = new System.Drawing.Point(0, 0);
            this.lblArrayName.Name = "lblArrayName";
            this.lblArrayName.Size = new System.Drawing.Size(406, 428);
            this.lblArrayName.TabIndex = 0;
            this.lblArrayName.TabStop = false;
            this.lblArrayName.Text = "Array Name";
            // 
            // lblBottomRight
            // 
            this.lblBottomRight.BackColor = System.Drawing.Color.Transparent;
            this.lblBottomRight.BorderColor = System.Drawing.Color.Fuchsia;
            this.lblBottomRight.BorderEnabled = false;
            this.lblBottomRight.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Inset;
            this.lblBottomRight.BorderWidth = 2;
            this.lblBottomRight.Enabled = false;
            this.lblBottomRight.Location = new System.Drawing.Point(328, 298);
            this.lblBottomRight.Name = "lblBottomRight";
            this.lblBottomRight.OldHeight = 0D;
            this.lblBottomRight.OldLeft = 0D;
            this.lblBottomRight.OldTop = 0D;
            this.lblBottomRight.OldWidth = 0D;
            this.lblBottomRight.Size = new System.Drawing.Size(20, 20);
            this.lblBottomRight.TabIndex = 5;
            this.lblBottomRight.Visible = false;
            this.lblBottomRight.MouseMove += new System.Windows.Forms.MouseEventHandler(this.LblBottomRightMouseMove);
            // 
            // lblBottomLeft
            // 
            this.lblBottomLeft.BackColor = System.Drawing.Color.Transparent;
            this.lblBottomLeft.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.lblBottomLeft.BorderEnabled = false;
            this.lblBottomLeft.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Inset;
            this.lblBottomLeft.BorderWidth = 2;
            this.lblBottomLeft.Location = new System.Drawing.Point(20, 322);
            this.lblBottomLeft.Name = "lblBottomLeft";
            this.lblBottomLeft.OldHeight = 0D;
            this.lblBottomLeft.OldLeft = 0D;
            this.lblBottomLeft.OldTop = 0D;
            this.lblBottomLeft.OldWidth = 0D;
            this.lblBottomLeft.Size = new System.Drawing.Size(20, 20);
            this.lblBottomLeft.TabIndex = 4;
            this.lblBottomLeft.Visible = false;
            this.lblBottomLeft.MouseMove += new System.Windows.Forms.MouseEventHandler(this.LblBottomLeftMouseMove);
            // 
            // lblTopRight
            // 
            this.lblTopRight.BackColor = System.Drawing.Color.Transparent;
            this.lblTopRight.BorderColor = System.Drawing.Color.Lime;
            this.lblTopRight.BorderEnabled = false;
            this.lblTopRight.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Inset;
            this.lblTopRight.BorderWidth = 2;
            this.lblTopRight.Location = new System.Drawing.Point(328, 55);
            this.lblTopRight.Name = "lblTopRight";
            this.lblTopRight.OldHeight = 0D;
            this.lblTopRight.OldLeft = 0D;
            this.lblTopRight.OldTop = 0D;
            this.lblTopRight.OldWidth = 0D;
            this.lblTopRight.Size = new System.Drawing.Size(20, 20);
            this.lblTopRight.TabIndex = 3;
            this.lblTopRight.Visible = false;
            this.lblTopRight.MouseMove += new System.Windows.Forms.MouseEventHandler(this.LblTopRightMouseMove);
            // 
            // lblTopLeft
            // 
            this.lblTopLeft.BackColor = System.Drawing.Color.Transparent;
            this.lblTopLeft.BorderColor = System.Drawing.Color.Red;
            this.lblTopLeft.BorderEnabled = false;
            this.lblTopLeft.BorderStyle = System.Windows.Forms.ButtonBorderStyle.Inset;
            this.lblTopLeft.BorderWidth = 2;
            this.lblTopLeft.Location = new System.Drawing.Point(20, 55);
            this.lblTopLeft.Name = "lblTopLeft";
            this.lblTopLeft.OldHeight = 0D;
            this.lblTopLeft.OldLeft = 0D;
            this.lblTopLeft.OldTop = 0D;
            this.lblTopLeft.OldWidth = 0D;
            this.lblTopLeft.Size = new System.Drawing.Size(20, 20);
            this.lblTopLeft.TabIndex = 2;
            this.lblTopLeft.Visible = false;
            this.lblTopLeft.MouseMove += new System.Windows.Forms.MouseEventHandler(this.LblTopLeftMouseMove);
            // 
            // tbrContrast
            // 
            this.tbrContrast.AutoSize = false;
            this.tbrContrast.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tbrContrast.Location = new System.Drawing.Point(3, 400);
            this.tbrContrast.Maximum = 255;
            this.tbrContrast.Minimum = -100;
            this.tbrContrast.Name = "tbrContrast";
            this.tbrContrast.Size = new System.Drawing.Size(400, 25);
            this.tbrContrast.TabIndex = 1;
            this.tbrContrast.TickStyle = System.Windows.Forms.TickStyle.None;
            this.tbrContrast.Visible = false;
            this.tbrContrast.Scroll += new System.EventHandler(this.TbrContrastScroll);
            // 
            // pbx
            // 
            this.pbx.Cursor = System.Windows.Forms.Cursors.Cross;
            this.pbx.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbx.Location = new System.Drawing.Point(3, 25);
            this.pbx.Name = "pbx";
            this.pbx.Size = new System.Drawing.Size(400, 400);
            this.pbx.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbx.TabIndex = 0;
            this.pbx.TabStop = false;
            this.pbx.SizeChanged += new System.EventHandler(this.PbxSizeChanged);
            this.pbx.MouseUp += new System.Windows.Forms.MouseEventHandler(this.PbxMouseUp);
            // 
            // splitContainer4
            // 
            this.splitContainer4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer4.Location = new System.Drawing.Point(0, 0);
            this.splitContainer4.Name = "splitContainer4";
            // 
            // splitContainer4.Panel1
            // 
            this.splitContainer4.Panel1.Controls.Add(this.gbxSettings);
            // 
            // splitContainer4.Panel2
            // 
            this.splitContainer4.Panel2.Controls.Add(this.pbxReplicates);
            this.splitContainer4.Size = new System.Drawing.Size(410, 125);
            this.splitContainer4.SplitterDistance = 193;
            this.splitContainer4.TabIndex = 0;
            // 
            // gbxSettings
            // 
            this.gbxSettings.Controls.Add(this.chkCalibrate);
            this.gbxSettings.Controls.Add(this.chkAdjustContrast);
            this.gbxSettings.Controls.Add(this.chkSpotNumbers);
            this.gbxSettings.Controls.Add(this.chkShowGrid);
            this.gbxSettings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbxSettings.Location = new System.Drawing.Point(0, 0);
            this.gbxSettings.Name = "gbxSettings";
            this.gbxSettings.Size = new System.Drawing.Size(189, 121);
            this.gbxSettings.TabIndex = 0;
            this.gbxSettings.TabStop = false;
            this.gbxSettings.Text = "Settings";
            // 
            // chkCalibrate
            // 
            this.chkCalibrate.AutoSize = true;
            this.chkCalibrate.Location = new System.Drawing.Point(24, 95);
            this.chkCalibrate.Name = "chkCalibrate";
            this.chkCalibrate.Size = new System.Drawing.Size(153, 17);
            this.chkCalibrate.TabIndex = 3;
            this.chkCalibrate.Text = "Calibrate Spot Grid Overlay";
            this.chkCalibrate.UseVisualStyleBackColor = true;
            this.chkCalibrate.CheckedChanged += new System.EventHandler(this.ChkCalibrateCheckedChanged);
            // 
            // chkAdjustContrast
            // 
            this.chkAdjustContrast.AutoSize = true;
            this.chkAdjustContrast.Location = new System.Drawing.Point(24, 72);
            this.chkAdjustContrast.Name = "chkAdjustContrast";
            this.chkAdjustContrast.Size = new System.Drawing.Size(97, 17);
            this.chkAdjustContrast.TabIndex = 2;
            this.chkAdjustContrast.Text = "Adjust Contrast";
            this.chkAdjustContrast.UseVisualStyleBackColor = true;
            this.chkAdjustContrast.CheckedChanged += new System.EventHandler(this.ChkContrastCheckedChanged);
            // 
            // chkSpotNumbers
            // 
            this.chkSpotNumbers.AutoSize = true;
            this.chkSpotNumbers.Checked = true;
            this.chkSpotNumbers.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkSpotNumbers.Location = new System.Drawing.Point(24, 49);
            this.chkSpotNumbers.Name = "chkSpotNumbers";
            this.chkSpotNumbers.Size = new System.Drawing.Size(123, 17);
            this.chkSpotNumbers.TabIndex = 1;
            this.chkSpotNumbers.Text = "Show Spot Numbers";
            this.chkSpotNumbers.UseVisualStyleBackColor = true;
            this.chkSpotNumbers.CheckedChanged += new System.EventHandler(this.ChkSpotNumbersCheckedChanged);
            // 
            // chkShowGrid
            // 
            this.chkShowGrid.AutoSize = true;
            this.chkShowGrid.Location = new System.Drawing.Point(24, 26);
            this.chkShowGrid.Name = "chkShowGrid";
            this.chkShowGrid.Size = new System.Drawing.Size(75, 17);
            this.chkShowGrid.TabIndex = 0;
            this.chkShowGrid.Text = "Show Grid";
            this.chkShowGrid.UseVisualStyleBackColor = true;
            this.chkShowGrid.CheckedChanged += new System.EventHandler(this.ChkGridCheckedChanged);
            // 
            // pbxReplicates
            // 
            this.pbxReplicates.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbxReplicates.Location = new System.Drawing.Point(0, 0);
            this.pbxReplicates.Name = "pbxReplicates";
            this.pbxReplicates.Size = new System.Drawing.Size(209, 121);
            this.pbxReplicates.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxReplicates.TabIndex = 0;
            this.pbxReplicates.TabStop = false;
            // 
            // lvwATinfo
            // 
            this.lvwATinfo.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader5,
            this.columnHeader6,
            this.columnHeader7,
            this.columnHeader8,
            this.columnHeader9,
            this.columnHeader10,
            this.columnHeader11,
            this.columnHeader12});
            this.lvwATinfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvwATinfo.FullRowSelect = true;
            this.lvwATinfo.GridLines = true;
            this.lvwATinfo.HideSelection = false;
            this.lvwATinfo.Location = new System.Drawing.Point(0, 0);
            this.lvwATinfo.MultiSelect = false;
            this.lvwATinfo.Name = "lvwATinfo";
            this.lvwATinfo.ShowItemToolTips = true;
            this.lvwATinfo.Size = new System.Drawing.Size(362, 557);
            this.lvwATinfo.TabIndex = 0;
            this.lvwATinfo.UseCompatibleStateImageBehavior = false;
            this.lvwATinfo.View = System.Windows.Forms.View.Details;
            this.lvwATinfo.SelectedIndexChanged += new System.EventHandler(this.LvwATinfoSelectedIndexChanged);
            this.lvwATinfo.DoubleClick += new System.EventHandler(this.LvwATinfoDoubleClick);
            this.lvwATinfo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.LvwATinfoKeyPress);
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Spot";
            this.columnHeader5.Width = 35;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Marker";
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Replicate";
            this.columnHeader7.Width = 22;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Signal";
            this.columnHeader8.Width = 49;
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "%Error";
            this.columnHeader9.Width = 47;
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "Delta Signal";
            this.columnHeader10.Width = 48;
            // 
            // columnHeader11
            // 
            this.columnHeader11.Text = "Average Signal";
            this.columnHeader11.Width = 44;
            // 
            // columnHeader12
            // 
            this.columnHeader12.Text = "Flagging Status";
            this.columnHeader12.Width = 41;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ddbFile,
            this.ddbView,
            this.ddbSettings,
            this.ddbSamples,
            this.ddbFlagging,
            this.btnMarkerThresholdAnalysis});
            this.toolStrip1.Location = new System.Drawing.Point(3, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(532, 25);
            this.toolStrip1.TabIndex = 0;
            // 
            // ddbFile
            // 
            this.ddbFile.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.ddbFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newATAnalysisFileToolStripMenuItem,
            this.btnOpenZippedAnalysis,
            this.btnSaveZippedAnalysis,
            this.btnOpenATAnalysisFile,
            this.btnSaveMeta,
            this.btnSaveAsMeta,
            this.btnExportSignalData,
            this.aboutToolStripMenuItem,
            this.closeToolStripMenuItem,
            this.btnExit});
            this.ddbFile.Image = ((System.Drawing.Image)(resources.GetObject("ddbFile.Image")));
            this.ddbFile.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ddbFile.Name = "ddbFile";
            this.ddbFile.Size = new System.Drawing.Size(38, 22);
            this.ddbFile.Text = "File";
            // 
            // newATAnalysisFileToolStripMenuItem
            // 
            this.newATAnalysisFileToolStripMenuItem.Image = global::ArrayTubeAnalyzer.Properties.Resources.Document_Add;
            this.newATAnalysisFileToolStripMenuItem.Name = "newATAnalysisFileToolStripMenuItem";
            this.newATAnalysisFileToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.newATAnalysisFileToolStripMenuItem.Size = new System.Drawing.Size(231, 22);
            this.newATAnalysisFileToolStripMenuItem.Text = "New AT Analysis File";
            // 
            // btnOpenZippedAnalysis
            // 
            this.btnOpenZippedAnalysis.Name = "btnOpenZippedAnalysis";
            this.btnOpenZippedAnalysis.Size = new System.Drawing.Size(231, 22);
            this.btnOpenZippedAnalysis.Text = "Open Zipped Analysis File";
            // 
            // btnSaveZippedAnalysis
            // 
            this.btnSaveZippedAnalysis.Name = "btnSaveZippedAnalysis";
            this.btnSaveZippedAnalysis.Size = new System.Drawing.Size(231, 22);
            this.btnSaveZippedAnalysis.Text = "Save Zipped Analysis File";
            // 
            // btnOpenATAnalysisFile
            // 
            this.btnOpenATAnalysisFile.Image = global::ArrayTubeAnalyzer.Properties.Resources.Folder_With_Document_other;
            this.btnOpenATAnalysisFile.Name = "btnOpenATAnalysisFile";
            this.btnOpenATAnalysisFile.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.btnOpenATAnalysisFile.Size = new System.Drawing.Size(231, 22);
            this.btnOpenATAnalysisFile.Text = "Open AT Analysis File";
            // 
            // btnSaveMeta
            // 
            this.btnSaveMeta.Image = global::ArrayTubeAnalyzer.Properties.Resources.Save;
            this.btnSaveMeta.Name = "btnSaveMeta";
            this.btnSaveMeta.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.btnSaveMeta.Size = new System.Drawing.Size(231, 22);
            this.btnSaveMeta.Text = "Save";
            // 
            // btnSaveAsMeta
            // 
            this.btnSaveAsMeta.Image = global::ArrayTubeAnalyzer.Properties.Resources.document_save_as;
            this.btnSaveAsMeta.Name = "btnSaveAsMeta";
            this.btnSaveAsMeta.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.S)));
            this.btnSaveAsMeta.Size = new System.Drawing.Size(231, 22);
            this.btnSaveAsMeta.Text = "Save As";
            this.btnSaveAsMeta.Click += new System.EventHandler(this.OnSaveAsClick);
            // 
            // btnExportSignalData
            // 
            this.btnExportSignalData.Name = "btnExportSignalData";
            this.btnExportSignalData.Size = new System.Drawing.Size(231, 22);
            this.btnExportSignalData.Text = "Export QC\'ed Signal Data";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Image = global::ArrayTubeAnalyzer.Properties.Resources.info_16;
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(231, 22);
            this.aboutToolStripMenuItem.Text = "About";
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Image = global::ArrayTubeAnalyzer.Properties.Resources.Delete;
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(231, 22);
            this.closeToolStripMenuItem.Text = "Close";
            // 
            // btnExit
            // 
            this.btnExit.Name = "btnExit";
            this.btnExit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.btnExit.Size = new System.Drawing.Size(231, 22);
            this.btnExit.Text = "Exit";
            // 
            // ddbView
            // 
            this.ddbView.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.ddbView.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.arrayStatisticsToolStripMenuItem,
            this.markerThresholdAnalysisToolStripMenuItem,
            this.markerThresholdReviewToolStripMenuItem,
            this.reviewMarkerCallsToolStripMenuItem,
            this.clusteringToolStripMenuItem,
            this.diagnosticsToolStripMenuItem});
            this.ddbView.Image = ((System.Drawing.Image)(resources.GetObject("ddbView.Image")));
            this.ddbView.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ddbView.Name = "ddbView";
            this.ddbView.Size = new System.Drawing.Size(45, 22);
            this.ddbView.Text = "View";
            // 
            // arrayStatisticsToolStripMenuItem
            // 
            this.arrayStatisticsToolStripMenuItem.Image = global::ArrayTubeAnalyzer.Properties.Resources.info_16;
            this.arrayStatisticsToolStripMenuItem.Name = "arrayStatisticsToolStripMenuItem";
            this.arrayStatisticsToolStripMenuItem.Size = new System.Drawing.Size(213, 22);
            this.arrayStatisticsToolStripMenuItem.Text = "Array Statistics";
            this.arrayStatisticsToolStripMenuItem.Click += new System.EventHandler(this.OnShowArrayStatisticsClick);
            // 
            // markerThresholdAnalysisToolStripMenuItem
            // 
            this.markerThresholdAnalysisToolStripMenuItem.Image = global::ArrayTubeAnalyzer.Properties.Resources.Chart2;
            this.markerThresholdAnalysisToolStripMenuItem.Name = "markerThresholdAnalysisToolStripMenuItem";
            this.markerThresholdAnalysisToolStripMenuItem.Size = new System.Drawing.Size(213, 22);
            this.markerThresholdAnalysisToolStripMenuItem.Text = "Marker Threshold Analysis";
            this.markerThresholdAnalysisToolStripMenuItem.Click += new System.EventHandler(this.OnShowGeneThresholdReview);
            // 
            // markerThresholdReviewToolStripMenuItem
            // 
            this.markerThresholdReviewToolStripMenuItem.Image = global::ArrayTubeAnalyzer.Properties.Resources.statistics_16;
            this.markerThresholdReviewToolStripMenuItem.Name = "markerThresholdReviewToolStripMenuItem";
            this.markerThresholdReviewToolStripMenuItem.Size = new System.Drawing.Size(213, 22);
            this.markerThresholdReviewToolStripMenuItem.Text = "Marker Threshold Review";
            this.markerThresholdReviewToolStripMenuItem.Click += new System.EventHandler(this.OnShowSpotSignalReview);
            // 
            // reviewMarkerCallsToolStripMenuItem
            // 
            this.reviewMarkerCallsToolStripMenuItem.Image = global::ArrayTubeAnalyzer.Properties.Resources.OK_All;
            this.reviewMarkerCallsToolStripMenuItem.Name = "reviewMarkerCallsToolStripMenuItem";
            this.reviewMarkerCallsToolStripMenuItem.Size = new System.Drawing.Size(213, 22);
            this.reviewMarkerCallsToolStripMenuItem.Text = "Review Marker Calls";
            this.reviewMarkerCallsToolStripMenuItem.Click += new System.EventHandler(this.OnShowGeneCallReview);
            // 
            // clusteringToolStripMenuItem
            // 
            this.clusteringToolStripMenuItem.Image = global::ArrayTubeAnalyzer.Properties.Resources.tree_16;
            this.clusteringToolStripMenuItem.Name = "clusteringToolStripMenuItem";
            this.clusteringToolStripMenuItem.Size = new System.Drawing.Size(213, 22);
            this.clusteringToolStripMenuItem.Text = "Clustering";
            this.clusteringToolStripMenuItem.Click += new System.EventHandler(this.OnShowClustering);
            // 
            // diagnosticsToolStripMenuItem
            // 
            this.diagnosticsToolStripMenuItem.Image = global::ArrayTubeAnalyzer.Properties.Resources.search_16;
            this.diagnosticsToolStripMenuItem.Name = "diagnosticsToolStripMenuItem";
            this.diagnosticsToolStripMenuItem.Size = new System.Drawing.Size(213, 22);
            this.diagnosticsToolStripMenuItem.Text = "Diagnostics";
            this.diagnosticsToolStripMenuItem.Click += new System.EventHandler(this.OnShowDiagnostics);
            // 
            // ddbSettings
            // 
            this.ddbSettings.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.changeColourProfileToolStripMenuItem,
            this.arrayLayoutToolStripMenuItem,
            this.spotImageSizeToolStripMenuItem});
            this.ddbSettings.Image = global::ArrayTubeAnalyzer.Properties.Resources.gear_16;
            this.ddbSettings.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ddbSettings.Name = "ddbSettings";
            this.ddbSettings.Size = new System.Drawing.Size(78, 22);
            this.ddbSettings.Text = "Settings";
            // 
            // changeColourProfileToolStripMenuItem
            // 
            this.changeColourProfileToolStripMenuItem.Name = "changeColourProfileToolStripMenuItem";
            this.changeColourProfileToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.changeColourProfileToolStripMenuItem.Text = "Change Colour Profile";
            this.changeColourProfileToolStripMenuItem.Click += new System.EventHandler(this.ChangeColourProfileToolStripMenuItemClick);
            // 
            // arrayLayoutToolStripMenuItem
            // 
            this.arrayLayoutToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.numberOfRowsToolStripMenuItem,
            this.numberOfColumnsToolStripMenuItem,
            this.applyNewGridToolStripMenuItem});
            this.arrayLayoutToolStripMenuItem.Name = "arrayLayoutToolStripMenuItem";
            this.arrayLayoutToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.arrayLayoutToolStripMenuItem.Text = "Array Layout";
            // 
            // numberOfRowsToolStripMenuItem
            // 
            this.numberOfRowsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.txtNumRows});
            this.numberOfRowsToolStripMenuItem.Name = "numberOfRowsToolStripMenuItem";
            this.numberOfRowsToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.numberOfRowsToolStripMenuItem.Text = "Number of Rows";
            // 
            // txtNumRows
            // 
            this.txtNumRows.Name = "txtNumRows";
            this.txtNumRows.Size = new System.Drawing.Size(100, 23);
            this.txtNumRows.Text = "16";
            this.txtNumRows.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtNumRowsKeyPress);
            // 
            // numberOfColumnsToolStripMenuItem
            // 
            this.numberOfColumnsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.txtNumColumns});
            this.numberOfColumnsToolStripMenuItem.Name = "numberOfColumnsToolStripMenuItem";
            this.numberOfColumnsToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.numberOfColumnsToolStripMenuItem.Text = "Number of Columns";
            // 
            // txtNumColumns
            // 
            this.txtNumColumns.Name = "txtNumColumns";
            this.txtNumColumns.Size = new System.Drawing.Size(100, 23);
            this.txtNumColumns.Text = "16";
            this.txtNumColumns.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtNumColumnsKeyPress);
            // 
            // applyNewGridToolStripMenuItem
            // 
            this.applyNewGridToolStripMenuItem.Name = "applyNewGridToolStripMenuItem";
            this.applyNewGridToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.applyNewGridToolStripMenuItem.Text = "Apply New Grid";
            this.applyNewGridToolStripMenuItem.Click += new System.EventHandler(this.ApplyNewGridToolStripMenuItemClick);
            // 
            // spotImageSizeToolStripMenuItem
            // 
            this.spotImageSizeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.widthToolStripMenuItem,
            this.heightToolStripMenuItem,
            this.xLocationOffsetToolStripMenuItem,
            this.yLocationOffsetToolStripMenuItem});
            this.spotImageSizeToolStripMenuItem.Name = "spotImageSizeToolStripMenuItem";
            this.spotImageSizeToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.spotImageSizeToolStripMenuItem.Text = "Spot Cropping Settings";
            // 
            // widthToolStripMenuItem
            // 
            this.widthToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.txtSpotX});
            this.widthToolStripMenuItem.Name = "widthToolStripMenuItem";
            this.widthToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.widthToolStripMenuItem.Text = "Width";
            // 
            // txtSpotX
            // 
            this.txtSpotX.Name = "txtSpotX";
            this.txtSpotX.Size = new System.Drawing.Size(100, 23);
            this.txtSpotX.Text = "30";
            // 
            // heightToolStripMenuItem
            // 
            this.heightToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.txtSpotY});
            this.heightToolStripMenuItem.Name = "heightToolStripMenuItem";
            this.heightToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.heightToolStripMenuItem.Text = "Height";
            // 
            // txtSpotY
            // 
            this.txtSpotY.Name = "txtSpotY";
            this.txtSpotY.Size = new System.Drawing.Size(100, 23);
            this.txtSpotY.Text = "30";
            // 
            // xLocationOffsetToolStripMenuItem
            // 
            this.xLocationOffsetToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.txtXOffset});
            this.xLocationOffsetToolStripMenuItem.Name = "xLocationOffsetToolStripMenuItem";
            this.xLocationOffsetToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.xLocationOffsetToolStripMenuItem.Text = "X Location Offset";
            // 
            // txtXOffset
            // 
            this.txtXOffset.Name = "txtXOffset";
            this.txtXOffset.Size = new System.Drawing.Size(100, 23);
            this.txtXOffset.Text = "0";
            // 
            // yLocationOffsetToolStripMenuItem
            // 
            this.yLocationOffsetToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.txtYOffset});
            this.yLocationOffsetToolStripMenuItem.Name = "yLocationOffsetToolStripMenuItem";
            this.yLocationOffsetToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.yLocationOffsetToolStripMenuItem.Text = "Y Location Offset";
            // 
            // txtYOffset
            // 
            this.txtYOffset.Name = "txtYOffset";
            this.txtYOffset.Size = new System.Drawing.Size(100, 23);
            this.txtYOffset.Text = "0";
            // 
            // ddbSamples
            // 
            this.ddbSamples.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnAddSample,
            this.btnRemoveSelectedSample,
            this.removeMultipleToolStripMenuItem,
            this.btnFinalizeSample,
            this.btnFinalizeAllSamples});
            this.ddbSamples.Image = global::ArrayTubeAnalyzer.Properties.Resources.search_icon;
            this.ddbSamples.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ddbSamples.Name = "ddbSamples";
            this.ddbSamples.Size = new System.Drawing.Size(80, 22);
            this.ddbSamples.Text = "Samples";
            // 
            // btnAddSample
            // 
            this.btnAddSample.Image = global::ArrayTubeAnalyzer.Properties.Resources.Add;
            this.btnAddSample.Name = "btnAddSample";
            this.btnAddSample.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A)));
            this.btnAddSample.Size = new System.Drawing.Size(237, 22);
            this.btnAddSample.Text = "Add";
            // 
            // btnRemoveSelectedSample
            // 
            this.btnRemoveSelectedSample.Image = global::ArrayTubeAnalyzer.Properties.Resources.Remove;
            this.btnRemoveSelectedSample.Name = "btnRemoveSelectedSample";
            this.btnRemoveSelectedSample.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.R)));
            this.btnRemoveSelectedSample.Size = new System.Drawing.Size(237, 22);
            this.btnRemoveSelectedSample.Text = "Remove";
            // 
            // removeMultipleToolStripMenuItem
            // 
            this.removeMultipleToolStripMenuItem.Image = global::ArrayTubeAnalyzer.Properties.Resources.Delete;
            this.removeMultipleToolStripMenuItem.Name = "removeMultipleToolStripMenuItem";
            this.removeMultipleToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.R)));
            this.removeMultipleToolStripMenuItem.Size = new System.Drawing.Size(237, 22);
            this.removeMultipleToolStripMenuItem.Text = "Remove Multiple";
            this.removeMultipleToolStripMenuItem.Click += new System.EventHandler(this.OnRemoveMultipleSamples);
            // 
            // btnFinalizeSample
            // 
            this.btnFinalizeSample.Image = global::ArrayTubeAnalyzer.Properties.Resources.Ok;
            this.btnFinalizeSample.Name = "btnFinalizeSample";
            this.btnFinalizeSample.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F)));
            this.btnFinalizeSample.Size = new System.Drawing.Size(237, 22);
            this.btnFinalizeSample.Text = "Finalize";
            // 
            // btnFinalizeAllSamples
            // 
            this.btnFinalizeAllSamples.Image = global::ArrayTubeAnalyzer.Properties.Resources.OK_All;
            this.btnFinalizeAllSamples.Name = "btnFinalizeAllSamples";
            this.btnFinalizeAllSamples.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.F)));
            this.btnFinalizeAllSamples.Size = new System.Drawing.Size(237, 22);
            this.btnFinalizeAllSamples.Text = "Finalize All";
            // 
            // ddbFlagging
            // 
            this.ddbFlagging.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sDThresholdLowSignalToolStripMenuItem,
            this.sDThresholdHighSignalToolStripMenuItem,
            this.highSignalThresholdToolStripMenuItem,
            this.globalErrorToolStripMenuItem,
            this.globalErrorSDToolStripMenuItem,
            this.globalErrorThresholdLowToolStripMenuItem,
            this.globalErrorThresholdHighToolStripMenuItem,
            this.toolStripSeparator3,
            this.resetFlaggingForSelectedSampleToolStripMenuItem,
            this.resetFlaggingForEntireDatasetToolStripMenuItem});
            this.ddbFlagging.Image = global::ArrayTubeAnalyzer.Properties.Resources.flag_16;
            this.ddbFlagging.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ddbFlagging.Name = "ddbFlagging";
            this.ddbFlagging.Size = new System.Drawing.Size(82, 22);
            this.ddbFlagging.Text = "Flagging";
            // 
            // sDThresholdLowSignalToolStripMenuItem
            // 
            this.sDThresholdLowSignalToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.txtSDThresholdLow});
            this.sDThresholdLowSignalToolStripMenuItem.Name = "sDThresholdLowSignalToolStripMenuItem";
            this.sDThresholdLowSignalToolStripMenuItem.Size = new System.Drawing.Size(260, 22);
            this.sDThresholdLowSignalToolStripMenuItem.Text = "SD Threshold - Low Signal";
            // 
            // txtSDThresholdLow
            // 
            this.txtSDThresholdLow.Name = "txtSDThresholdLow";
            this.txtSDThresholdLow.Size = new System.Drawing.Size(100, 23);
            this.txtSDThresholdLow.Text = "1.0";
            this.txtSDThresholdLow.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtSDThresholdLowKeyPress);
            // 
            // sDThresholdHighSignalToolStripMenuItem
            // 
            this.sDThresholdHighSignalToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.txtSDThresholdHigh});
            this.sDThresholdHighSignalToolStripMenuItem.Name = "sDThresholdHighSignalToolStripMenuItem";
            this.sDThresholdHighSignalToolStripMenuItem.Size = new System.Drawing.Size(260, 22);
            this.sDThresholdHighSignalToolStripMenuItem.Text = "SD Threshold - High Signal";
            // 
            // txtSDThresholdHigh
            // 
            this.txtSDThresholdHigh.Name = "txtSDThresholdHigh";
            this.txtSDThresholdHigh.Size = new System.Drawing.Size(100, 23);
            this.txtSDThresholdHigh.Text = "2.0";
            this.txtSDThresholdHigh.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtSDThresholdHighKeyPress);
            // 
            // highSignalThresholdToolStripMenuItem
            // 
            this.highSignalThresholdToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.txtHighSignalThreshold});
            this.highSignalThresholdToolStripMenuItem.Name = "highSignalThresholdToolStripMenuItem";
            this.highSignalThresholdToolStripMenuItem.Size = new System.Drawing.Size(260, 22);
            this.highSignalThresholdToolStripMenuItem.Text = "High Signal Threshold";
            // 
            // txtHighSignalThreshold
            // 
            this.txtHighSignalThreshold.Name = "txtHighSignalThreshold";
            this.txtHighSignalThreshold.Size = new System.Drawing.Size(100, 23);
            this.txtHighSignalThreshold.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtHighSignalThresholdKeyPress);
            // 
            // globalErrorToolStripMenuItem
            // 
            this.globalErrorToolStripMenuItem.Name = "globalErrorToolStripMenuItem";
            this.globalErrorToolStripMenuItem.Size = new System.Drawing.Size(260, 22);
            this.globalErrorToolStripMenuItem.Text = "Global Error";
            // 
            // globalErrorSDToolStripMenuItem
            // 
            this.globalErrorSDToolStripMenuItem.Name = "globalErrorSDToolStripMenuItem";
            this.globalErrorSDToolStripMenuItem.Size = new System.Drawing.Size(260, 22);
            this.globalErrorSDToolStripMenuItem.Text = "Global Error SD";
            // 
            // globalErrorThresholdLowToolStripMenuItem
            // 
            this.globalErrorThresholdLowToolStripMenuItem.Name = "globalErrorThresholdLowToolStripMenuItem";
            this.globalErrorThresholdLowToolStripMenuItem.Size = new System.Drawing.Size(260, 22);
            this.globalErrorThresholdLowToolStripMenuItem.Text = "Global Error Threshold (Low)";
            // 
            // globalErrorThresholdHighToolStripMenuItem
            // 
            this.globalErrorThresholdHighToolStripMenuItem.Name = "globalErrorThresholdHighToolStripMenuItem";
            this.globalErrorThresholdHighToolStripMenuItem.Size = new System.Drawing.Size(260, 22);
            this.globalErrorThresholdHighToolStripMenuItem.Text = "Global Error Threshold (High)";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(257, 6);
            // 
            // resetFlaggingForSelectedSampleToolStripMenuItem
            // 
            this.resetFlaggingForSelectedSampleToolStripMenuItem.Name = "resetFlaggingForSelectedSampleToolStripMenuItem";
            this.resetFlaggingForSelectedSampleToolStripMenuItem.Size = new System.Drawing.Size(260, 22);
            this.resetFlaggingForSelectedSampleToolStripMenuItem.Text = "Reset Flagging For Selected Sample";
            this.resetFlaggingForSelectedSampleToolStripMenuItem.Click += new System.EventHandler(this.ResetFlaggingForSelectedArrayToolStripMenuItemClick);
            // 
            // resetFlaggingForEntireDatasetToolStripMenuItem
            // 
            this.resetFlaggingForEntireDatasetToolStripMenuItem.Name = "resetFlaggingForEntireDatasetToolStripMenuItem";
            this.resetFlaggingForEntireDatasetToolStripMenuItem.Size = new System.Drawing.Size(260, 22);
            this.resetFlaggingForEntireDatasetToolStripMenuItem.Text = "Reset Flagging For Entire Dataset";
            this.resetFlaggingForEntireDatasetToolStripMenuItem.Click += new System.EventHandler(this.ResetFlaggingForEntireDatasetToolStripMenuItemClick);
            // 
            // btnMarkerThresholdAnalysis
            // 
            this.btnMarkerThresholdAnalysis.Image = global::ArrayTubeAnalyzer.Properties.Resources.Chart2;
            this.btnMarkerThresholdAnalysis.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnMarkerThresholdAnalysis.Name = "btnMarkerThresholdAnalysis";
            this.btnMarkerThresholdAnalysis.Size = new System.Drawing.Size(166, 22);
            this.btnMarkerThresholdAnalysis.Text = "Marker Threshold Analysis";
            this.btnMarkerThresholdAnalysis.Click += new System.EventHandler(this.OnAnalyzeClick);
            // 
            // FrmSampleQC
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(980, 608);
            this.Controls.Add(this.toolStripContainer1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmSampleQC";
            this.Text = "Array Tube Analyzer - Sample QC";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmVisualizeArraysFormClosing);
            this.DragDrop += new System.Windows.Forms.DragEventHandler(this.FileDragDrop);
            this.DragEnter += new System.Windows.Forms.DragEventHandler(this.FileDragOverOrEnter);
            this.DragOver += new System.Windows.Forms.DragEventHandler(this.FileDragOverOrEnter);
            this.toolStripContainer1.BottomToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.BottomToolStripPanel.PerformLayout();
            this.toolStripContainer1.ContentPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.PerformLayout();
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.tslStatus.ResumeLayout(false);
            this.tslStatus.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ContextMenuStrip1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
            this.lblArrayName.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tbrContrast)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx)).EndInit();
            this.splitContainer4.Panel1.ResumeLayout(false);
            this.splitContainer4.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).EndInit();
            this.splitContainer4.ResumeLayout(false);
            this.gbxSettings.ResumeLayout(false);
            this.gbxSettings.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxReplicates)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStripContainer toolStripContainer1;
        private System.Windows.Forms.StatusStrip tslStatus;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.GroupBox lblArrayName;
        private System.Windows.Forms.PictureBox pbx;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripDropDownButton ddbFile;
        private System.Windows.Forms.ToolStripMenuItem newATAnalysisFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem btnOpenATAnalysisFile;
        private System.Windows.Forms.ToolStripMenuItem btnSaveMeta;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem btnExit;
        private System.Windows.Forms.ToolStripDropDownButton ddbFlagging;
        private System.Windows.Forms.GroupBox gbxSettings;
        private System.Windows.Forms.ToolStripDropDownButton ddbSettings;
        private System.Windows.Forms.ToolStripStatusLabel lblStatus;
        private System.Windows.Forms.ToolStripProgressBar ProgressBar1;
        private ListViewFF lvwArrays;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.CheckBox chkCalibrate;
        private System.Windows.Forms.CheckBox chkAdjustContrast;
        private System.Windows.Forms.CheckBox chkSpotNumbers;
        private System.Windows.Forms.CheckBox chkShowGrid;
        private ListViewFF lvwATinfo;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ColumnHeader columnHeader10;
        private System.Windows.Forms.ColumnHeader columnHeader11;
        private System.Windows.Forms.ColumnHeader columnHeader12;
        private System.Windows.Forms.TrackBar tbrContrast;
        private System.Windows.Forms.ContextMenuStrip ContextMenuStrip1;
        private LabelWithColours lblBottomRight;
        private LabelWithColours lblBottomLeft;
        private LabelWithColours lblTopRight;
        private LabelWithColours lblTopLeft;
        private System.Windows.Forms.SplitContainer splitContainer4;
        private System.Windows.Forms.PictureBox pbxReplicates;
        private System.Windows.Forms.ToolStripDropDownButton ddbView;
        private System.Windows.Forms.ToolStripMenuItem arrayStatisticsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem markerThresholdAnalysisToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem markerThresholdReviewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reviewMarkerCallsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clusteringToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem diagnosticsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem changeColourProfileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem arrayLayoutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem numberOfRowsToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox txtNumRows;
        private System.Windows.Forms.ToolStripMenuItem numberOfColumnsToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox txtNumColumns;
        private System.Windows.Forms.ToolStripMenuItem applyNewGridToolStripMenuItem;
        private System.Windows.Forms.ToolStripDropDownButton ddbSamples;
        private System.Windows.Forms.ToolStripMenuItem btnAddSample;
        private System.Windows.Forms.ToolStripMenuItem btnRemoveSelectedSample;
        private System.Windows.Forms.ToolStripMenuItem removeMultipleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem btnFinalizeSample;
        private System.Windows.Forms.ToolStripMenuItem btnFinalizeAllSamples;
        private System.Windows.Forms.ToolStripMenuItem sDThresholdLowSignalToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox txtSDThresholdLow;
        private System.Windows.Forms.ToolStripMenuItem sDThresholdHighSignalToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox txtSDThresholdHigh;
        private System.Windows.Forms.ToolStripMenuItem highSignalThresholdToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox txtHighSignalThreshold;
        private System.Windows.Forms.ToolStripMenuItem globalErrorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem globalErrorSDToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem globalErrorThresholdLowToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem globalErrorThresholdHighToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem resetFlaggingForSelectedSampleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resetFlaggingForEntireDatasetToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton btnMarkerThresholdAnalysis;
        private System.Windows.Forms.ToolStripMenuItem btnSaveAsMeta;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ToolStripMenuItem changeArrayTubeSampleNamesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showMarkerStripToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem btnSaveSpotStripPic;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem btnSaveZippedAnalysis;
        private System.Windows.Forms.ToolStripMenuItem btnOpenZippedAnalysis;
        private System.Windows.Forms.ToolStripMenuItem spotImageSizeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem widthToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox txtSpotX;
        private System.Windows.Forms.ToolStripMenuItem heightToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox txtSpotY;
        private System.Windows.Forms.ToolStripMenuItem xLocationOffsetToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox txtXOffset;
        private System.Windows.Forms.ToolStripMenuItem yLocationOffsetToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox txtYOffset;
        private System.Windows.Forms.ToolStripMenuItem btnExportSignalData;
    }
}

