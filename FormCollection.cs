﻿using System.Collections.Generic;

namespace ArrayTubeAnalyzer
{
    
    public class FormCollection
    {
        public FrmArrayStats ArrayStats;
        public FrmClustering Clustering;
        public FrmDiagnosis Diagnosis;
        public FrmMarkerThresholdAnalysis MarkerThresholdAnalysis;
        public FrmMarkerThresholdReview MarkerThresholdReview;
        public FrmReviewMarkerCalls ReviewMarkerCalls;
        public FrmSampleQC SampleQC;
        

        public List<FrmShowMarkerSpots> ShowMarkerSpots = new List<FrmShowMarkerSpots>();
        public List<FrmSpotPreview> SpotPreview = new List<FrmSpotPreview>();

        public FormCollection(FrmSampleQC mainForm)
        {
            SampleQC = mainForm;
        }

        public void CloseAllWindows()
        {
            if ((ReviewMarkerCalls != null))
            {
                ReviewMarkerCalls.Close();
                ReviewMarkerCalls = null;
            }

            if ((MarkerThresholdAnalysis != null))
            {
                MarkerThresholdAnalysis.Close();
                MarkerThresholdAnalysis = null;
            }

            if ((MarkerThresholdReview != null))
            {
                MarkerThresholdReview.Close();
                MarkerThresholdReview = null;
            }

            if ((MarkerThresholdAnalysis != null))
            {
                MarkerThresholdAnalysis.Close();
                MarkerThresholdAnalysis = null;
            }

            if ((Clustering != null))
            {
                Clustering.Close();
                Clustering = null;
            }

            if ((ArrayStats != null))
            {
                ArrayStats.Close();
                ArrayStats = null;
            }

            if ((Diagnosis != null))
            {
                Diagnosis.Close();
                Diagnosis = null;
            }

            while (SpotPreview.Count > 0)
            {
                SpotPreview[0].Close();
            }

            while (ShowMarkerSpots.Count > 0)
            {
                ShowMarkerSpots[0].Close();
            }

        }

        /// <summary>Remove frmSpotPreview object from list of frmSpotPreview objects since form has been closed.</summary>
        /// <param name="spotPreview">frmSpotPreview object/form.</param>
        public void SpotPreviewWindowClosed(FrmSpotPreview spotPreview)
        {
            SpotPreview.Remove(spotPreview);
        }

        /// <summary>Remove frmShowMarkerSpots object from list of frmSpotMarkerSpots object since form has been closed.</summary>
        /// <param name="showMarkerSpots">frmShowMarkerSpots object/form.</param>
        public void ShowMarkerSpotsWindowClosed(FrmShowMarkerSpots showMarkerSpots)
        {
            ShowMarkerSpots.Remove(showMarkerSpots);
        }

    }
}
