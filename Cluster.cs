﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.IO;

public enum TreatAmbiguousAs
{
    Positive, Negative, ThresholdDetermined
}

namespace ArrayTubeAnalyzer
{
    [Serializable]
    public class Cluster
    {
        private List<List<bool>> _binary;
        public List<bool> GetBinary(int index)
        {
            return _binary[index];
        }
        /// <summary>List of markers to use for clustering.</summary>
        public List<int> MarkerList { get; set; }

        private double _threshold;
        public double Threshold
        {
            get { return _threshold * 100 / MarkerList.Count; }
        }
        /// <summary>Set the clustering threshold. Must be value between 0 and 100.</summary>
        /// <param name="dThreshold">Threshold value between 0 and 100.</param>
        public void SetThreshold(double dThreshold)
        {
            _threshold = dThreshold * MarkerList.Count / 100;
        }
        /// <summary>Stop clustering at threshold.</summary>
        public bool StopAtThreshold;
        /// <summary>Distance matrix 2D array.</summary>
        private double[,] _distanceMatrix;

        public bool WriteDistanceMatrix(string filename, bool useSimilarity)
        {
            try
            {
                double[,] dm = GetDistanceMatrix();
                double numMarkers = MarkerList.Count;

                using (var sw = new StreamWriter(filename))
                {
                    var line = new List<string> { "Strains", "Cluster" };
                    foreach (int i in ClusterGroup.NameIndices)
                    {
                        line.Add(_sd.SampleNames[i]);
                    }

                    sw.WriteLine(string.Join("\t", line));
                    line.Clear();
                    line.Add("Cluster");
                    line.Add("");
                    foreach (int i in ClusterGroup.ClusterNumber)
                    {
                        line.Add(i.ToString(CultureInfo.InvariantCulture));
                    }
                    sw.WriteLine(string.Join("\t", line));
                    foreach (int iIndex in ClusterGroup.NameIndices)
                    {
                        string sample = _sd.SampleNames[iIndex];
                        line.Clear();
                        line.Add(sample);
                        line.Add(ClusterGroup.ClusterNumber[iIndex].ToString(CultureInfo.InvariantCulture));
                        foreach (int jIndex in ClusterGroup.NameIndices)
                        {
                            line.Add(useSimilarity
                                         ? dm[iIndex, jIndex].ToString(CultureInfo.InvariantCulture)
                                         : (numMarkers - dm[iIndex, jIndex]).ToString(CultureInfo.InvariantCulture));
                        }
                        sw.WriteLine(string.Join("\t", line));
                    }
                    sw.Close();
                }
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        private readonly List<ClusterGroup> _clusters;
        public ClusterGroup ClusterGroup
        {
            get { return _clusters[0]; }
        }

        public bool UseJaccardIndex;

        private readonly SampleData _sd;

        public delegate void ClusterEventHandler(object sender, ClusterArgs cArgs);
        public event ClusterEventHandler EventCluster;


        public Cluster(object objSampleData)
        {
            _sd = objSampleData as SampleData;
            SetMarkerList();
            _clusters = new List<ClusterGroup>();
        }

        private void SetMarkerList()
        {
            var tmp = new List<int>();
            for (int i = 0; i < _sd.MarkerCount; i++)
            {
                if (!_sd.DiscardedMarkers[i])
                {
                    tmp.Add(i);
                }
            }
            MarkerList = tmp;
        }

        /// <summary>Write the binary data with or without the cluster number to a file.</summary>
        /// <param name="f">Filename path to save to.</param>
        /// <param name="bCluster">True=inc;ude cluster numbers; false=do not include cluster numbers.</param>
        public bool Write(string f, bool bCluster)
        {
            try
            {
                using (var sw = new StreamWriter(f))
                {
                    //write headers
                    var lHeader = new List<string> { "Sample" };
                    if (bCluster)
                    {
                        lHeader.Add("Cluster");
                    }
                    lHeader.AddRange(_sd.Markers);
                    sw.WriteLine(string.Join("\t", lHeader));


                    //write the rest of the file
                    for (int i = 0; i < _binary.Count; i++)
                    {
                        int index = _clusters[0].NameIndices[i];
                        List<bool> lb = _binary[index];

                        var ls = new List<string> { _sd.SampleNames[index] };
                        if (bCluster)
                        {
                            ls.Add(_clusters[0].ClusterNumber[i].ToString(CultureInfo.InvariantCulture));
                        }
                        foreach (bool b in lb)
                        {
                            ls.Add(b ? "1" : "0");
                        }

                        sw.WriteLine(string.Join("\t", ls));
                    }
                    sw.Close();
                }
                return true;
            }
            catch (Exception ex)
            {
                EventCluster(this, new ClusterArgs(ex));
                return false;
            }
        }
        /// <summary>Write the binary fasta file for the binary data file provided.</summary>
        /// <param name="f">Filename to write to.</param>
        public void WriteFasta(string f)
        {
            try
            {
                using (var sw = new StreamWriter(f))
                {
                    foreach (int i in _clusters[0].NameIndices)
                    {
                        sw.WriteLine(">" + _sd.SampleNames[i]);
                        sw.WriteLine(GetBinaryString(i));
                    }
                    sw.Close();
                }
            }
            catch (Exception ex)
            {
                EventCluster(this, new ClusterArgs(ex));
            }

        }
        /// <summary>Get the binary string representation of a binary profile for a sample, e.g. "0101010001"</summary>
        /// <param name="index">Index of the sample.</param>
        /// <returns></returns>
        private string GetBinaryString(int index)
        {
            List<bool> lb = _binary[index];
            var sb = new StringBuilder();
            foreach (bool b in lb)
            {
                sb.Append(b ? "1" : "0");
            }
            return sb.ToString();
        }

        /// <summary>UPGMA cluster binary data stopping at the provided threshold if specified. Create clusters based on threshold.</summary>
        public void UPGMACluster()
        {
            try
            {
                SetMarkerList();
                GetBinaryData();
                _distanceMatrix = GetDistanceMatrix();

                _clusters.Clear();

                for (int i = 0; i < _binary.Count; i++)
                {
                    var ld = new List<double>();
                    for (int j = 0; j < _binary.Count; j++)
                    {
                        ld.Add(_distanceMatrix[i, j]);
                    }
                    _clusters.Add(new ClusterGroup(i, ld));
                }

                //by default, run clustering until there is only one cluster
                int indexI = 0;
                int indexJ = 0;
                while (_clusters.Count > 1)
                {
                    //x coordinate of where the maximum similarity is found within the distance matrix
                    //y coordinate of where the maximum similarity is found within the distance matrix
                    double maxSim = 0;
                    //maximum similarity value

                    //Go through the distance matrix (2D List) looking for the maximum similarity value
                    //iterate through the clusters
                    for (int i = 0; i < _clusters.Count; i++)
                    {
                        //iterate through the values within each cluster
                        for (int j = 0; j < _clusters[i].Distances.Count; j++)
                        {
                            //cannot look at self vs self comparisons
                            if (i != j)
                            {
                                //see if the value is larger than the previous largest similarity value
                                if (_clusters[i].Distances[j] > maxSim)
                                {
                                    indexI = i;
                                    indexJ = j;
                                    maxSim = _clusters[i].Distances[j];
                                }
                            }
                        }
                    }

                    //check if the maximum similarity value for this iteration of UPGMA clustering is above the threshold
                    if (StopAtThreshold)
                    {
                        if (maxSim < _threshold)
                        {
                            break;
                        }
                    }

                    //Create temporary Cluster variables
                    var tmp1 = new ClusterGroup(_clusters[indexI]);
                    var tmp2 = new ClusterGroup(_clusters[indexJ]);

                    //swap j_ and i_ if j_ is larger than i_
                    if (indexJ > indexI)
                    {
                        int tmp = indexJ;
                        indexJ = indexI;
                        indexI = tmp;
                    }

                    //remove the maximum similarity clusters from the distance matrix
                    _clusters.RemoveAt(indexI);
                    _clusters.RemoveAt(indexJ);
                    //remove any remnants of the maximum similarity clusters within the distance matrix
                    foreach (ClusterGroup t in _clusters)
                    {
                        t.Distances.RemoveAt(indexI);
                        t.Distances.RemoveAt(indexJ);
                    }

                    //remove self values within the temporary Cluster variables
                    tmp1.Distances.RemoveAt(indexI);
                    tmp1.Distances.RemoveAt(indexJ);
                    tmp2.Distances.RemoveAt(indexI);
                    tmp2.Distances.RemoveAt(indexJ);

                    //Combine the two temporary Cluster variables within a new tmpNew Cluster variable
                    var tmpNew = new ClusterGroup(tmp1, tmp2);

                    //Add the new Cluster variable into the distance matrix
                    _clusters.Add(tmpNew);
                    //Add the remaining data into the distance matrix
                    for (int i = 0; i < _clusters.Count - 1; i++)
                    {
                        _clusters[i].Distances.Add(tmpNew.Distances[i]);
                    }
                    _clusters[_clusters.Count - 1].Distances.Add(0);
                }

                if (_clusters.Count > 1)
                    SortClustersBySize();

                NumberClusters();

                if (_clusters.Count > 1)
                    ConsolidateClusters();

            }
            catch (Exception ex)
            {
                EventCluster(this, new ClusterArgs(ex));
            }
        }

        /// <summary>Retrieve the binary data that is generated after thresholding.</summary>
        private void GetBinaryData()
        {
            _binary = new List<List<bool>>();
            foreach (SampleCalls sampleCall in _sd.SampleCalls)
            {

                var tmp = new List<bool>();
                //Use Threshold
                switch (_sd.Ambiguous)
                {
                    case TreatAmbiguousAs.ThresholdDetermined:
                        foreach (int j in MarkerList)
                        {
                            Calls mCall = sampleCall.Calls[j];
                            switch (mCall)
                            {
                                case Calls.AbsentAuto:
                                case Calls.Absent:
                                case Calls.AmbiguousAbsent:
                                case Calls.AmbiguousManual:
                                    tmp.Add(false);
                                    break;
                                case Calls.PresentAuto:
                                case Calls.Present:
                                case Calls.AmbiguousPresent:
                                    tmp.Add(true);
                                    break;
                            }
                        }
                        break;
                    case TreatAmbiguousAs.Positive:
                        foreach (int j in MarkerList)
                        {
                            Calls mCall = sampleCall.Calls[j];
                            switch (mCall)
                            {
                                case Calls.AbsentAuto:
                                case Calls.Absent:
                                    tmp.Add(false);
                                    break;
                                case Calls.PresentAuto:
                                case Calls.Present:
                                case Calls.AmbiguousPresent:
                                case Calls.AmbiguousAbsent:
                                    tmp.Add(true);
                                    break;
                            }
                        }
                        break;
                    case TreatAmbiguousAs.Negative:

                        foreach (int j in MarkerList)
                        {
                            Calls mCall = sampleCall.Calls[j];
                            switch (mCall)
                            {
                                case Calls.AbsentAuto:
                                case Calls.Absent:
                                case Calls.AmbiguousPresent:
                                case Calls.AmbiguousAbsent:
                                    tmp.Add(false);
                                    break;
                                case Calls.PresentAuto:
                                case Calls.Present:
                                    tmp.Add(true);
                                    break;
                            }
                        }
                        break;
                }

                _binary.Add(tmp);
            }
        }

        /// <summary>Combine the clusters together into one cluster maintaining the size sorting order and cluster numbers.</summary>
        private void ConsolidateClusters()
        {
            while (_clusters.Count > 1)
            {
                var cg1 = new ClusterGroup(_clusters[0]);
                var cg2 = new ClusterGroup(_clusters[1]);
                cg1.Distances.Clear();
                cg1.NameIndices.AddRange(cg2.NameIndices);
                cg1.ClusterNumber.AddRange(cg2.ClusterNumber);
                _clusters[0] = cg1;
                _clusters.RemoveAt(1);
            }
        }

        /// <summary>Sort the clusters by size.</summary>
        private void SortClustersBySize()
        {
            _clusters.Sort();
        }

        /// <summary>Enumerate sorted clusters or if everything was fully clustered, pass the task over to FullyClusteredClusterNumbering.</summary>
        private void NumberClusters()
        {
            if (_clusters.Count == 1)
            {
                FullyClusteredClusterNumbering();
            }
            else
            {
                int clusterNumber = 1;
                foreach (ClusterGroup t in _clusters)
                {
                    foreach (var nameIndex in t.NameIndices)
                    {
                        t.ClusterNumber.Add(clusterNumber++);
                    }
                }
            }
        }

        /// <summary>Adds cluster numbers to cluster groups.</summary>
        private void FullyClusteredClusterNumbering()
        {
            int clusterNumber = 1;

            _clusters[0].ClusterNumber.Add(clusterNumber);
            for (int i = 0; i < _clusters[0].NameIndices.Count - 1; i++)
            {
                List<bool> lb1 = _binary[_clusters[0].NameIndices[i]];
                List<bool> lb2 = _binary[_clusters[0].NameIndices[i + 1]];
                if (getSimilarities(lb1, lb2) < _threshold)
                {
                    clusterNumber++;
                }
                _clusters[0].ClusterNumber.Add(clusterNumber);
            }
        }
        /// <summary>Calculate the distance matrix for the binary data provided.</summary>
        /// <returns>Distance matrix in a double 2D array.</returns>
        private double[,] GetDistanceMatrix()
        {
            var dm = new double[_binary.Count, _binary.Count];
            if (UseJaccardIndex)
            {
                for (int i = 0; i < _binary.Count; i++)
                {
                    for (int j = 0; j < _binary.Count; j++)
                    {
                        dm[i, j] = getSimilaritiesJaccard(_binary[i], _binary[j]);
                    }
                }
            }
            else
            {
                for (int i = 0; i < _binary.Count; i++)
                {
                    for (int j = 0; j < _binary.Count; j++)
                    {
                        dm[i, j] = getSimilarities(_binary[i], _binary[j]);
                    }
                }
            }

            return dm;
        }

        /// <summary>Get the number of similarities between two binary profiles at each position.</summary>
        /// <param name="lb1">Binary profile 1.</param>
        /// <param name="lb2">Binary profile 2.</param>
        /// <returns></returns>
        private int getSimilarities(List<bool> lb1, List<bool> lb2)
        {
            int count = 0;
            for (int i = 0; i < lb1.Count; i++)
            {
                if (lb1[i] == lb2[i])
                    count++;
            }
            return count;
        }

        private int getSimilaritiesJaccard(List<bool> lb1, List<bool> lb2)
        {
            int count = 0;
            for (int i = 0; i < lb1.Count; i++)
            {
                if (lb1[i] && lb2[i])
                    count++;
            }
            return count;
        }
    }

    public class ClusterArgs
    {
        private readonly Exception _ex;

        public Exception Exception
        {
            get { return _ex; }
        }

        public ClusterArgs()
        {

        }

        public ClusterArgs(Exception exException)
        {
            _ex = exException;
        }

    }
}