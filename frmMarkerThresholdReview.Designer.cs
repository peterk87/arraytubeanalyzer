﻿namespace ArrayTubeAnalyzer
{
    partial class FrmMarkerThresholdReview
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title1 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMarkerThresholdReview));
            this.AboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.SplitContainer1 = new System.Windows.Forms.SplitContainer();
            this.lvwSpots = new ArrayTubeAnalyzer.ListViewFF();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ContextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ShowSpotsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.ThresholdToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.txtThreshold = new System.Windows.Forms.ToolStripTextBox();
            this.setThresholdManuallyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SplitContainer2 = new System.Windows.Forms.SplitContainer();
            this.chtSignals = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.SplitContainer3 = new System.Windows.Forms.SplitContainer();
            this.gbxPicArray = new System.Windows.Forms.GroupBox();
            this.picArray = new System.Windows.Forms.PictureBox();
            this.lvwSpotInfo = new ArrayTubeAnalyzer.ListViewFF();
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader12 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader13 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader14 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ToolStrip1 = new System.Windows.Forms.ToolStrip();
            this.ToolStripDropDownButton1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.SaveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.CloseMarkerThresholdReviewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.ExitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ddbView = new System.Windows.Forms.ToolStripDropDownButton();
            this.SampleQCToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MarkerThresholdAnalysisToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ReviewMarkerCallsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ClusteringToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SampleDiagnosticsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ColumnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ColumnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ColumnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ColumnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ColumnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ColumnHeader10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ColumnHeader11 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ToolStripContainer1.ContentPanel.SuspendLayout();
            this.ToolStripContainer1.TopToolStripPanel.SuspendLayout();
            this.ToolStripContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SplitContainer1)).BeginInit();
            this.SplitContainer1.Panel1.SuspendLayout();
            this.SplitContainer1.Panel2.SuspendLayout();
            this.SplitContainer1.SuspendLayout();
            this.ContextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SplitContainer2)).BeginInit();
            this.SplitContainer2.Panel1.SuspendLayout();
            this.SplitContainer2.Panel2.SuspendLayout();
            this.SplitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chtSignals)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SplitContainer3)).BeginInit();
            this.SplitContainer3.Panel1.SuspendLayout();
            this.SplitContainer3.Panel2.SuspendLayout();
            this.SplitContainer3.SuspendLayout();
            this.gbxPicArray.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picArray)).BeginInit();
            this.ToolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // AboutToolStripMenuItem
            // 
            this.AboutToolStripMenuItem.Image = global::ArrayTubeAnalyzer.Properties.Resources.info_16;
            this.AboutToolStripMenuItem.Name = "AboutToolStripMenuItem";
            this.AboutToolStripMenuItem.Size = new System.Drawing.Size(239, 22);
            this.AboutToolStripMenuItem.Text = "About";
            this.AboutToolStripMenuItem.Click += new System.EventHandler(this.AboutToolStripMenuItemClick);
            // 
            // ToolStripContainer1
            // 
            // 
            // ToolStripContainer1.ContentPanel
            // 
            this.ToolStripContainer1.ContentPanel.Controls.Add(this.SplitContainer1);
            this.ToolStripContainer1.ContentPanel.Size = new System.Drawing.Size(1016, 542);
            this.ToolStripContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ToolStripContainer1.Location = new System.Drawing.Point(0, 0);
            this.ToolStripContainer1.Name = "ToolStripContainer1";
            this.ToolStripContainer1.Size = new System.Drawing.Size(1016, 567);
            this.ToolStripContainer1.TabIndex = 1;
            this.ToolStripContainer1.Text = "ToolStripContainer1";
            // 
            // ToolStripContainer1.TopToolStripPanel
            // 
            this.ToolStripContainer1.TopToolStripPanel.Controls.Add(this.ToolStrip1);
            // 
            // SplitContainer1
            // 
            this.SplitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SplitContainer1.Location = new System.Drawing.Point(0, 0);
            this.SplitContainer1.Name = "SplitContainer1";
            // 
            // SplitContainer1.Panel1
            // 
            this.SplitContainer1.Panel1.Controls.Add(this.lvwSpots);
            // 
            // SplitContainer1.Panel2
            // 
            this.SplitContainer1.Panel2.Controls.Add(this.SplitContainer2);
            this.SplitContainer1.Size = new System.Drawing.Size(1016, 542);
            this.SplitContainer1.SplitterDistance = 235;
            this.SplitContainer1.TabIndex = 0;
            // 
            // lvwSpots
            // 
            this.lvwSpots.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader5,
            this.columnHeader6});
            this.lvwSpots.ContextMenuStrip = this.ContextMenuStrip1;
            this.lvwSpots.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvwSpots.FullRowSelect = true;
            this.lvwSpots.GridLines = true;
            this.lvwSpots.Location = new System.Drawing.Point(0, 0);
            this.lvwSpots.Name = "lvwSpots";
            this.lvwSpots.Size = new System.Drawing.Size(231, 538);
            this.lvwSpots.TabIndex = 0;
            this.lvwSpots.UseCompatibleStateImageBehavior = false;
            this.lvwSpots.View = System.Windows.Forms.View.Details;
            this.lvwSpots.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.LvwSpotsColumnClick);
            this.lvwSpots.SelectedIndexChanged += new System.EventHandler(this.LvwSpotsSelectedIndexChanged);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Marker";
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Threshold";
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Threshold Type";
            // 
            // ContextMenuStrip1
            // 
            this.ContextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ShowSpotsToolStripMenuItem,
            this.ToolStripSeparator3,
            this.ThresholdToolStripMenuItem,
            this.txtThreshold,
            this.setThresholdManuallyToolStripMenuItem});
            this.ContextMenuStrip1.Name = "ContextMenuStrip1";
            this.ContextMenuStrip1.Size = new System.Drawing.Size(199, 101);
            // 
            // ShowSpotsToolStripMenuItem
            // 
            this.ShowSpotsToolStripMenuItem.Name = "ShowSpotsToolStripMenuItem";
            this.ShowSpotsToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.ShowSpotsToolStripMenuItem.Text = "Show Marker Strip";
            this.ShowSpotsToolStripMenuItem.Click += new System.EventHandler(this.ShowSpotsToolStripMenuItemClick);
            // 
            // ToolStripSeparator3
            // 
            this.ToolStripSeparator3.Name = "ToolStripSeparator3";
            this.ToolStripSeparator3.Size = new System.Drawing.Size(195, 6);
            // 
            // ThresholdToolStripMenuItem
            // 
            this.ThresholdToolStripMenuItem.Name = "ThresholdToolStripMenuItem";
            this.ThresholdToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.ThresholdToolStripMenuItem.Text = "Threshold";
            // 
            // txtThreshold
            // 
            this.txtThreshold.Name = "txtThreshold";
            this.txtThreshold.Size = new System.Drawing.Size(100, 23);
            this.txtThreshold.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtThresholdKeyPress);
            // 
            // setThresholdManuallyToolStripMenuItem
            // 
            this.setThresholdManuallyToolStripMenuItem.Name = "setThresholdManuallyToolStripMenuItem";
            this.setThresholdManuallyToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.setThresholdManuallyToolStripMenuItem.Text = "Set Threshold Manually";
            this.setThresholdManuallyToolStripMenuItem.Click += new System.EventHandler(this.SetThresholdManuallyToolStripMenuItemClick);
            // 
            // SplitContainer2
            // 
            this.SplitContainer2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.SplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SplitContainer2.Location = new System.Drawing.Point(0, 0);
            this.SplitContainer2.Name = "SplitContainer2";
            // 
            // SplitContainer2.Panel1
            // 
            this.SplitContainer2.Panel1.Controls.Add(this.chtSignals);
            // 
            // SplitContainer2.Panel2
            // 
            this.SplitContainer2.Panel2.Controls.Add(this.SplitContainer3);
            this.SplitContainer2.Size = new System.Drawing.Size(777, 542);
            this.SplitContainer2.SplitterDistance = 499;
            this.SplitContainer2.TabIndex = 0;
            // 
            // chtSignals
            // 
            chartArea1.AxisX.Interval = 1D;
            chartArea1.AxisX.IsLabelAutoFit = false;
            chartArea1.AxisX.LabelStyle.Angle = 90;
            chartArea1.AxisX.MajorGrid.Enabled = false;
            chartArea1.AxisY.MajorGrid.Enabled = false;
            chartArea1.Name = "ChartArea1";
            this.chtSignals.ChartAreas.Add(chartArea1);
            this.chtSignals.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chtSignals.Location = new System.Drawing.Point(0, 0);
            this.chtSignals.Name = "chtSignals";
            series1.ChartArea = "ChartArea1";
            series1.Name = "Signals";
            this.chtSignals.Series.Add(series1);
            this.chtSignals.Size = new System.Drawing.Size(495, 538);
            this.chtSignals.TabIndex = 0;
            this.chtSignals.Text = "Chart1";
            title1.Name = "Title1";
            title1.Text = "TitleText";
            this.chtSignals.Titles.Add(title1);
            this.chtSignals.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ChtSignalsMouseUp);
            // 
            // SplitContainer3
            // 
            this.SplitContainer3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.SplitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SplitContainer3.Location = new System.Drawing.Point(0, 0);
            this.SplitContainer3.Name = "SplitContainer3";
            this.SplitContainer3.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // SplitContainer3.Panel1
            // 
            this.SplitContainer3.Panel1.Controls.Add(this.gbxPicArray);
            // 
            // SplitContainer3.Panel2
            // 
            this.SplitContainer3.Panel2.Controls.Add(this.lvwSpotInfo);
            this.SplitContainer3.Size = new System.Drawing.Size(274, 542);
            this.SplitContainer3.SplitterDistance = 297;
            this.SplitContainer3.TabIndex = 0;
            // 
            // gbxPicArray
            // 
            this.gbxPicArray.Controls.Add(this.picArray);
            this.gbxPicArray.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbxPicArray.Location = new System.Drawing.Point(0, 0);
            this.gbxPicArray.Name = "gbxPicArray";
            this.gbxPicArray.Size = new System.Drawing.Size(270, 293);
            this.gbxPicArray.TabIndex = 1;
            this.gbxPicArray.TabStop = false;
            this.gbxPicArray.Text = "GroupBox1";
            // 
            // picArray
            // 
            this.picArray.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picArray.Location = new System.Drawing.Point(3, 16);
            this.picArray.Name = "picArray";
            this.picArray.Size = new System.Drawing.Size(264, 274);
            this.picArray.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picArray.TabIndex = 0;
            this.picArray.TabStop = false;
            this.picArray.SizeChanged += new System.EventHandler(this.PicArraySizeChanged);
            // 
            // lvwSpotInfo
            // 
            this.lvwSpotInfo.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader7,
            this.columnHeader12,
            this.columnHeader13,
            this.columnHeader14});
            this.lvwSpotInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvwSpotInfo.FullRowSelect = true;
            this.lvwSpotInfo.GridLines = true;
            this.lvwSpotInfo.Location = new System.Drawing.Point(0, 0);
            this.lvwSpotInfo.Name = "lvwSpotInfo";
            this.lvwSpotInfo.Size = new System.Drawing.Size(270, 237);
            this.lvwSpotInfo.TabIndex = 0;
            this.lvwSpotInfo.UseCompatibleStateImageBehavior = false;
            this.lvwSpotInfo.View = System.Windows.Forms.View.Details;
            this.lvwSpotInfo.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.LvwSpotInfoColumnClick);
            this.lvwSpotInfo.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.LvwSpotInfoItemSelectionChanged);
            this.lvwSpotInfo.MouseClick += new System.Windows.Forms.MouseEventHandler(this.LvwSpotInfoMouseClick);
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Sample";
            // 
            // columnHeader12
            // 
            this.columnHeader12.Text = "Signal";
            // 
            // columnHeader13
            // 
            this.columnHeader13.Text = "Average Signal";
            // 
            // columnHeader14
            // 
            this.columnHeader14.Text = "Marker Call";
            // 
            // ToolStrip1
            // 
            this.ToolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.ToolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripDropDownButton1,
            this.ddbView});
            this.ToolStrip1.Location = new System.Drawing.Point(3, 0);
            this.ToolStrip1.Name = "ToolStrip1";
            this.ToolStrip1.Size = new System.Drawing.Size(95, 25);
            this.ToolStrip1.TabIndex = 0;
            // 
            // ToolStripDropDownButton1
            // 
            this.ToolStripDropDownButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.ToolStripDropDownButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.SaveToolStripMenuItem,
            this.AboutToolStripMenuItem,
            this.ToolStripSeparator2,
            this.CloseMarkerThresholdReviewToolStripMenuItem,
            this.ToolStripSeparator1,
            this.ExitToolStripMenuItem});
            this.ToolStripDropDownButton1.Image = ((System.Drawing.Image)(resources.GetObject("ToolStripDropDownButton1.Image")));
            this.ToolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStripDropDownButton1.Name = "ToolStripDropDownButton1";
            this.ToolStripDropDownButton1.Size = new System.Drawing.Size(38, 22);
            this.ToolStripDropDownButton1.Text = "File";
            // 
            // SaveToolStripMenuItem
            // 
            this.SaveToolStripMenuItem.Image = global::ArrayTubeAnalyzer.Properties.Resources.Save;
            this.SaveToolStripMenuItem.Name = "SaveToolStripMenuItem";
            this.SaveToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.SaveToolStripMenuItem.Size = new System.Drawing.Size(239, 22);
            this.SaveToolStripMenuItem.Text = "Save";
            this.SaveToolStripMenuItem.Click += new System.EventHandler(this.SaveToolStripMenuItemClick);
            // 
            // ToolStripSeparator2
            // 
            this.ToolStripSeparator2.Name = "ToolStripSeparator2";
            this.ToolStripSeparator2.Size = new System.Drawing.Size(236, 6);
            // 
            // CloseMarkerThresholdReviewToolStripMenuItem
            // 
            this.CloseMarkerThresholdReviewToolStripMenuItem.Image = global::ArrayTubeAnalyzer.Properties.Resources.Delete;
            this.CloseMarkerThresholdReviewToolStripMenuItem.Name = "CloseMarkerThresholdReviewToolStripMenuItem";
            this.CloseMarkerThresholdReviewToolStripMenuItem.Size = new System.Drawing.Size(239, 22);
            this.CloseMarkerThresholdReviewToolStripMenuItem.Text = "Close Marker Threshold Review";
            this.CloseMarkerThresholdReviewToolStripMenuItem.Click += new System.EventHandler(this.CloseMarkerThresholdReviewToolStripMenuItemClick);
            // 
            // ToolStripSeparator1
            // 
            this.ToolStripSeparator1.Name = "ToolStripSeparator1";
            this.ToolStripSeparator1.Size = new System.Drawing.Size(236, 6);
            // 
            // ExitToolStripMenuItem
            // 
            this.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem";
            this.ExitToolStripMenuItem.Size = new System.Drawing.Size(239, 22);
            this.ExitToolStripMenuItem.Text = "Exit";
            this.ExitToolStripMenuItem.Click += new System.EventHandler(this.ExitToolStripMenuItemClick);
            // 
            // ddbView
            // 
            this.ddbView.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.ddbView.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.SampleQCToolStripMenuItem,
            this.MarkerThresholdAnalysisToolStripMenuItem,
            this.ReviewMarkerCallsToolStripMenuItem,
            this.ClusteringToolStripMenuItem,
            this.SampleDiagnosticsToolStripMenuItem});
            this.ddbView.Image = ((System.Drawing.Image)(resources.GetObject("ddbView.Image")));
            this.ddbView.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ddbView.Name = "ddbView";
            this.ddbView.Size = new System.Drawing.Size(45, 22);
            this.ddbView.Text = "View";
            // 
            // SampleQCToolStripMenuItem
            // 
            this.SampleQCToolStripMenuItem.Image = global::ArrayTubeAnalyzer.Properties.Resources.warning_16;
            this.SampleQCToolStripMenuItem.Name = "SampleQCToolStripMenuItem";
            this.SampleQCToolStripMenuItem.Size = new System.Drawing.Size(213, 22);
            this.SampleQCToolStripMenuItem.Text = "Sample QC";
            this.SampleQCToolStripMenuItem.Click += new System.EventHandler(this.SampleQCToolStripMenuItemClick);
            // 
            // MarkerThresholdAnalysisToolStripMenuItem
            // 
            this.MarkerThresholdAnalysisToolStripMenuItem.Image = global::ArrayTubeAnalyzer.Properties.Resources.Chart2;
            this.MarkerThresholdAnalysisToolStripMenuItem.Name = "MarkerThresholdAnalysisToolStripMenuItem";
            this.MarkerThresholdAnalysisToolStripMenuItem.Size = new System.Drawing.Size(213, 22);
            this.MarkerThresholdAnalysisToolStripMenuItem.Text = "Marker Threshold Analysis";
            this.MarkerThresholdAnalysisToolStripMenuItem.Click += new System.EventHandler(this.MarkerThresholdAnalysisToolStripMenuItemClick);
            // 
            // ReviewMarkerCallsToolStripMenuItem
            // 
            this.ReviewMarkerCallsToolStripMenuItem.Image = global::ArrayTubeAnalyzer.Properties.Resources.OK_All;
            this.ReviewMarkerCallsToolStripMenuItem.Name = "ReviewMarkerCallsToolStripMenuItem";
            this.ReviewMarkerCallsToolStripMenuItem.Size = new System.Drawing.Size(213, 22);
            this.ReviewMarkerCallsToolStripMenuItem.Text = "Review Marker Calls";
            this.ReviewMarkerCallsToolStripMenuItem.Click += new System.EventHandler(this.ReviewMarkerCallsToolStripMenuItemClick);
            // 
            // ClusteringToolStripMenuItem
            // 
            this.ClusteringToolStripMenuItem.Image = global::ArrayTubeAnalyzer.Properties.Resources.tree_16;
            this.ClusteringToolStripMenuItem.Name = "ClusteringToolStripMenuItem";
            this.ClusteringToolStripMenuItem.Size = new System.Drawing.Size(213, 22);
            this.ClusteringToolStripMenuItem.Text = "Clustering";
            this.ClusteringToolStripMenuItem.Click += new System.EventHandler(this.ClusteringToolStripMenuItemClick);
            // 
            // SampleDiagnosticsToolStripMenuItem
            // 
            this.SampleDiagnosticsToolStripMenuItem.Image = global::ArrayTubeAnalyzer.Properties.Resources.search_16;
            this.SampleDiagnosticsToolStripMenuItem.Name = "SampleDiagnosticsToolStripMenuItem";
            this.SampleDiagnosticsToolStripMenuItem.Size = new System.Drawing.Size(213, 22);
            this.SampleDiagnosticsToolStripMenuItem.Text = "Sample Diagnostics";
            this.SampleDiagnosticsToolStripMenuItem.Click += new System.EventHandler(this.SampleDiagnosticsToolStripMenuItemClick);
            // 
            // ColumnHeader2
            // 
            this.ColumnHeader2.Text = "Marker";
            // 
            // ColumnHeader3
            // 
            this.ColumnHeader3.Text = "Threshold";
            // 
            // ColumnHeader4
            // 
            this.ColumnHeader4.Text = "Threshold Type";
            // 
            // ColumnHeader8
            // 
            this.ColumnHeader8.Text = "Sample";
            // 
            // ColumnHeader9
            // 
            this.ColumnHeader9.Text = "Signal";
            // 
            // ColumnHeader10
            // 
            this.ColumnHeader10.Text = "Average Signal";
            // 
            // ColumnHeader11
            // 
            this.ColumnHeader11.Text = "Marker Call";
            // 
            // FrmMarkerThresholdReview
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1016, 567);
            this.Controls.Add(this.ToolStripContainer1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmMarkerThresholdReview";
            this.Text = "Array Tube Analyzer - Marker Threshold Review";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmMarkerThresholdReviewFormClosing);
            this.ToolStripContainer1.ContentPanel.ResumeLayout(false);
            this.ToolStripContainer1.TopToolStripPanel.ResumeLayout(false);
            this.ToolStripContainer1.TopToolStripPanel.PerformLayout();
            this.ToolStripContainer1.ResumeLayout(false);
            this.ToolStripContainer1.PerformLayout();
            this.SplitContainer1.Panel1.ResumeLayout(false);
            this.SplitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SplitContainer1)).EndInit();
            this.SplitContainer1.ResumeLayout(false);
            this.ContextMenuStrip1.ResumeLayout(false);
            this.ContextMenuStrip1.PerformLayout();
            this.SplitContainer2.Panel1.ResumeLayout(false);
            this.SplitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SplitContainer2)).EndInit();
            this.SplitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chtSignals)).EndInit();
            this.SplitContainer3.Panel1.ResumeLayout(false);
            this.SplitContainer3.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SplitContainer3)).EndInit();
            this.SplitContainer3.ResumeLayout(false);
            this.gbxPicArray.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picArray)).EndInit();
            this.ToolStrip1.ResumeLayout(false);
            this.ToolStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.ToolStripMenuItem AboutToolStripMenuItem;
        internal System.Windows.Forms.ToolStripContainer ToolStripContainer1;
        internal System.Windows.Forms.SplitContainer SplitContainer1;
        private ListViewFF lvwSpots;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        internal System.Windows.Forms.SplitContainer SplitContainer2;
        internal System.Windows.Forms.DataVisualization.Charting.Chart chtSignals;
        internal System.Windows.Forms.SplitContainer SplitContainer3;
        internal System.Windows.Forms.GroupBox gbxPicArray;
        internal System.Windows.Forms.PictureBox picArray;
        private ListViewFF lvwSpotInfo;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader12;
        private System.Windows.Forms.ColumnHeader columnHeader13;
        private System.Windows.Forms.ColumnHeader columnHeader14;
        internal System.Windows.Forms.ToolStrip ToolStrip1;
        internal System.Windows.Forms.ToolStripDropDownButton ToolStripDropDownButton1;
        internal System.Windows.Forms.ToolStripMenuItem SaveToolStripMenuItem;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator2;
        internal System.Windows.Forms.ToolStripMenuItem CloseMarkerThresholdReviewToolStripMenuItem;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator1;
        internal System.Windows.Forms.ToolStripMenuItem ExitToolStripMenuItem;
        internal System.Windows.Forms.ToolStripDropDownButton ddbView;
        internal System.Windows.Forms.ToolStripMenuItem SampleQCToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem MarkerThresholdAnalysisToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem ReviewMarkerCallsToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem ClusteringToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem SampleDiagnosticsToolStripMenuItem;
        internal System.Windows.Forms.ColumnHeader ColumnHeader2;
        internal System.Windows.Forms.ColumnHeader ColumnHeader3;
        internal System.Windows.Forms.ColumnHeader ColumnHeader4;
        internal System.Windows.Forms.ColumnHeader ColumnHeader8;
        internal System.Windows.Forms.ColumnHeader ColumnHeader9;
        internal System.Windows.Forms.ColumnHeader ColumnHeader10;
        internal System.Windows.Forms.ColumnHeader ColumnHeader11;
        internal System.Windows.Forms.ContextMenuStrip ContextMenuStrip1;
        internal System.Windows.Forms.ToolStripMenuItem ShowSpotsToolStripMenuItem;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator3;
        internal System.Windows.Forms.ToolStripMenuItem ThresholdToolStripMenuItem;
        internal System.Windows.Forms.ToolStripTextBox txtThreshold;
        private System.Windows.Forms.ToolStripMenuItem setThresholdManuallyToolStripMenuItem;
    }
}