﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace ArrayTubeAnalyzer
{
    public partial class FrmDiagnosis : Form
    {


        private readonly SampleDiagnosticCollection _sdc;
        
        private readonly SampleData _sd;

        readonly bool _allowListviewUpdate;

        public FrmDiagnosis(object objSampleData)
        {
            // This call is required by the designer.
            InitializeComponent();
            _sd = objSampleData as SampleData;

            // Add any initialization after the InitializeComponent() call.
            if (_sd != null)
            {
                TreatAmbiguousAs ambiguous = _sd.Ambiguous;

                _allowListviewUpdate = false;
                cbxAmbiguous.SelectedIndex = (int)ambiguous;
            }
            _allowListviewUpdate = true;

            if (_sd.SampleDiagnostics != null)
            {
                _sdc = _sd.SampleDiagnostics;
            } 
            else
            {
                _sdc = new SampleDiagnosticCollection(_sd);
                _sd.SetSampleDiagnostics(_sdc);
            }

            UpdateListview();
        }


        public void UpdateListview()
        {
            //setup columns
            _sdc.UpdateSampleDiagnosis();
            lvwProfiles.Columns.Clear();
            lvwProfiles.Columns.Add("Samples", 100);
            foreach (DiagnosisProfile diag in _sdc.Profiles.DiagnosisProfileList)
            {
                lvwProfiles.Columns.Add(diag.Name);
            }
            //populate listview with listviewitems
            var lvList = new List<ListViewItem>();
            foreach (SampleDiagnosis sample in _sdc.Samples)
            {
                var lvi = new ListViewItem(sample.Name) {UseItemStyleForSubItems = false};
                foreach (DiagnosisProfile diag in _sdc.Profiles.DiagnosisProfileList)
                {
                    if (sample.DiagnosticProfiles.Contains(diag))
                    {
                        lvi.SubItems.Add("1", Color.White, Color.Black, new Font(FontFamily.GenericSansSerif, 8));
                    }
                    else
                    {
                        lvi.SubItems.Add("0", Color.Black, Color.White, new Font(FontFamily.GenericSansSerif, 8));
                    }
                }
                lvList.Add(lvi);
            }
            lvwProfiles.Items.Clear();
            lvwProfiles.Items.AddRange(lvList.ToArray());
        }

        private void AddDiagnosticProfile()
        {
            var dp = new DiagnosisProfile(_sd.MarkerCount);
            var f = new frmProfileCreation(_sd, dp);
            if (f.ShowDialog() == DialogResult.OK)
            {
                _sdc.AddDiagnosticProfile(dp);
                UpdateListview();
            }
        }

        private void AddDiagnosisProfileToolStripMenuItemClick(Object sender, EventArgs e)
        {
            AddDiagnosticProfile();
        }

        private void RemoveDiagnosticProfile()
        {
            var lNames = new List<string>();
            var indices = new List<int>();

            foreach (DiagnosisProfile dp in _sdc.Profiles.DiagnosisProfileList)
            {
                lNames.Add(dp.Name);
            }

            if (Misc.ListviewSelect("Remove Diagnostic Profile(s)?",
                "Which diagnostic profiles would you like to remove?",
                "Diagnostic profiles",
                lNames.ToArray(),
                ref indices,
                true) == DialogResult.OK)
            {
                _sdc.RemoveDiagnosticProfiles(indices);
            }
            UpdateListview();
        }

        private void RemoveDiagnosisProfileToolStripMenuItemClick(Object sender, EventArgs e)
        {
            RemoveDiagnosticProfile();
        }

        private void ChangeDiagProfile()
        {
            var lNames = new List<string>();
            var indices = new List<int> {0};
            foreach (DiagnosisProfile dp in _sdc.Profiles.DiagnosisProfileList)
            {
                lNames.Add(dp.Name);
            }

            if (Misc.ListviewSelect("Select Diagnostic Profile To Modify",
                "Which diagnostic profile would you like to modify?",
                "Diagnostic profiles",
                lNames.ToArray(),
                ref indices,
                false) == DialogResult.OK)
            {
                if (indices.Count > 0)
                {
                    var dp = new DiagnosisProfile(_sdc.Profiles.DiagnosisProfileList[indices[0]]);
                    var f = new frmProfileCreation(_sd, dp);
                    if (f.ShowDialog() == DialogResult.OK)
                    {
                        _sdc.ChangeDiagnosticProfile(indices[0], dp);
                    }
                }
            }
            UpdateListview();
        }

        private void ChangeDiagnosisProfileToolStripMenuItemClick(Object sender, EventArgs e)
        {
            ChangeDiagProfile();
        }

        private void ImportProfiles()
        {
            using (var ofd = new OpenFileDialog())
            {
                ofd.Filter = "ATA Diagnostic Profiles (*.atadiag)|*.atadiag|All Files (*.*)|*.*";
                ofd.Multiselect = true;
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    foreach (string filename in ofd.FileNames)
                    {
                        if (!_sdc.ReadDiagnosticProfiles(filename, _sd.MarkerCount))
                        {
                            MessageBox.Show("Incorrect format for '" + filename + "'. Please specify valid ATA diagnostic profile files.");
                        }
                    }
                    UpdateListview();
                }
            }
        }

        private void ImportProfilesToolStripMenuItemClick(Object sender, EventArgs e)
        {
            ImportProfiles();
        }


        private void ExportProfiles()
        {
            var lNames = new List<string>();
            foreach (DiagnosisProfile dp in _sdc.Profiles.DiagnosisProfileList)
            {
                lNames.Add(dp.Name);
            }
            var indices = new List<int>();
            if (Misc.ListviewSelect("Select Profiles To Export",
                "Which diagnostic profiles would you like to export?",
                "Diagnostic Profiles",
                lNames.ToArray(),
                ref indices,
                true) == DialogResult.OK)
            {
                if (indices.Count > 0)
                {
                    using (var sfd = new SaveFileDialog())
                    {
                        sfd.Title = "Specify save filename for diagnostic profiles";
                        sfd.Filter = "ATA Diagnostic Profiles (*.atadiag)|*.atadiag|All Files (*.*)|*.*";
                        if (sfd.ShowDialog() == DialogResult.OK)
                        {
                            _sdc.WriteDiagnosticProfiles(_sd.Markers.ToList(), indices, sfd.FileName);
                        }
                    }
                }
            }
        }

        private void ExportProfilesToolStripMenuItemClick(Object sender, EventArgs e)
        {
            ExportProfiles();
        }

        private void SaveSampleInfo()
        {
            using (var sfd = new SaveFileDialog())
            {
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    _sdc.WriteSampleInfo(sfd.FileName);
                }
            }
        }

        private void SaveSampleInfoToolStripMenuItemClick(Object sender, EventArgs e)
        {
            SaveSampleInfo();
        }

        private void FrmDiagnosisFormClosing(object sender, FormClosingEventArgs e)
        {
            _sd.Forms.Diagnosis = null;
        }

        private void CbxAmbiguousSelectedIndexChanged(object sender, EventArgs e)
        {
            if (_allowListviewUpdate)
            {
                int index = cbxAmbiguous.SelectedIndex;
                switch (index)
                {
                    case 0:
                        _sd.Ambiguous = TreatAmbiguousAs.ThresholdDetermined;
                        break;
                    case 1:
                        _sd.Ambiguous = TreatAmbiguousAs.Positive;
                        break;
                    case 2:
                        _sd.Ambiguous = TreatAmbiguousAs.Negative;
                        break;
                }
                _sd.ClusterUPGMA();
                if (_sd.Forms.Clustering != null)
                {
                    _sd.Forms.Clustering.UpdateForm();
                }
                UpdateListview();
            }
        }

        private void CbxAmbiguousMarkersKeyPress(Object sender, KeyPressEventArgs e)
        {//don't allow the combobox items to be modified
            e.Handled = true;
        }

    }

}
