﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;

namespace ArrayTubeAnalyzer
{
    public partial class FrmShowMarkerSpots : Form
    {
        private readonly SpotImageCollection _spotImages;

        private SpotMarkerArrayIndices _indices;
        private readonly List<PictureBox> _picBoxes = new List<PictureBox>();

        private readonly List<RichTextBox> _textBoxes = new List<RichTextBox>();

        private readonly SampleData _sd;

        private readonly bool _showDiscardedReplicates;

        public FrmShowMarkerSpots(SampleData sampleData)
        {
            // This call is required by the designer.
            InitializeComponent();
            Deactivate += FrmShowMarkerSpotsDeactivated;
            Activated += FrmShowMarkerSpotsActivated;
            // Add any initialization after the InitializeComponent() call.
            _sd = sampleData;
            _sd.UpdateCalls();
            cbxCall.Items.AddRange(new string[] {
			"Auto",
			"Present",
			"Absent",
			"Ambiguous"
		});
        }

        /// <summary>Find all of the replicates for a marker and sort by a specified parameter.</summary>
        /// <param name="sampleData">SampleData object.</param>
        /// <param name="markerIndex">Marker index.</param>
        /// <param name="sortBy">What to sort spot images by.</param>
        public FrmShowMarkerSpots(SampleData sampleData, int markerIndex, SpotImageCrop.Sorting sortBy)
            : this(sampleData)
        {
            _spotImages = new SpotImageCollection(
                _sd,
                _sd.SampleStats,
                _sd.Images,
                _sd.Calibration,
                markerIndex,
                _sd.Spots[markerIndex].ToArray(),
                _sd.Spots
                );

            if (sortBy == SpotImageCrop.Sorting.BySignal)
            {
                _spotImages.SortBySignal();
            }
            else if (sortBy == SpotImageCrop.Sorting.ByFinalAverageSignal)
            {
                _spotImages.SortByFinalAverageSignal();
            }
            else
            {
                _spotImages.SortBySpot();
            }

            CreateSpotPictures();
            Text = string.Format("Spot Distribution - {0}", _sd.Markers[markerIndex]);
        }

        /// <summary>Show marker strip for a sample where the replicate spots for each marker are stacked. Spawned at the Sample QC, Review Marker Calls, Clustering windows.</summary>
        /// <param name="sampleData">Sample data object.</param>
        /// <param name="sampleIndex">Sample index</param>
        /// <param name="showClumpedSampleSpotView">Dummy variable to distinguish this constructor from the others.</param>
        public FrmShowMarkerSpots(SampleData sampleData, int sampleIndex, bool showClumpedSampleSpotView)
            : this(sampleData)
        {
            _spotImages = new SpotImageCollection(
                _sd,
                sampleIndex,
                _sd.SampleStats[sampleIndex],
                _sd.GetImage(sampleIndex),
                _sd.GetCalibrationInfo(sampleIndex),
                _sd.Spots,
                _sd.SampleCalls[sampleIndex]);
            CreateClumpedReplicatePictures();
            Text = string.Format("Marker Strip - {0}", _sd.SampleNames[sampleIndex]);
        }

        /// <summary>Get all replicates for a marker and sort by average signal and clump corresponding replicates together. Spawned at the Marker Threshold Analysis window.</summary>
        /// <param name="sampleData">SampleData object.</param>
        /// <param name="markerIndex">Marker index.</param>
        public FrmShowMarkerSpots(SampleData sampleData, int markerIndex)
            : this(sampleData)
        {
            _spotImages = new SpotImageCollection(
                _sd,
                _sd.SampleStats,
                _sd.Images,
                _sd.Calibration,
                markerIndex,
                _sd.Spots[markerIndex].ToArray(),
                _sd.Spots);
            _spotImages.SortByFinalAverageSignal();
            _showDiscardedReplicates = true;
            CreateClumpedReplicatePictures();
            Text = string.Format("Marker Strip - {0}", _sd.Markers[markerIndex]);
        }

        /// <summary>Get the unsorted clumped marker replicate marker spots. The order will be that of the samples. Spawned at the Marker Threshold Review window.</summary>
        /// <param name="sampleData">SampleData object.</param>
        /// <param name="markerIndex">Marker index.</param>
        /// <param name="notSorted">Dummy variable to distinguish this constructor from the others.</param>
        public FrmShowMarkerSpots(SampleData sampleData, int markerIndex, int notSorted)
            : this(sampleData)
        {
            _spotImages = new SpotImageCollection(
                _sd,
                _sd.SampleStats,
                _sd.Images,
                _sd.Calibration,
                markerIndex,
                _sd.Spots[markerIndex].ToArray(),
                _sd.Spots);
            _showDiscardedReplicates = true;
            CreateClumpedReplicatePictures();
            Text = string.Format("Marker Strip - {0}", _sd.Markers[markerIndex]);
        }

        public override sealed string Text
        {
            get { return base.Text; }
            set { base.Text = value; }
        }

        private void CreateSpotPictures()
        {
            int iX = 1;
            foreach (SpotImageCrop s in _spotImages.SpotImages)
            {
                CreatePictureBoxesAndTextBox(s, ref iX);
            }
        }

        private void CreateClumpedReplicatePictures()
        {
            int iX = 1;
            var li = new List<int>();
            //indices of sic.SpotImages in an int list
            for (int i = 0; i <= _spotImages.SpotImages.Count - 1; i++)
            {
                li.Add(i);
            }

            var discardedSpots = new List<int>();
            for (int i = 0; i < _sd.DiscardedMarkers.Count; i++)
            {
                if (_sd.DiscardedMarkers[i])
                {
                    discardedSpots.Add(i);
                }
            }

            //create spot pictures for each marker
            while (li.Count > 0)
            {
                var lSic = new List<SpotImageCrop>();//list to hold all spotimagecrop objects of the same gene and array
                int index = li[0];//use the indices from the int list
                li.RemoveAt(0);//remove the index for the spotimagecrop that has been added to the spotimagecrop list; reduce search space
                SpotImageCrop s = _spotImages.SpotImages[index];
                if (!discardedSpots.Contains(s.Gene) || _showDiscardedReplicates)
                {
                    lSic.Add(s);
                }
                bool bFound = true;
                //while something is found to put into lSic
                while (bFound)
                {
                    bFound = false;
                    foreach (int i in li)
                    {
                        SpotImageCrop si = _spotImages.SpotImages[i];
                        //check if it's the same gene and same array
                        if (!(si.Gene == s.Gene & si.Array == s.Array)) continue;
                        bFound = true;
                        //something has been found
                        li.Remove(i);
                        //remove the spotimagecrop object index
                        if (!discardedSpots.Contains(si.Gene) || _showDiscardedReplicates)
                            lSic.Add(si);
                        break; // TODO: might not be correct. Was : Exit For
                    }
                }
                //when no more spotimagecrop objects can be added to the list then create the pictures
                if (lSic.Count > 0)
                    CreatePictureBoxesAndTextBox(lSic, ref iX);
            }
        }

        private void CreatePictureBoxesAndTextBox(List<SpotImageCrop> ls, ref int iX)
        {
            int iY = 1;
            var rtb = new RichTextBox
                          {
                              Parent = SplitContainer1.Panel1,
                              BorderStyle = BorderStyle.FixedSingle,
                              Location = new Point(iX, iY),
                              Size = new Size(80, 66),
                              Text =
                                  string.Format("{0}\n{1}\n{2}",
                                  _sd.SampleNames[ls[0].Array],
                                  _sd.Markers[ls[0].Gene],
                                  _sd.SampleStats[ls[0].Array][ls[0].Gene].FinalAverageSignal.ToString("0.0000"))
                          };
            rtb.Tag = rtb.Text;
            rtb.TextChanged += RtbTextChanged;
            _textBoxes.Add(rtb);
            iY += rtb.Size.Height;

            foreach (SpotImageCrop s in ls)
            {
                var p = new PictureBox();
                _indices = new SpotMarkerArrayIndices(s.Gene, _sd.Spots[s.Gene].IndexOf(s.Spot), s.Array);
                MarkerInfo at = _sd.SampleStats[_indices.Array][_indices.Marker];
                p.Parent = SplitContainer1.Panel1;
                p.BorderStyle = BorderStyle.FixedSingle;
                p.Location = new Point(iX, iY);
                p.Size = new Size(80, 80);
                p.SizeMode = PictureBoxSizeMode.StretchImage;
                string sToolTipText = string.Format("Sample: {0}\nMarker: {1}\nSpot: {2}\nSignal: {3}\nAverage Signal: {4}\nFinal Average Signal: {5}\nFlagging: {6}",
                    _sd.SampleNames[s.Array],
                    _sd.Markers[s.Gene],
                    s.Spot,
                    s.Signal.ToString("0.0000"),
                    at.AverageSignal.ToString("0.0000"),
                    at.FinalAverageSignal.ToString("0.0000"),
                    at.Flag[_indices.Spot]);
                ToolTip1.SetToolTip(p, sToolTipText);
                p.Show();
                p.Tag = _indices;
                p.ContextMenuStrip = ContextMenuStrip1;
                var bmp = new Bitmap(80, 80);
                Graphics g = Graphics.FromImage(bmp);
                var f = new Font(FontFamily.GenericSansSerif, 8);
                Brush b = Brushes.Black;
                Color colorBorder = default(Color);
                Calls markerCall = _sd.SampleCalls[_indices.Array].Calls[_indices.Marker];
                switch (markerCall)
                {
                    case Calls.Absent:
                        colorBorder = _sd.Settings.C.AbsentColour;
                        break;
                    case Calls.Present:
                        colorBorder = _sd.Settings.C.PresentColour;
                        break;
                    case Calls.AbsentAuto:
                        colorBorder = _sd.Settings.C.AbsentAutoColour;
                        break;
                    case Calls.AmbiguousAbsent:
                    case Calls.AmbiguousManual:
                        colorBorder = _sd.Settings.C.AbsentFlaggedColour;
                        break;
                    case Calls.AmbiguousPresent:
                        colorBorder = _sd.Settings.C.PresentFlaggedColour;
                        break;
                    case Calls.PresentAuto:
                        colorBorder = _sd.Settings.C.PresentAutoColour;
                        break;
                }
                g.FillRectangle(new SolidBrush(colorBorder), 0, 0, 80, 80);
                g.DrawImage(s.ImageAT, 1, 1, 78, 78);
                g.DrawString(s.Spot.ToString(CultureInfo.InvariantCulture), f, b, new PointF(1, 1));

                if (at.Flag[_indices.Spot] == Flagging.Discard)
                {
                    var topRight = new PointF(79 - g.MeasureString("Discarded", f).Width, 1);
                    g.DrawString("Discarded", f, Brushes.Red, topRight);
                }

                var pBottomLeft = new PointF(1, 79 - g.MeasureString(s.Signal.ToString(CultureInfo.InvariantCulture), f).Height);
                g.DrawString(s.Signal.ToString(CultureInfo.InvariantCulture), f, b, pBottomLeft);
                p.Image = bmp;
                _picBoxes.Add(p);
                iY += p.Size.Height;
            }
            iX += 81;
            if (Size.Height < (iY + 80))
            {
                Size = new Size(Size.Width, iY + 100);
            }
        }

        private void CreatePictureBoxesAndTextBox(SpotImageCrop s, ref int iX)
        {
            var p = new PictureBox();
            _indices = new SpotMarkerArrayIndices(s.Gene, _sd.Spots[s.Gene].IndexOf(s.Spot), s.Array);
            MarkerInfo at = _sd.SampleStats[_indices.Array][_indices.Marker];

            p.Parent = SplitContainer1.Panel1;
            p.BorderStyle = BorderStyle.FixedSingle;
            p.Location = new Point(iX, 1);
            p.Size = new Size(80, 80);

            p.SizeMode = PictureBoxSizeMode.StretchImage;

            string sToolTipText = string.Format("Sample: {0}\nMarker: {1}\nSpot: {2}\nSignal: {3}\nAverage Signal: {4}\nFinal Average Signal: {5}\nFlagging: {6}",
                _sd.SampleNames[s.Array],
                _sd.Markers[s.Gene],
                s.Spot,
                s.Signal.ToString("0.0000"),
                at.AverageSignal.ToString("0.0000"),
                at.FinalAverageSignal.ToString("0.0000"),
                at.Flag[_indices.Spot]);
            ToolTip1.SetToolTip(p, sToolTipText);
            p.Show();
            p.Tag = _indices;
            p.ContextMenuStrip = ContextMenuStrip1;
            var bmp = new Bitmap(80, 80);
            Graphics g = Graphics.FromImage(bmp);
            var f = new Font(FontFamily.GenericSansSerif, 8);
            Brush b = Brushes.Black;
            Color colorBorder = default(Color);
            Calls markerCall = _sd.SampleCalls[_indices.Array].Calls[_indices.Marker];
            switch (markerCall)
            {
                case Calls.Absent:
                    colorBorder = _sd.Settings.C.AbsentColour;
                    break;
                case Calls.Present:
                    colorBorder = _sd.Settings.C.PresentColour;
                    break;
                case Calls.AbsentAuto:
                    colorBorder = _sd.Settings.C.AbsentAutoColour;
                    break;
                case Calls.AmbiguousAbsent:
                case Calls.AmbiguousManual:
                    colorBorder = _sd.Settings.C.AbsentFlaggedColour;
                    break;
                case Calls.AmbiguousPresent:
                    colorBorder = _sd.Settings.C.PresentFlaggedColour;
                    break;
                case Calls.PresentAuto:
                    colorBorder = _sd.Settings.C.PresentAutoColour;
                    break;
            }
            g.FillRectangle(new SolidBrush(colorBorder), 0, 0, 80, 80);
            g.DrawImage(s.ImageAT, 1, 1, 78, 78);
            g.DrawString(s.Spot.ToString(CultureInfo.InvariantCulture), f, b, new PointF(1, 1));

            if (at.Flag[_indices.Spot] == Flagging.Discard)
            {
                var topRight = new PointF(79 - g.MeasureString("Discarded", f).Width, 1);
                g.DrawString("Discarded", f, Brushes.Red, topRight);
            }

            var pBottomLeft = new PointF(1, 79 - g.MeasureString(s.Signal.ToString(CultureInfo.InvariantCulture), f).Height);
            g.DrawString(s.Signal.ToString(CultureInfo.InvariantCulture), f, b, pBottomLeft);
            p.Image = bmp;
            _picBoxes.Add(p);
            var rtb = new RichTextBox
                          {
                              Parent = SplitContainer1.Panel1,
                              BorderStyle = BorderStyle.FixedSingle,
                              Location = new Point(p.Location.X, p.Location.Y + p.Size.Height),
                              Size = new Size(p.Size.Width, 66),
                              Text =
                                  string.Format("{0}\n{1}\n{2}", _sd.SampleNames[s.Array], _sd.Markers[s.Gene],
                                                at.Flag[_indices.Spot])
                          };
            rtb.Tag = rtb.Text;
            _textBoxes.Add(rtb);
            rtb.TextChanged += RtbTextChanged;

            iX += 81;
        }

        private static void RtbTextChanged(object sender, EventArgs e)
        {
            var rtb = sender as RichTextBox;
            if (rtb != null) rtb.Text = (string)rtb.Tag;
        }

        private void CbxCallSelectedIndexChanged(object sender, EventArgs e)
        {
            int index = cbxCall.SelectedIndex;
            Calls markerCall = default(Calls);
            switch (index)
            {
                case 0:
                    markerCall = Calls.AmbiguousAbsent;
                    break;
                case 1:
                    markerCall = Calls.Present;
                    break;
                case 2:
                    markerCall = Calls.Absent;
                    break;
                case 3:
                    markerCall = Calls.AmbiguousManual;
                    break;
            }
            _sd.SampleCalls[_indices.Array].Calls[_indices.Marker] = markerCall;

            //remake images for all of the pictures from the same gene
            RecreateSpotPics();

            //change textbox contents
            ChangeTextboxContents();

            UpdateForms();

        }

        private void ChkDiscardCheckedChanged(object sender, EventArgs e)
        {
            if (!_chkDiscardTriggerCheckedEvent) return;
            Calls markerCall = _sd.SampleCalls[_indices.Array].Calls[_indices.Marker];
            if (chkDiscard.Checked)
            {
                _sd.SampleStats[_indices.Array][_indices.Marker].Flag[_indices.Spot] = Flagging.Discard;
            }
            else
            {
                _sd.SampleStats[_indices.Array][_indices.Marker].Flag[_indices.Spot] = Flagging.KeepManual;
            }

            _sd.RecalculateFinalAverageSignal(_indices);

            _sd.Forms.SampleQC.UpdatePicAndListview(_indices.Array);


            if (markerCall != Calls.Absent | markerCall != Calls.Present | markerCall != Calls.AmbiguousManual)
            {
                Calls geneCall;

                MarkerThresholdInfo thresholdInfo = _sd.Thresholds.Markers[_indices.Marker];
                double finalAverageSignal = _sd.SampleStats[_indices.Array][_indices.Marker].FinalAverageSignal;
                if (finalAverageSignal >= thresholdInfo.UpperThreshold)
                {
                    geneCall = Calls.PresentAuto;
                }
                else if (finalAverageSignal >= thresholdInfo.Threshold)
                {
                    geneCall = Calls.AmbiguousPresent;
                }
                else if (finalAverageSignal >= thresholdInfo.LowerThreshold)
                {
                    geneCall = Calls.AmbiguousAbsent;
                }
                else
                {
                    geneCall = Calls.AbsentAuto;
                }

                if (markerCall != geneCall)
                {
                    _sd.SampleCalls[_indices.Array].Calls[_indices.Marker] = geneCall;

                    UpdateForms();

                }

            }

            //remake all pictures for the selected gene
            RecreateSpotPics();

            //change textbox contents
            ChangeTextboxContents();
        }

        private void RecreateSpotPics()
        {
            MarkerInfo at = _sd.SampleStats[_indices.Array][_indices.Marker];
            foreach (PictureBox p in _picBoxes)
            {
                var i = (SpotMarkerArrayIndices)p.Tag;
                if (i.Array == _indices.Array & i.Marker == _indices.Marker)
                {
                    string sToolTipText = string.Format("Array: {0}\nGene: {1}\nSpot: {2}\nSignal: {3}\nAverage Signal: {4}\nFinal Average Signal: {5}\nFlagging: {6}",
                        _sd.SampleNames[i.Array],
                        _sd.Markers[i.Marker],
                        _sd.Spots[i.Marker][i.Spot],
                        at.Signal[i.Spot].ToString("0.0000"),
                        at.AverageSignal.ToString("0.0000"),
                        at.FinalAverageSignal.ToString("0.0000"),
                        at.Flag[i.Spot]);
                    ToolTip1.SetToolTip(p, sToolTipText);

                    var bmp = new Bitmap(80, 80);
                    Graphics g = Graphics.FromImage(bmp);
                    var f = new Font(FontFamily.GenericSansSerif, 8);
                    Brush b = Brushes.Black;
                    Color colorBorder;
                    Calls geneCall = _sd.SampleCalls[_indices.Array].Calls[_indices.Marker];
                    switch (geneCall)
                    {
                        case Calls.Present:
                            colorBorder = _sd.Settings.C.PresentColour;
                            break;
                        case Calls.Absent:
                            colorBorder = _sd.Settings.C.AbsentColour;
                            break;
                        case Calls.AmbiguousManual:
                            colorBorder = _sd.Settings.C.AbsentFlaggedColour;
                            break;
                        default:
                            {
                                MarkerThresholdInfo thresholdInfo = _sd.Thresholds.Markers[i.Marker];
                                double avg = at.FinalAverageSignal;
                                if (avg < thresholdInfo.LowerThreshold)
                                {
                                    colorBorder = _sd.Settings.C.AbsentAutoColour;
                                }
                                else if (avg < thresholdInfo.Threshold)
                                {
                                    colorBorder = _sd.Settings.C.AbsentFlaggedColour;
                                }
                                else if (avg < thresholdInfo.UpperThreshold)
                                {
                                    colorBorder = _sd.Settings.C.PresentFlaggedColour;
                                }
                                else
                                {
                                    colorBorder = _sd.Settings.C.PresentAutoColour;
                                }
                            }
                            break;
                    }
                    g.FillRectangle(new SolidBrush(colorBorder), 0, 0, 80, 80);

                    Image imgAT = GetSpotImage(i);

                    g.DrawImage(imgAT, 1, 1, 78, 78);
                    g.DrawString(_sd.Spots[i.Marker][i.Spot].ToString(CultureInfo.InvariantCulture), f, b, new PointF(1, 1));

                    if (at.Flag[i.Spot] == Flagging.Discard)
                    {
                        var topRight = new PointF(79 - g.MeasureString("Discarded", f).Width, 1);
                        g.DrawString("Discarded", f, Brushes.Red, topRight);
                    }
                    string sSignal = at.Signal[i.Spot].ToString(CultureInfo.InvariantCulture);
                    var pBottomLeft = new PointF(1, 79 - g.MeasureString(sSignal, f).Height);
                    g.DrawString(sSignal, f, b, pBottomLeft);
                    p.Image = bmp;
                }
            }

        }

        private void ChangeTextboxContents()
        {
            string sArray = _sd.SampleNames[_indices.Array];
            string sGene = _sd.Markers[_indices.Marker];
            foreach (RichTextBox t in _textBoxes)
            {
                if (t.Text.Contains(sArray) & t.Text.Contains(sGene))
                {
                    if (_textBoxes.Count == _picBoxes.Count)
                    {
                        var i = (SpotMarkerArrayIndices)_picBoxes[_textBoxes.IndexOf(t)].Tag;
                        t.Tag = sArray + "\n" + sGene + "\n" + _sd.SampleStats[i.Array][i.Marker].Flag[i.Spot].ToString();
                    }
                    else
                    {
                        t.Tag = sArray + "\n" + sGene + "\n" + _sd.SampleStats[_indices.Array][_indices.Marker].FinalAverageSignal.ToString("0.0000");
                    }
                    t.Text = (string)t.Tag;
                }
            }
        }

        private Bitmap GetSpotImage(SpotMarkerArrayIndices i)
        {
            foreach (SpotImageCrop s in _spotImages.SpotImages)
            {
                if (s.Gene == i.Marker & s.Array == i.Array & s.Spot == _sd.Spots[i.Marker][i.Spot])
                {
                    return s.ImageAT;
                }
            }
            return null;
        }

        private bool _chkDiscardTriggerCheckedEvent;

        private void ContextMenuStrip1Opening(object sender, CancelEventArgs e)
        {
            var p = (PictureBox)ContextMenuStrip1.SourceControl;
            _indices = (SpotMarkerArrayIndices)p.Tag;
            _chkDiscardTriggerCheckedEvent = false;
            chkDiscard.Checked = (_sd.SampleStats[_indices.Array][_indices.Marker].Flag[_indices.Spot] == Flagging.Discard);
            //chkDiscardMarker.Checked = _sd.DiscardedMarkers[_indices.Marker];
            _chkDiscardTriggerCheckedEvent = true;
        }

        private void UpdateForms()
        {
            _sd.UpdateCalls();
            _sd.ClusterUPGMA();
            _sd.Forms.SampleQC.UpdatePicAndListview(0);

            if (_sd.Forms.MarkerThresholdAnalysis != null)
            {
                _sd.Forms.MarkerThresholdAnalysis.UpdateForm();
            }
            if (_sd.Forms.MarkerThresholdReview != null)
            {
                _sd.Forms.MarkerThresholdReview.UpdateForm();
            }
            if (_sd.Forms.ReviewMarkerCalls != null)
            {
                _sd.Forms.ReviewMarkerCalls.UpdateForm(_indices.Array);
            }
            if (_sd.Forms.Clustering != null)
            {
                _sd.Forms.Clustering.UpdateForm();
            }
            if (_sd.Forms.Diagnosis != null)
            {
                _sd.Forms.Diagnosis.UpdateListview();
            }
        }

        #region "Scroll bar - make sure it doesn't go back to the initial position after form loses and regains focus"

        private Point _scrollPoint = new Point(0, 0);
        private void FrmShowMarkerSpotsActivated(object sender, EventArgs e)
        {
            SplitContainer1.Panel1.AutoScrollPosition = new Point(Math.Abs(_scrollPoint.X), _scrollPoint.Y);
        }

        private void Panel1Scroll(object sender, ScrollEventArgs e)
        {
            if (e.ScrollOrientation == ScrollOrientation.HorizontalScroll)
            {
                _scrollPoint = SplitContainer1.Panel1.AutoScrollPosition;
            }
        }

        private void FrmShowMarkerSpotsDeactivated(object sender, EventArgs e)
        {
            _scrollPoint = SplitContainer1.Panel1.AutoScrollPosition;
        }
        #endregion

        private void FrmShowMarkerSpotsFormClosing(object sender, FormClosingEventArgs e)
        {
            _sd.Forms.ShowMarkerSpotsWindowClosed(this);
        }

        private void ChkDiscardMarkerCheckedChanged(object sender, EventArgs e)
        {
            if (_chkDiscardTriggerCheckedEvent)
            {
                _sd.DiscardMarker(_indices.Marker, chkDiscardMarker.Checked);
                UpdateForms();
            }
        }

    }

}
