﻿using System;
using System.Collections.Generic;

namespace ArrayTubeAnalyzer
{

    /// <summary>Marker threshold info and graphing data.</summary>
    [Serializable]
    public class MarkerThresholdInfo
    {
        public string MarkerName
        {
            get { return _sd.GetMarkerName(MarkerIndex); }
        }
        private readonly int _markerIndex;
        /// <summary>Marker index.</summary>
        public int MarkerIndex
        {
            get { return _markerIndex; }
        }

        public string MarkerComment
        {
            get { return _sd.GetMarkerComment(_markerIndex); }
            set { _sd.SetMarkerComment(_markerIndex, value); }
        }

        private List<double> _signals = new List<double>();
        /// <summary>All of the signal values for marker in dataset samples.</summary>
        public List<double> Signals
        {
            get { return _signals; }
        }

        private readonly List<int> _frequencies = new List<int>();
        /// <summary>Binned frequencies of signal values.</summary>
        public List<int> Frequencies
        {
            get { return _frequencies; }
        }

        private bool _thresholdsSet;

        private double _lowerThreshold = -1;
        public double LowerThreshold
        {
            get
            {//if lower threshold is set to -1 or if it isn't set (_thresholdsSet == false)
                if (Math.Abs(_lowerThreshold - -1) < EPSILON || !_thresholdsSet || ThresholdType != ThresholdType.UserDefined)
                {
                    GetThreshold();
                    double errorThreshold = _sd.GetThresholdFlagRange();
                    _lowerThreshold = Threshold - errorThreshold;
                    _upperThreshold = Threshold + errorThreshold;
                    _thresholdsSet = true;
                }
                return _lowerThreshold;
            }
            set { _lowerThreshold = value; }
        }

        private double _upperThreshold = -1;
        public double UpperThreshold
        {
            get
            {//if upper threshold is set to -1 or if it isn't set (_thresholdsSet == false)
                if (Math.Abs(_upperThreshold - -1) < EPSILON || !_thresholdsSet || ThresholdType != ThresholdType.UserDefined)
                {
                    GetThreshold();
                    double errorThreshold = _sd.GetThresholdFlagRange();
                    _lowerThreshold = Threshold - errorThreshold;
                    _upperThreshold = Threshold + errorThreshold;
                    _thresholdsSet = true;
                }
                return _upperThreshold;
            }
            set { _upperThreshold = value; }
        }

        private double _threshold;
        public double GetThreshold()
        {
            if (ThresholdType == ThresholdType.GlobalBackground)
            {
                _threshold = _sd.Settings.GlobalBackgroundFactor * _thresholds.GlobalBackground;
            }
            else if (ThresholdType == ThresholdType.PercentOfPositiveSignal)
            {
                _threshold = AveragePositiveSignal * _sd.Settings.PositiveSignalFactor;
                //        Else 'ThresholdType.UserDefined
            }
            return _threshold;
        }

        public void SetUserDefinedThreshold(double threshold)
        {
            ThresholdType = ThresholdType.UserDefined;
            _threshold = threshold;
        }

        /// <summary>Threshold for determining marker call.</summary>
        public double Threshold
        {
            get { return GetThreshold(); }
        }
        /// <summary>The type of threshold specified for this marker.</summary>
        public ThresholdType ThresholdType { get; set; }
        /// <summary>Number of instances that a marker is flagged or ambiguous across all samples.</summary>
        public int FlaggedCount
        {
            get { return GetFlaggedCount(); }
        }

        private int GetFlaggedCount()
        {
            int countFlagged = 0;
            List<List<MarkerInfo>> samples = _sd.SampleStats;
            for (int i = 0; i <= samples.Count - 1; i++)
            {
                MarkerInfo marker = samples[i][_markerIndex];
                double signal = marker.FinalAverageSignal;
                if (signal >= LowerThreshold & signal <= UpperThreshold)
                {
                    countFlagged += 1;
                }
            }
            return countFlagged;
        }
        //DetermineGeneFlagging


        private double _averagePositiveSignal = -1;
        public double CalcAveragePositiveSignal()
        {
            var lSignal = new List<double>();
            var limit = (int)(Signals.Count * _sd.Settings.PositiveSignalCutoff);
            int start = Signals.Count - 1;
            while (lSignal.Count < limit)
            {
                lSignal.Add(Signals[start]);
                start -= 1;
            }
            _averagePositiveSignal = Misc.Average(lSignal);
            return _averagePositiveSignal;
        }
        /// <summary>Average positive signal for the gene using an user-defined % of top positive signals for gene.</summary>
        public double AveragePositiveSignal
        {
            get
            {
                const double epsilon = 0.00001;
                if (Math.Abs(_averagePositiveSignal - -1) < epsilon)
                {
                    return CalcAveragePositiveSignal();
                }
                return _averagePositiveSignal;
            }
        }
        /// <summary>User-defined sliding window determined standard deviation values for the gene signals.</summary>
        public List<double> WindowedDeviations = new List<double>();
        /// <summary>User-defined sliding window determined average values for the gene signals.</summary>
        public List<double> WindowedAverages = new List<double>();
        /// <summary>User-defined sliding window determined median values for the gene signals.</summary>
        public List<double> WindowedMedians = new List<double>();
        /// <summary>User-defined sliding window determined median variance (e.g. |x1 - x2|) values for the gene signals.</summary>

        public List<double> WindowedMedianVariances = new List<double>();

        private readonly SampleData _sd;

        private readonly MarkerThresholdCollection _thresholds;
        private const double EPSILON = 0.00001;

        public MarkerThresholdInfo(SampleData sd, MarkerThresholdCollection thresholds, int indexMarker, ThresholdType typeOfThreshold, double markerThreshold = 0.01)
        {
            _sd = sd;
            _thresholds = thresholds;
            _markerIndex = indexMarker;
            GetSignals(false);
            ThresholdType = typeOfThreshold;
            _threshold = markerThreshold;
            GetThreshold();

            CalcWindowedStats();
            CalcFrequencies();
        }

        /// <summary>Get final average signals.</summary>
        /// <param name="useOnlyQCSamples"></param>
        public void GetSignals(bool useOnlyQCSamples)
        {
            var tmp = new List<double>();
            for (int i = 0; i < _sd.SampleStats.Count; i++)
            {
                if (_sd.SampleQC[i] | !useOnlyQCSamples)
                {
                    tmp.Add(_sd.SampleStats[i][_markerIndex].FinalAverageSignal);
                }
            }
            tmp.Sort();
            _signals = tmp;
        }

        /// <summary>Calculates windowed averages, standard deviations and medians.</summary>
        public void CalcWindowedStats()
        {
            WindowedAverages = new List<double>();
            WindowedDeviations = new List<double>();
            WindowedMedians = new List<double>();
            WindowedMedianVariances = new List<double>();
            int windowSize = _sd.Settings.WindowSize;
            for (int i = 0; i <= Signals.Count - windowSize - 1; i++)
            {
                var tmp = new List<double>();
                for (int j = 0; j <= windowSize - 1; j++)
                {
                    tmp.Add(Signals[i + j]);
                }
                WindowedAverages.Add(Misc.Average(tmp));
                WindowedDeviations.Add(Misc.StandardDeviation(tmp, WindowedAverages[i]));
                WindowedMedians.Add(Misc.GetMedian(tmp));
            }
        }

        /// <summary>Calculates the histogram frequencies for a gene using the bin sizes and raw signals for that gene.</summary>
        public void CalcFrequencies()
        {
            List<double> bin = _thresholds.Bin;
            Frequencies.Clear();
            for (int i = 0; i <= bin.Count - 1; i++)
            {
                Frequencies.Add(0);
            }
            foreach (double signal in Signals)
            {
                foreach (double binVal in bin)
                {
                    if (binVal >= signal)
                    {
                        int index = bin.IndexOf(binVal);
                        Frequencies[index] += 1;
                        break;
                    }
                }
            }
        }


    }

}
