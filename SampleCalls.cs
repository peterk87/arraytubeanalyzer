﻿using System;
using System.Collections.Generic;

namespace ArrayTubeAnalyzer
{
    
    /// <summary>Sample absent/present/ambiguous calls for each marker.</summary>
    [Serializable]
    public class SampleCalls
    {
        /// <summary>Whether the gene spot calling is finalized for the array tube sample.</summary>
        public bool Finalized;
        /// <summary>Marker calls for sample.</summary>

        public List<Calls> Calls;
        public SampleCalls(int markerCount)
        {
            var tmp = new Calls[markerCount];
            Finalized = false;
            Calls = new List<Calls>();
            Calls.AddRange(tmp);
        }

        public SampleCalls(string markerCalls, string finalizationStatus)
        {
            Calls = new List<Calls>();
            foreach (string markerCall in markerCalls.Split('\t'))
            {
                Calls.Add((Calls)int.Parse(markerCall));
            }
            Finalized = bool.Parse(finalizationStatus);
        }
    }

}
