﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace ArrayTubeAnalyzer
{
    [Serializable]
    public class DiagnosisProfile
    {
        public List<Diagnosis> DiagnosisList { get; set; }
        public string Name { get; set; }

        public DiagnosisProfile(int iNumMarkers, string sName = "")
        {
            InitDiagnosisList(iNumMarkers);
            Name = sName;
        }

        public DiagnosisProfile(DiagnosisProfile dp)
        {
            DiagnosisList = new List<Diagnosis>();
            DiagnosisList.AddRange(dp.DiagnosisList);
            Name = dp.Name;
        }

        public DiagnosisProfile(string sFileString, int iNumGenes, ref bool bError)
        {
            string[] sArray = sFileString.Split('\t');
            if (sArray.Length - 1 == iNumGenes)
            {
                Name = sArray[0];
                DiagnosisList = new List<Diagnosis>();
                for (int i = 1; i < sArray.Length; i++)
                {
                    DiagnosisList.Add((Diagnosis)int.Parse(sArray[i]));
                }
                bError = false;
            }
            else
            {
                bError = true;
            }
        }

        private void InitDiagnosisList(int iNumMarkers)
        {
            DiagnosisList = new List<Diagnosis>();
            for (int i = 0; i < iNumMarkers; i++)
            {
                DiagnosisList.Add(Diagnosis.Ignore);
            }
        }

        public string ToFileString()
        {
            var ls = new List<string> {Name};
            foreach (int d in DiagnosisList)
            {
                ls.Add(d.ToString(CultureInfo.InvariantCulture));
            }
            return string.Join("\t", ls);
        }

    }

    public enum Diagnosis
    {
        Ignore = 0,
        Present = 1,
        Absent = 2,
        MaybePresent = 3,
        MaybeAbsent = 4
    }

}
