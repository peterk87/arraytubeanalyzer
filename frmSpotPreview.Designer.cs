﻿namespace ArrayTubeAnalyzer
{
    partial class FrmSpotPreview
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ToolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.ToolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.cbxGeneCall = new System.Windows.Forms.ToolStripComboBox();
            this.MarkerCallStatusToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ContextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.chkDiscardReplicate = new System.Windows.Forms.ToolStripMenuItem();
            this.chkDiscardMarker = new System.Windows.Forms.ToolStripMenuItem();
            this.ContextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ToolTip1
            // 
            this.ToolTip1.ShowAlways = true;
            // 
            // ToolStripSeparator1
            // 
            this.ToolStripSeparator1.Name = "ToolStripSeparator1";
            this.ToolStripSeparator1.Size = new System.Drawing.Size(178, 6);
            // 
            // cbxGeneCall
            // 
            this.cbxGeneCall.Name = "cbxGeneCall";
            this.cbxGeneCall.Size = new System.Drawing.Size(121, 23);
            this.cbxGeneCall.SelectedIndexChanged += new System.EventHandler(this.CbxGeneCallSelectedIndexChanged);
            // 
            // MarkerCallStatusToolStripMenuItem
            // 
            this.MarkerCallStatusToolStripMenuItem.Name = "MarkerCallStatusToolStripMenuItem";
            this.MarkerCallStatusToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.MarkerCallStatusToolStripMenuItem.Text = "Marker Call Status";
            // 
            // ContextMenuStrip1
            // 
            this.ContextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MarkerCallStatusToolStripMenuItem,
            this.cbxGeneCall,
            this.ToolStripSeparator1,
            this.chkDiscardReplicate,
            this.chkDiscardMarker});
            this.ContextMenuStrip1.Name = "ContextMenuStrip1";
            this.ContextMenuStrip1.Size = new System.Drawing.Size(182, 125);
            this.ContextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.ContextMenuStrip1Opening);
            // 
            // chkDiscardReplicate
            // 
            this.chkDiscardReplicate.CheckOnClick = true;
            this.chkDiscardReplicate.Name = "chkDiscardReplicate";
            this.chkDiscardReplicate.Size = new System.Drawing.Size(181, 22);
            this.chkDiscardReplicate.Text = "Discard Replicate?";
            this.chkDiscardReplicate.CheckedChanged += new System.EventHandler(this.ChkDiscardCheckedChanged);
            // 
            // chkDiscardMarker
            // 
            this.chkDiscardMarker.CheckOnClick = true;
            this.chkDiscardMarker.Name = "chkDiscardMarker";
            this.chkDiscardMarker.Size = new System.Drawing.Size(181, 22);
            this.chkDiscardMarker.Text = "Discard Marker?";
            this.chkDiscardMarker.CheckedChanged += new System.EventHandler(this.ChkDiscardMarkerCheckedChanged);
            // 
            // frmSpotPreview
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmSpotPreview";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmSpotPreviewFormClosing);
            this.ContextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.ToolTip ToolTip1;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator1;
        internal System.Windows.Forms.ToolStripComboBox cbxGeneCall;
        internal System.Windows.Forms.ToolStripMenuItem MarkerCallStatusToolStripMenuItem;
        internal System.Windows.Forms.ContextMenuStrip ContextMenuStrip1;
        internal System.Windows.Forms.ToolStripMenuItem chkDiscardReplicate;
        private System.Windows.Forms.ToolStripMenuItem chkDiscardMarker;
    }
}