﻿using System.Collections.Generic;
using System.Windows.Forms;

namespace ArrayTubeAnalyzer
{
    public partial class FrmChangeArrayNames : Form
    {
        private readonly SampleData _sd;
        private readonly List<string> _originalSampleNames = new List<string>();

        private readonly List<string> _newSampleNames = new List<string>();

        public FrmChangeArrayNames(SampleData sampleData)
        {
            // This call is required by the designer.
            InitializeComponent();

            // Add any initialization after the InitializeComponent() call.
            _sd = sampleData;

            _originalSampleNames.AddRange(_sd.SampleNames);
            _newSampleNames.AddRange(_sd.SampleNames);


            UpdateListview();

        }

        public void UpdateListview()
        {
            lvwArrayNames.Items.Clear();
            var lvList = new List<ListViewItem>();
            for (int i = 0; i <= _newSampleNames.Count - 1; i++)
            {
                var lvi = new ListViewItem(new[] { _newSampleNames[i], _originalSampleNames[i] });
                lvList.Add(lvi);
            }
            lvwArrayNames.Items.AddRange(lvList.ToArray());
        }

        private void BtnOkClick(System.Object sender, System.EventArgs e)
        {
            _sd.SetSampleNames(_newSampleNames);
            _sd.Forms.SampleQC.PopulateSamplesListview();
            Close();
        }

        private void BtnCancelClick(System.Object sender, System.EventArgs e)
        {
            Close();
        }

        private void ChangeArrayNamesToolStripMenuItemClick(System.Object sender, System.EventArgs e)
        {
            string sName = lvwArrayNames.SelectedItems[0].Text;

            int index = _newSampleNames.IndexOf(sName);

            if (Misc.InputBox("Change name of array tube sample", "Specify new name for array tube sample", ref sName) == DialogResult.OK)
            {
                lvwArrayNames.SelectedItems[0].Text = sName;
                _newSampleNames[index] = sName;
            }
        }

        private void ReplaceAllToolStripMenuItemClick(System.Object sender, System.EventArgs e)
        {
            var fr = new FindReplace("", "", Cursor.Position.X - 50, Cursor.Position.Y - 60);
            var ls = new List<string>();
            ls.AddRange(_newSampleNames);
            var f = new frmReplaceAll_ChangeArrayNames(_newSampleNames, ls, fr);
            DialogResult fDialogResult = f.ShowDialog();
            while (fDialogResult == DialogResult.Retry)
            {
                UpdateListview();
                f = new frmReplaceAll_ChangeArrayNames(_newSampleNames, ls, fr);
                fDialogResult = f.ShowDialog();
            }

            UpdateListview();
        }

        private void ContextMenuStrip1Opening(System.Object sender, System.ComponentModel.CancelEventArgs e)
        {
            ChangeArrayNamesToolStripMenuItem.Enabled = lvwArrayNames.SelectedItems.Count > 0;
        }
    }


}
