﻿using System;

namespace ArrayTubeAnalyzer
{
    /// <summary>
    /// Object to store indices for the spot and gene.
    /// </summary>
    /// <remarks></remarks>
    [Serializable]
    public class SpotMarkerIndices
    {
        private readonly int _iSpot;
        public int Spot
        {
            get { return _iSpot; }
        }

        private readonly int _iGene;
        public int Marker
        {
            get { return _iGene; }
        }

        public SpotMarkerIndices(int geneIndex, int spotIndex)
        {
            _iSpot = spotIndex;
            _iGene = geneIndex;
        }

    }
}
