﻿namespace ArrayTubeAnalyzer
{
    partial class FrmClustering
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmClustering));
            this.ddbAmbiguousMarkers = new System.Windows.Forms.ToolStripDropDownButton();
            this.cbxAmbiguousMarkers = new System.Windows.Forms.ToolStripComboBox();
            this.HighlightAmbiguousMarkersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.StatusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tslStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.ProgressBar1 = new System.Windows.Forms.ToolStripProgressBar();
            this.ToolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnCluster = new System.Windows.Forms.ToolStripMenuItem();
            this.ChooseMarkersToIncludeInClusteringToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.ToolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.ToolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.btnDiagnostics = new System.Windows.Forms.ToolStripButton();
            this.SaveSpotArrayImageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ContextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ShowMarkerStripToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.chkJaccard = new System.Windows.Forms.ToolStripMenuItem();
            this.chkUserDefined = new System.Windows.Forms.ToolStripMenuItem();
            this.ExportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BinaryMultiFASTAToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ClusteringOutputToTextFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BinarizedSampleDataForGenomeFisherToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DistanceMatrixToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DistanceMatrixDifferencesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.txtThreshold = new System.Windows.Forms.ToolStripTextBox();
            this.ToolStripDropDownButton1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.SaveClusteredSpotArrayImageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ExitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStrip1 = new System.Windows.Forms.ToolStrip();
            this.ToolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.ddbClustering = new System.Windows.Forms.ToolStripDropDownButton();
            this.ClusteringThresholdToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lvwClustering = new ArrayTubeAnalyzer.ListViewFF();
            this.StatusStrip1.SuspendLayout();
            this.ContextMenuStrip1.SuspendLayout();
            this.ToolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ddbAmbiguousMarkers
            // 
            this.ddbAmbiguousMarkers.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.ddbAmbiguousMarkers.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cbxAmbiguousMarkers,
            this.HighlightAmbiguousMarkersToolStripMenuItem});
            this.ddbAmbiguousMarkers.Image = ((System.Drawing.Image)(resources.GetObject("ddbAmbiguousMarkers.Image")));
            this.ddbAmbiguousMarkers.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ddbAmbiguousMarkers.Name = "ddbAmbiguousMarkers";
            this.ddbAmbiguousMarkers.Size = new System.Drawing.Size(127, 22);
            this.ddbAmbiguousMarkers.Text = "Ambiguous Markers";
            // 
            // cbxAmbiguousMarkers
            // 
            this.cbxAmbiguousMarkers.Items.AddRange(new object[] {
            "Use Threshold",
            "Treat As Positive",
            "Treat As Negative"});
            this.cbxAmbiguousMarkers.Name = "cbxAmbiguousMarkers";
            this.cbxAmbiguousMarkers.Size = new System.Drawing.Size(171, 23);
            this.cbxAmbiguousMarkers.Text = "Use Threshold";
            this.cbxAmbiguousMarkers.SelectedIndexChanged += new System.EventHandler(this.CbxAmbiguousMarkersSelectedIndexChanged);
            this.cbxAmbiguousMarkers.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CbxAmbiguousMarkersKeyPress);
            // 
            // HighlightAmbiguousMarkersToolStripMenuItem
            // 
            this.HighlightAmbiguousMarkersToolStripMenuItem.CheckOnClick = true;
            this.HighlightAmbiguousMarkersToolStripMenuItem.Name = "HighlightAmbiguousMarkersToolStripMenuItem";
            this.HighlightAmbiguousMarkersToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.HighlightAmbiguousMarkersToolStripMenuItem.Text = "Highlight Ambiguous Markers";
            this.HighlightAmbiguousMarkersToolStripMenuItem.CheckedChanged += new System.EventHandler(this.HighlightAmbiguousMarkersToolStripMenuItemCheckedChanged);
            // 
            // StatusStrip1
            // 
            this.StatusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tslStatus,
            this.ProgressBar1});
            this.StatusStrip1.Location = new System.Drawing.Point(0, 537);
            this.StatusStrip1.Name = "StatusStrip1";
            this.StatusStrip1.Size = new System.Drawing.Size(1079, 22);
            this.StatusStrip1.TabIndex = 2;
            this.StatusStrip1.Text = "StatusStrip1";
            // 
            // tslStatus
            // 
            this.tslStatus.Name = "tslStatus";
            this.tslStatus.Size = new System.Drawing.Size(39, 17);
            this.tslStatus.Text = "Ready";
            // 
            // ProgressBar1
            // 
            this.ProgressBar1.Name = "ProgressBar1";
            this.ProgressBar1.Size = new System.Drawing.Size(100, 16);
            this.ProgressBar1.Visible = false;
            // 
            // ToolStripSeparator2
            // 
            this.ToolStripSeparator2.Name = "ToolStripSeparator2";
            this.ToolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // btnCluster
            // 
            this.btnCluster.Image = global::ArrayTubeAnalyzer.Properties.Resources.gear_16;
            this.btnCluster.Name = "btnCluster";
            this.btnCluster.Size = new System.Drawing.Size(288, 22);
            this.btnCluster.Text = "Cluster";
            this.btnCluster.Click += new System.EventHandler(this.BtnClusterClick);
            // 
            // ChooseMarkersToIncludeInClusteringToolStripMenuItem
            // 
            this.ChooseMarkersToIncludeInClusteringToolStripMenuItem.Name = "ChooseMarkersToIncludeInClusteringToolStripMenuItem";
            this.ChooseMarkersToIncludeInClusteringToolStripMenuItem.Size = new System.Drawing.Size(288, 22);
            this.ChooseMarkersToIncludeInClusteringToolStripMenuItem.Text = "Choose Markers To Include In Clustering";
            this.ChooseMarkersToIncludeInClusteringToolStripMenuItem.Click += new System.EventHandler(this.ChooseMarkersToIncludeInClusteringToolStripMenuItemClick);
            // 
            // ToolStripSeparator4
            // 
            this.ToolStripSeparator4.Name = "ToolStripSeparator4";
            this.ToolStripSeparator4.Size = new System.Drawing.Size(285, 6);
            // 
            // ToolStripSeparator3
            // 
            this.ToolStripSeparator3.Name = "ToolStripSeparator3";
            this.ToolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // btnDiagnostics
            // 
            this.btnDiagnostics.Image = global::ArrayTubeAnalyzer.Properties.Resources.search_16;
            this.btnDiagnostics.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnDiagnostics.Name = "btnDiagnostics";
            this.btnDiagnostics.Size = new System.Drawing.Size(130, 22);
            this.btnDiagnostics.Text = "Sample Diagnostics";
            this.btnDiagnostics.Click += new System.EventHandler(this.BtnDiagnosticsClick);
            // 
            // SaveSpotArrayImageToolStripMenuItem
            // 
            this.SaveSpotArrayImageToolStripMenuItem.Name = "SaveSpotArrayImageToolStripMenuItem";
            this.SaveSpotArrayImageToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.SaveSpotArrayImageToolStripMenuItem.Text = "Save Spot Array Image";
            this.SaveSpotArrayImageToolStripMenuItem.Click += new System.EventHandler(this.SaveSpotArrayImageToolStripMenuItemClick);
            // 
            // ContextMenuStrip1
            // 
            this.ContextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ShowMarkerStripToolStripMenuItem,
            this.SaveSpotArrayImageToolStripMenuItem});
            this.ContextMenuStrip1.Name = "ContextMenuStrip1";
            this.ContextMenuStrip1.Size = new System.Drawing.Size(193, 48);
            // 
            // ShowMarkerStripToolStripMenuItem
            // 
            this.ShowMarkerStripToolStripMenuItem.Name = "ShowMarkerStripToolStripMenuItem";
            this.ShowMarkerStripToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.ShowMarkerStripToolStripMenuItem.Text = "Show Marker Strip";
            this.ShowMarkerStripToolStripMenuItem.Click += new System.EventHandler(this.ShowMarkerStripToolStripMenuItemClick);
            // 
            // chkJaccard
            // 
            this.chkJaccard.CheckOnClick = true;
            this.chkJaccard.Name = "chkJaccard";
            this.chkJaccard.Size = new System.Drawing.Size(288, 22);
            this.chkJaccard.Text = "Use Jaccard Index";
            this.chkJaccard.CheckedChanged += new System.EventHandler(this.ChkJaccardCheckedChanged);
            // 
            // chkUserDefined
            // 
            this.chkUserDefined.CheckOnClick = true;
            this.chkUserDefined.Name = "chkUserDefined";
            this.chkUserDefined.Size = new System.Drawing.Size(288, 22);
            this.chkUserDefined.Text = "Stop At Threshold";
            this.chkUserDefined.CheckedChanged += new System.EventHandler(this.ChkUserDefinedCheckedChanged);
            // 
            // ExportToolStripMenuItem
            // 
            this.ExportToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BinaryMultiFASTAToolStripMenuItem,
            this.ClusteringOutputToTextFileToolStripMenuItem,
            this.BinarizedSampleDataForGenomeFisherToolStripMenuItem,
            this.DistanceMatrixToolStripMenuItem,
            this.DistanceMatrixDifferencesToolStripMenuItem});
            this.ExportToolStripMenuItem.Name = "ExportToolStripMenuItem";
            this.ExportToolStripMenuItem.Size = new System.Drawing.Size(245, 22);
            this.ExportToolStripMenuItem.Text = "Export...";
            // 
            // BinaryMultiFASTAToolStripMenuItem
            // 
            this.BinaryMultiFASTAToolStripMenuItem.Name = "BinaryMultiFASTAToolStripMenuItem";
            this.BinaryMultiFASTAToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.B)));
            this.BinaryMultiFASTAToolStripMenuItem.Size = new System.Drawing.Size(290, 22);
            this.BinaryMultiFASTAToolStripMenuItem.Text = "Binary Multi-FASTA";
            this.BinaryMultiFASTAToolStripMenuItem.Click += new System.EventHandler(this.BinaryMultiFastaToolStripMenuItemClick);
            // 
            // ClusteringOutputToTextFileToolStripMenuItem
            // 
            this.ClusteringOutputToTextFileToolStripMenuItem.Name = "ClusteringOutputToTextFileToolStripMenuItem";
            this.ClusteringOutputToTextFileToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
            this.ClusteringOutputToTextFileToolStripMenuItem.Size = new System.Drawing.Size(290, 22);
            this.ClusteringOutputToTextFileToolStripMenuItem.Text = "Clustering Output To Text File";
            this.ClusteringOutputToTextFileToolStripMenuItem.Click += new System.EventHandler(this.ClusteringOutputToTextFileToolStripMenuItemClick);
            // 
            // BinarizedSampleDataForGenomeFisherToolStripMenuItem
            // 
            this.BinarizedSampleDataForGenomeFisherToolStripMenuItem.Name = "BinarizedSampleDataForGenomeFisherToolStripMenuItem";
            this.BinarizedSampleDataForGenomeFisherToolStripMenuItem.Size = new System.Drawing.Size(290, 22);
            this.BinarizedSampleDataForGenomeFisherToolStripMenuItem.Text = "Binarized Sample Data For GenomeFisher";
            this.BinarizedSampleDataForGenomeFisherToolStripMenuItem.Click += new System.EventHandler(this.BinarizedSampleDataForGenomeFisherToolStripMenuItemClick);
            // 
            // DistanceMatrixToolStripMenuItem
            // 
            this.DistanceMatrixToolStripMenuItem.Name = "DistanceMatrixToolStripMenuItem";
            this.DistanceMatrixToolStripMenuItem.Size = new System.Drawing.Size(290, 22);
            this.DistanceMatrixToolStripMenuItem.Text = "Distance Matrix (Similarities)";
            this.DistanceMatrixToolStripMenuItem.Click += new System.EventHandler(this.DistanceMatrixToolStripMenuItemClick);
            // 
            // DistanceMatrixDifferencesToolStripMenuItem
            // 
            this.DistanceMatrixDifferencesToolStripMenuItem.Name = "DistanceMatrixDifferencesToolStripMenuItem";
            this.DistanceMatrixDifferencesToolStripMenuItem.Size = new System.Drawing.Size(290, 22);
            this.DistanceMatrixDifferencesToolStripMenuItem.Text = "Distance Matrix (Differences)";
            this.DistanceMatrixDifferencesToolStripMenuItem.Click += new System.EventHandler(this.DistanceMatrixDifferencesToolStripMenuItemClick);
            // 
            // txtThreshold
            // 
            this.txtThreshold.Name = "txtThreshold";
            this.txtThreshold.Size = new System.Drawing.Size(100, 23);
            this.txtThreshold.Text = "100";
            this.txtThreshold.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtThresholdKeyPress);
            // 
            // ToolStripDropDownButton1
            // 
            this.ToolStripDropDownButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.ToolStripDropDownButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ExportToolStripMenuItem,
            this.SaveClusteredSpotArrayImageToolStripMenuItem,
            this.ExitToolStripMenuItem});
            this.ToolStripDropDownButton1.Image = ((System.Drawing.Image)(resources.GetObject("ToolStripDropDownButton1.Image")));
            this.ToolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStripDropDownButton1.Name = "ToolStripDropDownButton1";
            this.ToolStripDropDownButton1.Size = new System.Drawing.Size(38, 22);
            this.ToolStripDropDownButton1.Text = "File";
            // 
            // SaveClusteredSpotArrayImageToolStripMenuItem
            // 
            this.SaveClusteredSpotArrayImageToolStripMenuItem.Name = "SaveClusteredSpotArrayImageToolStripMenuItem";
            this.SaveClusteredSpotArrayImageToolStripMenuItem.Size = new System.Drawing.Size(245, 22);
            this.SaveClusteredSpotArrayImageToolStripMenuItem.Text = "Save Clustered Spot Array Image";
            this.SaveClusteredSpotArrayImageToolStripMenuItem.Click += new System.EventHandler(this.SaveSpotArrayImageToolStripMenuItemClick);
            // 
            // ExitToolStripMenuItem
            // 
            this.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem";
            this.ExitToolStripMenuItem.Size = new System.Drawing.Size(245, 22);
            this.ExitToolStripMenuItem.Text = "Exit";
            this.ExitToolStripMenuItem.Click += new System.EventHandler(this.ExitToolStripMenuItemClick);
            // 
            // ToolStrip1
            // 
            this.ToolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripDropDownButton1,
            this.ToolStripSeparator1,
            this.ddbClustering,
            this.ToolStripSeparator2,
            this.ddbAmbiguousMarkers,
            this.ToolStripSeparator3,
            this.btnDiagnostics});
            this.ToolStrip1.Location = new System.Drawing.Point(0, 0);
            this.ToolStrip1.Name = "ToolStrip1";
            this.ToolStrip1.Size = new System.Drawing.Size(1079, 25);
            this.ToolStrip1.TabIndex = 3;
            this.ToolStrip1.Text = "ToolStrip1";
            // 
            // ToolStripSeparator1
            // 
            this.ToolStripSeparator1.Name = "ToolStripSeparator1";
            this.ToolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // ddbClustering
            // 
            this.ddbClustering.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ClusteringThresholdToolStripMenuItem,
            this.txtThreshold,
            this.chkUserDefined,
            this.chkJaccard,
            this.btnCluster,
            this.ToolStripSeparator4,
            this.ChooseMarkersToIncludeInClusteringToolStripMenuItem});
            this.ddbClustering.Image = global::ArrayTubeAnalyzer.Properties.Resources.tree_16;
            this.ddbClustering.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ddbClustering.Name = "ddbClustering";
            this.ddbClustering.Size = new System.Drawing.Size(90, 22);
            this.ddbClustering.Text = "Clustering";
            // 
            // ClusteringThresholdToolStripMenuItem
            // 
            this.ClusteringThresholdToolStripMenuItem.Name = "ClusteringThresholdToolStripMenuItem";
            this.ClusteringThresholdToolStripMenuItem.Size = new System.Drawing.Size(288, 22);
            this.ClusteringThresholdToolStripMenuItem.Text = "Clustering Threshold";
            // 
            // lvwClustering
            // 
            this.lvwClustering.ContextMenuStrip = this.ContextMenuStrip1;
            this.lvwClustering.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvwClustering.FullRowSelect = true;
            this.lvwClustering.GridLines = true;
            this.lvwClustering.Location = new System.Drawing.Point(0, 25);
            this.lvwClustering.Name = "lvwClustering";
            this.lvwClustering.ShowItemToolTips = true;
            this.lvwClustering.Size = new System.Drawing.Size(1079, 512);
            this.lvwClustering.TabIndex = 4;
            this.lvwClustering.UseCompatibleStateImageBehavior = false;
            this.lvwClustering.View = System.Windows.Forms.View.Details;
            this.lvwClustering.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.LvwClusteringColumnClick);
            this.lvwClustering.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.LvwClusteringItemSelectionChanged);
            this.lvwClustering.MouseUp += new System.Windows.Forms.MouseEventHandler(this.LvwClusteringMouseUp);
            // 
            // frmClustering
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1079, 559);
            this.Controls.Add(this.lvwClustering);
            this.Controls.Add(this.StatusStrip1);
            this.Controls.Add(this.ToolStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmClustering";
            this.Text = "Array Tube Analyzer - UPGMA Clustering";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmClusteringFormClosing);
            this.StatusStrip1.ResumeLayout(false);
            this.StatusStrip1.PerformLayout();
            this.ContextMenuStrip1.ResumeLayout(false);
            this.ToolStrip1.ResumeLayout(false);
            this.ToolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.ToolStripDropDownButton ddbAmbiguousMarkers;
        internal System.Windows.Forms.ToolStripComboBox cbxAmbiguousMarkers;
        internal System.Windows.Forms.ToolStripMenuItem HighlightAmbiguousMarkersToolStripMenuItem;
        internal System.Windows.Forms.StatusStrip StatusStrip1;
        internal System.Windows.Forms.ToolStripStatusLabel tslStatus;
        internal System.Windows.Forms.ToolStripProgressBar ProgressBar1;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator2;
        internal System.Windows.Forms.ToolStripMenuItem btnCluster;
        internal System.Windows.Forms.ToolStripMenuItem ChooseMarkersToIncludeInClusteringToolStripMenuItem;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator4;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator3;
        internal System.Windows.Forms.ToolTip ToolTip1;
        internal System.Windows.Forms.ToolStripButton btnDiagnostics;
        internal System.Windows.Forms.ToolStripMenuItem SaveSpotArrayImageToolStripMenuItem;
        internal System.Windows.Forms.ContextMenuStrip ContextMenuStrip1;
        internal System.Windows.Forms.ToolStripMenuItem ShowMarkerStripToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem chkJaccard;
        internal System.Windows.Forms.ToolStripMenuItem chkUserDefined;
        internal System.Windows.Forms.ToolStripMenuItem ExportToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem BinaryMultiFASTAToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem ClusteringOutputToTextFileToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem BinarizedSampleDataForGenomeFisherToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem DistanceMatrixToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem DistanceMatrixDifferencesToolStripMenuItem;
        internal System.Windows.Forms.ToolStripTextBox txtThreshold;
        internal System.Windows.Forms.ToolStripDropDownButton ToolStripDropDownButton1;
        internal System.Windows.Forms.ToolStripMenuItem SaveClusteredSpotArrayImageToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem ExitToolStripMenuItem;
        internal System.Windows.Forms.ToolStrip ToolStrip1;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator1;
        internal System.Windows.Forms.ToolStripDropDownButton ddbClustering;
        internal System.Windows.Forms.ToolStripMenuItem ClusteringThresholdToolStripMenuItem;
        private ListViewFF lvwClustering;
    }
}