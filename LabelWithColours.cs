﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace ArrayTubeAnalyzer
{
    class LabelWithColours : Label
    {
        public Color BorderColor { get; set; }

        public int BorderWidth { get; set; }

        public bool BorderEnabled { get; set; }

        public ButtonBorderStyle  BorderStyle { get; set; }

        public double OldTop { get; set; }
        public double OldLeft { get; set; }

        public double OldWidth { get; set; }
        public double OldHeight { get; set; }

        public LabelWithColours()
        {
            Text = "";
            AutoSize = false;
            Size = new System.Drawing.Size(20, 20);
            BackColor = Color.Transparent;
            BorderStyle = ButtonBorderStyle.Solid;
            BorderWidth = 1;
            BorderColor = Color.Black;
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            ControlPaint.DrawBorder(e.Graphics,
                this.DisplayRectangle,
                BorderColor,
                BorderWidth,
                BorderStyle,
                BorderColor,
                BorderWidth,
                BorderStyle,
                BorderColor,
                BorderWidth,
                BorderStyle,
                BorderColor,
                BorderWidth,
                BorderStyle);
                
        }
    }
}
