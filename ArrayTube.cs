namespace ArrayTubeAnalyzer
{
    /// <summary>Raw array tube information structure.</summary>
    public struct ArrayTube
    {
        /// <summary>Gene marker ID. Also referred to as "gene name".</summary>
        public string ID;

        /// <summary>Raw signal value.</summary>
        public double Signal;

        /// <summary>Spot number.</summary>
        public int Spot;
    }
}