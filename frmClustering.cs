﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;

namespace ArrayTubeAnalyzer
{
    public partial class FrmClustering : Form
    {
        private readonly SampleData _sd;

        private int _iSelectedSample = -1;

        public FrmClustering(object objSampleData)
        {
            _sd = objSampleData as SampleData;
            // This call is required by the designer.
            InitializeComponent();
            // Add any initialization after the InitializeComponent() call.
            UpdateForm();
        }

        /// <summary>Setup the listview with the clustered binary data showing cluster numbers, sample IDs and absence/presence of each gene.</summary>
        private void SetupClusteringListview()
        {
            lvwClustering.Clear();
            lvwClustering.Columns.Add("Sample", 100);
            lvwClustering.Columns.Add("Cluster", 20);
            for (int i = 0; i < _sd.DiscardedMarkers.Count; i++ )
            {
                if (!_sd.DiscardedMarkers[i])
                    lvwClustering.Columns.Add(_sd.Markers[i], 10);
            }
        }

        private void UpdateListview()
        {
            SetupClusteringListview();
            var lvl = new List<ListViewItem>();
            var f = new Font(FontFamily.GenericSansSerif, 8);
            for (int i = 0; i < _sd.SampleCount; i++)
            {
                int sampleIndex = _sd.Cluster.ClusterGroup.NameIndices[i];
                var lvi = new ListViewItem(_sd.SampleNames[sampleIndex]) {UseItemStyleForSubItems = false};

                int iCluster = _sd.Cluster.ClusterGroup.ClusterNumber[i];
                bool bEvenCluster = (iCluster % 2 == 0);
                lvi.BackColor = bEvenCluster ? _sd.Settings.C.StrainEvenColour : _sd.Settings.C.StrainOddColour;
                lvi.SubItems.Add(iCluster.ToString(CultureInfo.InvariantCulture), Color.Black, bEvenCluster ? _sd.Settings.C.StrainEvenColour : _sd.Settings.C.StrainOddColour, f);

                List<bool> lb = _sd.Cluster.GetBinary(sampleIndex);

                for (int j = 0; j < lb.Count; j++)
                {
                    bool b = lb[j];
                    int markerIndex = -1;
                    int markerCount = 0;
                    for (int k = 0; k < _sd.DiscardedMarkers.Count; k++ )
                    {
                        if (!_sd.DiscardedMarkers[k])
                        {
                            if (markerCount == j)
                            {
                                markerIndex = k;
                                break;
                            }
                            markerCount++;
                        }
                    }
                    string lviString = _sd.GetMarkerName(markerIndex) + "; " + _sd.SampleStats[sampleIndex][markerIndex].FinalAverageSignal.ToString("0.0000");
                    var subitem = lvi.SubItems.Add(lviString, 
                        bEvenCluster ?
                        (b ? _sd.Settings.C.PresentClusteringEvenColour : _sd.Settings.C.AbsentClusteringEvenColour) :
                        (b ? _sd.Settings.C.PresentClusteringOddColour : _sd.Settings.C.AbsentClusteringOddColour),
                        bEvenCluster ?
                        (b ? _sd.Settings.C.PresentClusteringEvenColour : _sd.Settings.C.AbsentClusteringEvenColour) :
                        (b ? _sd.Settings.C.PresentClusteringOddColour : _sd.Settings.C.AbsentClusteringOddColour), f);
                    if (HighlightAmbiguousMarkersToolStripMenuItem.Checked)
                    {
                        switch (_sd.SampleCalls[sampleIndex].Calls[markerIndex])
                        {
                            case Calls.AmbiguousManual:
                            case Calls.AmbiguousAbsent:
                                subitem.BackColor = _sd.Settings.C.AbsentFlaggedListviewColour;
                                break;
                            case Calls.AmbiguousPresent:
                                subitem.BackColor = _sd.Settings.C.PresentFlaggedListviewColour;
                                break;
                        }
                    }
                    subitem.Tag = new SpotMarkerArrayIndices(markerIndex, 0, sampleIndex);
                }
                lvl.Add(lvi);
            }
            lvwClustering.Items.Clear();
            lvwClustering.Items.AddRange(lvl.ToArray());
        }

        private void BtnClusterClick(Object sender, EventArgs e)
        {
            _sd.ClusterUPGMA();
            UpdateListview();
        }

        public void UpdateForm()
        {
            if (_sd.Cluster == null)
                _sd.ClusterUPGMA();
            _sd.Cluster.SetThreshold(double.Parse(txtThreshold.Text));
            _sd.Cluster.UseJaccardIndex = chkJaccard.Checked;
            _sd.ClusterUPGMA();
            UpdateListview();
        }

        /// <summary>When the column header is clicked, show the column header text in the status bar.</summary>
        private void LvwClusteringColumnClick(object sender, ColumnClickEventArgs e)
        {
            tslStatus.Text = lvwClustering.Columns[e.Column].Text;
        }

/*
        private void TxtThresholdLostFocus(object sender, EventArgs e)
        {
            KeyPressEventArgs keyPressEvent = new KeyPressEventArgs((char)(Keys.Enter));
            txtThreshold_KeyPress(sender, keyPressEvent);
        }
*/

        /// <summary>Validate input into the threshold textbox.</summary>
        private void TxtThresholdKeyPress(object sender, KeyPressEventArgs e)
        {
            //Only numbers and backspaces are valid input in the textbox
            int i;
            if (int.TryParse(e.KeyChar.ToString(CultureInfo.InvariantCulture), out i) | e.KeyChar == (char)(Keys.Back))
            {
                e.Handled = false;
            }
            else if (e.KeyChar == '.')
            {
                e.Handled = (txtThreshold.Text.Contains('.'));
            }
            else if (e.KeyChar == (char)Keys.Enter || e.KeyChar == (char)Keys.Space || e.KeyChar == (char)Keys.Tab)
            {
                double d;
                if (double.TryParse(txtThreshold.Text, out d))
                {
                    if (d > 0 && d <= 100)
                    {
                        _sd.Cluster.SetThreshold(d);
                    }
                    else
                    {
                        txtThreshold.Text = _sd.Cluster.Threshold.ToString(CultureInfo.InvariantCulture);
                        tslStatus.Text = "Numeric values between 0 and 100% for the clustering/cluster grouping threshold.";
                    }
                    _sd.ClusterUPGMA();
                    UpdateListview();
                    tslStatus.Text = string.Format("Clustered at {0}%", d);
                }
            }
            else
            {
                e.Handled = true;
            }

        }

        /// <summary>User clicks "Export binary fasta file" or uses shortcut key of Ctrl+B</summary>
        private void BinaryMultiFastaToolStripMenuItemClick(Object sender, EventArgs e)
        {
            using (var sfd = new SaveFileDialog())
            {
                sfd.DefaultExt = ".txt";
                sfd.Filter = "Text File (*.txt)|*.txt|All Files (*.*)|*.*";
                sfd.FileName = DateTime.Now.ToString("MMM-d-yyyy") + "-Binary_FASTA.txt";
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    _sd.Cluster.WriteFasta(sfd.FileName);
                    tslStatus.Text = "Ready. Binary FastA written to '" + sfd.FileName + "'";
                }
            }
        }
        /// <summary>User clicks "Export clustering output to file"</summary>
        private void ClusteringOutputToTextFileToolStripMenuItemClick(Object sender, EventArgs e)
        {
            WriteClusteringOuputToFile(true);
        }

        /// <summary>Write the clustering output to a file.</summary>
        private void WriteClusteringOuputToFile(bool bWriteClusterNumber)
        {
            tslStatus.Text = "Writing Binary Data To File...";
            using (var sfd = new SaveFileDialog())
            {
                sfd.DefaultExt = ".txt";
                sfd.Filter = "Text File (*.txt)|*.txt|All Files (*.*)|*.*";
                sfd.FileName = string.Format("{0}-Clustering_Output-{1}percent.txt", DateTime.Now.ToString("MMM-d-yyyy"), txtThreshold.Text);
                sfd.Title = "Save Binary Data";
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    _sd.Cluster.Write(sfd.FileName, bWriteClusterNumber);
                    tslStatus.Text = string.Format("Ready. Binary data exported to '{0}'", sfd.FileName);
                }
            }
        }

        private void BinarizedSampleDataForGenomeFisherToolStripMenuItemClick(Object sender, EventArgs e)
        {
            WriteClusteringOuputToFile(false);
        }

        private void SaveSpotArrayImageToolStripMenuItemClick(Object sender, EventArgs e)
        {
            var f = new FrmSaveSpotArray(_sd);
            f.Show();
        }

        private void BtnDiagnosticsClick(Object sender, EventArgs e)
        {
            var f = new FrmDiagnosis(_sd);
            f.Show();
        }

        private void ExitToolStripMenuItemClick(Object sender, EventArgs e)
        {
            if (_sd.Forms.SampleQC.ExitApplication() == false)
            {
                Environment.Exit(0);
            }
        }

        private void LvwClusteringMouseUp(Object sender, MouseEventArgs e)
        {

            if (e.Button == MouseButtons.Left)
            {
                ListViewHitTestInfo ht = lvwClustering.HitTest(e.Location);
                if (ht.Item == null)
                    return;
                if (ht.SubItem.Tag == null)
                    return;
                var indices = ht.SubItem.Tag as SpotMarkerArrayIndices;

                Point pointFrmSpot = Cursor.Position;
                pointFrmSpot.X += 1;
                pointFrmSpot.Y += 1;
                //in the spot image collection only get the spot images for the spots that are highlighted

                if (indices != null)
                {
                    var f = new FrmSpotPreview(_sd, pointFrmSpot, indices, new SpotImageCollection(_sd, _sd.SampleStats, _sd.Images, _sd.Calibration, indices.Marker, _sd.Spots[indices.Marker].ToArray(), _sd.Spots));
                    _sd.Forms.SpotPreview.Add(f);
                    f.Show();
                }
            }
        }

        private void LvwClusteringItemSelectionChanged(Object sender, ListViewItemSelectionChangedEventArgs e)
        {
            if (e.IsSelected)
            {
                _iSelectedSample = e.ItemIndex;
            }
            e.Item.Selected = false;
        }

        private void ShowMarkerStripToolStripMenuItemClick(Object sender, EventArgs e)
        {
            string sArray = lvwClustering.Items[_iSelectedSample].Text;
            int iArray = _sd.SampleNames.IndexOf(sArray);

            var f = new FrmShowMarkerSpots(_sd, iArray, true);
            _sd.Forms.ShowMarkerSpots.Add(f);
            f.Show();
        }

        private void CbxAmbiguousMarkersKeyPress(Object sender, KeyPressEventArgs e)
        {//don't allow the combobox items to be modified
            e.Handled = true;
        }

        private void CbxAmbiguousMarkersSelectedIndexChanged(Object sender, EventArgs e)
        {
            //get how the ambiguous markers should be treated
            switch (cbxAmbiguousMarkers.SelectedIndex)
            {
                case 0://Use Threshold
                    _sd.Ambiguous = TreatAmbiguousAs.ThresholdDetermined;
                    break;
                case 1:// Treat As Positive
                    _sd.Ambiguous = TreatAmbiguousAs.Positive;
                    break;
                case 2:// Treat As Negative
                    _sd.Ambiguous = TreatAmbiguousAs.Negative;
                    break;
            }
            _sd.ClusterUPGMA();
            UpdateListview();
            if (_sd.Forms.Diagnosis != null)
            {
                _sd.Forms.Diagnosis.UpdateListview();
            }
        }

        private void DistanceMatrixToolStripMenuItemClick(Object sender, EventArgs e)
        {
            using (var sfd = new SaveFileDialog())
            {
                sfd.FileName = "Distance Matrix - Similarities.txt";
                sfd.Title = "Save Distance Matrix (Similarities)";
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    if (_sd.Cluster.WriteDistanceMatrix(sfd.FileName, true))
                    {
                        tslStatus.Text = "Ready. Distance matrix saved to '" + sfd.FileName + "'";
                    }
                    else
                    {
                        tslStatus.Text = "Unable to write distance matrix";
                    }
                }
            }
        }

        private void DistanceMatrixDifferencesToolStripMenuItemClick(Object sender, EventArgs e)
        {
            using (var sfd = new SaveFileDialog())
            {
                sfd.FileName = "Distance Matrix - Differences.txt";
                sfd.Title = "Save Distance Matrix (Differences)";
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    if (_sd.Cluster.WriteDistanceMatrix(sfd.FileName, false))
                    {
                        tslStatus.Text = "Ready. Distance matrix saved to '" + sfd.FileName + "'";
                    }
                    else
                    {
                        tslStatus.Text = "Unable to write distance matrix";
                    }

                }
            }
        }

        private void ChkJaccardCheckedChanged(Object sender, EventArgs e)
        {
            _sd.Cluster.UseJaccardIndex = chkJaccard.Checked;
            _sd.ClusterUPGMA();
            UpdateListview();
        }

        private void HighlightAmbiguousMarkersToolStripMenuItemCheckedChanged(Object sender, EventArgs e)
        {
            UpdateListview();
        }

        private void ChooseMarkersToIncludeInClusteringToolStripMenuItemClick(Object sender, EventArgs e)
        {
            var tmp = new List<int>();
            tmp.AddRange(_sd.Cluster.MarkerList);
            var ambiguousCount = new List<string>();
            for (int i = 0; i < _sd.MarkerCount; i++)
            {
                int count = 0;
                foreach (SampleCalls mCalls in _sd.SampleCalls)
                {
                    Calls mCall = mCalls.Calls[i];
                    if (mCall == Calls.AmbiguousAbsent | mCall == Calls.AmbiguousPresent | mCall == Calls.AmbiguousManual)
                    {
                        count++;
                    }
                }
                ambiguousCount.Add(count.ToString(CultureInfo.InvariantCulture));
            }
            var maxSignals = new List<string>();
            foreach (MarkerThresholdInfo marker in _sd.Thresholds.Markers)
            {
                maxSignals.Add(marker.Signals.Max().ToString("0.0000"));
            }

            if (Misc.SelectItemsListview("Select markers to include in clustering analysis",
                                        "Select the markers to include in UPGMA clustering",
                                        "Marker",
                                        "Ambiguous Count",
                                        "Max Signal",
                                        _sd.Markers,
                                        ambiguousCount,
                                        maxSignals,
                                        ref tmp,
                                        true) != DialogResult.OK) return;
            if (tmp.Count <= 0) return;
            for (int i = 0; i < _sd.DiscardedMarkers.Count; i++)
            {
                _sd.DiscardMarker(i, !tmp.Contains(i));
            }

            _sd.Cluster.MarkerList = tmp;
            _sd.ClusterUPGMA();
            UpdateListview();

            if (_sd.Forms.MarkerThresholdAnalysis != null)
            {
                _sd.Forms.MarkerThresholdAnalysis.UpdateForm();
            }
        }

        private void FrmClusteringFormClosing(object sender, FormClosingEventArgs e)
        {
            _sd.Forms.Clustering = null;
        }

        private void ChkUserDefinedCheckedChanged(object sender, EventArgs e)
        {
            _sd.Cluster.StopAtThreshold = chkUserDefined.Checked;
            _sd.ClusterUPGMA();
            UpdateListview();
        }

    }

}
