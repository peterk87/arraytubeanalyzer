﻿using System;
using System.Collections.Generic;

namespace ArrayTubeAnalyzer
{
    [Serializable]
    public class MarkerInfo
    {
        /// <summary>Signal values for all replicates.</summary>
        public List<double> Signal;
        /// <summary>Flagging status of each replicate - flagged (0), keep(auto) (1), keep(manual) (2), discard (3).</summary>
        public List<Flagging> Flag;
        /// <summary>Percent error values for replicates, i.e. (|signal - average signal for replicates| / average signal for replicates).</summary>
        public List<double> PercentError;
        /// <summary>Delta signal for replicates, i.e. |signal 1 - signal 2|.</summary>
        public List<double> DeltaSignal;
        /// <summary>Final average signal for the replicates determined by flagging status of individual replicates.</summary>
        public double FinalAverageSignal;
        /// <summary>Average signal value calculated with the raw signals for the replicates.</summary>

        public double AverageSignal;
        public MarkerInfo(List<double> lSignal, List<Flagging> lFlag, List<double> lPercentError, List<double> lDeltaSignal, double finalAvgSignal, double avgSignal)
        {
            Signal = lSignal;
            Flag = lFlag;
            PercentError = lPercentError;
            DeltaSignal = lDeltaSignal;
            FinalAverageSignal = finalAvgSignal;
            AverageSignal = avgSignal;
        }

        public MarkerInfo(List<double> lSignal)
        {
            Signal = lSignal;
            Flag = new List<Flagging>();
            for (int i = 0; i <= Signal.Count - 1; i++)
            {
                Flag.Add(Flagging.KeepAuto);
            }
            AverageSignal = Misc.Average(Signal);
            FinalAverageSignal = AverageSignal;
            DeltaSignal = new List<double>();
            CalcDeltaSignal();
            PercentError = new List<double>();
            CalcPercentError();
        }


        public MarkerInfo(string[] signals, string[] flags)
        {
            Flag = new List<Flagging>();
            Signal = new List<double>();
            for (int i = 0; i < signals.Length; i++)
            {
                double tmpSignal = double.Parse(signals[i]);
                int tmpFlag = int.Parse(flags[i]);
                Signal.Add(tmpSignal);
                Flag.Add((Flagging)tmpFlag);
            }
            AverageSignal = Misc.Average(Signal);
            CalcFinalAverageSignal();
            DeltaSignal = new List<double>();
            CalcDeltaSignal();
            PercentError = new List<double>();
            CalcPercentError();
        }

        public void CalcFinalAverageSignal()
        {
            var ld = new List<double>();
            for (int i = 0; i < Signal.Count; i++)
            {
                if (Flag[i] != Flagging.Discard)
                {
                    ld.Add(Signal[i]);
                }
            }
            FinalAverageSignal = Misc.Average(ld);
        }

        private void CalcPercentError()
        {
            double sumDifferences = 0.0;
            for (int i = 0; i <= Signal.Count - 2; i++)
            {
                for (int j = i + 1; j <= Signal.Count - 1; j++)
                {
                    sumDifferences += Math.Abs(Signal[i] - Signal[j]);
                }
            }
            double sumValues = 0.0;
            foreach (double d in Signal)
            {
                sumValues += d;
            }
            double dPercentError = sumDifferences / sumValues;
            for (int i = 0; i <= Signal.Count - 1; i++)
            {
                PercentError.Add(dPercentError);
            }
        }
        private void CalcDeltaSignal()
        {
            double dtmp = 0.0;
            int iCount = 0;
            for (int i = 0; i <= Signal.Count - 2; i++)
            {
                for (int j = i + 1; j <= Signal.Count - 1; j++)
                {
                    dtmp += Math.Abs(Signal[i] - Signal[j]);
                    iCount += 1;
                }
            }
            dtmp = dtmp / iCount;

            for (int k = 0; k <= Signal.Count - 1; k++)
            {
                DeltaSignal.Add(dtmp);
            }
        }

    }
}
