﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Forms;
using System.IO;
using Ionic.Zip;

namespace ArrayTubeAnalyzer
{
    public partial class FrmSampleQC : Form
    {
        /// <summary>Object containing all sample data for dataset.</summary>
        private SampleData _sd;

        private readonly INISettings _cp;

        private readonly FormCollection _forms;

        /// <summary>'Top' values for each label within the spot highlighting grid. Changed with resizing and spot highlighter calibration.</summary>
        private double[,] _dTop;
        /// <summary>'Left' values for each label within the spot highlighting grid. Changed with resizing and spot highlighter calibration.</summary>
        private double[,] _dLeft;
        /// <summary>Array of custom labels (coloured borders, variable border width) for the grid overlay.</summary>
        private LabelWithColours[] _lbl;
        /// <summary>Whether or not the grid overlay has been created.</summary>
        private bool _lblCreated;
        /// <summary>Index of selected array in left-hand side listview.</summary>
        public static int SelectedArrayIndex;
        /// <summary>Array tube picture box old height.</summary>
        private double _imgOldHeight;
        /// <summary>Array tube picture box old width.</summary>
        private double _imgOldWidth;
        /// <summary>Array tube picture box default height. Starting height.</summary>
        private readonly double _defaultImgHeight;
        /// <summary>Array tube picture box default width. Starting width.</summary>
        private readonly double _defaultImgWidth;

        /// <summary>Contrast factor of the AT image. Adjusted with the slider that appears when "Adjust Contrast" checkbox is checked.</summary>
        private float _contrastFactor;

        private bool _skipFinalization;

        public double GlobalError
        {
            get { return _sd.GetGlobalError(); }
        }

        public double GlobalErrorSD
        {
            get { return _sd.GetGlobalErrorSD(); }
        }

        public FrmSampleQC()
        {
            FormClosing += FrmVisualizeArraysFormClosing;
            // This call is required by the designer.
            InitializeComponent();

            _forms = new FormCollection(this);

            txtSpotX.KeyPress += SpotSizeKeyPress;
            txtSpotY.KeyPress += SpotSizeKeyPress;
            txtXOffset.KeyPress += SpotSizeKeyPress;
            txtYOffset.KeyPress += SpotSizeKeyPress;

            txtSpotX.LostFocus += delegate
            {
                int i;
                if (int.TryParse(txtSpotX.Text, out i))
                {
                    if (_sd != null) _sd.Settings.SpotWidth = i;
                    _cp.SpotWidth = i;
                }
            };
            txtSpotY.LostFocus += delegate
            {
                int i;
                if (int.TryParse(txtSpotY.Text, out i))
                {
                    if (_sd != null) _sd.Settings.SpotHeight = i;
                    _cp.SpotHeight = i;
                }
            };
            txtXOffset.LostFocus += delegate
            {
                int i;
                if (int.TryParse(txtXOffset.Text, out i))
                {
                    if (_sd != null) _sd.Settings.SpotXOffset = i;
                    _cp.SpotXOffset = i;
                }
            };
            txtYOffset.LostFocus += delegate
            {
                int i;
                if (int.TryParse(txtYOffset.Text, out i))
                {
                    if (_sd != null) _sd.Settings.SpotYOffset = i;
                    _cp.SpotYOffset = i;
                }
            };

            txtSDThresholdLow.LostFocus += TxtLostFocus;
            txtSDThresholdHigh.LostFocus += TxtLostFocus;
            txtHighSignalThreshold.LostFocus += TxtLostFocus;

            // Add any initialization after the InitializeComponent() call.
            var fInfo = new FileInfo(Application.ExecutablePath);
            string iniPath = fInfo.DirectoryName + "\\ArrayTubeAnalyzer.ini";
            _cp = new INISettings(iniPath);
            if (!_cp.Read())
            {
                _cp.Write();
            }

            if (_cp.Rows != 0)
            {
                txtNumRows.Text = _cp.Rows.ToString(CultureInfo.InvariantCulture);
            }
            else
            {
                _cp.Rows = int.Parse(txtNumRows.Text);
            }

            if (_cp.Columns != 0)
            {
                txtNumColumns.Text = _cp.Columns.ToString(CultureInfo.InvariantCulture);
            }
            else
            {
                _cp.Columns = int.Parse(txtNumColumns.Text);
            }

            if (_cp.SpotWidth == 0)
            {
                _cp.SpotWidth = int.Parse(txtSpotX.Text);
            }
            else
            {
                txtSpotX.Text = _cp.SpotWidth.ToString(CultureInfo.InvariantCulture);
            }

            if (_cp.SpotHeight == 0)
            {
                _cp.SpotHeight = int.Parse(txtSpotY.Text);
            }
            else
            {
                txtSpotY.Text = _cp.SpotHeight.ToString(CultureInfo.InvariantCulture);
            }

            if (_cp.SpotXOffset == 0)
            {
                _cp.SpotXOffset = int.Parse(txtXOffset.Text);
            }
            else
            {
                txtXOffset.Text = _cp.SpotXOffset.ToString(CultureInfo.InvariantCulture);
            }

            if (_cp.SpotYOffset == 0)
            {
                _cp.SpotYOffset = int.Parse(txtYOffset.Text);
            }
            else
            {
                txtYOffset.Text = _cp.SpotYOffset.ToString(CultureInfo.InvariantCulture);
            }

            txtSDThresholdHigh.Text = _cp.HighThresholdSD.ToString(CultureInfo.InvariantCulture);
            txtSDThresholdLow.Text = _cp.LowThresholdSD.ToString(CultureInfo.InvariantCulture);
            txtHighSignalThreshold.Text = _cp.HighSignalThreshold.ToString(CultureInfo.InvariantCulture);



            SetupCalibrationLabels();

            _defaultImgHeight = pbx.Height;
            _defaultImgWidth = pbx.Width;

            _imgOldHeight = pbx.Height;
            _imgOldWidth = pbx.Width;
            _dTop = new double[_cp.Rows, _cp.Columns];
            _dLeft = new double[_cp.Rows, _cp.Columns];
            GetNewGridPositions();
            CreateGrid();
            HideGrid();

            lvwArrays.ListViewItemSorter = new ListViewColumnSorter();
            lvwArrays.ColumnClick += (sender, args) => Misc.SortLvwByColumn(lvwArrays, args);
            lvwATinfo.ListViewItemSorter = new ListViewColumnSorter();
            lvwATinfo.ColumnClick += (sender, args) => Misc.SortLvwByColumn(lvwATinfo, args);

            EnableControls(false);

            btnExportSignalData.Click += (sender, args) =>  PromptUserToExportSignalData();
            btnOpenATAnalysisFile.Click += (sender, args) => OpenMetaDataFile();
            newATAnalysisFileToolStripMenuItem.Click += (sender, args) => NewMetaDataFile();
            aboutToolStripMenuItem.Click += (sender, args) => new AboutBox1(_forms).ShowDialog();
            btnSaveMeta.Click += (sender, args) => SaveMetaDataFile();
            btnExit.Click += (sender, args) => Close();
            btnRemoveSelectedSample.Click += (sender, args) => RemoveSelectedArray();
            btnAddSample.Click += (sender, args) => AddNewArrays();
            btnFinalizeSample.Click += (sender, args) => FinalizeArray();
            btnFinalizeAllSamples.Click += (sender, args) => FinalizeAll();
            closeToolStripMenuItem.Click += (sender, args) => CloseCurrentAnalysis();
            btnSaveSpotStripPic.Click += (sender, args) => new FrmSpotStripPic(_sd, _sd.SampleStats, _sd.Images, _sd.Calibration, _sd.Spots).Show();
            btnSaveZippedAnalysis.Click += (sender, args) => SaveZippedAnalysis();
            btnOpenZippedAnalysis.Click += (sender, args) => OpenZippedAnalysis();

        }

        private void PromptUserToExportSignalData()
        {
            using (var sfd = new SaveFileDialog())
            {
                sfd.Title = "Export Signal Data";
                sfd.FileName = string.Format("ATA_SignalData_{0}_samples_{1}", _sd.SampleCount, DateTime.Now.ToString("MM_dd_yyyy"));
                sfd.DefaultExt = "txt";

                if (sfd.ShowDialog() != DialogResult.OK)
                    return;

                WriteSignalData(sfd.FileName);
            }
        }

        private void WriteSignalData(string filename)
        {
            using (var sw = new StreamWriter(filename))
            {
                var headers = new List<string> {"Spot #", "ID"};

                foreach (string sample in _sd.SampleNames)
                {
                    headers.Add(sample);
                }
                sw.WriteLine(string.Join("\t", headers));

                for (int i = 0; i < _sd.Spots.Count; i++)
                {
                    string marker = _sd.Markers[i];

                    List<int> spots = _sd.Spots[i];
                    for (int j = 0; j < spots.Count; j++)
                    {
                        var spot = spots[j];
                        var list = new List<string> { spot.ToString(CultureInfo.InvariantCulture), marker};
                        for (int k = 0; k < _sd.SampleStats.Count; k++)
                        {
                            MarkerInfo markerInfo = _sd.SampleStats[k][i];
                            list.Add((markerInfo.Flag[j] == Flagging.Discard
                                          ? "Discarded"
                                          : markerInfo.Signal[j].ToString(CultureInfo.InvariantCulture)));
                        }

                        sw.WriteLine(string.Join("\t", list));
                    }
                }
            }
        }

        private void SpotSizeKeyPress(object sender, KeyPressEventArgs e)
        {
            int i;
            if (int.TryParse(e.KeyChar.ToString(CultureInfo.InvariantCulture), out i) || e.KeyChar == '\b')
            {
                e.Handled = false;
            }
            else if (e.KeyChar == '-')
            {
                var toolStripTextBox = sender as ToolStripTextBox;
                if (toolStripTextBox != null) e.Handled = toolStripTextBox.Text.Contains("-");
            }
            else if (e.KeyChar == (char)Keys.Enter || e.KeyChar == '\t' || e.KeyChar == (char)Keys.Space)
            {
                var textbox = sender as ToolStripTextBox;
                if (textbox == null) return;
                int size;
                if (int.TryParse(textbox.Text, out size))
                {
                    if (textbox == txtSpotX)
                    {
                        if (_sd != null) _sd.Settings.SpotWidth = size;
                        _cp.SpotWidth = size;
                    }
                    else if (textbox == txtSpotY)
                    {
                        if (_sd != null) _sd.Settings.SpotHeight = size;
                        _cp.SpotHeight = size;
                    }
                    else if (textbox == txtXOffset)
                    {
                        if (_sd != null) _sd.Settings.SpotXOffset = size;
                        _cp.SpotXOffset = size;
                    }
                    else if (textbox == txtYOffset)
                    {
                        if (_sd != null) _sd.Settings.SpotYOffset = size;
                        _cp.SpotYOffset = size;
                    }
                }

            }
            else
            {
                e.Handled = true;
            }

        }
        
        private void TxtLostFocus(object sender, EventArgs e)
        {
            double d;
            if (double.TryParse(txtSDThresholdLow.Text, out d))
            {
                _cp.LowThresholdSD = d;
            }
            if (double.TryParse(txtSDThresholdHigh.Text, out d))
            {
                _cp.HighThresholdSD = d;
            }
            if (double.TryParse(txtHighSignalThreshold.Text, out d))
            {
                _cp.HighSignalThreshold = d;
            }
            if ((_sd != null))
                RedoFlagging();
        }

        /// <summary>When the form is closed, ask the user if he wants to save any changes he has made to the metadata file.</summary>
        private void FrmVisualizeArraysFormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = ExitApplication();
        }

        public bool ExitApplication()
        {
            _cp.Write();
            if (_sd == null)
                return false;
            DialogResult dr = MessageBox.Show("Do you want to save any changes?", "Array Tube Analyzer", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
            if (dr == DialogResult.Cancel)
            {
                return true;
            }
            if (dr == DialogResult.Yes)
            {
                _sd.WriteToMetaDataFile();
            }
            _sd = null;
            return false;
        }

        #region Contrast adjustments
        /// <summary>If the contrast of the AT image is to be adjusted.</summary>
        private void ChkContrastCheckedChanged(Object sender, EventArgs e)
        {
            if (_sd == null)
                return;
            if (pbx.Image == null)
                return;
            tbrContrast.Visible = chkAdjustContrast.Checked;
            tbrContrast.Value = 25;
            tslStatus.Text = "Ready";
        }

        /// <summary>User scrolls with the trackbar to adjust the contrast on the AT image.</summary>
        private void TbrContrastScroll(Object sender, EventArgs e)
        {
            if (pbx.Image == null)
                return;
            _contrastFactor = tbrContrast.Value;
            int index = SelectedArray();
            pbx.Image = Misc.AdjustContrast((Bitmap)_sd.GetImage(index), _contrastFactor);
        }

        #endregion

        private void DrawSpotPictures(SpotMarkerArrayIndices indices)
        {
            var sic = new SpotImageCollection(_sd,
                                                              _sd.SampleStats,
                                                              _sd.Images,
                                                              _sd.Calibration,
                                                              indices.Marker,
                                                              _sd.Spots[indices.Marker].ToArray(),
                                                              _sd.Spots);
            int picWidth = _sd.Spots[indices.Marker].Count * 80;
            var bmp = new Bitmap(picWidth, 80);
            Graphics g = Graphics.FromImage(bmp);
            var f = new Font(FontFamily.GenericSansSerif, 8);
            Brush b = Brushes.Black;
            int iX = 0;
            foreach (SpotImageCrop s in sic.SpotImages)
            {
                if (s.Array != indices.Array) continue;
                MarkerInfo at = _sd.SampleStats[indices.Array][indices.Marker];
                g.DrawImage(s.ImageAT, iX, 0, 80, 80);
                g.DrawString(s.Spot.ToString(CultureInfo.InvariantCulture), f, b, new PointF(iX, 0));
                if (at.Flag[_sd.Spots[indices.Marker].IndexOf(s.Spot)] == Flagging.Discard)
                {
                    var topRight = new PointF(80 + iX - g.MeasureString("Discarded", f).Width, 0);
                    g.DrawString("Discarded", f, Brushes.Red, topRight);
                }
                var pBottomLeft = new PointF(iX, 80 - g.MeasureString(s.Signal.ToString(CultureInfo.InvariantCulture), f).Height);
                g.DrawString(s.Signal.ToString(CultureInfo.InvariantCulture), f, b, pBottomLeft);

                iX += 80;
            }
            pbxReplicates.Image = bmp;
            g.Dispose();

        }

        /// <summary>User selects something within the listview showing all of the information related to a array tube sample.</summary>
        private void LvwATinfoSelectedIndexChanged(Object sender, EventArgs e)
        {
            if (lvwATinfo.SelectedItems.Count == 0)
                return;
            if (chkCalibrate.Checked)
                return;
            int index = 0;
            for (int i = 0; i <= lvwATinfo.Items.Count - 1; i++)
            {
                if (!lvwATinfo.Items[i].Selected) continue;
                index = i;
                break;
            }

            int spot = int.Parse(lvwATinfo.Items[index].Text);
            string marker = lvwATinfo.Items[index].SubItems[3].Text;
            tslStatus.Text = string.Format("Looking at spot {0} - {1}", spot, marker);
            IEnumerable<int> spots = FindAndShowSpots(spot);
            if ((spots != null))
            {
                ShowSpot(spots);
                //point of interest for bug regarding clicking of grid spot, changing status and having the
                //highlighted spot revert back to the last listview clicked spot
                DrawSpotPictures((SpotMarkerArrayIndices)lvwATinfo.SelectedItems[0].Tag);
            }
        }

        /// <summary>When the left mouse button is released, highlight a spot on the array tube image if the left mouse button is released in a valid location.</summary>
        private void PbxMouseUp(object sender, MouseEventArgs e)
        {
            if (_sd == null)
            {
                return;
            }
            if (chkCalibrate.Checked)
                return;
            if (e.Button != MouseButtons.Left)
                return;
            try
            {
                double x = e.X;
                double y = e.Y;

                double v1 = lblTopLeft.Top;
                double h1 = lblTopLeft.Left;
                double v2 = lblTopRight.Top;
                double h2 = lblTopRight.Left;
                double v3 = lblBottomRight.Top;
                double h3 = lblBottomRight.Left;
                double v4 = lblBottomLeft.Top;
                double h4 = lblBottomLeft.Left;

                double minLeft = h1 >= h4 ? h4 : h1;

                double maxLeft;
                if (h2 >= h3)
                {
                    maxLeft = h2 + lblTopRight.Width;
                }
                else
                {
                    maxLeft = h3 + lblBottomRight.Width;
                }

                double minTop = v1 >= v2 ? v2 : v1;

                double maxtop;
                if (v4 >= v3)
                {
                    maxtop = v4 + lblBottomLeft.Height;
                }
                else
                {
                    maxtop = v3 + lblBottomRight.Height;
                }

                //make sure that the click was in a valid area on the picture box - may be problems if the picture heavily misaligned

                if (x >= minLeft & x <= maxLeft & y >= minTop & y <= maxtop)
                {
                    //find the vertical position that most closely matches the mouse button click vertical location
                    int i;
                    int j;
                    for (i = (_sd.Layout.Rows - 1); i >= 0; i += -1)
                    {
                        if (y >= _dTop[i, 0])
                        {
                            break;
                        }
                    }

                    if (i == -1)
                        return;
                    //if the value is out of the range of the array then the sub is exited 

                    //find the horizontal position that most closely matches the mouse button click horizontal location
                    for (j = (_sd.Layout.Columns - 1); j >= 0; j += -1)
                    {
                        if (x >= _dLeft[0, j])
                        {
                            break;
                        }
                    }

                    if (j == -1)
                        return;
                    //if the value is out of the range of the array then the sub is exited 

                    //find out which spot the click is closest to
                    int k = (_sd.Layout.Columns - (i + 1)) * _sd.Layout.Rows + (j + 1);
                    //determine which spot is highlighted with row and column information 
                    List<int> spots = SpotExists(k);
                    if (spots == null)
                        return;
                    //if the spot doesn't exist within the data set then the spot cannot be highlighted = exit sub
                    ShowSpot(spots);
                    //look for the spot of interest within the listview table 
                    for (int l = 0; l <= lvwATinfo.Items.Count - 1; l++)
                    {
                        int spot = int.Parse(lvwATinfo.Items[l].SubItems[0].Text);
                        if (spot == k)
                        {
                            //cannot get the highlighter to show up all of the time, only when there is a previous selection by the user on the listview table
                            lvwATinfo.Items[l].Selected = true;
                            //make sure that the clicked spot is also visible in the listview table
                            lvwATinfo.Items[l].EnsureVisible();
                            lvwATinfo.Focus();
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format("imgArray_MouseUp\n{0}", ex.Message), "Array Tube Analyzer", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>Find the spot to highlight on the array tube picture.</summary>
        /// <param name="spot">Spot number.</param>
        private IEnumerable<int> FindAndShowSpots(int spot)
        {
            foreach (List<int> spots in _sd.Spots)
            {
                if (spots.Contains(spot))
                {
                    return spots;
                }
            }
            return null;
        }

        /// <summary>Show the spot and its replicate spots on the array tube picture.</summary>
        /// <param name="spots">Array of spot numbers.</param>
        private void ShowSpot(IEnumerable<int> spots)
        {
            var font = new Font(FontFamily.GenericSansSerif, 6);
            for (int i = 0; i <= _lbl.Length - 1; i++)
            {
                LabelWithColours lbl = _lbl[i];
                if (lbl.BorderStyle == ButtonBorderStyle.Outset)
                {
                    lbl.BorderStyle = ButtonBorderStyle.Solid;
                    //need different border styles --> dotted, dashed, etc
                    lbl.BorderWidth = 1;
                    lbl.Refresh();
                    lbl.Font = font;
                }
                if (_lblCreated == false)
                {
                    lbl.Visible = false;
                }
            }

            double borderWidth = 2d * (pbx.Width / 400d);
            foreach (int spt in spots)
            {
                LabelWithColours lbl = _lbl[spt - 1];
                if (_lblCreated)
                {
                    lbl.BorderWidth = (int)borderWidth;
                    lbl.BorderStyle = ButtonBorderStyle.Outset;
                    //need different border styles --> dotted, dashed, etc
                    lbl.Font = font;
                    lbl.Refresh();
                }
                lbl.Visible = true;
            }
        }

        /// <summary>Change the flagging info for a listview item that has been double-clicked.</summary>
        private void LvwATinfoDoubleClick(object sender, EventArgs e)
        {
            if (lvwATinfo.SelectedItems.Count == 0)
                return;
            ChangeSpotStatus();
        }

        /// <summary>Change the flagging status of the selected spot on the listview and within the spot information collection.</summary>
        private void ChangeSpotStatus()
        {
            ListViewItem lvi = lvwATinfo.SelectedItems[0];
            var smaIndices = (SpotMarkerArrayIndices)lvi.Tag;

            Flagging flag = _sd.SampleStats[smaIndices.Array][smaIndices.Marker].Flag[smaIndices.Spot];
            switch (flag)
            {
                case Flagging.Flagged:
                    lvi.BackColor = _cp.C.KeepListviewColour;
                    flag = Flagging.KeepManual;
                    break;
                case Flagging.KeepAuto:
                    lvi.BackColor = _cp.C.DiscardListviewColour;
                    flag = Flagging.Discard;
                    break;
                case Flagging.KeepManual:
                    lvi.BackColor = _cp.C.DiscardListviewColour;
                    flag = Flagging.Discard;
                    break;
                case Flagging.Discard:
                    lvi.BackColor = _cp.C.FlaggedListviewColour;
                    flag = Flagging.Flagged;
                    break;
            }
            lvi.SubItems[7].Text = flag.ToString();
            _sd.SampleStats[smaIndices.Array][smaIndices.Marker].Flag[smaIndices.Spot] = flag;
            _sd.RecalculateFinalAverageSignal(smaIndices);
            RefreshFinalAverageSignal(smaIndices);
            AdjustSpotLabel(smaIndices);
            lvwArrays.SelectedItems[0].SubItems[3].Text = _sd.NumberOfFlaggedSpots(SelectedArray()).ToString(CultureInfo.InvariantCulture);
        }

        /// <summary>Returns the index number for the selected array tube sample in the listview.</summary>
        private int SelectedArray()
        {
            if (lvwArrays.SelectedIndices.Count == 0)
            {
                SelectedArrayIndex = _sd.Images.IndexOf(pbx.Image);
                return SelectedArrayIndex;
            }
            SelectedArrayIndex = (int)lvwArrays.SelectedItems[0].Tag;
            return SelectedArrayIndex;
        }

        private void RefreshFinalAverageSignal(SpotMarkerArrayIndices sga)
        {
            string sFinalAverageSignal = _sd.SampleStats[sga.Array][sga.Marker].FinalAverageSignal.ToString("0.0000");
            foreach (ListViewItem lvi in lvwATinfo.Items)
            {
                var sga2 = (SpotMarkerArrayIndices)lvi.Tag;
                // assuming same array
                if (sga.Marker == sga2.Marker)
                {
                    lvi.SubItems[6].Text = sFinalAverageSignal;
                }
            }
        }

        /// <summary>Change the spot highlighter label border colour for the spot that has had its flagging info changed.</summary>
        private void AdjustSpotLabel(SpotMarkerArrayIndices indices)
        {
            Flagging flag = _sd.SampleStats[indices.Array][indices.Marker].Flag[indices.Spot];
            LabelWithColours lbl = _lbl[_sd.Spots[indices.Marker][indices.Spot] - 1];
            switch (flag)
            {
                case Flagging.Flagged:
                    lbl.BorderColor = _cp.C.FlaggedColour;
                    //Color.Yellow
                    break;
                case Flagging.KeepAuto:
                    lbl.BorderColor = _cp.C.KeepAutoColour;
                    //Color.Lime
                    break;
                case Flagging.KeepManual:
                    lbl.BorderColor = _cp.C.KeepColour;
                    //Color.Lime
                    break;
                case Flagging.Discard:
                    lbl.BorderColor = _cp.C.DiscardColour;
                    //Color.Red
                    break;
            }
            lbl.Refresh();
        }

        /// <summary>Whether or not to show the spot numbers in the grid overlay.</summary>
        private void ChkSpotNumbersCheckedChanged(Object sender, EventArgs e)
        {
            if (_sd == null)
                return;
            ShowSpotNumbers(chkSpotNumbers.Checked);
        }

        /// <summary>Use the 'Tag' property to temporarily store the spot number if the user wants to hide the spot number in the spot highlighter labels.</summary>
        /// <param name="showNumber">Whether or not to show the spot numbers.</param>
        private void ShowSpotNumbers(bool showNumber)
        {
            if (showNumber)
            {
                foreach (LabelWithColours lbl in _lbl)
                {
                    lbl.Text = ((int)lbl.Tag).ToString(CultureInfo.InvariantCulture);
                }
            }
            else
            {
                foreach (LabelWithColours lbl in _lbl)
                {
                    lbl.Tag = int.Parse(lbl.Text);
                    lbl.Text = "";
                }
            }
        }

        /// <summary>Default positions and settings for the calibration labels.</summary>
        private void SetupCalibrationLabels()
        {
            lblTopLeft.Parent = pbx;
            lblTopLeft.BackColor = Color.Transparent;
            lblTopLeft.BringToFront();
            lblTopLeft.Location = new Point(40, 40);
            lblTopLeft.OldLeft = lblTopLeft.Left;
            lblTopLeft.OldTop = lblTopLeft.Top;

            lblTopRight.Parent = pbx;
            lblTopRight.BackColor = Color.Transparent;
            lblTopRight.BringToFront();
            lblTopRight.Location = new Point(pbx.Width - 40, 40);
            lblTopRight.OldLeft = lblTopRight.Left;
            lblTopRight.OldTop = lblTopRight.Top;

            lblBottomLeft.Parent = pbx;
            lblBottomLeft.BackColor = Color.Transparent;
            lblBottomLeft.BringToFront();
            lblBottomLeft.Location = new Point(40, pbx.Height - 40);
            lblBottomLeft.OldLeft = lblBottomLeft.Left;
            lblBottomLeft.OldTop = lblBottomLeft.Top;

            lblBottomRight.Parent = pbx;
            lblBottomRight.BackColor = Color.Transparent;
            lblBottomRight.BringToFront();
            lblBottomRight.Location = new Point(pbx.Width - 40, pbx.Height - 40);
            lblBottomRight.OldLeft = lblBottomRight.Left;
            lblBottomRight.OldTop = lblBottomRight.Top;

        }

        private void ChangeColourProfileToolStripMenuItemClick(Object sender, EventArgs e)
        {
            var f = new FrmColourProfile(_cp);
            f.Show();
        }

        /// <summary>When 'Enter' or 'Space' is pressed while looking through the listview with the array tube information, you can cycle through the flagging status.</summary>
        private void LvwATinfoKeyPress(object sender, KeyPressEventArgs e)
        {
            if (lvwATinfo.SelectedItems.Count == 0)
                return;
            if (e.KeyChar == (char)(Keys.Enter) | e.KeyChar == (char)(Keys.Space))
            {
                ChangeSpotStatus();
            }
        }

        /// <summary>Populate the listview with information about the selected array tube sample.</summary>
        private void PopulateSampleInfoListview(int index)
        {
            if (index == -1)
                return;

            var lvList = new List<ListViewItem>();

            List<MarkerInfo> sample = _sd.SampleStats[index];
            for (int i = 0; i < sample.Count; i++)
            {
                MarkerInfo marker = sample[i];
                for (int j = 0; j < marker.Signal.Count; j++)
                {
                    int spot = _sd.Spots[i][j];
                    Color bc = Color.LightGreen;
                    switch (marker.Flag[j])
                    {
                        case Flagging.Flagged:
                            bc = _cp.C.FlaggedListviewColour;
                            break;
                        case Flagging.KeepAuto:
                            bc = _cp.C.KeepAutoListviewColour;
                            break;
                        case Flagging.KeepManual:
                            bc = _cp.C.KeepListviewColour;
                            break;
                        case Flagging.Discard:
                            bc = _cp.C.DiscardListviewColour;
                            break;
                    }
                    var lvItem = new ListViewItem(new[]
                                                               {
                                                                   spot.ToString(CultureInfo.InvariantCulture).PadLeft(
                                                                       3, '0'),
                                                                   _sd.Markers[i],
                                                                   (j + 1).ToString(CultureInfo.InvariantCulture),
                                                                   marker.Signal[j].ToString("0.0000"),
                                                                   marker.PercentError[j].ToString("p"),
                                                                   marker.DeltaSignal[j].ToString("0.0000"),
                                                                   marker.FinalAverageSignal.ToString("0.0000"),
                                                                   marker.Flag[j].ToString()
                                                               }) { BackColor = bc, Tag = new SpotMarkerArrayIndices(i, j, index) };
                    lvList.Add(lvItem);
                }
            }
            lvwATinfo.Items.Clear();
            lvwATinfo.Items.AddRange(lvList.ToArray());
        }

        /// <summary>Close the current analysis.</summary>
        private void CloseCurrentAnalysis()
        {
            if (_sd == null)
                return;
            DialogResult msgboxResult = MessageBox.Show("Would you like to save your current Analysis Report?", "Array Tube Analyzer", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
            if (msgboxResult == DialogResult.Cancel)
            {
                return;
            }
            if (msgboxResult == DialogResult.Yes)
            {
                SaveMetaDataFile();
            }
            //Restore defaults for variables and controls

            pbx.Image = null;
            HideGrid();
            lvwArrays.Items.Clear();
            lvwATinfo.Items.Clear();
            lblBottomLeft.Visible = false;
            lblBottomRight.Visible = false;
            lblTopLeft.Visible = false;
            lblTopRight.Visible = false;
            lblArrayName.Text = "Array Name";
            pbxReplicates.Image = null;
            //close all other windows
            _forms.CloseAllWindows();
            EnableControls(false);
            _sd = null;
            tslStatus.Text = "Ready";
        }

        private void EnableControls(bool bEnable)
        {
            chkAdjustContrast.Enabled =
            chkCalibrate.Enabled =
            chkShowGrid.Enabled =
            chkSpotNumbers.Enabled =
            ddbView.Enabled =
            ddbSamples.Enabled =
            btnMarkerThresholdAnalysis.Enabled =
            ddbFlagging.Enabled =
            btnSaveMeta.Enabled =
            btnSaveAsMeta.Enabled =
            closeToolStripMenuItem.Enabled =
            lvwArrays.Enabled =
            lvwATinfo.Enabled =
            pbx.Enabled =
            lblArrayName.Enabled = bEnable;
        }

        /// <summary>Remove selected array tube sample.</summary>
        /// <remarks>When the user no longer wants an array tube sample to be present within the dataset along with all of its data.</remarks>
        private void RemoveSelectedArray()
        {
            if (_sd == null)
                return;
            int index = SelectedArray();
            //prompt user to see if they want to really delete this sample from the analysis
            DialogResult r = MessageBox.Show(string.Format("Are you sure you want to delete sample {0} from the analysis?", _sd.SampleNames[index]), "Remove sample from dataset?", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
            if (r == DialogResult.Yes)
            {
                _sd.RemoveSample(index);
                //update the AT listview
                PopulateSamplesListview();
                //show the first AT sample
                lvwArrays.Items[0].Selected = true;
            }
        }

        /// <summary>Open an existing metadata file.</summary>
        private void OpenMetaDataFile()
        {
            CloseCurrentAnalysis();

            var ofd = new OpenFileDialog { Multiselect = false, Filter = "Metadata File (*.meta)|*.meta|All Files (*.*)|*.*" };
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                OpenAnalysisFile(ofd.FileName);
            }
            tslStatus.Text = "Ready";
        }

        private void OpenAnalysisFile(string filename)
        {
            lvwArrays.Items.Clear();
            EnableControls(true);

            _sd = new SampleData(filename, _cp, _forms);
            if (_lbl.Length != _sd.Layout.Spots)
                CreateGrid();
            if (_sd.Forms.ArrayStats == null)
            {
                _sd.Forms.ArrayStats = new FrmArrayStats(_sd);
                _sd.Forms.ArrayStats.Show();
            }

            PopulateSamplesListview();
            lvwArrays.Items[0].Selected = true;
        }

        /// <summary>Add the array tube names to the listview with accompanying "finalization" status.</summary>
        public void PopulateSamplesListview()
        {
            if (_sd == null)
                return;
            lvwArrays.Items.Clear();
            var lvList = new List<ListViewItem>();
            for (int i = 0; i < _sd.SampleNames.Count; i++)
            {
                var lvItem = new ListViewItem { Text = _sd.SampleNames[i], Checked = _sd.SampleQC[i] };
                //Whether array tube grid has been calibrated
                const double epsilon = 0.00001;
                if (Math.Abs(_sd.GetCalibrationInfo(i).BottomLeftX - -1) < epsilon)
                {
                    lvItem.SubItems.Add("No");
                    lvItem.BackColor = Color.LightYellow;
                }
                else
                {
                    lvItem.SubItems.Add("Yes");
                    lvItem.BackColor = Color.LightGreen;
                }
                //Whether array tube sample has been finalized for further analysis
                if (_sd.SampleQC[i])
                {
                    lvItem.SubItems.Add("Yes");
                    lvItem.BackColor = Color.LimeGreen;
                }
                else
                {
                    lvItem.SubItems.Add("No");
                }

                lvItem.SubItems.Add(_sd.NumberOfFlaggedSpots(i).ToString(CultureInfo.InvariantCulture));
                lvItem.Tag = i;
                lvList.Add(lvItem);
            }
            _skipFinalization = true;
            lvwArrays.Items.AddRange(lvList.ToArray());
            _skipFinalization = false;
        }

        /// <summary>Save any changes to the current metadata file.</summary>
        public void SaveMetaDataFile()
        {
            if (_sd.FilePath == null)
            {
                SaveAsMetaFile();
            }
            else
            {
                _sd.WriteToMetaDataFile();
            }
        }

        public void SaveAsMetaFile()
        {
            using (var sfd = new SaveFileDialog())
            {
                if (_sd.FilePath == null)
                {
                    sfd.FileName = DateTime.Now.ToShortDateString().Replace("/", "-") + "-ATA_Analysis_Report.meta";
                }
                else
                {
                    sfd.FileName = _sd.FilePath.Substring(_sd.FilePath.LastIndexOf("\\", StringComparison.Ordinal) + 1);
                }

                sfd.Title = "Specify save path of analysis report file";
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    _sd.FilePath = sfd.FileName;
                    _sd.WriteToMetaDataFile();
                }
            }
        }

        /// <summary>Finalize the AT sample that the user checks in the listview.</summary>
        /// <param name="index">Index of the AT sample in the listview.</param>
        private void FinalizeArrayCheckbox(int index)
        {
            if (_skipFinalization)
                return;
            tslStatus.Text = "Finalizing Array...";
            try
            {
                _sd.SetSampleQC(index, true);
                lvwArrays.Items.Clear();
                PopulateSamplesListview();

                lvwArrays.Items[index].Selected = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("FinalizeArray\n" + ex.Message, "Array Tube Analyzer", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            tslStatus.Text = "Ready";
        }

        /// <summary>Create a new metadata file.</summary>
        private void NewMetaDataFile()
        {
            CloseCurrentAnalysis();
            AddNewArrays();
        }

        /// <summary>Add new array tube samples to new or existing metadata file.</summary>
        private void AddNewArrays()
        {
            var ofd = new OpenFileDialog
                          {
                              Multiselect = true,
                              Title = "Select Array Tube Files To Include In Analysis...",
                              Filter = "Text Files (*.txt)|*.txt|All Files (*.*)|*.*"
                          };
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                if (_sd == null)
                {
                    _sd = new SampleData(ofd.FileNames, _cp, _forms);
                }
                else
                {
                    _sd.AddSamplesToDataset(ofd.FileNames);
                }

                PopulateSamplesListview();
                lvwArrays.Items[0].Selected = true;
                SelectedArrayIndex = 0;
                EnableControls(true);
                if (_sd.Forms.ArrayStats == null)
                {
                    _sd.Forms.ArrayStats = new FrmArrayStats(_sd);
                    _sd.Forms.ArrayStats.Show();
                }
            }
        }

        /// <summary>Finalize the selected sample.</summary>
        private void FinalizeArray()
        {
            if (_sd == null)
                return;
            if (chkCalibrate.Checked)
                return;
            if (HasFlaggedSpots())
            {
                if (MessageBox.Show("There are flagged spots in this array that have not been looked at. Are you sure you want to proceed?", "Finalize Sample", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No)
                    return;
            }
            try
            {
                if (lvwArrays.Items.Count == 0)
                    return;
                int index = SelectedArray();
                _sd.SampleQC[index] = true;
                lvwArrays.Items.Clear();
                PopulateSamplesListview();

                lvwArrays.Items[index].Selected = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("FinalizeArray\n" + ex.Message, "Array Tube Analyzer", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>Definalize the array tube sample that the user unchecks in the listview with the AT samples.</summary>
        /// <param name="index">Index of the AT sample in the listview.</param>
        private void DefinalizeArrayCheckbox(int index)
        {
            if (_skipFinalization)
                return;
            try
            {
                _sd.SampleQC[index] = false;
                //WriteToMetaDataFile()
                lvwArrays.Items.Clear();
                PopulateSamplesListview();

                lvwArrays.Items[index].Selected = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("DefinalizeArrayCheckbox\n" + ex.Message, "Array Tube Analyzer", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>Check if there are flagged spots in the selected sample that have not been decided upon.</summary>
        /// <returns>Whether there are undecided upon flags within the dataset.</returns>
        private bool HasFlaggedSpots()
        {
            List<MarkerInfo> sample = _sd.SampleStats[SelectedArray()];
            foreach (MarkerInfo marker in sample)
            {
                foreach (Flagging flag in marker.Flag)
                {
                    if (flag == Flagging.Flagged)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        /// <summary>Finalize QC for all of samples.</summary>
        private void FinalizeAll()
        {
            if (_sd == null)
                return;
            if (MessageBox.Show("Are you sure you want to finalize all of the array tube samples?\n\nThere may be flagged spots in these arrays", "Array Tube Analyzer", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No)
                return;
            for (int i = 0; i < _sd.SampleCount; i++)
            {
                _sd.SampleQC[i] = true;
            }
            lvwArrays.Items.Clear();
            PopulateSamplesListview();
            lvwArrays.Items[0].Selected = true;
        }

        #region Calibration subs and events
        /// <summary>When the top left hand corner calibration label is moved with the mouse.</summary>
        private void LblTopLeftMouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                AdjustCalibrationLabel(ref lblTopLeft, e);
                SetNewOldTopAndOldLeft();
            }
        }

        /// <summary>When the top right hand corner calibration label is moved with the mouse.</summary>
        private void LblTopRightMouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                AdjustCalibrationLabel(ref lblTopRight, e);
                SetNewOldTopAndOldLeft();
            }
        }

        /// <summary>When the bottom left hand corner calibration label is moved with the mouse.</summary>
        private void LblBottomLeftMouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                AdjustCalibrationLabel(ref lblBottomLeft, e);
                SetNewOldTopAndOldLeft();
            }
        }

        /// <summary>When the bottom right hand corner calibration label is moved with the mouse.</summary>
        private void LblBottomRightMouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                AdjustCalibrationLabel(ref lblBottomRight, e);
                SetNewOldTopAndOldLeft();
            }
        }

        /// <summary>When the "Calibrate Spot Grid Overlay" checkbox is checked or unchecked.</summary>
        private void ChkCalibrateCheckedChanged(Object sender, EventArgs e)
        {
            //following code for 21x21 AT layouts to adjust for where the spots are in relation to 
            //the AT border to which the user is asked to align the grid calibrators
            if (!chkCalibrate.Checked)
            {
                if (_cp.Rows == 21 & _cp.Columns == 21)
                {
                    lblTopLeft.Location = new Point((int)Math.Round(lblTopLeft.Left * 1.12), (int)Math.Round(lblTopLeft.Top * 1.12));
                    lblTopRight.Location = new Point((int)Math.Round(lblTopRight.Left * 0.98), (int)Math.Round(lblTopRight.Top * 1.12));
                    lblBottomLeft.Location = new Point((int)Math.Round(lblBottomLeft.Left * 1.12), (int)Math.Round(lblBottomLeft.Top * 0.99));
                }
            }
            Calibrate(chkCalibrate.Checked, SelectedArray());
        }

        /// <summary>Handles checked status of chkCalibrate to either start calibration or end it and calculate and set the standardized calibration values.</summary>
        /// <param name="b"></param>
        /// <param name="index"> </param>
        private void Calibrate(bool b, int index)
        {
            chkCalibrate.Checked = b;
            if (b)
            {
                tslStatus.Text = "Calibrating corner spot highlighters...";
                //Show 4 corner calibration labels.
                lblTopLeft.Visible = true;
                lblTopRight.Visible = true;
                lblBottomLeft.Visible = true;
                lblBottomRight.Visible = false;
                chkShowGrid.Checked = false;
                HideGrid();
            }
            else
            {
                tslStatus.Text = "Ready";
                GetNewGridPositions();
                //Hide calibration labels.
                chkShowGrid.Checked = true;
                AdjustGrid(index);
                //Adjust the grid to compensate for the size of the window with the new calibration values.
            }
        }

        /// <summary>Stop the calibration with the 4 corner labels. Hide the labels and calculate the standardized calibration values.</summary>
        private void GetNewGridPositions()
        {
            lblTopLeft.Visible = false;
            lblTopRight.Visible = false;
            lblBottomLeft.Visible = false;
            lblBottomRight.Visible = false;

            CalcNewGridPositions();
            if ((_sd != null))
            {
                if (SelectedArray() == -1)
                    return;
                SaveCalibration(SelectedArrayIndex);
                int indexLvw = -1;
                for (int i = 0; i < lvwArrays.Items.Count; i++)
                {
                    if ((int)lvwArrays.Items[i].Tag == SelectedArrayIndex)
                    {
                        indexLvw = i;
                        break;
                    }
                }
                if (_sd.SampleQC[SelectedArrayIndex] == false)
                {
                    lvwArrays.Items[indexLvw].BackColor = Color.LightGreen;
                }
                lvwArrays.Items[indexLvw].SubItems[1].Text = "Yes";
            }
        }

        /// <summary>Calculate the new grid location values from the new calibration values for the 4 corner calibration labels.</summary>
        private void CalcNewGridPositions()
        {
            double h1 = lblTopLeft.Left;
            double h2 = lblTopRight.Left;
            double h4 = lblBottomLeft.Left;
            double v1 = lblTopLeft.Top;
            double v2 = lblTopRight.Top;
            double v4 = lblBottomLeft.Top;

            double dVTop = v1 - v2;
            //change in vertical position between top two corner spots; positive means that v1 is lower than v2; negative means that v2 is lower than v1
            double dHLeft = h1 - h4;
            //change in horiz. position between left most corner spots; positive = h1 farther right than h4; neg. = h4 farther right than h1

            //Distances between top, bottom, right and left corner spots
            double dw = lblTopRight.Width;
            double dh = lblBottomLeft.Height;
            //special case for 21x21 grid
            if (_cp.Rows == 21 & _cp.Columns == 21)
            {
                dw *= 0.8;
                dh *= 0.8;
            }
            double distTop = h2 - h1 + dw;
            //horiz. distance between top two corner spots
            double distLeft = v4 - v1 + dh;
            //vert. distance between left most corner spots

            //New grid overlay location values.
            for (int i = 0; i < _cp.Rows; i++)
            {
                for (int j = 0; j < _cp.Columns; j++)
                {
                    _dTop[i, j] = v1 + ((i + 1d) * distLeft / _cp.Rows) - (((j + 1d) / _cp.Columns) * dVTop) - (distLeft / _cp.Rows);
                    _dLeft[i, j] = h1 + ((j + 1d) * distTop / _cp.Columns) - (((i + 1d) / _cp.Rows) * dHLeft) - (distTop / _cp.Columns);
                }
            }
        }

        /// <summary>Handles the moving of the calibration labels when the user clicks, holds and drags the calibration labels around.</summary>
        /// <param name="l">Calibration label being moved.</param>
        /// <param name="e">Mouse events.</param>
        private void AdjustCalibrationLabel(ref LabelWithColours l, MouseEventArgs e)
        {
            l.Left = (int)(e.X + (double)l.Left - l.Width * 0.5);
            l.Top = (int)(e.Y + (double)l.Top - l.Height * 0.5);
            _imgOldHeight = pbx.Height;
            _imgOldWidth = pbx.Width;
            pbx.Refresh();
        }

        /// <summary>Set the new location properties to OldTop and OldLeft so that when the form is resized, those values can be used to adjust the grid.</summary>
        private void SetNewOldTopAndOldLeft()
        {
            lblBottomLeft.OldTop = lblBottomLeft.Top;
            lblBottomLeft.OldLeft = lblBottomLeft.Left;
            lblBottomRight.OldTop = lblBottomRight.Top;
            lblBottomRight.OldLeft = lblBottomRight.Left;
            lblTopLeft.OldTop = lblTopLeft.Top;
            lblTopLeft.OldLeft = lblTopLeft.Left;
            lblTopRight.OldTop = lblTopRight.Top;
            lblTopRight.OldLeft = lblTopRight.Left;
        }

        /// <summary>Sets the calibration data for the 4 corner calibration spots with respect to the default size of the array tube picturebox.</summary>
        /// <param name="index">Sample index</param>
        private void SaveCalibration(int index)
        {
            double heightRatio = _defaultImgHeight / pbx.Height;
            double widthRatio = _defaultImgWidth / pbx.Width;
            var c = new CalibrationInfo(lblTopLeft.Top * heightRatio, lblTopLeft.Left * widthRatio, lblTopRight.Top * heightRatio, lblTopRight.Left * widthRatio, lblBottomRight.Top * heightRatio, lblBottomRight.Left * widthRatio, lblBottomLeft.Top * heightRatio, lblBottomLeft.Left * widthRatio);
            _sd.SetCalibrationInfo(index, c);
        }

        /// <summary>Get the calibration information and adjust the 4 corner calibration spot locations to account for resizing of the form.</summary>
        /// <param name="index">Sample index.</param>
        private bool GetSpotCalibration(int index)
        {
            try
            {
                CalibrationInfo calibration = _sd.GetCalibrationInfo(index);
                const double epsilon = 0.00001;
                if (Math.Abs(calibration.BottomLeftX - -1) < epsilon)
                {
                    return false;
                }
                double heightRatio = pbx.Height / _defaultImgHeight;
                double widthRatio = pbx.Width / _defaultImgWidth;
                lblTopLeft.Top = (int)Math.Round(calibration.TopLeftY * heightRatio);
                lblTopLeft.Left = (int)Math.Round(calibration.TopLeftX * widthRatio);
                lblTopRight.Top = (int)Math.Round(calibration.TopRightY * heightRatio);
                lblTopRight.Left = (int)Math.Round(calibration.TopRightX * widthRatio);
                lblBottomRight.Top = (int)Math.Round(calibration.BottomRightY * heightRatio);
                lblBottomRight.Left = (int)Math.Round(calibration.BottomRightX * widthRatio);
                lblBottomLeft.Top = (int)Math.Round(calibration.BottomLeftY * heightRatio);
                lblBottomLeft.Left = (int)Math.Round(calibration.BottomLeftX * widthRatio);
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("GetSpotCalibration\n" + ex.Message, "Array Tube Analyzer", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }
        #endregion

        #region Image resized
        private void PbxSizeChanged(Object sender, EventArgs e)
        {
            ResizeGrid();
        }

        /// <summary>Handles adjustment of grid with respect to default calibration values.</summary>
        private void ResizeGrid()
        {
            if (lvwArrays.Items.Count == 0)
                return;
            HideGrid();
            AdjustCalibrationLabelSize(lblTopLeft);
            AdjustCalibrationLabelSize(lblBottomLeft);
            AdjustCalibrationLabelSize(lblTopRight);
            AdjustCalibrationLabelSize(lblBottomRight);

            bool b = !GetSpotCalibration(SelectedArray());
            Calibrate(b, SelectedArray());

            if (chkCalibrate.Checked == false)
            {
                chkShowGrid.Checked = true;
                AdjustGrid(SelectedArray());
            }
        }

        /// <summary>Change the size and location of the 4 corner calibration labels.</summary>
        /// <param name="l"></param>
        private void AdjustCalibrationLabelSize(LabelWithColours l)
        {
            l.Height = (int)(pbx.Height / _defaultImgHeight * 20d);
            l.Width = (int)(pbx.Width / _defaultImgWidth * 20d);
            l.Top = (int)((pbx.Height / _imgOldHeight) * (l.OldTop));
            l.Left = (int)((pbx.Width / _imgOldWidth) * (l.OldLeft));
        }
        #endregion

        #region Grid - apply, hide, adjust
        /// <summary>When the grid overlay checkbox is checked or unchecked, show or hide the grid overlay, respectively.</summary>
        private void ChkGridCheckedChanged(Object sender, EventArgs e)
        {
            if (chkShowGrid.Checked)
                chkCalibrate.Checked = false;
            ShowGrid(chkShowGrid.Checked);
        }

        /// <summary>Show or hide the spot highlighting grid overlay.</summary>
        private void ShowGrid(bool show)
        {
            if (show)
            {
                AdjustGrid(SelectedArray());
            }
            else
            {
                HideGrid();
            }
        }

        private void DisposeOfGrid()
        {
            foreach (LabelWithColours mylbl in _lbl)
            {
                mylbl.Dispose();
            }
        }

        /// <summary>Create a new spot highlighting grid overlay.</summary>
        private void CreateGrid()
        {
            _lbl = new LabelWithColours[_cp.Rows * _cp.Columns];
            _dTop = new double[_cp.Rows, _cp.Columns];
            _dLeft = new double[_cp.Rows, _cp.Columns];
            double newHeight = (lblBottomLeft.Top - lblTopLeft.Top + lblBottomLeft.Height) / (double)_cp.Rows;
            double newWidth = (lblTopRight.Left - lblTopLeft.Left + lblTopRight.Width) / (double)_cp.Columns;
            for (int i = 0; i < _cp.Rows; i++)
            {
                for (int j = 0; j < _cp.Columns; j++)
                {
                    int spot = (_cp.Rows - (i + 1)) * _cp.Columns + (j + 1);
                    var lbl = new LabelWithColours();
                    lbl.Click += LblClick;
                    lbl.Parent = pbx;
                    lbl.BringToFront();
                    lbl.Top = (int)(_dTop[i, j] + 1);
                    lbl.Left = (int)(_dLeft[i, j] + 1);
                    lbl.Height = (int)(newHeight - 1);
                    lbl.Width = (int)(newWidth - 1);
                    lbl.Text = spot.ToString(CultureInfo.InvariantCulture);
                    lbl.Tag = spot;
                    _lbl[spot - 1] = lbl;
                }
            }
            _lblCreated = true;
        }

        private void LblClick(object sender, EventArgs e)
        {
            var lbl = sender as LabelWithColours;
            if (lbl != null)
            {
                var lblSpot = (int)lbl.Tag;
                for (int i = 0; i < lvwATinfo.Items.Count; i++)
                {
                    int spot = int.Parse(lvwATinfo.Items[i].SubItems[0].Text);

                    if (spot == lblSpot)
                    {
                        //cannot get the highlighter to show up all of the time, only when there is a previous selection by the user on the listview table
                        lvwATinfo.Items[i].Selected = true;
                        //make sure that the clicked spot is also visible in the listview table
                        lvwATinfo.Items[i].EnsureVisible();
                        lvwATinfo.Focus();
                        return;
                    }
                }
            }
        }

        /// <summary>Adjust the positions and colors and visibility of the spot highlighters in the grid overlay.</summary>
        private void AdjustGrid(int sampleIndex)
        {
            if (_sd == null)
                return;
            if (sampleIndex == -1)
            {
                sampleIndex = _sd.Images.IndexOf(pbx.Image);
            }
            double dh = lblBottomLeft.Height;
            double dw = lblBottomLeft.Width;

            var font = new Font(FontFamily.GenericSansSerif, 6);

            double newHeight = Math.Round((lblBottomLeft.Top - (double)lblTopLeft.Top + dh) / _sd.Layout.Rows);
            double newWidth = Math.Round((lblTopRight.Left - (double)lblTopLeft.Left + dw) / _sd.Layout.Columns);
            for (int i = 0; i < _sd.Layout.Rows; i++)
            {
                for (int j = 0; j < _sd.Layout.Columns; j++)
                {
                    int spot = (_sd.Layout.Columns - (i + 1)) * _sd.Layout.Rows + (j + 1);
                    LabelWithColours lbl = _lbl[spot - 1];
                    List<int> spots = SpotExists(spot);
                    if (spots != null)
                    {
                        lbl.Size = new Size((int)(newWidth - 1), (int)(newHeight - 1));
                        lbl.Location = new Point((int)(_dLeft[i, j] + 1), (int)(_dTop[i, j] + 1));
                        lbl.BorderWidth = 1;
                        lbl.Font = font;
                        int markerIndex = _sd.Spots.IndexOf(spots);
                        int spotIndex = spots.IndexOf(spot);
                        MarkerInfo marker = _sd.SampleStats[sampleIndex][markerIndex];
                        string sToolTipText = string.Format("Marker: {0}\nSpot: {1}\nSignal: {2}\nDelta Signal: {3}\nPercent Error: {4}\nAverage Signal: {5}\nFlagging Status: {6}",
                            _sd.Markers[markerIndex],
                            spot,
                            marker.Signal[spotIndex].ToString("0.0000"),
                            marker.DeltaSignal[spotIndex].ToString("0.0000"),
                            marker.PercentError[spotIndex].ToString("p"),
                            marker.FinalAverageSignal.ToString("0.0000"),
                            marker.Flag[spotIndex]);
                        toolTip1.SetToolTip(_lbl[spot - 1], sToolTipText);
                        switch (marker.Flag[spotIndex])
                        {
                            case Flagging.Flagged:
                                lbl.BorderColor = _cp.C.FlaggedColour;
                                break;
                            case Flagging.KeepAuto:
                                lbl.BorderColor = _cp.C.KeepAutoColour;
                                break;
                            case Flagging.KeepManual:
                                lbl.BorderColor = _cp.C.KeepColour;
                                break;
                            case Flagging.Discard:
                                lbl.BorderColor = _cp.C.DiscardColour;
                                break;
                        }
                        lbl.Visible = true;
                    }
                    else
                    {
                        lbl.Visible = false;
                    }
                }
            }
            _lblCreated = true;
        }

        /// <summary>Check if the spot of interest exists within the dataset.</summary>
        /// <param name="spt">Spot of interest.</param>
        private List<int> SpotExists(int spt)
        {
            foreach (List<int> li in _sd.Spots)
            {
                if (li.Contains(spt))
                {
                    return li;
                }
            }
            return null;
        }

        /// <summary>Hide the spot highlighting grid overlay.</summary>
        private void HideGrid()
        {
            if (_lblCreated == false)
                return;
            foreach (LabelWithColours lbl in _lbl)
            {
                lbl.Visible = false;
            }
            _lblCreated = false;
        }
        #endregion

        /// <summary>When the checkbox beside each item in the list of array tube samples listview is checked or unchecked, the finalized status of the array tube sample is affected.</summary>
        private void OnArraysItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if (SelectedArray() == -1)
                return;
            if (e.Item.Checked)
            {
                FinalizeArrayCheckbox(e.Item.Index);
            }
            else
            {
                DefinalizeArrayCheckbox(e.Item.Index);
            }
        }

        /// <summary>When the selected array tube file is changed, get rid of the listview, setup a new listview, populate the listview, hide the grid overlay, show the correct image with the correct title, get or set grid overlay calibration.</summary>
        private void OnArraysSelectedIndexChanged(Object sender, EventArgs e)
        {
            int index = SelectedArray();
            if (index == -1)
                return;
            UpdatePicAndListview(index);
        }

        public void UpdatePicAndListview(int index)
        {
            if (index == -1)
                return;
            PopulateSampleInfoListview(index);

            pbx.Image = _sd.GetImage(index);
            lblArrayName.Text = _sd.SampleNames[index];
            //if there is calibration information for that array in the metadata file then it will be retrieved, if not then calibration will be started for the corner spots
            bool b = !GetSpotCalibration(index);
            Calibrate(b, index);

            if (_sd.Forms.ArrayStats != null)
            {
                _sd.Forms.ArrayStats.UpdateForm();
            }

            if (chkAdjustContrast.Checked)
            {
                pbx.Image = Misc.AdjustContrast((Bitmap)_sd.GetImage(index), _contrastFactor);
            }
            pbxReplicates.Image = null;
        }

        /// <summary>Recalculate flagging status for each spot within each array tube sample.</summary>
        private void RedoFlagging()
        {
            int index = SelectedArray();
            _sd.CalcFlagStatus();
            PopulateSamplesListview();
            lvwArrays.Items[index].Selected = true;
        }

        /// <summary>Show the gene threshold form after finalization of all of the data.</summary>
        private void OnAnalyzeClick(Object sender, EventArgs e)
        {
            foreach (bool sampleQC in _sd.SampleQC)
            {
                if (!sampleQC)
                {
                    if (MessageBox.Show("Not all arrays are finalized. Do you want to finalize all arrays to analyze gene thresholds?", "Array Tube Analyzer", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        FinalizeAll();
                    break; // TODO: might not be correct. Was : Exit For
                }
            }
            if (_sd.Forms.MarkerThresholdAnalysis == null)
            {
                _sd.Forms.MarkerThresholdAnalysis = new FrmMarkerThresholdAnalysis(_sd);
            }
            _sd.Forms.MarkerThresholdAnalysis.Show();
            _sd.Forms.MarkerThresholdAnalysis.Focus();
        }

        private void OnRemoveMultipleSamples(Object sender, EventArgs e)
        {
            var lSelected = new List<int>();
            DialogResult dr = Misc.ListviewSelect("Select Samples To Remove",
                                                          "Which samples would you like to remove from analysis?",
                                                          "Samples",
                                                          _sd.SampleNames.ToArray(),
                                                          ref lSelected,
                                                          true);
            if (dr == DialogResult.OK)
            {
                _sd.RemoveSamples(lSelected);
                //update the AT listview
                PopulateSamplesListview();
                //show the first AT sample
                lvwArrays.Items[0].Selected = true;
            }
        }

        private void OnShowGeneThresholdReview(Object sender, EventArgs e)
        {
            if (_sd == null)
                return;
            foreach (bool sampleChecked in _sd.SampleQC)
            {
                if (sampleChecked) continue;
                if (MessageBox.Show("Not all arrays are checked for QC. Do you want to force QC checks for all samples to continue analysis?", "Sample QC incomplete", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    FinalizeAll();
                break; // TODO: might not be correct. Was : Exit For
            }
            if (_sd.Forms.MarkerThresholdAnalysis == null)
            {
                _sd.Forms.MarkerThresholdAnalysis = new FrmMarkerThresholdAnalysis(_sd);
            }
            _sd.Forms.MarkerThresholdAnalysis.Show();
            _sd.Forms.MarkerThresholdAnalysis.Focus();
        }

        private void OnShowGeneCallReview(Object sender, EventArgs e)
        {
            if (_sd == null)
                return;
            foreach (bool sampleChecked in _sd.SampleQC)
            {
                if (sampleChecked) continue;
                if (MessageBox.Show("Not all arrays are checked for QC. Do you want to force QC checks for all samples to continue analysis?", "Sample QC incomplete", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    FinalizeAll();
                break; // TODO: might not be correct. Was : Exit For
            }

            _sd.UpdateCalls();
            //need to update gene thresholds


            if (_sd.Forms.MarkerThresholdAnalysis != null)
            {
                _sd.Forms.MarkerThresholdAnalysis.UpdateForm();
            }

            if (_sd.Forms.ReviewMarkerCalls == null)
            {
                _sd.Forms.ReviewMarkerCalls = new FrmReviewMarkerCalls(_sd);
                _sd.Forms.ReviewMarkerCalls.Show();
            }
            _sd.Forms.ReviewMarkerCalls.Focus();
        }

        private void OnShowClustering(Object sender, EventArgs e)
        {
            foreach (bool sampleChecked in _sd.SampleQC)
            {
                if (sampleChecked) continue;
                if (MessageBox.Show("Not all arrays are checked for QC. Do you want to force QC checks for all samples to continue analysis?", "Sample QC incomplete", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    FinalizeAll();
                break;
            }

            if ((_sd.Forms.MarkerThresholdAnalysis != null))
            {
                _sd.Forms.MarkerThresholdAnalysis.UpdateForm();
            }

            _sd.UpdateCalls();

            if (_sd.Forms.Clustering == null)
            {
                _sd.Forms.Clustering = new FrmClustering(_sd);
                _sd.Forms.Clustering.Show();
            }

            _sd.Forms.Clustering.Focus();
        }

        private void OnShowDiagnostics(Object sender, EventArgs e)
        {
            foreach (bool sampleChecked in _sd.SampleQC)
            {
                if (sampleChecked) continue;
                if (MessageBox.Show("Not all arrays are checked for QC. Do you want to force QC checks for all samples to continue analysis?", "Sample QC incomplete", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    FinalizeAll();
                break; // TODO: might not be correct. Was : Exit For
            }
            if ((_sd.Forms.MarkerThresholdAnalysis != null))
            {
                _sd.Forms.MarkerThresholdAnalysis.UpdateForm();
            }
            _sd.UpdateCalls();
            _sd.ClusterUPGMA();
            if (_sd.Forms.Clustering != null)
            {
                _sd.Forms.Clustering.UpdateForm();
            }

            if (_sd.Forms.Diagnosis == null)
            {
                _sd.Forms.Diagnosis = new FrmDiagnosis(_sd);
                _sd.Forms.Diagnosis.Show();
            }
            _sd.Forms.Diagnosis.Focus();
        }

        private void OnShowArrayStatisticsClick(Object sender, EventArgs e)
        {
            if (_sd.Forms.ArrayStats == null)
            {
                _sd.Forms.ArrayStats = new FrmArrayStats(_sd);
                _sd.Forms.ArrayStats.Show();
            }
            _sd.Forms.ArrayStats.Focus();
        }

        private void OnShowSpotSignalReview(Object sender, EventArgs e)
        {
            _sd.UpdateCalls();
            if (_sd.Forms.MarkerThresholdReview == null)
            {
                double dMin = Math.Floor(_sd.MinSignal * 10) / 10;
                double dMax = Math.Ceiling(_sd.MaxSignal * 10) / 10;



                _sd.Forms.MarkerThresholdReview = new FrmMarkerThresholdReview(_sd, dMin, dMax);
                _sd.Forms.MarkerThresholdReview.Show();
            }
            _sd.Forms.MarkerThresholdReview.Focus();
        }

        private void OnContextMenuOpening(Object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (_sd == null)
            {
                e.Cancel = true;
            }
            else
            {
                int index = SelectedArray();
                const double epsilon = 0.0001;
                showMarkerStripToolStripMenuItem.Enabled =
                    (Math.Abs(_sd.GetCalibrationInfo(index).BottomLeftX - -1) > epsilon);
            }
        }

        private void OnSaveAsClick(Object sender, EventArgs e)
        {
            SaveAsMetaFile();
        }

        private void OnShowMarkerStrip(Object sender, EventArgs e)
        {
            if (_sd.Forms.MarkerThresholdAnalysis != null)
            {
                _sd.Forms.MarkerThresholdAnalysis.UpdateForm();
            }

            _sd.UpdateCalls();

            int index = SelectedArray();
            var f = new FrmShowMarkerSpots(_sd, index, true);
            _sd.Forms.ShowMarkerSpots.Add(f);
            f.Show();
        }

        private void TxtNumRowsKeyPress(Object sender, KeyPressEventArgs e)
        {
            int i;
            if (int.TryParse(e.KeyChar.ToString(CultureInfo.InvariantCulture), out i) | e.KeyChar == '\b')
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void TxtNumColumnsKeyPress(Object sender, KeyPressEventArgs e)
        {
            int i;
            if (int.TryParse(e.KeyChar.ToString(CultureInfo.InvariantCulture), out i) | e.KeyChar == '\b')
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void ApplyNewGridToolStripMenuItemClick(Object sender, EventArgs e)
        {
            int rows, columns;
            if (int.TryParse(txtNumRows.Text, out rows) & int.TryParse(txtNumColumns.Text, out columns))
            {
                _sd.SetNewLayout(new SampleData.ArrayLayout(columns, rows));
                _cp.Rows = rows;
                _cp.Columns = columns;
                DisposeOfGrid();
                CreateGrid();
                tslStatus.Text = string.Format("New grid created - {0} rows, {1} columns, {2} grid spots", rows, columns, (rows * columns));
                if ((_sd != null))
                {
                    UpdatePicAndListview(SelectedArray());
                }
            }
            else
            {
                MessageBox.Show(string.Format("Invalid number of rows specified. Number of rows set back to {0}\nInvalid number of columns specified. Number of columns set back to {1}", _cp.Rows, _cp.Columns));
                txtNumRows.Text = _cp.Rows.ToString(CultureInfo.InvariantCulture);
                txtNumColumns.Text = _cp.Columns.ToString(CultureInfo.InvariantCulture);
            }
        }

        private void TxtHighSignalThresholdKeyPress(Object sender, KeyPressEventArgs e)
        {
            int i;
            if (int.TryParse(e.KeyChar.ToString(CultureInfo.InvariantCulture), out i) | e.KeyChar == '\b')
            {
                e.Handled = false;
            }
            else if (e.KeyChar == '.')
            {
                e.Handled = (txtHighSignalThreshold.Text.Contains("."));
            }
            else if (e.KeyChar == (char)(Keys.Enter) | e.KeyChar == (char)(Keys.Space) | e.KeyChar == '\t')
            {
                double d;
                if (double.TryParse(txtHighSignalThreshold.Text, out d))
                {
                    _cp.HighSignalThreshold = d;
                    if ((_sd != null))
                        RedoFlagging();
                }
            }
            else
            {
                e.Handled = true;
            }
        }

        private void TxtSDThresholdHighKeyPress(Object sender, KeyPressEventArgs e)
        {
            int i;
            if (int.TryParse(e.KeyChar.ToString(CultureInfo.InvariantCulture), out i) | e.KeyChar == '\b')
            {
                e.Handled = false;
            }
            else if (e.KeyChar == '.')
            {
                e.Handled = (txtSDThresholdHigh.Text.Contains("."));
            }
            else if (e.KeyChar == (char)(Keys.Enter) | e.KeyChar == (char)(Keys.Space) | e.KeyChar == '\t')
            {
                double d;
                if (double.TryParse(txtSDThresholdHigh.Text, out d))
                {
                    _cp.HighThresholdSD = d;
                    if ((_sd != null))
                        RedoFlagging();
                }
            }
            else
            {
                e.Handled = true;
            }
        }

        private void TxtSDThresholdLowKeyPress(object sender, KeyPressEventArgs e)
        {
            int i;
            if (int.TryParse(e.KeyChar.ToString(CultureInfo.InvariantCulture), out i) | e.KeyChar == '\b')
            {
                e.Handled = false;
            }
            else if (e.KeyChar == '.')
            {
                e.Handled = (txtSDThresholdLow.Text.Contains("."));
            }
            else if (e.KeyChar == (char)(Keys.Enter) | e.KeyChar == (char)(Keys.Space) | e.KeyChar == '\t')
            {
                double d;
                if (double.TryParse(txtSDThresholdLow.Text, out d))
                {
                    _cp.LowThresholdSD = d;
                    if ((_sd != null))
                        RedoFlagging();
                }
            }
            else
            {
                e.Handled = true;
            }
        }

        private void ResetFlaggingForSelectedArrayToolStripMenuItemClick(Object sender, EventArgs e)
        {
            if (MessageBox.Show("Reset flagging for the selected sample?", "Reset flagging for selected sample?", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                int index = SelectedArray();
                List<MarkerInfo> lat = _sd.SampleStats[index];
                foreach (MarkerInfo at in lat)
                {
                    for (int j = 0; j < at.Flag.Count; j++)
                    {
                        at.Flag[j] = Flagging.KeepAuto;
                    }
                }
                _sd.SetSampleQC(index, false);
                RedoFlagging();
                PopulateSamplesListview();
                UpdatePicAndListview(index);
            }
        }

        private void ResetFlaggingForEntireDatasetToolStripMenuItemClick(Object sender, EventArgs e)
        {
            if (MessageBox.Show("Reset flagging for entire dataset?", "Reset flagging for entire dataset?", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                foreach (List<MarkerInfo> sample in _sd.SampleStats)
                {
                    foreach (MarkerInfo marker in sample)
                    {
                        for (int i = 0; i < marker.Flag.Count; i++)
                        {
                            marker.Flag[i] = Flagging.KeepAuto;
                        }
                    }
                    _sd.SetSampleQC(_sd.SampleStats.IndexOf(sample), false);
                }
                RedoFlagging();
                PopulateSamplesListview();
                UpdatePicAndListview(SelectedArray());
            }
        }


        private void FileDragDrop(object sender, DragEventArgs e)
        {
            var files = (string[])e.Data.GetData(DataFormats.FileDrop);
            if (files.Length != 1) return;
            var f = new FileInfo(files[0]);
            if (f.Extension == ".meta")
            {
                CloseCurrentAnalysis();
                OpenAnalysisFile(f.FullName);
            }
            if (f.Extension == ".zip")
            {
                CloseCurrentAnalysis();
                LoadFromZipFile(f.FullName);
            }
        }

        private void FileDragOverOrEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.All;
        }


        private void SaveZippedAnalysis()
        {
            var sfd = new SaveFileDialog {Title = "Save analysis", FileName = "ATA.zip", DefaultExt = "zip"};
            if (sfd.ShowDialog() != DialogResult.OK) return;

            var zip = new ZipFile(sfd.FileName);

            zip.UpdateEntry("ATA.bin", SaveToBinFile().ToArray());
            foreach (Image image in _sd.Images)
            {
                var ms2 = new MemoryStream();
                Image pic = new Bitmap(image);
                pic.Save(ms2, ImageFormat.Jpeg);
                byte[] b = ms2.ToArray();
                int index = _sd.Images.IndexOf(image);
                zip.UpdateEntry(Path.Combine("pics",string.Format("{0}.jpg", _sd.GetSampleName(index))), b);
                zip.UpdateEntry(Path.Combine("rawdata", string.Format("{0}.txt", _sd.GetSampleName(index))), _sd.WriteRawDataFile(index).ToArray());
            }
            zip.Save();
        }

        public MemoryStream SaveToBinFile()
        {
            IFormatter formatter = new BinaryFormatter();
            var stream = new MemoryStream();
            formatter.Serialize(stream, _sd);
            return stream;
        }

        private void OpenZippedAnalysis()
        {
            using (var ofd = new OpenFileDialog())
            {
                ofd.Title = @"Load MIST data from zip file";
                ofd.Filter = "Zip Files (*.zip)|*.zip|All Files (*.*)|*.*";
                ofd.FileName = "ATA.zip";
                if (ofd.ShowDialog() != DialogResult.OK) return;
                CloseCurrentAnalysis();
                LoadFromZipFile(ofd.FileName);
            }
        }

        private void LoadFromZipFile(string filename)
        {
            var path = new MemoryStream();
            var pics = new List<Image>();
            using (var zip = new ZipFile(filename))
            {
                foreach (ZipEntry ze in zip.Entries)
                {
                    if (ze.FileName.Contains("ATA.bin"))
                    {
                        ze.Extract(path);
                    }
                    else if (ze.FileName.Contains("pics"))
                    {
                        var pic = new MemoryStream();
                        ze.Extract(pic);
                        pics.Add(Image.FromStream(pic));
                    }
                }
            }
            //deserialize and get SampleData from binary file
            IFormatter formatter = new BinaryFormatter();
            using (var stream = new MemoryStream(path.ToArray()))
                _sd = (SampleData)formatter.Deserialize(stream);
            path.Close();
            //finish up initializing SampleData
            _sd.SetImages(pics);
            _sd.SetFormCollection(_forms);


            lvwArrays.Items.Clear();
            EnableControls(true);

            if (_lbl.Length != _sd.Layout.Spots)
                CreateGrid();
            if (_sd.Forms.ArrayStats == null)
            {
                _sd.Forms.ArrayStats = new FrmArrayStats(_sd);
                _sd.Forms.ArrayStats.Show();
            }

            PopulateSamplesListview();
            lvwArrays.Items[0].Selected = true;
        }

    }
}
