﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;

namespace ArrayTubeAnalyzer
{
    [Serializable]
    public class INISettings
    {

        private readonly string _filename;
        public int Rows { get; set; }
        public int Columns { get; set; }

        public int SpotWidth { get; set; }
        public int SpotHeight { get; set; }

        public int SpotXOffset { get; set; }
        public int SpotYOffset { get; set; }

        #region "frmSampleQC"
        public double LowThresholdSD { get; set; }
        public double HighThresholdSD { get; set; }

        public double HighSignalThreshold { get; set; }
        #endregion
        [Serializable]
        public struct Colours
        {
            #region "frmSampleQC"
            public Color FlaggedColour { get; set; }
            public Color KeepColour { get; set; }
            public Color KeepAutoColour { get; set; }
            public Color DiscardColour { get; set; }

            public Color FlaggedListviewColour { get; set; }
            public Color KeepListviewColour { get; set; }
            public Color KeepAutoListviewColour { get; set; }
            public Color DiscardListviewColour { get; set; }
            #endregion

            #region "frmReviewMarkerCalls"
            public Color AbsentColour { get; set; }
            public Color PresentColour { get; set; }
            public Color AbsentFlaggedColour { get; set; }
            public Color PresentFlaggedColour { get; set; }
            public Color AbsentAutoColour { get; set; }
            public Color PresentAutoColour { get; set; }

            public Color AbsentListviewColour { get; set; }
            public Color PresentListviewColour { get; set; }
            public Color AbsentFlaggedListviewColour { get; set; }
            public Color PresentFlaggedListviewColour { get; set; }
            public Color AbsentAutoListviewColour { get; set; }
            public Color PresentAutoListviewColour { get; set; }
            #endregion

            #region "frmClustering"
            public Color StrainOddColour { get; set; }
            public Color StrainEvenColour { get; set; }

            public Color AbsentClusteringOddColour { get; set; }
            public Color PresentClusteringOddColour { get; set; }

            public Color AbsentClusteringEvenColour { get; set; }
            public Color PresentClusteringEvenColour { get; set; }
            #endregion
        }

        #region "frmReviewMarkerCalls"
        public double ThresholdFlaggingFactor { get; set; }

        public double GlobalBackgroundCutoff { get; set; }
        public double GlobalBackgroundFactor { get; set; }

        public double PositiveSignalCutoff { get; set; }
        public double PositiveSignalFactor { get; set; }

        public int WindowSize { get; set; }
        public double BinSize { get; set; }
        #endregion


        public Colours C;

        public INISettings(string filename)
        {
            _filename = filename;

            //frmVisualizeArrays
            C.FlaggedColour = Color.Yellow;
            C.KeepColour = Color.Lime;
            C.KeepAutoColour = Color.Lime;
            C.DiscardColour = Color.Gray;

            C.FlaggedListviewColour = Color.Yellow;
            C.KeepListviewColour = Color.Lime;
            C.KeepAutoListviewColour = Color.LightGreen;
            C.DiscardListviewColour = Color.Gray;
            //frmReviewSpotCalling
            C.AbsentColour = Color.Red;
            C.PresentColour = Color.Lime;
            C.AbsentFlaggedColour = Color.Yellow;
            C.PresentFlaggedColour = Color.Yellow;
            C.AbsentAutoColour = Color.Red;
            C.PresentAutoColour = Color.Lime;

            C.AbsentListviewColour = Color.Red;
            C.PresentListviewColour = Color.Lime;
            C.AbsentFlaggedListviewColour = Color.Yellow;
            C.PresentFlaggedListviewColour = Color.Yellow;
            C.AbsentAutoListviewColour = Color.LightSalmon;
            C.PresentAutoListviewColour = Color.LightGreen;

            //frmClustering
            C.StrainOddColour = Color.White;
            C.StrainEvenColour = Color.PaleGreen;

            C.AbsentClusteringOddColour = Color.LightBlue;
            C.PresentClusteringOddColour = Color.DarkBlue;

            C.AbsentClusteringEvenColour = Color.PaleTurquoise;
            C.PresentClusteringEvenColour = Color.DarkSlateBlue;

            HighThresholdSD = 2;
            LowThresholdSD = 1;
            HighSignalThreshold = 0.2;

            ThresholdFlaggingFactor = 1;

            GlobalBackgroundCutoff = 0.25;
            GlobalBackgroundFactor = 0.2;

            PositiveSignalCutoff = 0.25;
            PositiveSignalFactor = 0.50;

            WindowSize = 5;
            BinSize = 0.02;

            SpotWidth = 30;
            SpotHeight = 30;

            SpotXOffset = 0;
            SpotYOffset = 0;
        }

        public bool Read()
        {
            if (!System.IO.File.Exists(_filename))
                return false;
            try
            {
                using (var sr = new System.IO.StreamReader(_filename))
                {
                    C.FlaggedColour = ColorTranslator.FromHtml(sr.ReadLine());
                    C.KeepColour = ColorTranslator.FromHtml(sr.ReadLine());
                    C.KeepAutoColour = ColorTranslator.FromHtml(sr.ReadLine());
                    C.DiscardColour = ColorTranslator.FromHtml(sr.ReadLine());

                    C.FlaggedListviewColour = ColorTranslator.FromHtml(sr.ReadLine());
                    C.KeepListviewColour = ColorTranslator.FromHtml(sr.ReadLine());
                    C.KeepAutoListviewColour = ColorTranslator.FromHtml(sr.ReadLine());
                    C.DiscardListviewColour = ColorTranslator.FromHtml(sr.ReadLine());

                    C.AbsentColour = ColorTranslator.FromHtml(sr.ReadLine());
                    C.PresentColour = ColorTranslator.FromHtml(sr.ReadLine());
                    C.AbsentFlaggedColour = ColorTranslator.FromHtml(sr.ReadLine());
                    C.PresentFlaggedColour = ColorTranslator.FromHtml(sr.ReadLine());
                    C.AbsentAutoColour = ColorTranslator.FromHtml(sr.ReadLine());
                    C.PresentAutoColour = ColorTranslator.FromHtml(sr.ReadLine());

                    C.AbsentListviewColour = ColorTranslator.FromHtml(sr.ReadLine());
                    C.PresentListviewColour = ColorTranslator.FromHtml(sr.ReadLine());
                    C.AbsentFlaggedListviewColour = ColorTranslator.FromHtml(sr.ReadLine());
                    C.PresentFlaggedListviewColour = ColorTranslator.FromHtml(sr.ReadLine());
                    C.AbsentAutoListviewColour = ColorTranslator.FromHtml(sr.ReadLine());
                    C.PresentAutoListviewColour = ColorTranslator.FromHtml(sr.ReadLine());

                    C.StrainOddColour = ColorTranslator.FromHtml(sr.ReadLine());
                    C.StrainEvenColour = ColorTranslator.FromHtml(sr.ReadLine());

                    C.AbsentClusteringOddColour = ColorTranslator.FromHtml(sr.ReadLine());
                    C.PresentClusteringOddColour = ColorTranslator.FromHtml(sr.ReadLine());

                    C.AbsentClusteringEvenColour = ColorTranslator.FromHtml(sr.ReadLine());
                    C.PresentClusteringEvenColour = ColorTranslator.FromHtml(sr.ReadLine());

                    if (!sr.EndOfStream)
                    {
                        Rows = int.Parse(sr.ReadLine());
                        Columns = int.Parse(sr.ReadLine());
                    }
                    if (!sr.EndOfStream)
                    {
                        HighThresholdSD = double.Parse(sr.ReadLine());
                        LowThresholdSD = double.Parse(sr.ReadLine());

                        HighSignalThreshold = double.Parse(sr.ReadLine());

                        ThresholdFlaggingFactor = double.Parse(sr.ReadLine());

                        GlobalBackgroundCutoff = double.Parse(sr.ReadLine());
                        GlobalBackgroundFactor = double.Parse(sr.ReadLine());

                        PositiveSignalCutoff = double.Parse(sr.ReadLine());
                        PositiveSignalFactor = double.Parse(sr.ReadLine());

                        WindowSize = int.Parse(sr.ReadLine());
                        BinSize = double.Parse(sr.ReadLine());
                    }
                    if (!sr.EndOfStream)
                    {
                        SpotWidth = int.Parse(sr.ReadLine());
                        SpotHeight = int.Parse(sr.ReadLine());
                        SpotXOffset = int.Parse(sr.ReadLine());
                        SpotYOffset = int.Parse(sr.ReadLine());
                    }
                    //sr.Close()
                }
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        public void Write()
        {
            using (var sw = new System.IO.StreamWriter(_filename))
            {
                sw.WriteLine(ColorTranslator.ToHtml(C.FlaggedColour));
                sw.WriteLine(ColorTranslator.ToHtml(C.KeepColour));
                sw.WriteLine(ColorTranslator.ToHtml(C.KeepAutoColour));
                sw.WriteLine(ColorTranslator.ToHtml(C.DiscardColour));

                sw.WriteLine(ColorTranslator.ToHtml(C.FlaggedListviewColour));
                sw.WriteLine(ColorTranslator.ToHtml(C.KeepListviewColour));
                sw.WriteLine(ColorTranslator.ToHtml(C.KeepAutoListviewColour));
                sw.WriteLine(ColorTranslator.ToHtml(C.DiscardListviewColour));

                sw.WriteLine(ColorTranslator.ToHtml(C.AbsentColour));
                sw.WriteLine(ColorTranslator.ToHtml(C.PresentColour));
                sw.WriteLine(ColorTranslator.ToHtml(C.AbsentFlaggedColour));
                sw.WriteLine(ColorTranslator.ToHtml(C.PresentFlaggedColour));
                sw.WriteLine(ColorTranslator.ToHtml(C.AbsentAutoColour));
                sw.WriteLine(ColorTranslator.ToHtml(C.PresentAutoColour));

                sw.WriteLine(ColorTranslator.ToHtml(C.AbsentListviewColour));
                sw.WriteLine(ColorTranslator.ToHtml(C.PresentListviewColour));
                sw.WriteLine(ColorTranslator.ToHtml(C.AbsentFlaggedListviewColour));
                sw.WriteLine(ColorTranslator.ToHtml(C.PresentFlaggedListviewColour));
                sw.WriteLine(ColorTranslator.ToHtml(C.AbsentAutoListviewColour));
                sw.WriteLine(ColorTranslator.ToHtml(C.PresentAutoListviewColour));

                sw.WriteLine(ColorTranslator.ToHtml(C.StrainOddColour));
                sw.WriteLine(ColorTranslator.ToHtml(C.StrainEvenColour));

                sw.WriteLine(ColorTranslator.ToHtml(C.AbsentClusteringOddColour));
                sw.WriteLine(ColorTranslator.ToHtml(C.PresentClusteringOddColour));

                sw.WriteLine(ColorTranslator.ToHtml(C.AbsentClusteringEvenColour));
                sw.WriteLine(ColorTranslator.ToHtml(C.PresentClusteringEvenColour));

                sw.WriteLine(Rows);
                sw.WriteLine(Columns);

                sw.WriteLine(HighThresholdSD);
                sw.WriteLine(LowThresholdSD);
                sw.WriteLine(HighSignalThreshold);

                sw.WriteLine(ThresholdFlaggingFactor);

                sw.WriteLine(GlobalBackgroundCutoff);
                sw.WriteLine(GlobalBackgroundFactor);

                sw.WriteLine(PositiveSignalCutoff);
                sw.WriteLine(PositiveSignalFactor);

                sw.WriteLine(WindowSize);
                sw.WriteLine(BinSize);

                sw.WriteLine(SpotWidth);
                sw.WriteLine(SpotHeight);
                sw.WriteLine(SpotXOffset);
                sw.WriteLine(SpotYOffset);
                //sw.Close
            }
        }
        
#region The following methods are used for the old serialization method (writing the .meta file); basically obsolete now

        public string GetINIValues()
        {
            var tmp = new List<string>
                          {
                              ColorTranslator.ToHtml(C.FlaggedColour),
                              ColorTranslator.ToHtml(C.KeepColour),
                              ColorTranslator.ToHtml(C.KeepAutoColour),
                              ColorTranslator.ToHtml(C.DiscardColour),
                              ColorTranslator.ToHtml(C.FlaggedListviewColour),
                              ColorTranslator.ToHtml(C.KeepListviewColour),
                              ColorTranslator.ToHtml(C.KeepAutoListviewColour),
                              ColorTranslator.ToHtml(C.DiscardListviewColour),
                              ColorTranslator.ToHtml(C.AbsentColour),
                              ColorTranslator.ToHtml(C.PresentColour),
                              ColorTranslator.ToHtml(C.AbsentFlaggedColour),
                              ColorTranslator.ToHtml(C.PresentFlaggedColour),
                              ColorTranslator.ToHtml(C.AbsentAutoColour),
                              ColorTranslator.ToHtml(C.PresentAutoColour),
                              ColorTranslator.ToHtml(C.AbsentListviewColour),
                              ColorTranslator.ToHtml(C.PresentListviewColour),
                              ColorTranslator.ToHtml(C.AbsentFlaggedListviewColour),
                              ColorTranslator.ToHtml(C.PresentFlaggedListviewColour),
                              ColorTranslator.ToHtml(C.AbsentAutoListviewColour),
                              ColorTranslator.ToHtml(C.PresentAutoListviewColour),
                              ColorTranslator.ToHtml(C.StrainOddColour),
                              ColorTranslator.ToHtml(C.StrainEvenColour),
                              ColorTranslator.ToHtml(C.AbsentClusteringOddColour),
                              ColorTranslator.ToHtml(C.PresentClusteringOddColour),
                              ColorTranslator.ToHtml(C.AbsentClusteringEvenColour),
                              ColorTranslator.ToHtml(C.PresentClusteringEvenColour),
                              Rows.ToString(CultureInfo.InvariantCulture),
                              Columns.ToString(CultureInfo.InvariantCulture),
                              HighThresholdSD.ToString(CultureInfo.InvariantCulture),
                              LowThresholdSD.ToString(CultureInfo.InvariantCulture),
                              HighSignalThreshold.ToString(CultureInfo.InvariantCulture),
                              ThresholdFlaggingFactor.ToString(CultureInfo.InvariantCulture),
                              GlobalBackgroundCutoff.ToString(CultureInfo.InvariantCulture),
                              GlobalBackgroundFactor.ToString(CultureInfo.InvariantCulture),
                              PositiveSignalCutoff.ToString(CultureInfo.InvariantCulture),
                              PositiveSignalFactor.ToString(CultureInfo.InvariantCulture),
                              WindowSize.ToString(CultureInfo.InvariantCulture),
                              BinSize.ToString(CultureInfo.InvariantCulture)
                          };

            return string.Join("\t", tmp);
        }

        public void SetINIValues(string values)
        {
            string[] tmp = values.Split('\t');
            int i = 0;
            C.FlaggedColour = ColorTranslator.FromHtml(tmp[i++]);
            C.KeepColour = ColorTranslator.FromHtml(tmp[i++]);
            C.KeepAutoColour = ColorTranslator.FromHtml(tmp[i++]);
            C.DiscardColour = ColorTranslator.FromHtml(tmp[i++]);

            C.FlaggedListviewColour = ColorTranslator.FromHtml(tmp[i++]);
            C.KeepListviewColour = ColorTranslator.FromHtml(tmp[i++]);
            C.KeepAutoListviewColour = ColorTranslator.FromHtml(tmp[i++]);
            C.DiscardListviewColour = ColorTranslator.FromHtml(tmp[i++]);

            C.AbsentColour = ColorTranslator.FromHtml(tmp[i++]);
            C.PresentColour = ColorTranslator.FromHtml(tmp[i++]);
            C.AbsentFlaggedColour = ColorTranslator.FromHtml(tmp[i++]);
            C.PresentFlaggedColour = ColorTranslator.FromHtml(tmp[i++]);
            C.AbsentAutoColour = ColorTranslator.FromHtml(tmp[i++]);
            C.PresentAutoColour = ColorTranslator.FromHtml(tmp[i++]);

            C.AbsentListviewColour = ColorTranslator.FromHtml(tmp[i++]);
            C.PresentListviewColour = ColorTranslator.FromHtml(tmp[i++]);
            C.AbsentFlaggedListviewColour = ColorTranslator.FromHtml(tmp[i++]);
            C.PresentFlaggedListviewColour = ColorTranslator.FromHtml(tmp[i++]);
            C.AbsentAutoListviewColour = ColorTranslator.FromHtml(tmp[i++]);
            C.PresentAutoListviewColour = ColorTranslator.FromHtml(tmp[i++]);

            C.StrainOddColour = ColorTranslator.FromHtml(tmp[i++]);
            C.StrainEvenColour = ColorTranslator.FromHtml(tmp[i++]);

            C.AbsentClusteringOddColour = ColorTranslator.FromHtml(tmp[i++]);
            C.PresentClusteringOddColour = ColorTranslator.FromHtml(tmp[i++]);

            C.AbsentClusteringEvenColour = ColorTranslator.FromHtml(tmp[i++]);
            C.PresentClusteringEvenColour = ColorTranslator.FromHtml(tmp[i++]);

            Rows = int.Parse(tmp[i++]);
            Columns = int.Parse(tmp[i++]);
            HighThresholdSD = double.Parse(tmp[i++]);
            LowThresholdSD = double.Parse(tmp[i++]);

            HighSignalThreshold = double.Parse(tmp[i++]);

            ThresholdFlaggingFactor = double.Parse(tmp[i++]);

            GlobalBackgroundCutoff = double.Parse(tmp[i++]);
            GlobalBackgroundFactor = double.Parse(tmp[i++]);

            PositiveSignalCutoff = double.Parse(tmp[i++]);
            PositiveSignalFactor = double.Parse(tmp[i++]);

            WindowSize = int.Parse(tmp[i++]);
            BinSize = double.Parse(tmp[i]);
        }
#endregion

    }

}
