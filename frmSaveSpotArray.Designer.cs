﻿namespace ArrayTubeAnalyzer
{
    partial class FrmSaveSpotArray
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSaveSpotArray));
            this.chkAmbiguous = new System.Windows.Forms.CheckBox();
            this.btnAmbiguous = new System.Windows.Forms.Button();
            this.lblAmbiguous = new System.Windows.Forms.Label();
            this.btnSaveSampleSelection = new System.Windows.Forms.Button();
            this.btnLoadSampleSelection = new System.Windows.Forms.Button();
            this.lblSamples = new System.Windows.Forms.Label();
            this.ColumnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chkOneSpotPerMarker = new System.Windows.Forms.CheckBox();
            this.btnSaveGeneSelection = new System.Windows.Forms.Button();
            this.btnLoadGeneSelection = new System.Windows.Forms.Button();
            this.btnSpotSignalFont = new System.Windows.Forms.Button();
            this.lblSignalFont = new System.Windows.Forms.Label();
            this.chkShowSignal = new System.Windows.Forms.CheckBox();
            this.lblGenes = new System.Windows.Forms.Label();
            this.ColumnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnBuildSavePic = new System.Windows.Forms.Button();
            this.btnFont = new System.Windows.Forms.Button();
            this.lblFont = new System.Windows.Forms.Label();
            this.txtSave = new System.Windows.Forms.TextBox();
            this.lblPresent = new System.Windows.Forms.Label();
            this.btnAbsent = new System.Windows.Forms.Button();
            this.ToolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.StatusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tslStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.ProgressBar1 = new System.Windows.Forms.ToolStripProgressBar();
            this.lvwSamples = new ArrayTubeAnalyzer.ListViewFF();
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvwGenes = new ArrayTubeAnalyzer.ListViewFF();
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnSave = new System.Windows.Forms.Button();
            this.lblSave = new System.Windows.Forms.Label();
            this.btnPresent = new System.Windows.Forms.Button();
            this.lblAbsent = new System.Windows.Forms.Label();
            this.ToolStripContainer1.BottomToolStripPanel.SuspendLayout();
            this.ToolStripContainer1.ContentPanel.SuspendLayout();
            this.ToolStripContainer1.SuspendLayout();
            this.StatusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // chkAmbiguous
            // 
            this.chkAmbiguous.AutoSize = true;
            this.chkAmbiguous.Location = new System.Drawing.Point(355, 51);
            this.chkAmbiguous.Name = "chkAmbiguous";
            this.chkAmbiguous.Size = new System.Drawing.Size(138, 17);
            this.chkAmbiguous.TabIndex = 24;
            this.chkAmbiguous.Text = "Show Ambiguous Spots";
            this.chkAmbiguous.UseVisualStyleBackColor = true;
            this.chkAmbiguous.CheckedChanged += new System.EventHandler(this.ChkAmbiguousCheckedChanged);
            // 
            // btnAmbiguous
            // 
            this.btnAmbiguous.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnAmbiguous.Enabled = false;
            this.btnAmbiguous.Location = new System.Drawing.Point(75, 46);
            this.btnAmbiguous.Name = "btnAmbiguous";
            this.btnAmbiguous.Size = new System.Drawing.Size(274, 23);
            this.btnAmbiguous.TabIndex = 23;
            this.btnAmbiguous.Text = "Select Colour For Absent Spots";
            this.btnAmbiguous.UseVisualStyleBackColor = false;
            this.btnAmbiguous.Click += new System.EventHandler(this.BtnAmbiguousClick);
            // 
            // lblAmbiguous
            // 
            this.lblAmbiguous.AutoSize = true;
            this.lblAmbiguous.Enabled = false;
            this.lblAmbiguous.Location = new System.Drawing.Point(12, 51);
            this.lblAmbiguous.Name = "lblAmbiguous";
            this.lblAmbiguous.Size = new System.Drawing.Size(59, 13);
            this.lblAmbiguous.TabIndex = 22;
            this.lblAmbiguous.Text = "Ambiguous";
            // 
            // btnSaveSampleSelection
            // 
            this.btnSaveSampleSelection.Location = new System.Drawing.Point(529, 353);
            this.btnSaveSampleSelection.Name = "btnSaveSampleSelection";
            this.btnSaveSampleSelection.Size = new System.Drawing.Size(160, 23);
            this.btnSaveSampleSelection.TabIndex = 21;
            this.btnSaveSampleSelection.Text = "Save Sample Selection";
            this.btnSaveSampleSelection.UseVisualStyleBackColor = true;
            this.btnSaveSampleSelection.Click += new System.EventHandler(this.BtnSaveSampleSelectionClick);
            // 
            // btnLoadSampleSelection
            // 
            this.btnLoadSampleSelection.Location = new System.Drawing.Point(355, 353);
            this.btnLoadSampleSelection.Name = "btnLoadSampleSelection";
            this.btnLoadSampleSelection.Size = new System.Drawing.Size(168, 23);
            this.btnLoadSampleSelection.TabIndex = 20;
            this.btnLoadSampleSelection.Text = "Load Sample Selection";
            this.btnLoadSampleSelection.UseVisualStyleBackColor = true;
            this.btnLoadSampleSelection.Click += new System.EventHandler(this.BtnLoadSampleSelectionClick);
            // 
            // lblSamples
            // 
            this.lblSamples.AutoSize = true;
            this.lblSamples.Location = new System.Drawing.Point(352, 333);
            this.lblSamples.Name = "lblSamples";
            this.lblSamples.Size = new System.Drawing.Size(39, 13);
            this.lblSamples.TabIndex = 19;
            this.lblSamples.Text = "Label1";
            // 
            // ColumnHeader2
            // 
            this.ColumnHeader2.Text = "Samples To Show";
            this.ColumnHeader2.Width = 300;
            // 
            // chkOneSpotPerMarker
            // 
            this.chkOneSpotPerMarker.AutoSize = true;
            this.chkOneSpotPerMarker.Location = new System.Drawing.Point(529, 383);
            this.chkOneSpotPerMarker.Name = "chkOneSpotPerMarker";
            this.chkOneSpotPerMarker.Size = new System.Drawing.Size(156, 17);
            this.chkOneSpotPerMarker.TabIndex = 17;
            this.chkOneSpotPerMarker.Text = "Show One Spot Per Marker";
            this.chkOneSpotPerMarker.UseVisualStyleBackColor = true;
            // 
            // btnSaveGeneSelection
            // 
            this.btnSaveGeneSelection.Location = new System.Drawing.Point(189, 353);
            this.btnSaveGeneSelection.Name = "btnSaveGeneSelection";
            this.btnSaveGeneSelection.Size = new System.Drawing.Size(160, 23);
            this.btnSaveGeneSelection.TabIndex = 16;
            this.btnSaveGeneSelection.Text = "Save Gene Selection";
            this.btnSaveGeneSelection.UseVisualStyleBackColor = true;
            this.btnSaveGeneSelection.Click += new System.EventHandler(this.BtnSaveGeneSelectionClick);
            // 
            // btnLoadGeneSelection
            // 
            this.btnLoadGeneSelection.Location = new System.Drawing.Point(15, 353);
            this.btnLoadGeneSelection.Name = "btnLoadGeneSelection";
            this.btnLoadGeneSelection.Size = new System.Drawing.Size(168, 23);
            this.btnLoadGeneSelection.TabIndex = 15;
            this.btnLoadGeneSelection.Text = "Load Gene Selection";
            this.btnLoadGeneSelection.UseVisualStyleBackColor = true;
            this.btnLoadGeneSelection.Click += new System.EventHandler(this.BtnLoadGeneSelectionClick);
            // 
            // btnSpotSignalFont
            // 
            this.btnSpotSignalFont.BackColor = System.Drawing.SystemColors.Control;
            this.btnSpotSignalFont.Enabled = false;
            this.btnSpotSignalFont.Location = new System.Drawing.Point(415, 75);
            this.btnSpotSignalFont.Name = "btnSpotSignalFont";
            this.btnSpotSignalFont.Size = new System.Drawing.Size(274, 43);
            this.btnSpotSignalFont.TabIndex = 14;
            this.btnSpotSignalFont.Text = "Font For Spot Signal";
            this.btnSpotSignalFont.UseVisualStyleBackColor = false;
            this.btnSpotSignalFont.Click += new System.EventHandler(this.BtnSpotSignalFontClick);
            // 
            // lblSignalFont
            // 
            this.lblSignalFont.AutoSize = true;
            this.lblSignalFont.Enabled = false;
            this.lblSignalFont.Location = new System.Drawing.Point(352, 80);
            this.lblSignalFont.Name = "lblSignalFont";
            this.lblSignalFont.Size = new System.Drawing.Size(60, 13);
            this.lblSignalFont.TabIndex = 13;
            this.lblSignalFont.Text = "Signal Font";
            // 
            // chkShowSignal
            // 
            this.chkShowSignal.AutoSize = true;
            this.chkShowSignal.Location = new System.Drawing.Point(355, 381);
            this.chkShowSignal.Name = "chkShowSignal";
            this.chkShowSignal.Size = new System.Drawing.Size(156, 17);
            this.chkShowSignal.TabIndex = 12;
            this.chkShowSignal.Text = "Show Signal For Each Spot";
            this.chkShowSignal.UseVisualStyleBackColor = true;
            this.chkShowSignal.CheckedChanged += new System.EventHandler(this.ChkShowSignalCheckedChanged);
            // 
            // lblGenes
            // 
            this.lblGenes.AutoSize = true;
            this.lblGenes.Location = new System.Drawing.Point(12, 333);
            this.lblGenes.Name = "lblGenes";
            this.lblGenes.Size = new System.Drawing.Size(39, 13);
            this.lblGenes.TabIndex = 11;
            this.lblGenes.Text = "Label1";
            // 
            // ColumnHeader1
            // 
            this.ColumnHeader1.Text = "Markers/Genes To Show";
            this.ColumnHeader1.Width = 300;
            // 
            // btnBuildSavePic
            // 
            this.btnBuildSavePic.Enabled = false;
            this.btnBuildSavePic.Location = new System.Drawing.Point(15, 417);
            this.btnBuildSavePic.Name = "btnBuildSavePic";
            this.btnBuildSavePic.Size = new System.Drawing.Size(674, 23);
            this.btnBuildSavePic.TabIndex = 9;
            this.btnBuildSavePic.Text = "Save Spot Array Image";
            this.btnBuildSavePic.UseVisualStyleBackColor = true;
            this.btnBuildSavePic.Click += new System.EventHandler(this.BtnBuildSavePicClick);
            // 
            // btnFont
            // 
            this.btnFont.BackColor = System.Drawing.SystemColors.Control;
            this.btnFont.Location = new System.Drawing.Point(75, 75);
            this.btnFont.Name = "btnFont";
            this.btnFont.Size = new System.Drawing.Size(274, 43);
            this.btnFont.TabIndex = 8;
            this.btnFont.Text = "Font For Spot Array Headers";
            this.btnFont.UseVisualStyleBackColor = false;
            this.btnFont.Click += new System.EventHandler(this.BtnFontClick);
            // 
            // lblFont
            // 
            this.lblFont.AutoSize = true;
            this.lblFont.Location = new System.Drawing.Point(12, 80);
            this.lblFont.Name = "lblFont";
            this.lblFont.Size = new System.Drawing.Size(66, 13);
            this.lblFont.TabIndex = 7;
            this.lblFont.Text = "Header Font";
            // 
            // txtSave
            // 
            this.txtSave.Location = new System.Drawing.Point(75, 382);
            this.txtSave.Name = "txtSave";
            this.txtSave.Size = new System.Drawing.Size(242, 20);
            this.txtSave.TabIndex = 6;
            // 
            // lblPresent
            // 
            this.lblPresent.AutoSize = true;
            this.lblPresent.Location = new System.Drawing.Point(352, 22);
            this.lblPresent.Name = "lblPresent";
            this.lblPresent.Size = new System.Drawing.Size(43, 13);
            this.lblPresent.TabIndex = 2;
            this.lblPresent.Text = "Present";
            // 
            // btnAbsent
            // 
            this.btnAbsent.BackColor = System.Drawing.Color.Red;
            this.btnAbsent.Location = new System.Drawing.Point(75, 17);
            this.btnAbsent.Name = "btnAbsent";
            this.btnAbsent.Size = new System.Drawing.Size(274, 23);
            this.btnAbsent.TabIndex = 1;
            this.btnAbsent.Text = "Select Colour For Absent Spots";
            this.btnAbsent.UseVisualStyleBackColor = false;
            this.btnAbsent.Click += new System.EventHandler(this.BtnAbsentClick);
            // 
            // ToolStripContainer1
            // 
            // 
            // ToolStripContainer1.BottomToolStripPanel
            // 
            this.ToolStripContainer1.BottomToolStripPanel.Controls.Add(this.StatusStrip1);
            // 
            // ToolStripContainer1.ContentPanel
            // 
            this.ToolStripContainer1.ContentPanel.Controls.Add(this.lvwSamples);
            this.ToolStripContainer1.ContentPanel.Controls.Add(this.lvwGenes);
            this.ToolStripContainer1.ContentPanel.Controls.Add(this.chkAmbiguous);
            this.ToolStripContainer1.ContentPanel.Controls.Add(this.btnAmbiguous);
            this.ToolStripContainer1.ContentPanel.Controls.Add(this.lblAmbiguous);
            this.ToolStripContainer1.ContentPanel.Controls.Add(this.btnSaveSampleSelection);
            this.ToolStripContainer1.ContentPanel.Controls.Add(this.btnLoadSampleSelection);
            this.ToolStripContainer1.ContentPanel.Controls.Add(this.lblSamples);
            this.ToolStripContainer1.ContentPanel.Controls.Add(this.chkOneSpotPerMarker);
            this.ToolStripContainer1.ContentPanel.Controls.Add(this.btnSaveGeneSelection);
            this.ToolStripContainer1.ContentPanel.Controls.Add(this.btnLoadGeneSelection);
            this.ToolStripContainer1.ContentPanel.Controls.Add(this.btnSpotSignalFont);
            this.ToolStripContainer1.ContentPanel.Controls.Add(this.lblSignalFont);
            this.ToolStripContainer1.ContentPanel.Controls.Add(this.chkShowSignal);
            this.ToolStripContainer1.ContentPanel.Controls.Add(this.lblGenes);
            this.ToolStripContainer1.ContentPanel.Controls.Add(this.btnBuildSavePic);
            this.ToolStripContainer1.ContentPanel.Controls.Add(this.btnFont);
            this.ToolStripContainer1.ContentPanel.Controls.Add(this.lblFont);
            this.ToolStripContainer1.ContentPanel.Controls.Add(this.txtSave);
            this.ToolStripContainer1.ContentPanel.Controls.Add(this.btnSave);
            this.ToolStripContainer1.ContentPanel.Controls.Add(this.lblSave);
            this.ToolStripContainer1.ContentPanel.Controls.Add(this.btnPresent);
            this.ToolStripContainer1.ContentPanel.Controls.Add(this.lblPresent);
            this.ToolStripContainer1.ContentPanel.Controls.Add(this.btnAbsent);
            this.ToolStripContainer1.ContentPanel.Controls.Add(this.lblAbsent);
            this.ToolStripContainer1.ContentPanel.Size = new System.Drawing.Size(695, 446);
            this.ToolStripContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ToolStripContainer1.LeftToolStripPanelVisible = false;
            this.ToolStripContainer1.Location = new System.Drawing.Point(0, 0);
            this.ToolStripContainer1.Name = "ToolStripContainer1";
            this.ToolStripContainer1.RightToolStripPanelVisible = false;
            this.ToolStripContainer1.Size = new System.Drawing.Size(695, 468);
            this.ToolStripContainer1.TabIndex = 1;
            this.ToolStripContainer1.Text = "ToolStripContainer1";
            this.ToolStripContainer1.TopToolStripPanelVisible = false;
            // 
            // StatusStrip1
            // 
            this.StatusStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.StatusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tslStatus,
            this.ProgressBar1});
            this.StatusStrip1.Location = new System.Drawing.Point(0, 0);
            this.StatusStrip1.Name = "StatusStrip1";
            this.StatusStrip1.Size = new System.Drawing.Size(695, 22);
            this.StatusStrip1.TabIndex = 0;
            // 
            // tslStatus
            // 
            this.tslStatus.Name = "tslStatus";
            this.tslStatus.Size = new System.Drawing.Size(39, 17);
            this.tslStatus.Text = "Ready";
            // 
            // ProgressBar1
            // 
            this.ProgressBar1.Name = "ProgressBar1";
            this.ProgressBar1.Size = new System.Drawing.Size(100, 16);
            this.ProgressBar1.Visible = false;
            // 
            // lvwSamples
            // 
            this.lvwSamples.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader4});
            this.lvwSamples.FullRowSelect = true;
            this.lvwSamples.GridLines = true;
            this.lvwSamples.Location = new System.Drawing.Point(355, 128);
            this.lvwSamples.Name = "lvwSamples";
            this.lvwSamples.Size = new System.Drawing.Size(334, 202);
            this.lvwSamples.TabIndex = 26;
            this.lvwSamples.UseCompatibleStateImageBehavior = false;
            this.lvwSamples.View = System.Windows.Forms.View.Details;
            this.lvwSamples.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.LvwSamplesItemSelectionChanged);
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Samples To Show";
            this.columnHeader4.Width = 308;
            // 
            // lvwGenes
            // 
            this.lvwGenes.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader3});
            this.lvwGenes.FullRowSelect = true;
            this.lvwGenes.GridLines = true;
            this.lvwGenes.Location = new System.Drawing.Point(15, 128);
            this.lvwGenes.Name = "lvwGenes";
            this.lvwGenes.Size = new System.Drawing.Size(334, 202);
            this.lvwGenes.TabIndex = 25;
            this.lvwGenes.UseCompatibleStateImageBehavior = false;
            this.lvwGenes.View = System.Windows.Forms.View.Details;
            this.lvwGenes.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.LvwGenesItemSelectionChanged);
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Markers To Show";
            this.columnHeader3.Width = 308;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(323, 379);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(26, 23);
            this.btnSave.TabIndex = 5;
            this.btnSave.Text = "...";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.BtnSaveClick);
            // 
            // lblSave
            // 
            this.lblSave.AutoSize = true;
            this.lblSave.Location = new System.Drawing.Point(12, 385);
            this.lblSave.Name = "lblSave";
            this.lblSave.Size = new System.Drawing.Size(57, 13);
            this.lblSave.TabIndex = 4;
            this.lblSave.Text = "Save Path";
            // 
            // btnPresent
            // 
            this.btnPresent.BackColor = System.Drawing.Color.Green;
            this.btnPresent.Location = new System.Drawing.Point(415, 17);
            this.btnPresent.Name = "btnPresent";
            this.btnPresent.Size = new System.Drawing.Size(274, 23);
            this.btnPresent.TabIndex = 3;
            this.btnPresent.Text = "Select Colour For Present Spots";
            this.btnPresent.UseVisualStyleBackColor = false;
            this.btnPresent.Click += new System.EventHandler(this.BtnPresentClick);
            // 
            // lblAbsent
            // 
            this.lblAbsent.AutoSize = true;
            this.lblAbsent.Location = new System.Drawing.Point(12, 22);
            this.lblAbsent.Name = "lblAbsent";
            this.lblAbsent.Size = new System.Drawing.Size(40, 13);
            this.lblAbsent.TabIndex = 0;
            this.lblAbsent.Text = "Absent";
            // 
            // frmSaveSpotArray
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(695, 468);
            this.Controls.Add(this.ToolStripContainer1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmSaveSpotArray";
            this.Text = "Save Clustered Spot Array";
            this.ToolStripContainer1.BottomToolStripPanel.ResumeLayout(false);
            this.ToolStripContainer1.BottomToolStripPanel.PerformLayout();
            this.ToolStripContainer1.ContentPanel.ResumeLayout(false);
            this.ToolStripContainer1.ContentPanel.PerformLayout();
            this.ToolStripContainer1.ResumeLayout(false);
            this.ToolStripContainer1.PerformLayout();
            this.StatusStrip1.ResumeLayout(false);
            this.StatusStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.CheckBox chkAmbiguous;
        internal System.Windows.Forms.Button btnAmbiguous;
        internal System.Windows.Forms.Label lblAmbiguous;
        internal System.Windows.Forms.Button btnSaveSampleSelection;
        internal System.Windows.Forms.Button btnLoadSampleSelection;
        internal System.Windows.Forms.Label lblSamples;
        internal System.Windows.Forms.ColumnHeader ColumnHeader2;
        internal System.Windows.Forms.CheckBox chkOneSpotPerMarker;
        internal System.Windows.Forms.Button btnSaveGeneSelection;
        internal System.Windows.Forms.Button btnLoadGeneSelection;
        internal System.Windows.Forms.Button btnSpotSignalFont;
        internal System.Windows.Forms.Label lblSignalFont;
        internal System.Windows.Forms.CheckBox chkShowSignal;
        internal System.Windows.Forms.Label lblGenes;
        internal System.Windows.Forms.ColumnHeader ColumnHeader1;
        internal System.Windows.Forms.Button btnBuildSavePic;
        internal System.Windows.Forms.Button btnFont;
        internal System.Windows.Forms.Label lblFont;
        internal System.Windows.Forms.TextBox txtSave;
        internal System.Windows.Forms.Label lblPresent;
        internal System.Windows.Forms.Button btnAbsent;
        internal System.Windows.Forms.ToolStripContainer ToolStripContainer1;
        internal System.Windows.Forms.StatusStrip StatusStrip1;
        internal System.Windows.Forms.ToolStripStatusLabel tslStatus;
        internal System.Windows.Forms.ToolStripProgressBar ProgressBar1;
        private ListViewFF lvwSamples;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private ListViewFF lvwGenes;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        internal System.Windows.Forms.Button btnSave;
        internal System.Windows.Forms.Label lblSave;
        internal System.Windows.Forms.Button btnPresent;
        internal System.Windows.Forms.Label lblAbsent;
    }
}