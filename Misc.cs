﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms.DataVisualization.Charting;

namespace ArrayTubeAnalyzer
{
    static class Misc
    {

        public static double Average(List<double> d)
        {
            if (d.Count == 0)
            {
                return 0;
            }
            double sum = 0;
            foreach (double value in d)
            {
                sum += value;
            }
            double avg = sum / d.Count;
            return avg;
        }

        public static double StandardDeviation(List<double> d, double avg)
        {
            double sum = 0.0;
            foreach (double value in d)
            {
                sum += Math.Pow((value - avg), 2);
            }
            return Math.Sqrt(sum / d.Count);
        }

        /// <summary>Calculates the median from an array of values.</summary>
        /// <param name="d">Array to calculate median from.</param>
        public static double GetMedian(List<double> d)
        {
            if (d.Count % 2 == 0)
            {
                return (d[d.Count / 2] + d[d.Count / 2 - 1]) / 2;
            }
            return d[d.Count / 2 - 1];
        }

        public static Bitmap AdjustContrast(Bitmap image, float value)
        {
            value = (100.0f + value) / 100.0f;
            value *= value;

            var bm = image;
            Graphics g = Graphics.FromImage(bm);
            var newBitmap = new Bitmap(bm);
            g.DrawImage(newBitmap, 0, 0);
            g.Dispose();

            BitmapData data = newBitmap.LockBits(
                new Rectangle(0, 0, newBitmap.Width, newBitmap.Height),
                ImageLockMode.ReadWrite,
                PixelFormat.Format32bppRgb);

            unsafe
            {
                for (int y = 0; y < newBitmap.Height; ++y)
                {
                    byte* row = (byte*)data.Scan0 + (y * data.Stride);
                    int columnOffset = 0;
                    for (int x = 0; x < newBitmap.Width; ++x)
                    {
                        byte B = row[columnOffset];
                        byte G = row[columnOffset + 1];
                        byte R = row[columnOffset + 2];

                        float Red = R / 255.0f;
                        float Green = G / 255.0f;
                        float Blue = B / 255.0f;
                        Red = (((Red - 0.5f) * value) + 0.5f) * 255.0f;
                        Green = (((Green - 0.5f) * value) + 0.5f) * 255.0f;
                        Blue = (((Blue - 0.5f) * value) + 0.5f) * 255.0f;

                        int iR = (int)Red;
                        iR = iR > 255 ? 255 : iR;
                        iR = iR < 0 ? 0 : iR;
                        int iG = (int)Green;
                        iG = iG > 255 ? 255 : iG;
                        iG = iG < 0 ? 0 : iG;
                        int iB = (int)Blue;
                        iB = iB > 255 ? 255 : iB;
                        iB = iB < 0 ? 0 : iB;

                        row[columnOffset] = (byte)iB;
                        row[columnOffset + 1] = (byte)iG;
                        row[columnOffset + 2] = (byte)iR;

                        columnOffset += 4;
                    }
                }
            }

            newBitmap.UnlockBits(data);

            return newBitmap;
        }

        /// <summary>Convert string into a byte array.</summary>
        /// <param name="str">String to be converted into byte array.</param>
        public static byte[] StrToByteArray(string str)
        {
            var encoding = new System.Text.ASCIIEncoding();
            return encoding.GetBytes(str);
        }

        /// <summary>When a column is clicked on the listview, change the sorting order for that particular column (A->Z, Z->A).</summary>
        public static void SortLvwByColumn(ListViewFF lvw, ColumnClickEventArgs e)
        {
            // Determine if the clicked column is already the column that is being sorted.
            var listViewColumnSorter = ((ListViewColumnSorter) lvw.ListViewItemSorter);
            if ((e.Column == listViewColumnSorter.SortColumn))
            {
                // Reverse the current sort direction for this column.
                listViewColumnSorter.Order = (listViewColumnSorter.Order == SortOrder.Ascending) ? SortOrder.Descending : SortOrder.Ascending;
            }
            else
            {
                // Set the column number that is to be sorted; default to ascending.
                listViewColumnSorter.SortColumn = e.Column;
                listViewColumnSorter.Order = SortOrder.Ascending;
            }
            // Perform the sort with these new sort options.
            lvw.Sort();
        }


        public static DialogResult InputBox(string title, string promptText, ref string value)
        {
            var form = new Form();
            var label = new Label();
            var textBox = new TextBox();
            var buttonOk = new Button();
            var buttonCancel = new Button();

            form.Text = title;
            label.Text = promptText;
            textBox.Text = value;

            buttonOk.Text = "OK";
            buttonCancel.Text = "Cancel";
            buttonOk.DialogResult = DialogResult.OK;
            buttonCancel.DialogResult = DialogResult.Cancel;

            label.SetBounds(9, 20, 372, 13);
            textBox.SetBounds(12, 36, 372, 20);
            buttonOk.SetBounds(228, 72, 75, 23);
            buttonCancel.SetBounds(309, 72, 75, 23);

            label.AutoSize = true;
            textBox.Anchor = textBox.Anchor | AnchorStyles.Right;
            buttonOk.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            buttonCancel.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;

            form.ClientSize = new Size(396, 107);
            form.Controls.AddRange(new Control[] {
			label,
			textBox,
			buttonOk,
			buttonCancel
		});
            form.ClientSize = new Size(Math.Max(300, label.Right + 10), form.ClientSize.Height);
            form.FormBorderStyle = FormBorderStyle.FixedDialog;
            form.StartPosition = FormStartPosition.CenterScreen;
            form.MinimizeBox = false;
            form.MaximizeBox = false;
            form.AcceptButton = buttonOk;
            form.CancelButton = buttonCancel;

            DialogResult dialogResult1 = form.ShowDialog();
            value = textBox.Text;
            return dialogResult1;
        }



        public static DialogResult ListviewSelect(string title, string prompt, string columnheader, string[] items, ref List<int> selected, bool bMultiselect)
        {
            var form = new Form();
            var label = new Label();
            var lvw = new ListViewFF();
            var buttonOk = new Button();
            var buttonCancel = new Button();

            lvw.MultiSelect = bMultiselect;
            form.Text = title;
            label.Text = prompt;

            buttonOk.Text = "OK";
            buttonCancel.Text = "Cancel";
            buttonOk.DialogResult = DialogResult.OK;
            buttonCancel.DialogResult = DialogResult.Cancel;

            label.SetBounds(12, 9, 35, 13);
            lvw.SetBounds(12, 44, 260, 487);
            buttonOk.SetBounds(197, 547, 75, 23);
            buttonCancel.SetBounds(116, 547, 75, 23);

            lvw.Columns.Add(columnheader, 230);

            var lvList = new List<ListViewItem>();
            foreach (string item in items)
            {
                var lvi = new ListViewItem(item);
                lvList.Add(lvi);
            }
            lvw.Items.AddRange(lvList.ToArray());

            label.AutoSize = true;
            //lvw.Anchor = lvw.Anchor | AnchorStyles.Right;
            buttonOk.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            buttonCancel.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;

            form.ClientSize = new Size(300, 600);
            form.Controls.AddRange(new Control[] {
			        label,
			        lvw,
			        buttonOk,
			        buttonCancel});

            //form.ClientSize = new Size(Math.Max(300, label.Right + 10), form.ClientSize.Height);
            form.FormBorderStyle = FormBorderStyle.FixedDialog;
            form.StartPosition = FormStartPosition.CenterScreen;
            form.MinimizeBox = false;
            form.MaximizeBox = false;
            form.AcceptButton = buttonOk;
            form.CancelButton = buttonCancel;

            DialogResult dialogResult1 = form.ShowDialog();

            var tmp = new List<int>();
            foreach (int i in lvw.SelectedIndices)
            {
                tmp.Add(i);
            }

            selected = tmp;
            return dialogResult1;
        }

        public static DialogResult SelectItemsListview(string title, 
            string prompt, 
            string columnheader,
            string columnheader2,
            string columnheader3,
            List<string> items, 
            List<string> items2, 
            List<string> items3,
            ref List<int> selected,
            bool bMultiselect)
        {
            var form = new Form();
            var label = new Label();
            var lvw = new ListViewFF();
            var buttonOk = new Button();
            var buttonCancel = new Button();

            lvw.MultiSelect = bMultiselect;
            form.Text = title;
            label.Text = prompt;

            buttonOk.Text = "OK";
            buttonCancel.Text = "Cancel";
            buttonOk.DialogResult = DialogResult.OK;
            buttonCancel.DialogResult = DialogResult.Cancel;

            label.SetBounds(12, 9, 35, 13);
            lvw.SetBounds(12, 44, 330, 487);
            buttonOk.SetBounds(197, 547, 75, 23);
            buttonCancel.SetBounds(116, 547, 75, 23);

            lvw.Columns.Add(columnheader, 230);
            lvw.Columns.Add(columnheader2, 50);
            lvw.Columns.Add(columnheader3, 50);

            var lvList = new List<ListViewItem>();
            for (int i = 0; i < items.Count; i++)
            {
                string item = items[i];
                var lvi = new ListViewItem(item);
                lvi.SubItems.Add(items2[i]);
                lvi.SubItems.Add(items3[i]);
                lvi.Selected = selected.Contains(i);
                lvList.Add(lvi);
            }
            lvw.Items.AddRange(lvList.ToArray());

            label.AutoSize = true;
            //lvw.Anchor = lvw.Anchor | AnchorStyles.Right;
            buttonOk.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            buttonCancel.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;

            form.ClientSize = new Size(360, 600);
            form.Controls.AddRange(new Control[] {
			label,
			lvw,
			buttonOk,
			buttonCancel
		});
            //form.ClientSize = new Size(Math.Max(300, label.Right + 10), form.ClientSize.Height);
            form.FormBorderStyle = FormBorderStyle.FixedDialog;
            form.StartPosition = FormStartPosition.CenterScreen;
            form.MinimizeBox = false;
            form.MaximizeBox = false;
            form.AcceptButton = buttonOk;
            form.CancelButton = buttonCancel;

            DialogResult dialogResult1 = form.ShowDialog();

            var tmp = new List<int>();
            foreach (int i in lvw.SelectedIndices)
            {
                tmp.Add(i);
            }

            selected = tmp;
            return dialogResult1;
        }


        //Borrowed from MS Samples Pack for creating a new independent Y axis on a chart.
        /// <summary>Creates Y axis for the specified series.</summary>
        /// <param name="chart">Chart control.</param>
        /// <param name="area">Original chart area.</param>
        /// <param name="series">Series.</param>
        /// <param name="axisOffset">New Y axis offset in relative coordinates.</param>
        /// <param name="labelsSize">Extra space for new Y axis labels in relative coordinates.</param>
        /// <remarks>Borrowed from VB10 MS samples.</remarks>
        public static void CreateYAxis(Chart chart, ChartArea area, Series series, float axisOffset, float labelsSize)
        {
            // Create new chart area for original series
            ChartArea areaSeries = chart.ChartAreas.Add(("ChartArea_" + series.Name));
            areaSeries.BackColor = Color.Transparent;
            areaSeries.BorderColor = Color.Transparent;
            areaSeries.Position.FromRectangleF(area.Position.ToRectangleF());
            areaSeries.InnerPlotPosition.FromRectangleF(area.InnerPlotPosition.ToRectangleF());
            areaSeries.AxisX.MajorGrid.Enabled = false;
            areaSeries.AxisX.MajorTickMark.Enabled = false;
            areaSeries.AxisX.LabelStyle.Enabled = false;
            areaSeries.AxisY.MajorGrid.Enabled = false;
            areaSeries.AxisY.MajorTickMark.Enabled = false;
            areaSeries.AxisY.LabelStyle.Enabled = false;
            areaSeries.AxisY.IsStartedFromZero = area.AxisY.IsStartedFromZero;

            series.ChartArea = areaSeries.Name;

            // Create new chart area for axis
            ChartArea areaAxis = chart.ChartAreas.Add(("AxisY_" + series.ChartArea));
            areaAxis.BackColor = Color.Transparent;
            areaAxis.BorderColor = Color.Transparent;
            areaAxis.Position.FromRectangleF(chart.ChartAreas[series.ChartArea].Position.ToRectangleF());
            areaAxis.InnerPlotPosition.FromRectangleF(chart.ChartAreas[series.ChartArea].InnerPlotPosition.ToRectangleF());

            // Create a copy of specified series
            Series seriesCopy = chart.Series.Add((series.Name + "_Copy"));
            seriesCopy.ChartType = series.ChartType;
            foreach (DataPoint point in series.Points)
            {
                seriesCopy.Points.AddXY(point.XValue, point.YValues[0]);
            }

            // Hide copied series
            seriesCopy.IsVisibleInLegend = false;
            seriesCopy.Color = Color.Transparent;
            seriesCopy.BorderColor = Color.Transparent;
            seriesCopy.ChartArea = areaAxis.Name;

            // Disable grid lines & tickmarks
            areaAxis.AxisX.LineWidth = 0;
            areaAxis.AxisX.MajorGrid.Enabled = false;
            areaAxis.AxisX.MajorTickMark.Enabled = false;
            areaAxis.AxisX.LabelStyle.Enabled = false;
            areaAxis.AxisY.MajorGrid.Enabled = false;
            areaAxis.AxisY.IsStartedFromZero = area.AxisY.IsStartedFromZero;

            // Adjust area position
            areaAxis.Position.X -= axisOffset;
            areaAxis.InnerPlotPosition.X += labelsSize;
        }
    }

}
