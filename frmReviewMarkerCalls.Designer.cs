﻿namespace ArrayTubeAnalyzer
{
    partial class FrmReviewMarkerCalls
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmReviewMarkerCalls));
            this.ToolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.ContextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ShowSpotStripToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ShowMarkerStripToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SplitContainer2 = new System.Windows.Forms.SplitContainer();
            this.SplitContainer3 = new System.Windows.Forms.SplitContainer();
            this.lblSampleName = new System.Windows.Forms.GroupBox();
            this.tbrContrast = new System.Windows.Forms.TrackBar();
            this.pbx = new System.Windows.Forms.PictureBox();
            this.SplitContainer4 = new System.Windows.Forms.SplitContainer();
            this.chkContrast = new System.Windows.Forms.CheckBox();
            this.chkGridNumbers = new System.Windows.Forms.CheckBox();
            this.chkGrid = new System.Windows.Forms.CheckBox();
            this.pbxReplicates = new System.Windows.Forms.PictureBox();
            this.lvwMarkerInfo = new ArrayTubeAnalyzer.ListViewFF();
            this.chSpot = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chMarker = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chReplicate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chSignal = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chPercentError = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chDeltaSignal = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chAverageSignal = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chThreshold = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chThresholdType = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chMarkerCall = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chReplicateStatus = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SplitContainer1 = new System.Windows.Forms.SplitContainer();
            this.lvwSamples = new ArrayTubeAnalyzer.ListViewFF();
            this.chSample = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chFinalized = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chAmbiguousCount = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.CloseReviewMarkerCallsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.ExitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.ToolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.AboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tslStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.SaveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tsbFile = new System.Windows.Forms.ToolStripDropDownButton();
            this.ddbView = new System.Windows.Forms.ToolStripDropDownButton();
            this.SampleReviewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.GeneThresholdAnalysisToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SpotSignalAnalysisToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ClusteringToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SampleDiagnosticsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.ddbResetGeneCalls = new System.Windows.Forms.ToolStripDropDownButton();
            this.ForSelectedSampleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ForAllSamplesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.btnShowClusteringForm = new System.Windows.Forms.ToolStripButton();
            this.StatusStrip1 = new System.Windows.Forms.StatusStrip();
            this.ContextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SplitContainer2)).BeginInit();
            this.SplitContainer2.Panel1.SuspendLayout();
            this.SplitContainer2.Panel2.SuspendLayout();
            this.SplitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SplitContainer3)).BeginInit();
            this.SplitContainer3.Panel1.SuspendLayout();
            this.SplitContainer3.Panel2.SuspendLayout();
            this.SplitContainer3.SuspendLayout();
            this.lblSampleName.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbrContrast)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SplitContainer4)).BeginInit();
            this.SplitContainer4.Panel1.SuspendLayout();
            this.SplitContainer4.Panel2.SuspendLayout();
            this.SplitContainer4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxReplicates)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SplitContainer1)).BeginInit();
            this.SplitContainer1.Panel1.SuspendLayout();
            this.SplitContainer1.Panel2.SuspendLayout();
            this.SplitContainer1.SuspendLayout();
            this.ToolStrip1.SuspendLayout();
            this.StatusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ToolTip1
            // 
            this.ToolTip1.ShowAlways = true;
            // 
            // ContextMenuStrip1
            // 
            this.ContextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ShowSpotStripToolStripMenuItem,
            this.ShowMarkerStripToolStripMenuItem});
            this.ContextMenuStrip1.Name = "ContextMenuStrip1";
            this.ContextMenuStrip1.Size = new System.Drawing.Size(171, 48);
            // 
            // ShowSpotStripToolStripMenuItem
            // 
            this.ShowSpotStripToolStripMenuItem.Name = "ShowSpotStripToolStripMenuItem";
            this.ShowSpotStripToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.ShowSpotStripToolStripMenuItem.Text = "Show Spot Strip";
            this.ShowSpotStripToolStripMenuItem.Click += new System.EventHandler(this.ShowSpotStripToolStripMenuItemClick);
            // 
            // ShowMarkerStripToolStripMenuItem
            // 
            this.ShowMarkerStripToolStripMenuItem.Name = "ShowMarkerStripToolStripMenuItem";
            this.ShowMarkerStripToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.ShowMarkerStripToolStripMenuItem.Text = "Show Marker Strip";
            this.ShowMarkerStripToolStripMenuItem.Click += new System.EventHandler(this.ShowMarkerStripToolStripMenuItemClick);
            // 
            // SplitContainer2
            // 
            this.SplitContainer2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.SplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SplitContainer2.Location = new System.Drawing.Point(0, 0);
            this.SplitContainer2.Name = "SplitContainer2";
            // 
            // SplitContainer2.Panel1
            // 
            this.SplitContainer2.Panel1.Controls.Add(this.SplitContainer3);
            // 
            // SplitContainer2.Panel2
            // 
            this.SplitContainer2.Panel2.Controls.Add(this.lvwMarkerInfo);
            this.SplitContainer2.Size = new System.Drawing.Size(802, 523);
            this.SplitContainer2.SplitterDistance = 410;
            this.SplitContainer2.TabIndex = 0;
            // 
            // SplitContainer3
            // 
            this.SplitContainer3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.SplitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SplitContainer3.Location = new System.Drawing.Point(0, 0);
            this.SplitContainer3.Name = "SplitContainer3";
            this.SplitContainer3.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // SplitContainer3.Panel1
            // 
            this.SplitContainer3.Panel1.Controls.Add(this.lblSampleName);
            // 
            // SplitContainer3.Panel2
            // 
            this.SplitContainer3.Panel2.Controls.Add(this.SplitContainer4);
            this.SplitContainer3.Size = new System.Drawing.Size(410, 523);
            this.SplitContainer3.SplitterDistance = 434;
            this.SplitContainer3.TabIndex = 0;
            // 
            // lblSampleName
            // 
            this.lblSampleName.Controls.Add(this.tbrContrast);
            this.lblSampleName.Controls.Add(this.pbx);
            this.lblSampleName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblSampleName.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSampleName.Location = new System.Drawing.Point(0, 0);
            this.lblSampleName.Name = "lblSampleName";
            this.lblSampleName.Size = new System.Drawing.Size(406, 430);
            this.lblSampleName.TabIndex = 0;
            this.lblSampleName.TabStop = false;
            this.lblSampleName.Text = "groupBox1";
            // 
            // tbrContrast
            // 
            this.tbrContrast.AutoSize = false;
            this.tbrContrast.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tbrContrast.Location = new System.Drawing.Point(3, 402);
            this.tbrContrast.Maximum = 255;
            this.tbrContrast.Minimum = -100;
            this.tbrContrast.Name = "tbrContrast";
            this.tbrContrast.Size = new System.Drawing.Size(400, 25);
            this.tbrContrast.TabIndex = 24;
            this.tbrContrast.TickStyle = System.Windows.Forms.TickStyle.None;
            this.tbrContrast.Visible = false;
            this.tbrContrast.Scroll += new System.EventHandler(this.TbrGeneCallContrastScroll);
            // 
            // pbx
            // 
            this.pbx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbx.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbx.Location = new System.Drawing.Point(3, 27);
            this.pbx.Name = "pbx";
            this.pbx.Size = new System.Drawing.Size(400, 400);
            this.pbx.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbx.TabIndex = 3;
            this.pbx.TabStop = false;
            this.pbx.MouseUp += new System.Windows.Forms.MouseEventHandler(this.PbxMouseUp);
            this.pbx.Resize += new System.EventHandler(this.PbxResize);
            // 
            // SplitContainer4
            // 
            this.SplitContainer4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.SplitContainer4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SplitContainer4.Location = new System.Drawing.Point(0, 0);
            this.SplitContainer4.Name = "SplitContainer4";
            // 
            // SplitContainer4.Panel1
            // 
            this.SplitContainer4.Panel1.Controls.Add(this.chkContrast);
            this.SplitContainer4.Panel1.Controls.Add(this.chkGridNumbers);
            this.SplitContainer4.Panel1.Controls.Add(this.chkGrid);
            // 
            // SplitContainer4.Panel2
            // 
            this.SplitContainer4.Panel2.Controls.Add(this.pbxReplicates);
            this.SplitContainer4.Size = new System.Drawing.Size(410, 85);
            this.SplitContainer4.SplitterDistance = 136;
            this.SplitContainer4.TabIndex = 0;
            // 
            // chkContrast
            // 
            this.chkContrast.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkContrast.AutoSize = true;
            this.chkContrast.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.chkContrast.Location = new System.Drawing.Point(3, 58);
            this.chkContrast.Name = "chkContrast";
            this.chkContrast.Size = new System.Drawing.Size(97, 17);
            this.chkContrast.TabIndex = 23;
            this.chkContrast.Text = "Adjust Contrast";
            this.chkContrast.UseVisualStyleBackColor = true;
            this.chkContrast.CheckedChanged += new System.EventHandler(this.ChkGeneCallContrastCheckedChanged);
            // 
            // chkGridNumbers
            // 
            this.chkGridNumbers.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkGridNumbers.AutoSize = true;
            this.chkGridNumbers.Checked = true;
            this.chkGridNumbers.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkGridNumbers.Location = new System.Drawing.Point(3, 35);
            this.chkGridNumbers.Name = "chkGridNumbers";
            this.chkGridNumbers.Size = new System.Drawing.Size(120, 17);
            this.chkGridNumbers.TabIndex = 7;
            this.chkGridNumbers.Text = "Show Grid Numbers";
            this.chkGridNumbers.UseVisualStyleBackColor = true;
            this.chkGridNumbers.CheckedChanged += new System.EventHandler(this.ChkGridNumbersCheckedChanged);
            // 
            // chkGrid
            // 
            this.chkGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkGrid.AutoSize = true;
            this.chkGrid.Checked = true;
            this.chkGrid.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkGrid.Location = new System.Drawing.Point(3, 12);
            this.chkGrid.Name = "chkGrid";
            this.chkGrid.Size = new System.Drawing.Size(75, 17);
            this.chkGrid.TabIndex = 6;
            this.chkGrid.Text = "Show Grid";
            this.chkGrid.UseVisualStyleBackColor = true;
            this.chkGrid.CheckedChanged += new System.EventHandler(this.ChkGridCheckedChanged);
            // 
            // pbxReplicates
            // 
            this.pbxReplicates.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbxReplicates.Location = new System.Drawing.Point(0, 0);
            this.pbxReplicates.Name = "pbxReplicates";
            this.pbxReplicates.Size = new System.Drawing.Size(266, 81);
            this.pbxReplicates.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxReplicates.TabIndex = 0;
            this.pbxReplicates.TabStop = false;
            this.pbxReplicates.MouseClick += new System.Windows.Forms.MouseEventHandler(this.PbxReplicatesMouseClick);
            // 
            // lvwMarkerInfo
            // 
            this.lvwMarkerInfo.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.chSpot,
            this.chMarker,
            this.chReplicate,
            this.chSignal,
            this.chPercentError,
            this.chDeltaSignal,
            this.chAverageSignal,
            this.chThreshold,
            this.chThresholdType,
            this.chMarkerCall,
            this.chReplicateStatus});
            this.lvwMarkerInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvwMarkerInfo.FullRowSelect = true;
            this.lvwMarkerInfo.GridLines = true;
            this.lvwMarkerInfo.Location = new System.Drawing.Point(0, 0);
            this.lvwMarkerInfo.MultiSelect = false;
            this.lvwMarkerInfo.Name = "lvwMarkerInfo";
            this.lvwMarkerInfo.Size = new System.Drawing.Size(384, 519);
            this.lvwMarkerInfo.TabIndex = 0;
            this.lvwMarkerInfo.UseCompatibleStateImageBehavior = false;
            this.lvwMarkerInfo.View = System.Windows.Forms.View.Details;
            this.lvwMarkerInfo.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.LvwMarkerInfoColumnClick);
            this.lvwMarkerInfo.SelectedIndexChanged += new System.EventHandler(this.LvwMarkerInfoSelectedIndexChanged);
            this.lvwMarkerInfo.DoubleClick += new System.EventHandler(this.LvwMarkerInfoDoubleClick);
            this.lvwMarkerInfo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.LvwMarkerInfoKeyPress);
            this.lvwMarkerInfo.MouseClick += new System.Windows.Forms.MouseEventHandler(this.LvwMarkerInfoMouseClick);
            // 
            // chSpot
            // 
            this.chSpot.Text = "Spot";
            this.chSpot.Width = 34;
            // 
            // chMarker
            // 
            this.chMarker.Text = "Marker";
            this.chMarker.Width = 90;
            // 
            // chReplicate
            // 
            this.chReplicate.Text = "Replicate";
            this.chReplicate.Width = 26;
            // 
            // chSignal
            // 
            this.chSignal.Text = "Signal";
            // 
            // chPercentError
            // 
            this.chPercentError.Text = "Percent Error";
            this.chPercentError.Width = 0;
            // 
            // chDeltaSignal
            // 
            this.chDeltaSignal.Text = "Delta Signal";
            this.chDeltaSignal.Width = 0;
            // 
            // chAverageSignal
            // 
            this.chAverageSignal.Text = "Average Signal";
            this.chAverageSignal.Width = 70;
            // 
            // chThreshold
            // 
            this.chThreshold.Text = "Threshold";
            // 
            // chThresholdType
            // 
            this.chThresholdType.Text = "Threshold Type";
            // 
            // chMarkerCall
            // 
            this.chMarkerCall.Text = "Marker Call";
            // 
            // chReplicateStatus
            // 
            this.chReplicateStatus.Text = "Replicate Status";
            // 
            // SplitContainer1
            // 
            this.SplitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SplitContainer1.Location = new System.Drawing.Point(0, 25);
            this.SplitContainer1.Name = "SplitContainer1";
            // 
            // SplitContainer1.Panel1
            // 
            this.SplitContainer1.Panel1.Controls.Add(this.lvwSamples);
            // 
            // SplitContainer1.Panel2
            // 
            this.SplitContainer1.Panel2.Controls.Add(this.SplitContainer2);
            this.SplitContainer1.Size = new System.Drawing.Size(1008, 523);
            this.SplitContainer1.SplitterDistance = 202;
            this.SplitContainer1.TabIndex = 29;
            // 
            // lvwSamples
            // 
            this.lvwSamples.CheckBoxes = true;
            this.lvwSamples.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.chSample,
            this.chFinalized,
            this.chAmbiguousCount});
            this.lvwSamples.ContextMenuStrip = this.ContextMenuStrip1;
            this.lvwSamples.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvwSamples.FullRowSelect = true;
            this.lvwSamples.GridLines = true;
            this.lvwSamples.Location = new System.Drawing.Point(0, 0);
            this.lvwSamples.Name = "lvwSamples";
            this.lvwSamples.Size = new System.Drawing.Size(198, 519);
            this.lvwSamples.TabIndex = 0;
            this.lvwSamples.UseCompatibleStateImageBehavior = false;
            this.lvwSamples.View = System.Windows.Forms.View.Details;
            this.lvwSamples.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.LvwSamplesColumnClick);
            this.lvwSamples.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.LvwSamplesItemChecked);
            this.lvwSamples.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.LvwSamplesItemSelectionChanged);
            // 
            // chSample
            // 
            this.chSample.Text = "Sample";
            // 
            // chFinalized
            // 
            this.chFinalized.Text = "Finalized";
            // 
            // chAmbiguousCount
            // 
            this.chAmbiguousCount.Text = "Ambiguous Count";
            // 
            // CloseReviewMarkerCallsToolStripMenuItem
            // 
            this.CloseReviewMarkerCallsToolStripMenuItem.Image = global::ArrayTubeAnalyzer.Properties.Resources.Delete;
            this.CloseReviewMarkerCallsToolStripMenuItem.Name = "CloseReviewMarkerCallsToolStripMenuItem";
            this.CloseReviewMarkerCallsToolStripMenuItem.Size = new System.Drawing.Size(211, 22);
            this.CloseReviewMarkerCallsToolStripMenuItem.Text = "Close Review Marker Calls";
            this.CloseReviewMarkerCallsToolStripMenuItem.Click += new System.EventHandler(this.CloseReviewMarkerCallsToolStripMenuItemClick);
            // 
            // ToolStripSeparator5
            // 
            this.ToolStripSeparator5.Name = "ToolStripSeparator5";
            this.ToolStripSeparator5.Size = new System.Drawing.Size(208, 6);
            // 
            // ExitToolStripMenuItem
            // 
            this.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem";
            this.ExitToolStripMenuItem.Size = new System.Drawing.Size(211, 22);
            this.ExitToolStripMenuItem.Text = "Exit";
            this.ExitToolStripMenuItem.Click += new System.EventHandler(this.ExitToolStripMenuItemClick);
            // 
            // ToolStripSeparator1
            // 
            this.ToolStripSeparator1.Name = "ToolStripSeparator1";
            this.ToolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // ToolStripSeparator4
            // 
            this.ToolStripSeparator4.Name = "ToolStripSeparator4";
            this.ToolStripSeparator4.Size = new System.Drawing.Size(208, 6);
            // 
            // AboutToolStripMenuItem
            // 
            this.AboutToolStripMenuItem.Image = global::ArrayTubeAnalyzer.Properties.Resources.info_16;
            this.AboutToolStripMenuItem.Name = "AboutToolStripMenuItem";
            this.AboutToolStripMenuItem.Size = new System.Drawing.Size(211, 22);
            this.AboutToolStripMenuItem.Text = "About";
            this.AboutToolStripMenuItem.Click += new System.EventHandler(this.AboutToolStripMenuItemClick);
            // 
            // tslStatus
            // 
            this.tslStatus.Name = "tslStatus";
            this.tslStatus.Size = new System.Drawing.Size(39, 17);
            this.tslStatus.Text = "Ready";
            // 
            // SaveToolStripMenuItem
            // 
            this.SaveToolStripMenuItem.Image = global::ArrayTubeAnalyzer.Properties.Resources.Save;
            this.SaveToolStripMenuItem.Name = "SaveToolStripMenuItem";
            this.SaveToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.SaveToolStripMenuItem.Size = new System.Drawing.Size(211, 22);
            this.SaveToolStripMenuItem.Text = "Save";
            this.SaveToolStripMenuItem.Click += new System.EventHandler(this.SaveToolStripMenuItemClick);
            // 
            // ToolStrip1
            // 
            this.ToolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbFile,
            this.ToolStripSeparator1,
            this.ddbView,
            this.ToolStripSeparator2,
            this.ddbResetGeneCalls,
            this.ToolStripSeparator3,
            this.btnShowClusteringForm});
            this.ToolStrip1.Location = new System.Drawing.Point(0, 0);
            this.ToolStrip1.Name = "ToolStrip1";
            this.ToolStrip1.Size = new System.Drawing.Size(1008, 25);
            this.ToolStrip1.TabIndex = 28;
            this.ToolStrip1.Text = "ToolStrip1";
            // 
            // tsbFile
            // 
            this.tsbFile.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.SaveToolStripMenuItem,
            this.AboutToolStripMenuItem,
            this.ToolStripSeparator4,
            this.CloseReviewMarkerCallsToolStripMenuItem,
            this.ToolStripSeparator5,
            this.ExitToolStripMenuItem});
            this.tsbFile.Image = ((System.Drawing.Image)(resources.GetObject("tsbFile.Image")));
            this.tsbFile.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbFile.Name = "tsbFile";
            this.tsbFile.Size = new System.Drawing.Size(38, 22);
            this.tsbFile.Text = "File";
            // 
            // ddbView
            // 
            this.ddbView.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.ddbView.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.SampleReviewToolStripMenuItem,
            this.GeneThresholdAnalysisToolStripMenuItem,
            this.SpotSignalAnalysisToolStripMenuItem,
            this.ClusteringToolStripMenuItem,
            this.SampleDiagnosticsToolStripMenuItem});
            this.ddbView.Image = ((System.Drawing.Image)(resources.GetObject("ddbView.Image")));
            this.ddbView.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ddbView.Name = "ddbView";
            this.ddbView.Size = new System.Drawing.Size(45, 22);
            this.ddbView.Text = "View";
            // 
            // SampleReviewToolStripMenuItem
            // 
            this.SampleReviewToolStripMenuItem.Image = global::ArrayTubeAnalyzer.Properties.Resources.warning_16;
            this.SampleReviewToolStripMenuItem.Name = "SampleReviewToolStripMenuItem";
            this.SampleReviewToolStripMenuItem.Size = new System.Drawing.Size(213, 22);
            this.SampleReviewToolStripMenuItem.Text = "Sample QC";
            this.SampleReviewToolStripMenuItem.Click += new System.EventHandler(this.SampleReviewToolStripMenuItemClick);
            // 
            // GeneThresholdAnalysisToolStripMenuItem
            // 
            this.GeneThresholdAnalysisToolStripMenuItem.Image = global::ArrayTubeAnalyzer.Properties.Resources.Chart2;
            this.GeneThresholdAnalysisToolStripMenuItem.Name = "GeneThresholdAnalysisToolStripMenuItem";
            this.GeneThresholdAnalysisToolStripMenuItem.Size = new System.Drawing.Size(213, 22);
            this.GeneThresholdAnalysisToolStripMenuItem.Text = "Marker Threshold Analysis";
            this.GeneThresholdAnalysisToolStripMenuItem.Click += new System.EventHandler(this.MarkerThresholdAnalysisToolStripMenuItemClick);
            // 
            // SpotSignalAnalysisToolStripMenuItem
            // 
            this.SpotSignalAnalysisToolStripMenuItem.Image = global::ArrayTubeAnalyzer.Properties.Resources.statistics_16;
            this.SpotSignalAnalysisToolStripMenuItem.Name = "SpotSignalAnalysisToolStripMenuItem";
            this.SpotSignalAnalysisToolStripMenuItem.Size = new System.Drawing.Size(213, 22);
            this.SpotSignalAnalysisToolStripMenuItem.Text = "Marker Threshold Review";
            this.SpotSignalAnalysisToolStripMenuItem.Click += new System.EventHandler(this.SpotSignalAnalysisToolStripMenuItemClick);
            // 
            // ClusteringToolStripMenuItem
            // 
            this.ClusteringToolStripMenuItem.Image = global::ArrayTubeAnalyzer.Properties.Resources.tree_16;
            this.ClusteringToolStripMenuItem.Name = "ClusteringToolStripMenuItem";
            this.ClusteringToolStripMenuItem.Size = new System.Drawing.Size(213, 22);
            this.ClusteringToolStripMenuItem.Text = "Clustering";
            this.ClusteringToolStripMenuItem.Click += new System.EventHandler(this.ClusteringToolStripMenuItemClick);
            // 
            // SampleDiagnosticsToolStripMenuItem
            // 
            this.SampleDiagnosticsToolStripMenuItem.Image = global::ArrayTubeAnalyzer.Properties.Resources.search_16;
            this.SampleDiagnosticsToolStripMenuItem.Name = "SampleDiagnosticsToolStripMenuItem";
            this.SampleDiagnosticsToolStripMenuItem.Size = new System.Drawing.Size(213, 22);
            this.SampleDiagnosticsToolStripMenuItem.Text = "Sample Diagnostics";
            this.SampleDiagnosticsToolStripMenuItem.Click += new System.EventHandler(this.SampleDiagnosticsToolStripMenuItemClick);
            // 
            // ToolStripSeparator2
            // 
            this.ToolStripSeparator2.Name = "ToolStripSeparator2";
            this.ToolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // ddbResetGeneCalls
            // 
            this.ddbResetGeneCalls.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.ddbResetGeneCalls.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ForSelectedSampleToolStripMenuItem,
            this.ForAllSamplesToolStripMenuItem});
            this.ddbResetGeneCalls.Image = ((System.Drawing.Image)(resources.GetObject("ddbResetGeneCalls.Image")));
            this.ddbResetGeneCalls.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ddbResetGeneCalls.Name = "ddbResetGeneCalls";
            this.ddbResetGeneCalls.Size = new System.Drawing.Size(116, 22);
            this.ddbResetGeneCalls.Text = "Reset Marker Calls";
            // 
            // ForSelectedSampleToolStripMenuItem
            // 
            this.ForSelectedSampleToolStripMenuItem.Name = "ForSelectedSampleToolStripMenuItem";
            this.ForSelectedSampleToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.ForSelectedSampleToolStripMenuItem.Text = "For Selected Sample";
            this.ForSelectedSampleToolStripMenuItem.Click += new System.EventHandler(this.ForSelectedSampleToolStripMenuItemClick);
            // 
            // ForAllSamplesToolStripMenuItem
            // 
            this.ForAllSamplesToolStripMenuItem.Name = "ForAllSamplesToolStripMenuItem";
            this.ForAllSamplesToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.ForAllSamplesToolStripMenuItem.Text = "For All Samples";
            this.ForAllSamplesToolStripMenuItem.Click += new System.EventHandler(this.ForAllSamplesToolStripMenuItemClick);
            // 
            // ToolStripSeparator3
            // 
            this.ToolStripSeparator3.Name = "ToolStripSeparator3";
            this.ToolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // btnShowClusteringForm
            // 
            this.btnShowClusteringForm.Image = global::ArrayTubeAnalyzer.Properties.Resources.tree_16;
            this.btnShowClusteringForm.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnShowClusteringForm.Name = "btnShowClusteringForm";
            this.btnShowClusteringForm.Size = new System.Drawing.Size(81, 22);
            this.btnShowClusteringForm.Text = "Clustering";
            this.btnShowClusteringForm.Click += new System.EventHandler(this.ClusteringToolStripMenuItemClick);
            // 
            // StatusStrip1
            // 
            this.StatusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tslStatus});
            this.StatusStrip1.Location = new System.Drawing.Point(0, 548);
            this.StatusStrip1.Name = "StatusStrip1";
            this.StatusStrip1.Size = new System.Drawing.Size(1008, 22);
            this.StatusStrip1.TabIndex = 27;
            this.StatusStrip1.Text = "StatusStrip1";
            // 
            // frmReviewMarkerCalls
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 570);
            this.Controls.Add(this.SplitContainer1);
            this.Controls.Add(this.ToolStrip1);
            this.Controls.Add(this.StatusStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmReviewMarkerCalls";
            this.Text = "Array Tube Analyzer - Review Marker Calls";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmReviewMarkerCallsFormClosing);
            this.ContextMenuStrip1.ResumeLayout(false);
            this.SplitContainer2.Panel1.ResumeLayout(false);
            this.SplitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SplitContainer2)).EndInit();
            this.SplitContainer2.ResumeLayout(false);
            this.SplitContainer3.Panel1.ResumeLayout(false);
            this.SplitContainer3.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SplitContainer3)).EndInit();
            this.SplitContainer3.ResumeLayout(false);
            this.lblSampleName.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tbrContrast)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx)).EndInit();
            this.SplitContainer4.Panel1.ResumeLayout(false);
            this.SplitContainer4.Panel1.PerformLayout();
            this.SplitContainer4.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SplitContainer4)).EndInit();
            this.SplitContainer4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbxReplicates)).EndInit();
            this.SplitContainer1.Panel1.ResumeLayout(false);
            this.SplitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SplitContainer1)).EndInit();
            this.SplitContainer1.ResumeLayout(false);
            this.ToolStrip1.ResumeLayout(false);
            this.ToolStrip1.PerformLayout();
            this.StatusStrip1.ResumeLayout(false);
            this.StatusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.ToolTip ToolTip1;
        internal System.Windows.Forms.ContextMenuStrip ContextMenuStrip1;
        internal System.Windows.Forms.ToolStripMenuItem ShowSpotStripToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem ShowMarkerStripToolStripMenuItem;
        internal System.Windows.Forms.SplitContainer SplitContainer2;
        internal System.Windows.Forms.SplitContainer SplitContainer3;
        internal System.Windows.Forms.TrackBar tbrContrast;
        internal System.Windows.Forms.PictureBox pbx;
        internal System.Windows.Forms.SplitContainer SplitContainer4;
        internal System.Windows.Forms.CheckBox chkContrast;
        internal System.Windows.Forms.CheckBox chkGrid;
        internal System.Windows.Forms.PictureBox pbxReplicates;
        private ListViewFF lvwMarkerInfo;
        private System.Windows.Forms.ColumnHeader chSpot;
        private System.Windows.Forms.ColumnHeader chMarker;
        private System.Windows.Forms.ColumnHeader chReplicate;
        private System.Windows.Forms.ColumnHeader chSignal;
        private System.Windows.Forms.ColumnHeader chAverageSignal;
        private System.Windows.Forms.ColumnHeader chThreshold;
        private System.Windows.Forms.ColumnHeader chThresholdType;
        private System.Windows.Forms.ColumnHeader chMarkerCall;
        internal System.Windows.Forms.SplitContainer SplitContainer1;
        private ListViewFF lvwSamples;
        private System.Windows.Forms.ColumnHeader chSample;
        private System.Windows.Forms.ColumnHeader chFinalized;
        private System.Windows.Forms.ColumnHeader chAmbiguousCount;
        internal System.Windows.Forms.ToolStripMenuItem CloseReviewMarkerCallsToolStripMenuItem;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator5;
        internal System.Windows.Forms.ToolStripMenuItem ExitToolStripMenuItem;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator1;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator4;
        internal System.Windows.Forms.ToolStripMenuItem AboutToolStripMenuItem;
        internal System.Windows.Forms.ToolStripStatusLabel tslStatus;
        internal System.Windows.Forms.ToolStripMenuItem SaveToolStripMenuItem;
        internal System.Windows.Forms.ToolStrip ToolStrip1;
        internal System.Windows.Forms.ToolStripDropDownButton tsbFile;
        internal System.Windows.Forms.ToolStripDropDownButton ddbView;
        internal System.Windows.Forms.ToolStripMenuItem SampleReviewToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem GeneThresholdAnalysisToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem SpotSignalAnalysisToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem ClusteringToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem SampleDiagnosticsToolStripMenuItem;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator2;
        internal System.Windows.Forms.ToolStripDropDownButton ddbResetGeneCalls;
        internal System.Windows.Forms.ToolStripMenuItem ForSelectedSampleToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem ForAllSamplesToolStripMenuItem;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator3;
        internal System.Windows.Forms.ToolStripButton btnShowClusteringForm;
        internal System.Windows.Forms.StatusStrip StatusStrip1;
        internal System.Windows.Forms.CheckBox chkGridNumbers;
        private System.Windows.Forms.ColumnHeader chPercentError;
        private System.Windows.Forms.ColumnHeader chDeltaSignal;
        private System.Windows.Forms.ColumnHeader chReplicateStatus;
        private System.Windows.Forms.GroupBox lblSampleName;
    }
}