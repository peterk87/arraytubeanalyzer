﻿using System;
using System.Drawing;

namespace ArrayTubeAnalyzer
{
    public class SpotImageCrop : IComparable<SpotImageCrop>
    {

        private readonly Bitmap _bmAT;
        public Bitmap ImageAT
        {
            get { return _bmAT; }
        }
        private readonly int _iSpot;
        public int Spot
        {
            get { return _iSpot; }
        }
        private readonly int _iGene;
        public int Gene
        {
            get { return _iGene; }
        }
        private readonly int _iArray;
        public int Array
        {
            get { return _iArray; }
        }
        private readonly double _dSignal;
        public double Signal
        {
            get { return _dSignal; }
        }
        private readonly double _dAvgSignal;
        public double AverageSignal
        {
            get { return _dAvgSignal; }
        }
        private readonly double _dFinalAvgSignal;
        public double FinalAverageSignal
        {
            get { return _dFinalAvgSignal; }
        }

        private readonly Flagging _eFlagging;
        public Flagging Flagging
        {
            get { return _eFlagging; }
        }

        private readonly bool _bGeneCall;
        public bool GeneCall
        {
            get { return _bGeneCall; }
        }

        public Sorting SortBy { get; set; }

        public enum Sorting
        {
            BySignal,
            ByFinalAverageSignal,
            BySpot
        }

        public SpotImageCrop(Bitmap bm, int spot, int gene, int at, double signal, double avg, double finalAvg, Flagging flagging)
        {
            _bmAT = bm;
            _iSpot = spot;
            _iGene = gene;
            _iArray = at;
            _dSignal = signal;
            _dAvgSignal = avg;
            _dFinalAvgSignal = finalAvg;
            _eFlagging = flagging;
        }

        public SpotImageCrop(Bitmap bm, int spot, int gene, int at, double signal, double avg, double finalAvg, Flagging flagging, bool gc)
        {
            _bmAT = bm;
            _iSpot = spot;
            _iGene = gene;
            _iArray = at;
            _dSignal = signal;
            _dAvgSignal = avg;
            _dFinalAvgSignal = finalAvg;
            _eFlagging = flagging;
            _bGeneCall = gc;
        }
        
        public int CompareTo(SpotImageCrop other)
        {
            if (SortBy == Sorting.BySignal)
            {
                if (other != null && _dSignal > other._dSignal)
                {
                    return 1;
                }
                if (other != null && _dSignal < other._dSignal)
                {
                    return -1;
                }
            }
            else if (SortBy == Sorting.ByFinalAverageSignal)
            {
                if (_dFinalAvgSignal > other._dFinalAvgSignal)
                {
                    return 1;
                }
                if (_dFinalAvgSignal < other._dFinalAvgSignal)
                {
                    return -1;
                }
            }
            else
            {
                if (_iSpot > other._iSpot)
                {
                    return 1;
                }
                if (_iSpot < other._iSpot)
                {
                    return -1;
                }
            }
            return 0;
        }
    }

}
