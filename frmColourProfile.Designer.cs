﻿namespace ArrayTubeAnalyzer
{
    partial class FrmColourProfile
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAbsent = new System.Windows.Forms.Button();
            this.btnAbsentListview = new System.Windows.Forms.Button();
            this.btnFlaggedPresent = new System.Windows.Forms.Button();
            this.btnPresentListview = new System.Windows.Forms.Button();
            this.btnFlaggedAbsent = new System.Windows.Forms.Button();
            this.btnPresent = new System.Windows.Forms.Button();
            this.btnFlaggedAbsentListview = new System.Windows.Forms.Button();
            this.GroupBox1 = new System.Windows.Forms.GroupBox();
            this.GroupBox3 = new System.Windows.Forms.GroupBox();
            this.btnFlaggedListview = new System.Windows.Forms.Button();
            this.btnKeepListview = new System.Windows.Forms.Button();
            this.btnDiscardListview = new System.Windows.Forms.Button();
            this.btnKeepAutoListview = new System.Windows.Forms.Button();
            this.GroupBox2 = new System.Windows.Forms.GroupBox();
            this.btnFlagged = new System.Windows.Forms.Button();
            this.btnKeep = new System.Windows.Forms.Button();
            this.btnDiscard = new System.Windows.Forms.Button();
            this.btnKeepAuto = new System.Windows.Forms.Button();
            this.btnFlaggedPresentListview = new System.Windows.Forms.Button();
            this.btnAbsentAuto = new System.Windows.Forms.Button();
            this.GroupBox9 = new System.Windows.Forms.GroupBox();
            this.btnAbsentOdd = new System.Windows.Forms.Button();
            this.btnPresentOdd = new System.Windows.Forms.Button();
            this.btnSampleOdd = new System.Windows.Forms.Button();
            this.btnPresentAuto = new System.Windows.Forms.Button();
            this.btnAbsentEven = new System.Windows.Forms.Button();
            this.btnPresentEven = new System.Windows.Forms.Button();
            this.GroupBox7 = new System.Windows.Forms.GroupBox();
            this.GroupBox8 = new System.Windows.Forms.GroupBox();
            this.btnSampleEven = new System.Windows.Forms.Button();
            this.GroupBox5 = new System.Windows.Forms.GroupBox();
            this.btnPresentAutoListview = new System.Windows.Forms.Button();
            this.btnAbsentAutoListview = new System.Windows.Forms.Button();
            this.GroupBox6 = new System.Windows.Forms.GroupBox();
            this.GroupBox4 = new System.Windows.Forms.GroupBox();
            this.GroupBox1.SuspendLayout();
            this.GroupBox3.SuspendLayout();
            this.GroupBox2.SuspendLayout();
            this.GroupBox9.SuspendLayout();
            this.GroupBox7.SuspendLayout();
            this.GroupBox8.SuspendLayout();
            this.GroupBox5.SuspendLayout();
            this.GroupBox6.SuspendLayout();
            this.GroupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnAbsent
            // 
            this.btnAbsent.Location = new System.Drawing.Point(6, 50);
            this.btnAbsent.Name = "btnAbsent";
            this.btnAbsent.Size = new System.Drawing.Size(93, 23);
            this.btnAbsent.TabIndex = 5;
            this.btnAbsent.Text = "Absent";
            this.btnAbsent.UseVisualStyleBackColor = true;
            this.btnAbsent.Click += new System.EventHandler(this.BtnAbsentClick);
            // 
            // btnAbsentListview
            // 
            this.btnAbsentListview.Location = new System.Drawing.Point(6, 50);
            this.btnAbsentListview.Name = "btnAbsentListview";
            this.btnAbsentListview.Size = new System.Drawing.Size(93, 23);
            this.btnAbsentListview.TabIndex = 5;
            this.btnAbsentListview.Text = "Absent";
            this.btnAbsentListview.UseVisualStyleBackColor = true;
            this.btnAbsentListview.Click += new System.EventHandler(this.BtnAbsentListviewClick);
            // 
            // btnFlaggedPresent
            // 
            this.btnFlaggedPresent.Location = new System.Drawing.Point(6, 143);
            this.btnFlaggedPresent.Name = "btnFlaggedPresent";
            this.btnFlaggedPresent.Size = new System.Drawing.Size(93, 23);
            this.btnFlaggedPresent.TabIndex = 1;
            this.btnFlaggedPresent.Text = "Flagged Present";
            this.btnFlaggedPresent.UseVisualStyleBackColor = true;
            this.btnFlaggedPresent.Click += new System.EventHandler(this.BtnFlaggedPresentListviewClick);
            // 
            // btnPresentListview
            // 
            this.btnPresentListview.Location = new System.Drawing.Point(6, 19);
            this.btnPresentListview.Name = "btnPresentListview";
            this.btnPresentListview.Size = new System.Drawing.Size(93, 23);
            this.btnPresentListview.TabIndex = 4;
            this.btnPresentListview.Text = "Present";
            this.btnPresentListview.UseVisualStyleBackColor = true;
            this.btnPresentListview.Click += new System.EventHandler(this.BtnPresentListviewClick);
            // 
            // btnFlaggedAbsent
            // 
            this.btnFlaggedAbsent.Location = new System.Drawing.Point(6, 174);
            this.btnFlaggedAbsent.Name = "btnFlaggedAbsent";
            this.btnFlaggedAbsent.Size = new System.Drawing.Size(93, 23);
            this.btnFlaggedAbsent.TabIndex = 0;
            this.btnFlaggedAbsent.Text = "Flagged Absent";
            this.btnFlaggedAbsent.UseVisualStyleBackColor = true;
            this.btnFlaggedAbsent.Click += new System.EventHandler(this.BtnFlaggedAbsentClick);
            // 
            // btnPresent
            // 
            this.btnPresent.Location = new System.Drawing.Point(6, 19);
            this.btnPresent.Name = "btnPresent";
            this.btnPresent.Size = new System.Drawing.Size(93, 23);
            this.btnPresent.TabIndex = 4;
            this.btnPresent.Text = "Present";
            this.btnPresent.UseVisualStyleBackColor = true;
            this.btnPresent.Click += new System.EventHandler(this.BtnPresentClick);
            // 
            // btnFlaggedAbsentListview
            // 
            this.btnFlaggedAbsentListview.Location = new System.Drawing.Point(6, 174);
            this.btnFlaggedAbsentListview.Name = "btnFlaggedAbsentListview";
            this.btnFlaggedAbsentListview.Size = new System.Drawing.Size(93, 23);
            this.btnFlaggedAbsentListview.TabIndex = 0;
            this.btnFlaggedAbsentListview.Text = "Flagged Absent";
            this.btnFlaggedAbsentListview.UseVisualStyleBackColor = true;
            this.btnFlaggedAbsentListview.Click += new System.EventHandler(this.BtnFlaggedAbsentListviewClick);
            // 
            // GroupBox1
            // 
            this.GroupBox1.Controls.Add(this.GroupBox3);
            this.GroupBox1.Controls.Add(this.GroupBox2);
            this.GroupBox1.Location = new System.Drawing.Point(12, 12);
            this.GroupBox1.Name = "GroupBox1";
            this.GroupBox1.Size = new System.Drawing.Size(223, 169);
            this.GroupBox1.TabIndex = 12;
            this.GroupBox1.TabStop = false;
            this.GroupBox1.Text = "Main Window";
            // 
            // GroupBox3
            // 
            this.GroupBox3.Controls.Add(this.btnFlaggedListview);
            this.GroupBox3.Controls.Add(this.btnKeepListview);
            this.GroupBox3.Controls.Add(this.btnDiscardListview);
            this.GroupBox3.Controls.Add(this.btnKeepAutoListview);
            this.GroupBox3.Location = new System.Drawing.Point(114, 19);
            this.GroupBox3.Name = "GroupBox3";
            this.GroupBox3.Size = new System.Drawing.Size(102, 142);
            this.GroupBox3.TabIndex = 9;
            this.GroupBox3.TabStop = false;
            this.GroupBox3.Text = "Listview Items";
            // 
            // btnFlaggedListview
            // 
            this.btnFlaggedListview.Location = new System.Drawing.Point(6, 19);
            this.btnFlaggedListview.Name = "btnFlaggedListview";
            this.btnFlaggedListview.Size = new System.Drawing.Size(90, 23);
            this.btnFlaggedListview.TabIndex = 4;
            this.btnFlaggedListview.Text = "Flagged";
            this.btnFlaggedListview.UseVisualStyleBackColor = true;
            this.btnFlaggedListview.Click += new System.EventHandler(this.BtnFlaggedListviewClick);
            // 
            // btnKeepListview
            // 
            this.btnKeepListview.Location = new System.Drawing.Point(6, 50);
            this.btnKeepListview.Name = "btnKeepListview";
            this.btnKeepListview.Size = new System.Drawing.Size(90, 23);
            this.btnKeepListview.TabIndex = 5;
            this.btnKeepListview.Text = "Keep";
            this.btnKeepListview.UseVisualStyleBackColor = true;
            this.btnKeepListview.Click += new System.EventHandler(this.BtnKeepListviewClick);
            // 
            // btnDiscardListview
            // 
            this.btnDiscardListview.Location = new System.Drawing.Point(6, 112);
            this.btnDiscardListview.Name = "btnDiscardListview";
            this.btnDiscardListview.Size = new System.Drawing.Size(90, 23);
            this.btnDiscardListview.TabIndex = 7;
            this.btnDiscardListview.Text = "Discard";
            this.btnDiscardListview.UseVisualStyleBackColor = true;
            this.btnDiscardListview.Click += new System.EventHandler(this.BtnDiscardListviewClick);
            // 
            // btnKeepAutoListview
            // 
            this.btnKeepAutoListview.Location = new System.Drawing.Point(6, 81);
            this.btnKeepAutoListview.Name = "btnKeepAutoListview";
            this.btnKeepAutoListview.Size = new System.Drawing.Size(90, 23);
            this.btnKeepAutoListview.TabIndex = 6;
            this.btnKeepAutoListview.Text = "Keep (Auto)";
            this.btnKeepAutoListview.UseVisualStyleBackColor = true;
            this.btnKeepAutoListview.Click += new System.EventHandler(this.BtnKeepAutoListviewClick);
            // 
            // GroupBox2
            // 
            this.GroupBox2.Controls.Add(this.btnFlagged);
            this.GroupBox2.Controls.Add(this.btnKeep);
            this.GroupBox2.Controls.Add(this.btnDiscard);
            this.GroupBox2.Controls.Add(this.btnKeepAuto);
            this.GroupBox2.Location = new System.Drawing.Point(6, 19);
            this.GroupBox2.Name = "GroupBox2";
            this.GroupBox2.Size = new System.Drawing.Size(102, 142);
            this.GroupBox2.TabIndex = 8;
            this.GroupBox2.TabStop = false;
            this.GroupBox2.Text = "Spot Highlighters";
            // 
            // btnFlagged
            // 
            this.btnFlagged.Location = new System.Drawing.Point(6, 19);
            this.btnFlagged.Name = "btnFlagged";
            this.btnFlagged.Size = new System.Drawing.Size(90, 23);
            this.btnFlagged.TabIndex = 0;
            this.btnFlagged.Text = "Flagged";
            this.btnFlagged.UseVisualStyleBackColor = true;
            this.btnFlagged.Click += new System.EventHandler(this.BtnFlaggedClick);
            // 
            // btnKeep
            // 
            this.btnKeep.Location = new System.Drawing.Point(6, 50);
            this.btnKeep.Name = "btnKeep";
            this.btnKeep.Size = new System.Drawing.Size(90, 23);
            this.btnKeep.TabIndex = 1;
            this.btnKeep.Text = "Keep";
            this.btnKeep.UseVisualStyleBackColor = true;
            this.btnKeep.Click += new System.EventHandler(this.BtnKeepColourClick);
            // 
            // btnDiscard
            // 
            this.btnDiscard.Location = new System.Drawing.Point(6, 112);
            this.btnDiscard.Name = "btnDiscard";
            this.btnDiscard.Size = new System.Drawing.Size(90, 23);
            this.btnDiscard.TabIndex = 3;
            this.btnDiscard.Text = "Discard";
            this.btnDiscard.UseVisualStyleBackColor = true;
            this.btnDiscard.Click += new System.EventHandler(this.BtnDiscardClick);
            // 
            // btnKeepAuto
            // 
            this.btnKeepAuto.Location = new System.Drawing.Point(6, 81);
            this.btnKeepAuto.Name = "btnKeepAuto";
            this.btnKeepAuto.Size = new System.Drawing.Size(90, 23);
            this.btnKeepAuto.TabIndex = 2;
            this.btnKeepAuto.Text = "Keep (Auto)";
            this.btnKeepAuto.UseVisualStyleBackColor = true;
            this.btnKeepAuto.Click += new System.EventHandler(this.BtnKeepAutoClick);
            // 
            // btnFlaggedPresentListview
            // 
            this.btnFlaggedPresentListview.Location = new System.Drawing.Point(6, 143);
            this.btnFlaggedPresentListview.Name = "btnFlaggedPresentListview";
            this.btnFlaggedPresentListview.Size = new System.Drawing.Size(93, 23);
            this.btnFlaggedPresentListview.TabIndex = 1;
            this.btnFlaggedPresentListview.Text = "Flagged Present";
            this.btnFlaggedPresentListview.UseVisualStyleBackColor = true;
            this.btnFlaggedPresentListview.Click += new System.EventHandler(this.BtnFlaggedPresentListviewClick);
            // 
            // btnAbsentAuto
            // 
            this.btnAbsentAuto.Location = new System.Drawing.Point(6, 112);
            this.btnAbsentAuto.Name = "btnAbsentAuto";
            this.btnAbsentAuto.Size = new System.Drawing.Size(93, 23);
            this.btnAbsentAuto.TabIndex = 3;
            this.btnAbsentAuto.Text = "Absent (Auto)";
            this.btnAbsentAuto.UseVisualStyleBackColor = true;
            this.btnAbsentAuto.Click += new System.EventHandler(this.BtnAbsentAutoClick);
            // 
            // GroupBox9
            // 
            this.GroupBox9.Controls.Add(this.btnAbsentOdd);
            this.GroupBox9.Controls.Add(this.btnPresentOdd);
            this.GroupBox9.Controls.Add(this.btnSampleOdd);
            this.GroupBox9.Location = new System.Drawing.Point(6, 19);
            this.GroupBox9.Name = "GroupBox9";
            this.GroupBox9.Size = new System.Drawing.Size(104, 111);
            this.GroupBox9.TabIndex = 9;
            this.GroupBox9.TabStop = false;
            this.GroupBox9.Text = "Odd Clusters";
            // 
            // btnAbsentOdd
            // 
            this.btnAbsentOdd.Location = new System.Drawing.Point(6, 50);
            this.btnAbsentOdd.Name = "btnAbsentOdd";
            this.btnAbsentOdd.Size = new System.Drawing.Size(93, 23);
            this.btnAbsentOdd.TabIndex = 5;
            this.btnAbsentOdd.Text = "Absent";
            this.btnAbsentOdd.UseVisualStyleBackColor = true;
            this.btnAbsentOdd.Click += new System.EventHandler(this.BtnAbsentOddClick);
            // 
            // btnPresentOdd
            // 
            this.btnPresentOdd.Location = new System.Drawing.Point(6, 19);
            this.btnPresentOdd.Name = "btnPresentOdd";
            this.btnPresentOdd.Size = new System.Drawing.Size(93, 23);
            this.btnPresentOdd.TabIndex = 4;
            this.btnPresentOdd.Text = "Present";
            this.btnPresentOdd.UseVisualStyleBackColor = true;
            this.btnPresentOdd.Click += new System.EventHandler(this.BtnPresentOddClick);
            // 
            // btnSampleOdd
            // 
            this.btnSampleOdd.Location = new System.Drawing.Point(6, 81);
            this.btnSampleOdd.Name = "btnSampleOdd";
            this.btnSampleOdd.Size = new System.Drawing.Size(93, 23);
            this.btnSampleOdd.TabIndex = 2;
            this.btnSampleOdd.Text = "Sample Name";
            this.btnSampleOdd.UseVisualStyleBackColor = true;
            this.btnSampleOdd.Click += new System.EventHandler(this.BtnSampleOddClick);
            // 
            // btnPresentAuto
            // 
            this.btnPresentAuto.Location = new System.Drawing.Point(6, 81);
            this.btnPresentAuto.Name = "btnPresentAuto";
            this.btnPresentAuto.Size = new System.Drawing.Size(93, 23);
            this.btnPresentAuto.TabIndex = 2;
            this.btnPresentAuto.Text = "Present (Auto)";
            this.btnPresentAuto.UseVisualStyleBackColor = true;
            this.btnPresentAuto.Click += new System.EventHandler(this.BtnPresentAutoClick);
            // 
            // btnAbsentEven
            // 
            this.btnAbsentEven.Location = new System.Drawing.Point(6, 50);
            this.btnAbsentEven.Name = "btnAbsentEven";
            this.btnAbsentEven.Size = new System.Drawing.Size(93, 23);
            this.btnAbsentEven.TabIndex = 5;
            this.btnAbsentEven.Text = "Absent";
            this.btnAbsentEven.UseVisualStyleBackColor = true;
            this.btnAbsentEven.Click += new System.EventHandler(this.BtnAbsentEvenClick);
            // 
            // btnPresentEven
            // 
            this.btnPresentEven.Location = new System.Drawing.Point(6, 19);
            this.btnPresentEven.Name = "btnPresentEven";
            this.btnPresentEven.Size = new System.Drawing.Size(93, 23);
            this.btnPresentEven.TabIndex = 4;
            this.btnPresentEven.Text = "Present";
            this.btnPresentEven.UseVisualStyleBackColor = true;
            this.btnPresentEven.Click += new System.EventHandler(this.BtnPresentEvenClick);
            // 
            // GroupBox7
            // 
            this.GroupBox7.Controls.Add(this.GroupBox8);
            this.GroupBox7.Controls.Add(this.GroupBox9);
            this.GroupBox7.Location = new System.Drawing.Point(12, 424);
            this.GroupBox7.Name = "GroupBox7";
            this.GroupBox7.Size = new System.Drawing.Size(223, 134);
            this.GroupBox7.TabIndex = 14;
            this.GroupBox7.TabStop = false;
            this.GroupBox7.Text = "Clustering Window";
            // 
            // GroupBox8
            // 
            this.GroupBox8.Controls.Add(this.btnAbsentEven);
            this.GroupBox8.Controls.Add(this.btnPresentEven);
            this.GroupBox8.Controls.Add(this.btnSampleEven);
            this.GroupBox8.Location = new System.Drawing.Point(113, 19);
            this.GroupBox8.Name = "GroupBox8";
            this.GroupBox8.Size = new System.Drawing.Size(104, 111);
            this.GroupBox8.TabIndex = 10;
            this.GroupBox8.TabStop = false;
            this.GroupBox8.Text = "Even Clusters";
            // 
            // btnSampleEven
            // 
            this.btnSampleEven.Location = new System.Drawing.Point(6, 81);
            this.btnSampleEven.Name = "btnSampleEven";
            this.btnSampleEven.Size = new System.Drawing.Size(93, 23);
            this.btnSampleEven.TabIndex = 2;
            this.btnSampleEven.Text = "Sample Name";
            this.btnSampleEven.UseVisualStyleBackColor = true;
            this.btnSampleEven.Click += new System.EventHandler(this.BtnSampleEvenClick);
            // 
            // GroupBox5
            // 
            this.GroupBox5.Controls.Add(this.btnAbsent);
            this.GroupBox5.Controls.Add(this.btnPresent);
            this.GroupBox5.Controls.Add(this.btnFlaggedAbsent);
            this.GroupBox5.Controls.Add(this.btnFlaggedPresent);
            this.GroupBox5.Controls.Add(this.btnAbsentAuto);
            this.GroupBox5.Controls.Add(this.btnPresentAuto);
            this.GroupBox5.Location = new System.Drawing.Point(6, 19);
            this.GroupBox5.Name = "GroupBox5";
            this.GroupBox5.Size = new System.Drawing.Size(104, 205);
            this.GroupBox5.TabIndex = 9;
            this.GroupBox5.TabStop = false;
            this.GroupBox5.Text = "Spot Highlighters";
            // 
            // btnPresentAutoListview
            // 
            this.btnPresentAutoListview.Location = new System.Drawing.Point(6, 81);
            this.btnPresentAutoListview.Name = "btnPresentAutoListview";
            this.btnPresentAutoListview.Size = new System.Drawing.Size(93, 23);
            this.btnPresentAutoListview.TabIndex = 2;
            this.btnPresentAutoListview.Text = "Present (Auto)";
            this.btnPresentAutoListview.UseVisualStyleBackColor = true;
            this.btnPresentAutoListview.Click += new System.EventHandler(this.BtnPresentAutoListviewClick);
            // 
            // btnAbsentAutoListview
            // 
            this.btnAbsentAutoListview.Location = new System.Drawing.Point(6, 112);
            this.btnAbsentAutoListview.Name = "btnAbsentAutoListview";
            this.btnAbsentAutoListview.Size = new System.Drawing.Size(93, 23);
            this.btnAbsentAutoListview.TabIndex = 3;
            this.btnAbsentAutoListview.Text = "Absent (Auto)";
            this.btnAbsentAutoListview.UseVisualStyleBackColor = true;
            this.btnAbsentAutoListview.Click += new System.EventHandler(this.BtnAbsentAutoListviewClick);
            // 
            // GroupBox6
            // 
            this.GroupBox6.Controls.Add(this.btnAbsentListview);
            this.GroupBox6.Controls.Add(this.btnPresentListview);
            this.GroupBox6.Controls.Add(this.btnFlaggedAbsentListview);
            this.GroupBox6.Controls.Add(this.btnFlaggedPresentListview);
            this.GroupBox6.Controls.Add(this.btnAbsentAutoListview);
            this.GroupBox6.Controls.Add(this.btnPresentAutoListview);
            this.GroupBox6.Location = new System.Drawing.Point(113, 19);
            this.GroupBox6.Name = "GroupBox6";
            this.GroupBox6.Size = new System.Drawing.Size(104, 205);
            this.GroupBox6.TabIndex = 10;
            this.GroupBox6.TabStop = false;
            this.GroupBox6.Text = "Listview Items";
            // 
            // GroupBox4
            // 
            this.GroupBox4.Controls.Add(this.GroupBox6);
            this.GroupBox4.Controls.Add(this.GroupBox5);
            this.GroupBox4.Location = new System.Drawing.Point(12, 187);
            this.GroupBox4.Name = "GroupBox4";
            this.GroupBox4.Size = new System.Drawing.Size(223, 231);
            this.GroupBox4.TabIndex = 13;
            this.GroupBox4.TabStop = false;
            this.GroupBox4.Text = "Marker Call Review Window";
            // 
            // frmColourProfile
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(246, 568);
            this.Controls.Add(this.GroupBox1);
            this.Controls.Add(this.GroupBox7);
            this.Controls.Add(this.GroupBox4);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FrmColourProfile";
            this.Text = "Colour Profile";
            this.GroupBox1.ResumeLayout(false);
            this.GroupBox3.ResumeLayout(false);
            this.GroupBox2.ResumeLayout(false);
            this.GroupBox9.ResumeLayout(false);
            this.GroupBox7.ResumeLayout(false);
            this.GroupBox8.ResumeLayout(false);
            this.GroupBox5.ResumeLayout(false);
            this.GroupBox6.ResumeLayout(false);
            this.GroupBox4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Button btnAbsent;
        internal System.Windows.Forms.Button btnAbsentListview;
        internal System.Windows.Forms.Button btnFlaggedPresent;
        internal System.Windows.Forms.Button btnPresentListview;
        internal System.Windows.Forms.Button btnFlaggedAbsent;
        internal System.Windows.Forms.Button btnPresent;
        internal System.Windows.Forms.Button btnFlaggedAbsentListview;
        internal System.Windows.Forms.GroupBox GroupBox1;
        internal System.Windows.Forms.GroupBox GroupBox3;
        internal System.Windows.Forms.Button btnFlaggedListview;
        internal System.Windows.Forms.Button btnKeepListview;
        internal System.Windows.Forms.Button btnDiscardListview;
        internal System.Windows.Forms.Button btnKeepAutoListview;
        internal System.Windows.Forms.GroupBox GroupBox2;
        internal System.Windows.Forms.Button btnFlagged;
        internal System.Windows.Forms.Button btnKeep;
        internal System.Windows.Forms.Button btnDiscard;
        internal System.Windows.Forms.Button btnKeepAuto;
        internal System.Windows.Forms.Button btnFlaggedPresentListview;
        internal System.Windows.Forms.Button btnAbsentAuto;
        internal System.Windows.Forms.GroupBox GroupBox9;
        internal System.Windows.Forms.Button btnAbsentOdd;
        internal System.Windows.Forms.Button btnPresentOdd;
        internal System.Windows.Forms.Button btnSampleOdd;
        internal System.Windows.Forms.Button btnPresentAuto;
        internal System.Windows.Forms.Button btnAbsentEven;
        internal System.Windows.Forms.Button btnPresentEven;
        internal System.Windows.Forms.GroupBox GroupBox7;
        internal System.Windows.Forms.GroupBox GroupBox8;
        internal System.Windows.Forms.Button btnSampleEven;
        internal System.Windows.Forms.GroupBox GroupBox5;
        internal System.Windows.Forms.Button btnPresentAutoListview;
        internal System.Windows.Forms.Button btnAbsentAutoListview;
        internal System.Windows.Forms.GroupBox GroupBox6;
        internal System.Windows.Forms.GroupBox GroupBox4;
    }
}