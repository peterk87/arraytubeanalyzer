﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;

namespace ArrayTubeAnalyzer
{
    public partial class FrmReviewMarkerCalls : Form
    {

        private readonly ListViewColumnSorter _lvwMarkerInfoColumnSorter = new ListViewColumnSorter();
        private readonly ListViewColumnSorter _lvwSamplesColumnSorter = new ListViewColumnSorter();
        /// <summary>2D array of "Top" location values for each label within the grid.</summary>
        private double[,] _topPoints;
        /// <summary>2D array of "Left" location values for each label within the grid.</summary>
        private double[,] _leftPoints;
        /// <summary>Grid overlay array.</summary>
        private LabelWithColours[] _lbl;
        /// <summary>Boolean to keep track of whether or not the grid has been created.</summary>
        private bool _gClblCreated;
        /// <summary>Contrast factor for AT image.</summary>
        private float _contrastFactor;
        /// <summary>Selected AT index number in listview.</summary>
        public SpotMarkerArrayIndices Indices;
        private readonly SampleData _sd;
        ///'''''''''''''''''''''''
        //Subs and functions below'
        ///'''''''''''''''''''''''

        public FrmReviewMarkerCalls(object objSampleData)
        {
            // This call is required by the designer.
            InitializeComponent();

            // Add any initialization after the InitializeComponent() call.
            _sd = objSampleData as SampleData;
            _sd.UpdateCalls();
            // Create an instance of a ListView column sorter and assign it to the ListView control.
            lvwMarkerInfo.ListViewItemSorter = _lvwMarkerInfoColumnSorter;
            lvwSamples.ListViewItemSorter = _lvwSamplesColumnSorter;
            SetupForm();
        }

        private void AboutToolStripMenuItemClick(object sender, EventArgs e)
        {
            var f = new AboutBox1(_sd.Forms);
            f.ShowDialog();
        }

        /// <summary>Adjust the positions and colors and visibility of the spot highlighters in the grid overlay.</summary>
        private void AdjustGrid()
        {
            int index = GetSelectedSample();
            if (index == -1)
                return;
            CalibrationInfo calibration = _sd.GetCalibrationInfo(index);
            double heightRatio = pbx.Height / 400d;
            double widthRatio = pbx.Width / 400d;
            double h1 = calibration.TopLeftX * widthRatio;
            double h2 = calibration.TopRightX * widthRatio;
            double v1 = calibration.TopLeftY * heightRatio;
            double v4 = calibration.BottomLeftY * heightRatio;

            var fontRegular = new Font(FontFamily.GenericSansSerif, 6, FontStyle.Regular);

            double newHeight = (v4 - v1 + (20d * heightRatio)) / _sd.Layout.Rows;
            double newWidth = (h2 - h1 + (20d * widthRatio)) / _sd.Layout.Columns;
            for (int i = 0; i < _sd.Layout.Rows; i++)
            {
                for (int j = 0; j < _sd.Layout.Columns; j++)
                {
                    int spot = (_sd.Layout.Columns - (i + 1)) * _sd.Layout.Rows + (j + 1);
                    LabelWithColours lbl = _lbl[spot - 1];
                    List<int> spots = SpotExistsGetSpots(spot);
                    if ((spots != null))
                    {
                        lbl.Top = (int)_topPoints[i, j] + 1;
                        lbl.Left = (int)_leftPoints[i, j] + 1;
                        lbl.Height = (int)newHeight - 1;
                        lbl.Width = (int)newWidth - 1;
                        lbl.BorderWidth = 1;
                        lbl.Font = fontRegular;

                        int markerIndex = _sd.Spots.IndexOf(spots);
                        int spotIndex = spots.IndexOf(spot);
                        MarkerInfo marker = _sd.SampleStats[index][markerIndex];
                        string sToolTipText = string.Format("Marker: {0}\nSpot: {1}\nSignal: {2}\nDelta Signal: {3}\nPercent Error: {4}\nAverage Signal: {5}\nFlagging Status: {6}",
                            _sd.Markers[markerIndex],
                            spot,
                            marker.Signal[spotIndex].ToString("0.0000"),
                            marker.DeltaSignal[spotIndex].ToString("0.0000"),
                            marker.PercentError[spotIndex].ToString("p"),
                            marker.FinalAverageSignal.ToString("0.0000"),
                            marker.Flag[spotIndex]);
                        ToolTip1.SetToolTip(lbl, sToolTipText);
                        Color borderColor = Color.Gray;
                        Calls markerCall = _sd.SampleCalls[index].Calls[markerIndex];
                        switch (markerCall)
                        {
                            case Calls.Absent:
                                borderColor = _sd.Settings.C.AbsentColour;
                                break;
                            case Calls.AbsentAuto:
                                borderColor = _sd.Settings.C.AbsentAutoColour;
                                break;
                            case Calls.AmbiguousAbsent:
                                borderColor = _sd.Settings.C.AbsentFlaggedColour;
                                break;
                            case Calls.AmbiguousPresent:
                                borderColor = _sd.Settings.C.PresentFlaggedColour;
                                break;
                            case Calls.AmbiguousManual:
                                borderColor = _sd.Settings.C.AbsentFlaggedColour;
                                break;
                            case Calls.Present:
                                borderColor = _sd.Settings.C.PresentColour;
                                break;
                            case Calls.PresentAuto:
                                borderColor = _sd.Settings.C.PresentAutoColour;
                                break;
                        }
                        if (marker.Flag[spotIndex] == Flagging.Discard)
                        {
                            borderColor = _sd.Settings.C.DiscardColour;
                        }
                        lbl.BorderColor = borderColor;
                        lbl.Visible = true;
                    }
                    else
                    {
                        lbl.Visible = false;
                    }
                }
            }
            _gClblCreated = true;
        }

        private void ChkGeneCallContrastCheckedChanged(object sender, EventArgs e)
        {
            if (pbx.Image == null)
                return;
            tbrContrast.Visible = chkContrast.Checked;
            tbrContrast.Value = 25;
            tslStatus.Text = "Ready";
        }

        /// <summary>Show or hide the grid.</summary>
        private void ChkGridCheckedChanged(object sender, EventArgs e)
        {
            if (lvwSamples.Items.Count == 0)
                return;
            if (chkGrid.Checked)
            {
                GetCalibrationInfo(GetSelectedSample());
                _gClblCreated = true;
                AdjustGrid();
            }
            else
            {
                HideGrid();
            }
        }

        /// <summary>Show/hide spot numbers in grid squares.</summary>
        private void ChkGridNumbersCheckedChanged(object sender, EventArgs e)
        {
            if (chkGridNumbers.Checked)
            {
                foreach (LabelWithColours lbl in _lbl)
                {
                    lbl.Text = ((int)lbl.Tag).ToString(CultureInfo.InvariantCulture);
                }
            }
            else
            {
                foreach (LabelWithColours lbl in _lbl)
                {
                    lbl.Text = "";
                }
            }
        }

        private void CloseReviewMarkerCallsToolStripMenuItemClick(object sender, EventArgs e)
        {
            Close();
        }

        private void ClusteringToolStripMenuItemClick(Object sender, EventArgs e)
        {
            if (_sd.Forms.Clustering != null)
            {
                _sd.Forms.Clustering.UpdateForm();
            }
            else
            {
                _sd.Forms.Clustering = new FrmClustering(_sd);
                _sd.Forms.Clustering.Show();
            }
            _sd.Forms.Clustering.Focus();
        }

        /*
                private void ContextMenuStrip1Opening(Object sender, System.ComponentModel.CancelEventArgs e)
                {
                    int index = getSelectedSample();
                    CalibrationInfo calibration = _sd.getCalibrationInfo(index);
                    e.Cancel = (calibration.BottomLeft_X == -1);
                }
        */

        /// <summary>Create a new grid overlay.</summary>
        private void CreateGrid()
        {
            if (_gClblCreated)
                return;
            _lbl = new LabelWithColours[_sd.Layout.Spots];
            int rows = _sd.Layout.Rows;
            int columns = _sd.Layout.Columns;
            CalibrationInfo calibration = _sd.GetCalibrationInfo(0);
            double newHeight = (calibration.BottomLeftY - calibration.TopLeftY + (20d * pbx.Height / 400d)) / rows - 1d;
            double newWidth = (calibration.TopRightX - calibration.TopLeftX + (20d * pbx.Width / 400d)) / columns - 1d;
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < columns; j++)
                {
                    int spt = (columns - (i + 1)) * rows + (j + 1);

                    var lbl = new LabelWithColours();
                    lbl.Click += LblClick;
                    lbl.MouseClick += LblMouseClick;
                    lbl.Parent = pbx;
                    lbl.BringToFront();
                    lbl.Location = new Point((int)_leftPoints[i, j] + 1, (int)_topPoints[i, j] + 1);
                    lbl.Size = new Size((int)newWidth, (int)newHeight);
                    lbl.Text = spt.ToString(CultureInfo.InvariantCulture);
                    lbl.Tag = spt;
                    _lbl[spt - 1] = lbl;
                }
            }
            _gClblCreated = true;
        }

        private void DrawSpotPictures(SpotMarkerArrayIndices indices)
        {
            var sic = new SpotImageCollection(_sd, _sd.SampleStats, _sd.Images, _sd.Calibration, indices.Marker, _sd.Spots[indices.Marker].ToArray(), _sd.Spots);
            int picWidth = _sd.Spots[indices.Marker].Count * 80;
            var bmp = new Bitmap(picWidth, 80);
            Graphics g = Graphics.FromImage(bmp);
            var f = new Font(FontFamily.GenericSansSerif, 8);
            Brush b = Brushes.Black;
            int iX = 0;
            foreach (SpotImageCrop s in sic.SpotImages)
            {
                if (s.Array == indices.Array)
                {
                    MarkerInfo at = _sd.SampleStats[indices.Array][indices.Marker];
                    g.DrawImage(s.ImageAT, iX, 0, 80, 80);
                    g.DrawString(s.Spot.ToString(CultureInfo.InvariantCulture), f, b, new PointF(iX, 0));
                    if (at.Flag[_sd.Spots[indices.Marker].IndexOf(s.Spot)] == Flagging.Discard)
                    {
                        var topRight = new PointF(80 + iX - g.MeasureString("Discarded", f).Width, 0);
                        g.DrawString("Discarded", f, Brushes.Red, topRight);
                    }
                    var pBottomLeft = new PointF(iX, 80 - g.MeasureString(s.Signal.ToString(CultureInfo.InvariantCulture), f).Height);
                    g.DrawString(s.Signal.ToString(CultureInfo.InvariantCulture), f, b, pBottomLeft);

                    iX += 80;
                }
            }
            pbxReplicates.Image = bmp;
            g.Dispose();

        }

        private void ExitToolStripMenuItemClick(Object sender, EventArgs e)
        {
            if (_sd.Forms.SampleQC.ExitApplication() == false)
            {
                Environment.Exit(0);
            }
        }

        /// <summary>Change the flagging status of the selected spot on the listview and within the spot information collection.</summary>
        private void ChangeMarkerCallStatus()
        {
            SpotMarkerArrayIndices indices = GetSelectedMarker();
            if (indices == null)
                return;

            tslStatus.Text = "Changing flagged status for " + _sd.Markers[indices.Marker];
            double avg = _sd.SampleStats[indices.Array][indices.Marker].FinalAverageSignal;
            MarkerThresholdInfo thresholdInfo = _sd.Thresholds.Markers[indices.Marker];
            Calls bFlag = _sd.SampleCalls[indices.Array].Calls[indices.Marker];
            string sFlag = "";
            Color backColor = Color.Black;
            Color borderColor = Color.Black;
            switch (bFlag)
            {
                case Calls.AbsentAuto:
                    sFlag = "Present";
                    bFlag = Calls.Present;
                    backColor = _sd.Settings.C.PresentListviewColour;
                    borderColor = _sd.Settings.C.PresentColour;
                    break;
                case Calls.PresentAuto:
                    sFlag = "Absent";
                    bFlag = Calls.Absent;
                    backColor = _sd.Settings.C.AbsentListviewColour;
                    borderColor = _sd.Settings.C.AbsentColour;
                    break;
                case Calls.Present:
                    sFlag = "Absent";
                    bFlag = Calls.Absent;
                    backColor = _sd.Settings.C.AbsentListviewColour;
                    borderColor = _sd.Settings.C.AbsentColour;
                    break;
                case Calls.Absent:
                    if (avg >= thresholdInfo.LowerThreshold && avg <= thresholdInfo.UpperThreshold)
                    {
                        if (avg > thresholdInfo.Threshold)
                        {
                            sFlag = "Ambiguous";
                            bFlag = Calls.AmbiguousPresent;
                            backColor = _sd.Settings.C.PresentFlaggedListviewColour;
                            borderColor = _sd.Settings.C.PresentFlaggedColour;
                        }
                        else
                        {
                            sFlag = "Ambiguous";
                            bFlag = Calls.AmbiguousAbsent;
                            backColor = _sd.Settings.C.AbsentFlaggedListviewColour;
                            borderColor = _sd.Settings.C.AbsentFlaggedColour;
                        }
                    }
                    else
                    {
                        sFlag = "Ambiguous";
                        bFlag = Calls.AmbiguousManual;
                        backColor = _sd.Settings.C.AbsentFlaggedListviewColour;
                        borderColor = _sd.Settings.C.AbsentFlaggedColour;
                    }
                    break;
                case Calls.AmbiguousAbsent:
                case Calls.AmbiguousPresent:
                case Calls.AmbiguousManual:
                    sFlag = "Present";
                    bFlag = Calls.Present;
                    backColor = _sd.Settings.C.PresentListviewColour;
                    borderColor = _sd.Settings.C.PresentColour;
                    break;
            }
            //change gene call status
            _sd.SampleCalls[indices.Array].Calls[indices.Marker] = bFlag;
            //update spot label
            foreach (int i in _sd.Spots[indices.Marker])
            {
                LabelWithColours lbl = _lbl[i - 1];
                lbl.BorderColor = borderColor;
                lbl.Refresh();
                if (_sd.SampleStats[indices.Array][indices.Marker].Flag[_sd.Spots[indices.Marker].IndexOf(i)] == Flagging.Discard)
                {
                    lbl.BorderColor = _sd.Settings.C.DiscardColour;
                }
            }

            //need to update replicate spots call status in listview
            //find replicate spots - same indices.Gene in listview
            foreach (ListViewItem l in lvwMarkerInfo.Items)
            {
                var i = (SpotMarkerArrayIndices)l.Tag;
                if (i.Marker == indices.Marker)
                {
                    l.SubItems[9].Text = sFlag;
                    l.BackColor = (_sd.SampleStats[i.Array][i.Marker].Flag[i.Spot] == Flagging.Discard) ? _sd.Settings.C.DiscardColour : backColor;
                    l.Selected = (i.Spot == indices.Spot);
                }
            }

            //update sample lvw with updated flagged spot counts

            int index = -1;
            foreach (ListViewItem l in lvwSamples.Items)
            {
                if ((int)l.Tag == indices.Array)
                {
                    index = l.Index;
                }
            }
            int count = 0;
            foreach (Calls markerCall in _sd.SampleCalls[indices.Array].Calls)
            {
                if (markerCall == Calls.AmbiguousAbsent || markerCall == Calls.AmbiguousPresent || markerCall == Calls.AmbiguousManual)
                {
                    count++;
                }
            }

            lvwSamples.Items[index].SubItems[2].Text = count.ToString(CultureInfo.InvariantCulture);
            UpdateForms();
        }

        private void ForSelectedSampleToolStripMenuItemClick(Object sender, EventArgs e)
        {
            if (MessageBox.Show("Reset calls for selected sample?", "Reset calls for selected sample?", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                int index = _sd.Images.IndexOf(pbx.Image);
                SampleCalls lcalls = _sd.SampleCalls[index];
                lcalls.Finalized = false;
                for (int i = 0; i < lcalls.Calls.Count; i++)
                {
                    lcalls.Calls[i] = Calls.AmbiguousAbsent;
                }
                _sd.UpdateCalls();
                UpdateSampleListView();
                lvwSamples.Items[index].Selected = true;
                UpdateForms();
            }
        }

        private void ForAllSamplesToolStripMenuItemClick(Object sender, EventArgs e)
        {
            if (MessageBox.Show("Reset calls for entire dataset?", "Reset calls for entire dataset?", MessageBoxButtons.OKCancel) != DialogResult.OK)
                return;
            foreach (SampleCalls lcalls in _sd.SampleCalls)
            {
                lcalls.Finalized = false;
                for (int j = 0; j < lcalls.Calls.Count; j++)
                {
                    lcalls.Calls[j] = Calls.AmbiguousAbsent;
                }
            }

            _sd.UpdateCalls();
            UpdateSampleListView();
            lvwSamples.Items[_sd.Images.IndexOf(pbx.Image)].Selected = true;
            UpdateForms();
        }

        private void UpdateForms()
        {
            _sd.ClusterUPGMA();
            _sd.Forms.SampleQC.UpdatePicAndListview(0);

            if (_sd.Forms.MarkerThresholdAnalysis != null)
            {
                _sd.Forms.MarkerThresholdAnalysis.UpdateForm();
            }
            if (_sd.Forms.MarkerThresholdReview != null)
            {
                _sd.Forms.MarkerThresholdReview.UpdateForm();
            }
            if (_sd.Forms.Clustering != null)
            {
                _sd.Forms.Clustering.UpdateForm();
            }
            if (_sd.Forms.Diagnosis != null)
            {
                _sd.Forms.Diagnosis.UpdateListview();
            }
        }

        private void FrmReviewMarkerCallsFormClosing(object sender, FormClosingEventArgs e)
        {
            _sd.Forms.ReviewMarkerCalls = null;
        }
        /// <summary>Get the new coordinates for the grid overlay.</summary>
        private void GetCalibrationInfo(int index)
        {
            CalibrationInfo calibration = _sd.GetCalibrationInfo(index);
            int rows = _sd.Layout.Rows;
            int columns = _sd.Layout.Columns;
            //Get the width and height ratios to the original height and width
            double heightRatio = pbx.Height / 400d;
            double widthRatio = pbx.Width / 400d;
            //Use calibration data for the selected AT in 'frmVisualizeArrays' for the same AT
            double h1 = calibration.TopLeftX * widthRatio;
            double h2 = calibration.TopRightX * widthRatio;
            double h4 = calibration.BottomLeftX * widthRatio;
            double v1 = calibration.TopLeftY * heightRatio;
            double v2 = calibration.TopRightY * heightRatio;
            double v4 = calibration.BottomLeftY * heightRatio;

            double dVTop = v1 - v2;
            //change in vertical position between top two corner spots; positive means that v1 is lower than v2; negative means that v2 is lower than v1
            double dHLeft = h1 - h4;
            //change in horiz. position between left most corner spots; positive = h1 farther right than h4; neg. = h4 farther right than h1

            //Distances between top, bottom, right and left corner spots
            double distTop = h2 - h1 + (20d * widthRatio);
            //horiz. distance between top two corner spots
            double distLeft = v4 - v1 + (20d * heightRatio);
            //vert. distance between left most corner spots

            //New grid overlay location values.
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < columns; j++)
                {
                    _topPoints[i, j] = v1 + ((i + 1d) * distLeft / rows) - (((j + 1d) / columns) * dVTop) - (distLeft / rows);
                    _leftPoints[i, j] = h1 + ((j + 1d) * distTop / columns) - (((i + 1d) / rows) * dHLeft) - (distTop / columns);
                }
            }
        }

        /// <summary>Return the index of the selected item in the marker call info listview.</summary>
        private SpotMarkerArrayIndices GetSelectedMarker()
        {
            if (lvwMarkerInfo.SelectedIndices.Count == 0)
            {
                return null;
            }
            Indices = (SpotMarkerArrayIndices)lvwMarkerInfo.SelectedItems[0].Tag;
            return Indices;
        }


        /// <summary>Return the index of the selected sample.</summary>
        private int GetSelectedSample()
        {
            if (lvwSamples.SelectedIndices.Count == 0)
            {
                return -1;
            }
            return (int)lvwSamples.SelectedItems[0].Tag;
        }

        private void MarkerThresholdAnalysisToolStripMenuItemClick(Object sender, EventArgs e)
        {
            if (_sd.Forms.MarkerThresholdAnalysis == null)
            {
                _sd.Forms.MarkerThresholdAnalysis = new FrmMarkerThresholdAnalysis(_sd);
                _sd.Forms.MarkerThresholdAnalysis.Show();
            }
            _sd.Forms.MarkerThresholdAnalysis.Focus();
        }

        /// <summary>Hide the grid.</summary>
        private void HideGrid()
        {
            if (_gClblCreated == false)
                return;
            _gClblCreated = false;
            foreach (LabelWithColours lbl in _lbl)
            {
                lbl.Visible = false;
            }
        }

        /// <summary>When the left mouse button is released, highlight a spot on the array tube image if the left mouse button is released in a valid location.</summary>
        private void PbxMouseUp(object sender, MouseEventArgs e)
        {
            int index = GetSelectedSample();
            if (index == -1)
                return;
            if (e.Button != MouseButtons.Left)
                return;

            double x = e.X;
            double y = e.Y;
            double heightRatio = pbx.Height / 400d;
            double widthRatio = pbx.Width / 400d;
            CalibrationInfo calibration = _sd.GetCalibrationInfo(index);
            double h1 = calibration.TopLeftX * widthRatio;
            double h2 = calibration.TopRightX * widthRatio;
            double h3 = calibration.BottomRightX * widthRatio;
            double h4 = calibration.BottomLeftX * widthRatio;
            double v1 = calibration.TopLeftY * heightRatio;
            double v2 = calibration.TopRightY * heightRatio;
            double v3 = calibration.BottomRightY * heightRatio;
            double v4 = calibration.BottomLeftY * heightRatio;

            double minLeft = (h1 >= h4) ? h4 : h1;

            double maxLeft;
            if (h2 >= h3)
            {
                maxLeft = h2 + (20d * pbx.Width / 400d);
            }
            else
            {
                maxLeft = h3 + (20d * pbx.Width / 400d);
            }

            double minTop = v1 >= v2 ? v2 : v1;

            double maxtop;
            if (v4 >= v3)
            {
                maxtop = v4 + (20d * pbx.Height / 400d);
            }
            else
            {
                maxtop = v3 + (20d * pbx.Height / 400d);
            }

            //make sure that the click was in a valid area on the picture box - may be problems if the picture heavily misaligned

            if (x >= minLeft & x <= maxLeft & y >= minTop & y <= maxtop)
            {
                //find the vertical position that most closely matches the mouse button click vertical location
                int i;
                int j;
                for (i = _sd.Layout.Rows - 1; i >= 0; i += -1)
                {
                    if (y >= _topPoints[i, 0])
                    {
                        break;
                    }
                }

                if (i == -1)
                    return;
                //if the value is out of the range of the array then the sub is exited 

                //find the horizontal position that most closely matches the mouse button click horizontal location
                for (j = _sd.Layout.Columns - 1; j >= 0; j += -1)
                {
                    if (x >= _leftPoints[0, j])
                    {
                        break;
                    }
                }

                if (j == -1)
                    return;
                //if the value is out of the range of the array then the sub is exited 

                //find out which spot the click is closest to
                int k = (_sd.Layout.Columns - (i + 1)) * _sd.Layout.Rows + (j + 1);
                //determine which spot is highlighted with row and column information 

                List<int> lSpots = SpotExistsGetSpots(k);
                if (lSpots == null)
                    return;
                HighlightReplicatesOnGrid(lSpots);
                //look for the spot of interest within the listview table 
                for (int l = 0; l < lvwMarkerInfo.Items.Count; l++)
                {
                    int spot = int.Parse(lvwMarkerInfo.Items[l].SubItems[0].Text);
                    if (spot != k) continue;
                    //cannot get the highlighter to show up all of the time, only when there is a previous selection by the user on the listview table
                    lvwMarkerInfo.Items[l].Selected = true;
                    //make sure that the clicked spot is also visible in the listview table
                    lvwMarkerInfo.Items[l].EnsureVisible();
                    lvwMarkerInfo.Focus();
                    return;
                }
            }
        }


        private void PbxResize(Object sender, EventArgs e)
        {
            if (lvwSamples.Items.Count == 0)
                return;
            int index = GetSelectedSample();
            if (index == -1)
                return;
            GetCalibrationInfo(index);
            AdjustGrid();
        }

        private void LblClick(object sender, EventArgs e)
        {
            var lbl = sender as LabelWithColours;
            if (lbl == null) return;
            var lblIndex = (int)lbl.Tag;

            for (int l = 0; l < lvwMarkerInfo.Items.Count; l++)
            {
                int spot = int.Parse(lvwMarkerInfo.Items[l].SubItems[0].Text);
                if (spot != lblIndex) continue;
                //cannot get the highlighter to show up all of the time, only when there is a previous selection by the user on the listview table
                foreach (ListViewItem lvi in lvwMarkerInfo.Items)
                {
                    lvi.Selected = false;
                }

                lvwMarkerInfo.Items[l].Selected = true;

                //make sure that the clicked spot is also visible in the listview table
                lvwMarkerInfo.Items[l].EnsureVisible();
                lvwMarkerInfo.Focus();
                break;
            }
        }

        private void LblMouseClick(object sender, MouseEventArgs e)
        {
            SpawnSpotPreview(e);
        }

        /// <summary>If the checked status of the listview item is changed.</summary>
        private void LvwSamplesItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if (lvwSamples.Items.Count == 0)
                return;
            int index = GetSelectedSample();
            if (index == -1)
                return;
            _sd.SampleCalls[index].Finalized = e.Item.Checked;
            e.Item.SubItems[1].Text = e.Item.Checked.ToString(CultureInfo.InvariantCulture);

            if (e.Item.Checked) return;
            _sd.UpdateCalls();
            lvwSamples.Items[GetSelectedSample()].Selected = true;
            UpdateForm(GetSelectedSample());
        }

        /// <summary>When the selected array is changed.</summary>
        private void LvwSamplesItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            int index = GetSelectedSample();
            if (index == -1)
                return;
            //frmSampleQC._iSelectedArray = e.ItemIndex
            UpdateForm(index);
        }

        private void LvwSamplesColumnClick(object sender, ColumnClickEventArgs e)
        {
            // Determine if the clicked column is already the column that is being sorted.
            if ((e.Column == _lvwSamplesColumnSorter.SortColumn))
            {
                // Reverse the current sort direction for this column.
                if ((_lvwSamplesColumnSorter.Order == SortOrder.Ascending))
                {
                    _lvwSamplesColumnSorter.Order = SortOrder.Descending;
                }
                else
                {
                    _lvwSamplesColumnSorter.Order = SortOrder.Ascending;
                }
            }
            else
            {
                // Set the column number that is to be sorted; default to ascending.
                _lvwSamplesColumnSorter.SortColumn = e.Column;
                _lvwSamplesColumnSorter.Order = SortOrder.Ascending;
            }
            // Perform the sort with these new sort options.
            lvwSamples.Sort();
        }

        /// <summary>When a column is clicked on the listview, change the sorting order for that particular column (A->Z, Z->A).</summary>
        private void LvwMarkerInfoColumnClick(object sender, ColumnClickEventArgs e)
        {
            // Determine if the clicked column is already the column that is being sorted.
            if ((e.Column == _lvwMarkerInfoColumnSorter.SortColumn))
            {
                // Reverse the current sort direction for this column.
                if ((_lvwMarkerInfoColumnSorter.Order == SortOrder.Ascending))
                {
                    _lvwMarkerInfoColumnSorter.Order = SortOrder.Descending;
                }
                else
                {
                    _lvwMarkerInfoColumnSorter.Order = SortOrder.Ascending;
                }
            }
            else
            {
                // Set the column number that is to be sorted; default to ascending.
                _lvwMarkerInfoColumnSorter.SortColumn = e.Column;
                _lvwMarkerInfoColumnSorter.Order = SortOrder.Ascending;
            }
            // Perform the sort with these new sort options.
            lvwMarkerInfo.Sort();
        }

        /// <summary>Change the flagging info for a listview item that has been double-clicked.</summary>
        private void LvwMarkerInfoDoubleClick(object sender, EventArgs e)
        {
            if (lvwMarkerInfo.SelectedItems.Count == 0)
                return;
            ChangeMarkerCallStatus();
        }

        /// <summary>Change the flagging status of the gene if Space or Enter are pressed.</summary>
        private void LvwMarkerInfoKeyPress(object sender, KeyPressEventArgs e)
        {
            if (lvwMarkerInfo.SelectedItems.Count == 0)
                return;
            if (e.KeyChar == (char)(Keys.Enter) | e.KeyChar == (char)(Keys.Space))
            {
                ChangeMarkerCallStatus();
            }
        }

        private void LvwMarkerInfoMouseClick(Object sender, MouseEventArgs e)
        {
            SpawnSpotPreview(e);
        }

        /// <summary>User selects something within the listview showing all of the information related to a array tube sample.</summary>
        private void LvwMarkerInfoSelectedIndexChanged(Object sender, EventArgs e)
        {
            if (lvwMarkerInfo.SelectedItems.Count == 0)
                return;

            //need some way of making sure that after flag change for the lbl clicked does not revert back to the last lvw clicked item
            SpotMarkerArrayIndices indices = GetSelectedMarker();

            tslStatus.Text = "Looking at spot " + _sd.Spots[indices.Marker][indices.Spot] + " - " + _sd.Markers[indices.Marker];

            HighlightReplicatesOnGrid(_sd.Spots[indices.Marker]);
            DrawSpotPictures(indices);
        }

        private void PbxReplicatesMouseClick(Object sender, MouseEventArgs e)
        {
            SpawnSpotPreview(e);
        }

        private void SampleDiagnosticsToolStripMenuItemClick(Object sender, EventArgs e)
        {
            _sd.ClusterUPGMA();
            if (_sd.Forms.Diagnosis == null)
            {
                _sd.Forms.Diagnosis = new FrmDiagnosis(_sd);
                _sd.Forms.Diagnosis.Show();
            }
            _sd.Forms.Diagnosis.Focus();
        }

        private void SampleReviewToolStripMenuItemClick(Object sender, EventArgs e)
        {
            _sd.Forms.SampleQC.Focus();
        }

        /// <summary>Save any changes to the underlying data.</summary>
        private void SaveToolStripMenuItemClick(Object sender, EventArgs e)
        {
            _sd.WriteToMetaDataFile();
        }

        private void SetupForm()
        {
            _leftPoints = new double[_sd.Layout.Rows, _sd.Layout.Columns];
            _topPoints = new double[_sd.Layout.Rows, _sd.Layout.Columns];
            UpdateSampleListView();
            GetCalibrationInfo(0);
            CreateGrid();
            lvwSamples.Items[0].Selected = true;
        }

        /// <summary>Show AT image for selected sample.</summary>
        private void ShowArrayImage(int index)
        {
            pbx.Image = _sd.GetImage(index);
        }

        /// <summary>Highlight the replicate spots on the array tube picture.</summary>
        /// <param name="spots">Array of spot numbers.</param>
        private void HighlightReplicatesOnGrid(IEnumerable<int> spots)
        {
            var fontRegular = new Font(FontFamily.GenericSansSerif, 6, FontStyle.Regular);
            var fontBold = new Font(FontFamily.GenericSansSerif, 6, FontStyle.Bold);
            foreach (LabelWithColours lbl in _lbl)
            {
                if (lbl.BorderStyle == ButtonBorderStyle.Outset)
                {
                    lbl.BorderWidth = 1;
                    lbl.BorderStyle = ButtonBorderStyle.Solid;
                    lbl.Font = fontRegular;
                    lbl.Refresh();
                }
                if (_gClblCreated == false)
                {
                    lbl.Visible = false;
                }
            }
            double borderWidth = 2d * (pbx.Width / 400d);
            foreach (int spt in spots)
            {
                LabelWithColours lbl = _lbl[spt - 1];
                if (_gClblCreated)
                {
                    lbl.BorderWidth = (int)borderWidth;
                    lbl.BorderStyle = ButtonBorderStyle.Outset;
                    lbl.Font = fontBold;
                    lbl.Refresh();
                }
                lbl.Visible = true;
            }
        }

        private void ShowMarkerStripToolStripMenuItemClick(Object sender, EventArgs e)
        {
            int index = GetSelectedSample();
            var f = new FrmShowMarkerSpots(_sd, index, true);
            _sd.Forms.ShowMarkerSpots.Add(f);
            f.Show();
        }

        private void ShowSpotStripToolStripMenuItemClick(Object sender, EventArgs e)
        {
            int index = GetSelectedSample();
            var f = new FrmShowMarkerSpots(_sd, index, true);
            _sd.Forms.ShowMarkerSpots.Add(f);
            f.Show();
        }

        private void SpotSignalAnalysisToolStripMenuItemClick(Object sender, EventArgs e)
        {
            if (_sd.Forms.MarkerThresholdReview == null)
            {
                double minSignal = Math.Floor(_sd.MinSignal * 10d) / 10d;
                double maxSignal = Math.Ceiling(_sd.MaxSignal * 10d) / 10d;

                _sd.Forms.MarkerThresholdReview = new FrmMarkerThresholdReview(_sd, minSignal, maxSignal);
                _sd.Forms.MarkerThresholdReview.Show();
            }
            _sd.Forms.MarkerThresholdReview.Focus();
        }

        private void SpawnSpotPreview(MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Right) return;
            SpotMarkerArrayIndices indices = GetSelectedMarker();
            if (indices == null) return;
            Point pointFrmSpot = Cursor.Position;
            pointFrmSpot.X += 1;
            pointFrmSpot.Y += 1;
            //in the spot image collection only get the spot images for the spots that are highlighted

            var f = new FrmSpotPreview(_sd, pointFrmSpot, indices, new SpotImageCollection(_sd, _sd.SampleStats, _sd.Images, _sd.Calibration, indices.Marker, _sd.Spots[indices.Marker].ToArray(), _sd.Spots));
            _sd.Forms.SpotPreview.Add(f);
            f.Show();
        }

        /// <summary>Determine if the spot exists and return the list of spot numbers corresponding to the marker. 
        /// Return list of spots if spot exists; nothing if it doesn't exist.</summary>
        /// <param name="spot">Spot of interest.</param>
        private List<int> SpotExistsGetSpots(int spot)
        {
            foreach (List<int> spotList in _sd.Spots)
            {
                if (spotList.Contains(spot))
                {
                    return spotList;
                }
            }
            return null;
        }

        //known bug when spot label is clicked and flagging status is changed with keypress after clicking and selecting listview item, the spot label selection reverts back to
        //the same item that was selected via listview item clicking; cannot cycle through flagging status for clicked on spot label

        private void TbrGeneCallContrastScroll(Object sender, EventArgs e)
        {
            if (pbx.Image == null)
                return;
            _contrastFactor = tbrContrast.Value;
            pbx.Image = Misc.AdjustContrast((Bitmap)_sd.Images[GetSelectedSample()], _contrastFactor);
        }

        public void UpdateForm(int index)
        {
            chkGrid.Checked = true;
            GetCalibrationInfo(index);
            AdjustGrid();
            UpdateMarkerInfoListview(index);
            ShowArrayImage(index);

            lblSampleName.Text = _sd.SampleNames[index];
            if (chkContrast.Checked)
            {
                pbx.Image = Misc.AdjustContrast((Bitmap)_sd.GetImage(index), _contrastFactor);
            }
        }

        /// <summary>Populate the listview with information about the selected array tube sample.</summary>
        private void UpdateMarkerInfoListview(int l)
        {
            lvwMarkerInfo.Items.Clear();
            var lvList = new List<ListViewItem>();
            List<MarkerInfo> sample = _sd.SampleStats[l];
            for (int i = 0; i < sample.Count; i++)
            {
                MarkerInfo marker = sample[i];

                for (byte j = 0; j < marker.Signal.Count; j++)
                {
                    string sPresence = "";
                    Color backColor = Color.Red;

                    switch (_sd.SampleCalls[l].Calls[i])
                    {
                        case Calls.AbsentAuto:
                            backColor = _sd.Settings.C.AbsentAutoListviewColour;
                            sPresence = "Absent(auto)";
                            break;
                        case Calls.PresentAuto:
                            backColor = _sd.Settings.C.PresentAutoListviewColour;
                            sPresence = "Present(auto)";
                            break;
                        case Calls.Absent:
                            backColor = _sd.Settings.C.AbsentListviewColour;
                            sPresence = "Absent";
                            break;
                        case Calls.Present:
                            backColor = _sd.Settings.C.PresentListviewColour;
                            sPresence = "Present";
                            break;
                        case Calls.AmbiguousAbsent:
                            backColor = _sd.Settings.C.AbsentFlaggedListviewColour;
                            sPresence = "Ambiguous";
                            break;
                        case Calls.AmbiguousPresent:
                            backColor = _sd.Settings.C.PresentFlaggedListviewColour;
                            sPresence = "Ambiguous";
                            break;
                        case Calls.AmbiguousManual:
                            backColor = _sd.Settings.C.AbsentFlaggedListviewColour;
                            sPresence = "Ambiguous";
                            break;
                    }
                    var lvItem = new ListViewItem(new[]
                                                      {
                                                          _sd.Spots[i][j].ToString(CultureInfo.InvariantCulture).PadLeft(3, '0'),
                                                          _sd.Markers[i],
                                                          (j + 1).ToString(CultureInfo.InvariantCulture),
                                                          marker.Signal[j].ToString("0.0000"),
                                                          marker.PercentError[j].ToString("0.0000"),
                                                          marker.DeltaSignal[j].ToString("0.0000"),
                                                          marker.FinalAverageSignal.ToString("0.0000"),
                                                          _sd.Thresholds.Markers[i].Threshold.ToString("0.0000"),
                                                          _sd.Thresholds.Markers[i].ThresholdType.ToString(),
                                                          sPresence,
                                                          marker.Flag[j].ToString()
                                                      }) { Tag = new SpotMarkerArrayIndices(i, j, l) };
                    if (marker.Flag[j] == Flagging.Discard)
                    {
                        backColor = _sd.Settings.C.DiscardListviewColour;
                    }
                    lvItem.BackColor = backColor;
                    lvList.Add(lvItem);
                }
            }
            lvwMarkerInfo.Items.AddRange(lvList.ToArray());
        }

        /// <summary>Populate the array tube listview with the names and status (finalized or not) of each array tube sample in the dataset. Previously finalized array tube samples only appear in the listview.</summary>
        private void UpdateSampleListView()
        {
            var lvList = new List<ListViewItem>();
            for (int i = 0; i <= _sd.SampleCount - 1; i++)
            {
                if (!_sd.SampleQC[i]) continue;
                int count = 0;
                foreach (Calls markerCall in _sd.SampleCalls[i].Calls)
                {
                    if (markerCall == Calls.AmbiguousAbsent | markerCall == Calls.AmbiguousPresent | markerCall == Calls.AmbiguousManual)
                    {
                        count++;
                    }
                }
                bool bFinalized = _sd.SampleCalls[i].Finalized;
                var lvItem = new ListViewItem(new[] { _sd.SampleNames[i], bFinalized.ToString(CultureInfo.InvariantCulture), count.ToString(CultureInfo.InvariantCulture) }) { Checked = bFinalized, Tag = i };

                lvList.Add(lvItem);
            }
            lvwSamples.Items.Clear();
            lvwSamples.Items.AddRange(lvList.ToArray());
        }

    }

}
