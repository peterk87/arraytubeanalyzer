﻿using System;
using System.Collections.Generic;

namespace ArrayTubeAnalyzer
{
    [Serializable]
    public class MarkerThresholdCollection
    {


        private readonly SampleData _sd;
        private double _globalBackground = -1;
        public double GlobalBackground
        {
            get
            {
                const double epsilon = 0.000001;
                if (Math.Abs(_globalBackground - -1) < epsilon)
                {
                    return GetGlobalBackground();
                }
                return _globalBackground;
            }
        }

        public double GetGlobalBackground()
        {
            var upperBound = (int)(_sd.Settings.GlobalBackgroundCutoff * _sd.AllSignals.Count);
            var lSignals = new List<double>();
            int start = 0;
            while (lSignals.Count < upperBound)
            {
                double tmp = _sd.AllSignals[start];
                if (tmp > 0)
                {
                    lSignals.Add(tmp);
                }
                start++;
            }
            _globalBackground = Misc.Average(lSignals);
            return _globalBackground;
        }
        private readonly List<MarkerThresholdInfo> _markerThresholds = new List<MarkerThresholdInfo>();
        public List<MarkerThresholdInfo> Markers { get { return _markerThresholds; } }

        /// <summary>Default constructor. Determine global background thresholds for all markers.</summary>
        /// <param name="sd">SampleData master class</param>
        public MarkerThresholdCollection(SampleData sd)
        {
            _sd = sd;
            GetBin();
            for (int i = 0; i < _sd.MarkerCount; i++)
            {
                var tmp = new MarkerThresholdInfo(_sd, this, i, ThresholdType.GlobalBackground);
                _markerThresholds.Add(tmp);
            }
        }

        public MarkerThresholdCollection(SampleData sd, List<ThresholdType> lThresholdTypes, List<double> lThresholdValues)
        {
            _sd = sd;
            GetBin();
            for (int i = 0; i < _sd.MarkerCount; i++)
            {
                var tmp = new MarkerThresholdInfo(_sd, this, i, lThresholdTypes[i], lThresholdValues[i]);
                _markerThresholds.Add(tmp);
            }
        }


        public void SetThresholdsToGlobalBackground()
        {
            foreach (MarkerThresholdInfo marker in Markers)
            {
                if (marker.ThresholdType != ThresholdType.UserDefined)
                {
                    marker.ThresholdType = ThresholdType.GlobalBackground;
                    marker.GetThreshold();
                }
            }
        }

        /// <summary>Calculates the new average positive signal for each gene in the dataset from the top % of raw signals for each gene.</summary>
        public void SetThresholdsToPercentPositiveSignal()
        {
            foreach (MarkerThresholdInfo marker in Markers)
            {
                if (marker.ThresholdType != ThresholdType.PercentOfPositiveSignal)
                {
                    marker.ThresholdType = ThresholdType.PercentOfPositiveSignal;
                    marker.CalcAveragePositiveSignal();
                    marker.GetThreshold();
                }
            }
        }

        public void CalcWindowedStats()
        {
            foreach (MarkerThresholdInfo marker in Markers)
            {
                marker.CalcWindowedStats();
            }
        }

        public void CalcFrequencies()
        {
            foreach (MarkerThresholdInfo marker in Markers)
            {
                marker.CalcFrequencies();
            }
        }

        public void CalcAveragePositiveSignals()
        {
            foreach (MarkerThresholdInfo marker in Markers)
            {
                marker.CalcAveragePositiveSignal();
            }
        }

        private List<double> _bin;
        /// <summary>Bin values array starting from 0 to the maximum bin size in user-defined bin size increments.</summary>
        public List<double> Bin
        {
            get
            {
                if (_bin == null)
                {
                    return GetBin();
                }
                return _bin;
            }
        }


        /// <summary>Retrieves the bin sizes for each gene starting from 0 to the maximum signal value in the dataset.</summary>
        public List<double> GetBin()
        {
            _bin = new List<double>();
            double binSize = _sd.Settings.BinSize;
            var binNum = (int)(_sd.MaxSignal / binSize + 1);
            for (int i = 0; i <= binNum; i++)
            {
                _bin.Add(Math.Round(binSize * i, 2));
            }
            return _bin;
        }

    }

}
