﻿using System;
using System.Collections.Generic;

namespace ArrayTubeAnalyzer
{
    [Serializable]
    public class DiagnosisProfileCollection
    {

        private readonly List<DiagnosisProfile> _ldp;
        public List<DiagnosisProfile> DiagnosisProfileList
        {
            get { return _ldp; }
        }

        public DiagnosisProfileCollection()
        {
            _ldp = new List<DiagnosisProfile>();

        }

        public void Add(DiagnosisProfile dp)
        {
            _ldp.Add(dp);
        }

        public void Remove(int index)
        {
            _ldp.RemoveAt(index);
        }

        public void Change(int index, DiagnosisProfile dp)
        {
            _ldp[index] = dp;
        }

        public void Write(int index, System.IO.StreamWriter sw)
        {
            sw.WriteLine(_ldp[index].ToFileString());
        }
    }

}
