﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ArrayTubeAnalyzer
{
    public partial class frmProfileCreation : Form
    {
        private DiagnosisProfile dp;

        private ListViewColumnSorter lvcs;

        private SampleData _sd;

        public frmProfileCreation(object objSampleData, DiagnosisProfile dpNew)
        {
            // This call is required by the designer.
            InitializeComponent();

            // Add any initialization after the InitializeComponent() call.
            _sd = objSampleData as SampleData;
            dp = dpNew;
            updateListview();
            lvcs = new ListViewColumnSorter();
            lvwMarkers.ListViewItemSorter = lvcs;
            btnOK.DialogResult = DialogResult.OK;
            btnCancel.DialogResult = DialogResult.Cancel;
            if (!string.IsNullOrEmpty(dp.Name))
            {
                txtName.Text = dp.Name;
            }
        }

        private void updateListview()
        {
            List<string> markers = _sd.Markers;
            List<ListViewItem> lvList = new List<ListViewItem>();
            for (int i = 0; i <= markers.Count - 1; i++)
            {
                ListViewItem lvi = new ListViewItem(new string[] {
				markers[i],
				dp.DiagnosisList[i].ToString()
			});
                Color bc = Color.Gray;
                switch (dp.DiagnosisList[i])
                {
                    case Diagnosis.Ignore:
                        bc = Color.LightGray;
                        break;
                    case Diagnosis.Absent:
                        bc = Color.LightSalmon;
                        break;
                    case Diagnosis.Present:
                        bc = Color.LightGreen;
                        break;
                    case Diagnosis.MaybeAbsent:
                        bc = Color.LightGoldenrodYellow;
                        break;
                    case Diagnosis.MaybePresent:
                        bc = Color.LightBlue;
                        break;
                }
                lvi.BackColor = bc;
                lvi.Tag = i;
                lvList.Add(lvi);
            }
            lvwMarkers.Items.Clear();
            lvwMarkers.Items.AddRange(lvList.ToArray());
        }

        private void cbxDiagStatus_SelectedIndexChanged(System.Object sender, System.EventArgs e)
        {
            /*Ignore
            Present
            Absent
            MaybePresent
            MaybeAbsent*/
            Diagnosis status = (Diagnosis)cbxDiagStatus.SelectedIndex;
            for (int i = 0; i <= lvwMarkers.SelectedItems.Count - 1; i++)
            {
                lvwMarkers.SelectedItems[i].SubItems[1].Text = status.ToString();
                Color bc = Color.Gray;
                Diagnosis d = Diagnosis.Ignore;
                switch (status)
                {
                    case Diagnosis.Ignore:
                        bc = Color.LightGray;
                        d = Diagnosis.Ignore;
                        break;
                    case Diagnosis.Absent:
                        bc = Color.LightSalmon;
                        d = Diagnosis.Absent;
                        break;
                    case Diagnosis.Present:
                        bc = Color.LightGreen;
                        d = Diagnosis.Present;
                        break;
                    case Diagnosis.MaybeAbsent:
                        bc = Color.LightGoldenrodYellow;
                        d = Diagnosis.MaybeAbsent;
                        break;
                    case Diagnosis.MaybePresent:
                        bc = Color.LightBlue;
                        d = Diagnosis.MaybePresent;
                        break;
                }
                lvwMarkers.SelectedItems[i].BackColor = bc;
                lvwMarkers.SelectedItems[i].SubItems[1].Text = status.ToString();
                int index = (int)lvwMarkers.SelectedItems[i].Tag;
                dp.DiagnosisList[index] = d;
            }
        }

        private void lvwMarkers_ColumnClick(System.Object sender, System.Windows.Forms.ColumnClickEventArgs e)
        {
            // Determine if the clicked column is already the column that is being sorted.
            if ((e.Column == lvcs.SortColumn))
            {
                // Reverse the current sort direction for this column.
                if ((lvcs.Order == SortOrder.Ascending))
                {
                    lvcs.Order = SortOrder.Descending;
                }
                else
                {
                    lvcs.Order = SortOrder.Ascending;
                }
            }
            else
            {
                // Set the column number that is to be sorted; default to ascending.
                lvcs.SortColumn = e.Column;
                lvcs.Order = SortOrder.Ascending;
            }
            // Perform the sort with these new sort options.
            lvwMarkers.Sort();
        }

        private void txtName_TextChanged(System.Object sender, System.EventArgs e)
        {
            if (dp == null)
                return;
            dp.Name = txtName.Text;
        }
    }
}

