﻿namespace ArrayTubeAnalyzer
{
    partial class FrmChangeArrayNames
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ReplaceAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ColumnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ColumnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ChangeArrayNamesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnCancel = new System.Windows.Forms.Button();
            this.ContextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.SplitContainer1 = new System.Windows.Forms.SplitContainer();
            this.lvwArrayNames = new ArrayTubeAnalyzer.ListViewFF();
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnOK = new System.Windows.Forms.Button();
            this.ContextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SplitContainer1)).BeginInit();
            this.SplitContainer1.Panel1.SuspendLayout();
            this.SplitContainer1.Panel2.SuspendLayout();
            this.SplitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ReplaceAllToolStripMenuItem
            // 
            this.ReplaceAllToolStripMenuItem.Name = "ReplaceAllToolStripMenuItem";
            this.ReplaceAllToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.ReplaceAllToolStripMenuItem.Text = "Replace All";
            this.ReplaceAllToolStripMenuItem.Click += new System.EventHandler(this.ReplaceAllToolStripMenuItemClick);
            // 
            // ColumnHeader1
            // 
            this.ColumnHeader1.Text = "New Array Name";
            this.ColumnHeader1.Width = 152;
            // 
            // ColumnHeader2
            // 
            this.ColumnHeader2.Text = "Original Array Name";
            this.ColumnHeader2.Width = 150;
            // 
            // ChangeArrayNamesToolStripMenuItem
            // 
            this.ChangeArrayNamesToolStripMenuItem.Name = "ChangeArrayNamesToolStripMenuItem";
            this.ChangeArrayNamesToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.ChangeArrayNamesToolStripMenuItem.Text = "Change Array Name";
            this.ChangeArrayNamesToolStripMenuItem.Click += new System.EventHandler(this.ChangeArrayNamesToolStripMenuItemClick);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(165, 8);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(159, 23);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.BtnCancelClick);
            // 
            // ContextMenuStrip1
            // 
            this.ContextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ChangeArrayNamesToolStripMenuItem,
            this.ReplaceAllToolStripMenuItem});
            this.ContextMenuStrip1.Name = "ContextMenuStrip1";
            this.ContextMenuStrip1.Size = new System.Drawing.Size(182, 48);
            this.ContextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.ContextMenuStrip1Opening);
            // 
            // SplitContainer1
            // 
            this.SplitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SplitContainer1.Location = new System.Drawing.Point(0, 0);
            this.SplitContainer1.Name = "SplitContainer1";
            this.SplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // SplitContainer1.Panel1
            // 
            this.SplitContainer1.Panel1.Controls.Add(this.lvwArrayNames);
            // 
            // SplitContainer1.Panel2
            // 
            this.SplitContainer1.Panel2.Controls.Add(this.btnCancel);
            this.SplitContainer1.Panel2.Controls.Add(this.btnOK);
            this.SplitContainer1.Size = new System.Drawing.Size(337, 559);
            this.SplitContainer1.SplitterDistance = 506;
            this.SplitContainer1.TabIndex = 1;
            // 
            // lvwArrayNames
            // 
            this.lvwArrayNames.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader3,
            this.columnHeader4});
            this.lvwArrayNames.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvwArrayNames.FullRowSelect = true;
            this.lvwArrayNames.GridLines = true;
            this.lvwArrayNames.Location = new System.Drawing.Point(0, 0);
            this.lvwArrayNames.Name = "lvwArrayNames";
            this.lvwArrayNames.Size = new System.Drawing.Size(333, 502);
            this.lvwArrayNames.TabIndex = 0;
            this.lvwArrayNames.UseCompatibleStateImageBehavior = false;
            this.lvwArrayNames.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "New Sample Name";
            this.columnHeader3.Width = 149;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Original Sample Name";
            this.columnHeader4.Width = 155;
            // 
            // btnOK
            // 
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Location = new System.Drawing.Point(10, 8);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(149, 23);
            this.btnOK.TabIndex = 0;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.BtnOkClick);
            // 
            // frmChangeArrayNames
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(337, 559);
            this.Controls.Add(this.SplitContainer1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "FrmChangeArrayNames";
            this.Text = "Change Sample Names";
            this.ContextMenuStrip1.ResumeLayout(false);
            this.SplitContainer1.Panel1.ResumeLayout(false);
            this.SplitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SplitContainer1)).EndInit();
            this.SplitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.ToolStripMenuItem ReplaceAllToolStripMenuItem;
        internal System.Windows.Forms.ColumnHeader ColumnHeader1;
        internal System.Windows.Forms.ColumnHeader ColumnHeader2;
        internal System.Windows.Forms.ToolStripMenuItem ChangeArrayNamesToolStripMenuItem;
        internal System.Windows.Forms.Button btnCancel;
        internal System.Windows.Forms.ContextMenuStrip ContextMenuStrip1;
        internal System.Windows.Forms.SplitContainer SplitContainer1;
        internal System.Windows.Forms.Button btnOK;
        private ListViewFF lvwArrayNames;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
    }
}