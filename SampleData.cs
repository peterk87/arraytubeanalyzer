﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

namespace ArrayTubeAnalyzer
{
    [Serializable]
    public class SampleData
    {
        private readonly List<CalibrationInfo> _calibrationInfo = new List<CalibrationInfo>();

        /// <summary>Which markers to discard from the analysis for downstream analysis.</summary>
        private readonly List<bool> _discardMarkers = new List<bool>();

        /// <summary>Comments for markers.</summary>
        private readonly List<string> _markerComments = new List<string>();

        private readonly List<SampleCalls> _sampleCalls = new List<SampleCalls>();
        private readonly List<bool> _sampleQC = new List<bool>();
        private readonly List<List<MarkerInfo>> _sampleStats = new List<List<MarkerInfo>>();
        private readonly INISettings _settings;

        public TreatAmbiguousAs Ambiguous = TreatAmbiguousAs.ThresholdDetermined;
        public Cluster Cluster;
        private List<double> _allSignals = new List<double>();

        private SampleDiagnosticCollection _diagnostics;

        [NonSerialized] private FormCollection _forms;
        [NonSerialized] private List<Image> _img = new List<Image>();
        private List<List<int>> _lSpots = new List<List<int>>();
        private ArrayLayout _layout;
        private List<string> _markers = new List<string>();
        private List<string> _sampleNames = new List<string>();
        private MarkerThresholdCollection _thresholds;

        /// <summary>Start a new analysis from text files and image files.</summary>
        /// <param name="files"></param>
        /// <param name="ini"> </param>
        /// <param name="forms"> </param>
        public SampleData(string[] files, INISettings ini, FormCollection forms)
        {
            _settings = ini;
            _forms = forms;
            _layout = new ArrayLayout(_settings.Columns, _settings.Rows);

            AddSamplesToDataset(files);
            SetDefaultMarkerComments();
            SetDefaultMarkerDiscardStatus();
        }

        /// <summary>Open a new analysis from a ATA analysis file (*.meta).</summary>
        /// <param name="filename">ATA analysis filename (*.meta)</param>
        /// <param name="ini"> </param>
        /// <param name="forms"> </param>
        public SampleData(string filename, INISettings ini, FormCollection forms)
        {
            _settings = ini;
            _forms = forms;
            if (!ReadMetaDataFile(filename))
            {
                //send event to dispose of current instance of sample data 
                //and alert the user that the meta file is no good
            }
            _layout = new ArrayLayout(_settings.Columns, _settings.Rows);
        }

        public SampleDiagnosticCollection SampleDiagnostics
        {
            get { return _diagnostics; }
        }

        public FormCollection Forms
        {
            get { return _forms; }
        }

        public string FilePath { get; set; }

        public List<Image> Images
        {
            get { return _img; }
        }

        public List<List<MarkerInfo>> SampleStats
        {
            get { return _sampleStats; }
        }

        //may want to be more protective with the following struct

        public MarkerThresholdCollection Thresholds
        {
            get { return _thresholds; }
        }

        //may want to be more protective with the following struct

        public List<SampleCalls> SampleCalls
        {
            get { return _sampleCalls; }
        }

        public int SampleCount
        {
            get { return SampleStats.Count; }
        }

        public INISettings Settings
        {
            get { return _settings; }
        }

        public ArrayLayout Layout
        {
            get { return _layout; }
        }

        /// <summary>Sample names.</summary>
        public List<string> SampleNames
        {
            get { return _sampleNames; }
        }

        /// <summary>Marker names.</summary>
        public List<string> Markers
        {
            get { return _markers; }
        }

        /// <summary>Number of non-redundant markers in the dataset.</summary>
        public int MarkerCount
        {
            get { return _markers.Count; }
        }

        public List<bool> DiscardedMarkers
        {
            get { return _discardMarkers; }
        }

        public List<CalibrationInfo> Calibration
        {
            get { return _calibrationInfo; }
        }

        public List<List<int>> Spots
        {
            get { return _lSpots; }
        }

        public List<bool> SampleQC
        {
            get { return _sampleQC; }
        }

        public List<double> AllSignals
        {
            get
            {
                if (_allSignals.Count == 0)
                {
                    return GetAllSignals();
                }
                return _allSignals;
            }
        }

        public double MaxSignal
        {
            get
            {
                if (_allSignals.Count == 0)
                {
                    GetAllSignals();
                }
                return _allSignals[_allSignals.Count - 1];
            }
        }

        public double MinSignal
        {
            get { return _allSignals[0]; }
        }

        public void SetSampleDiagnostics(SampleDiagnosticCollection sampleDiagnostics)
        {
            _diagnostics = sampleDiagnostics;
        }

        public void SetFormCollection(FormCollection forms)
        {
            _forms = forms;
        }

        public void SetImages(List<Image> images)
        {
            _img = images;
        }

        public Image GetImage(int index)
        {
            return _img[index];
        }

        public void SetNewLayout(ArrayLayout ar)
        {
            _layout = ar;
        }

        /// <summary>Get the sample name for the sample with specified index.</summary>
        /// <param name="index">Sample index.</param>
        public string GetSampleName(int index)
        {
            return _sampleNames[index];
        }

        public void SetSampleName(int index, string sampleName)
        {
            _sampleNames[index] = sampleName;
        }

        public bool SetSampleNames(List<string> newSampleNames)
        {
            if (_sampleNames.Count == newSampleNames.Count)
            {
                _sampleNames = newSampleNames;
                return true;
            }
            return false;
        }

        /// <summary>Get the marker name for the marker with the specified index.</summary>
        /// <param name="index">Index of marker.</param>
        public string GetMarkerName(int index)
        {
            return _markers[index];
        }

        public void DiscardMarker(int index, bool discard)
        {
            _discardMarkers[index] = discard;
        }

        //New - save to meta file

        public string GetMarkerComment(int index)
        {
            return _markerComments[index];
        }

        public void SetMarkerComment(int index, string comment)
        {
            _markerComments[index] = comment;
        }

        public CalibrationInfo GetCalibrationInfo(int index)
        {
            return _calibrationInfo[index];
        }

        public void SetCalibrationInfo(int index, CalibrationInfo calibration)
        {
            _calibrationInfo[index] = calibration;
        }

        public List<int> GetMarkerSpots(int markerIndex)
        {
            return _lSpots[markerIndex];
        }

        public void SetSampleQC(int index, bool @checked)
        {
            _sampleQC[index] = @checked;
        }

        public List<double> GetAllSignals()
        {
            var ld = new List<double>();
            foreach (var sample in SampleStats)
            {
                foreach (MarkerInfo marker in sample)
                {
                    for (int i = 0; i < marker.Signal.Count; i++)
                    {
                        if (marker.Flag[i] != Flagging.Discard)
                        {
                            ld.Add(marker.Signal[i]);
                        }
                    }
                }
            }
            ld.Sort();
            _allSignals = ld;
            return _allSignals;
        }

        /// <summary>Set default ("") comments for each marker. These can be changed in the threshold setting portion of the program.</summary>
        private void SetDefaultMarkerComments()
        {
            for (int i = 0; i < Markers.Count; i++)
            {
                _markerComments.Add("");
            }
        }

        /// <summary>By default set all the marker discard statuses to "false" or keep all markers within the analysis.</summary>
        private void SetDefaultMarkerDiscardStatus()
        {
            for (int i = 0; i < MarkerCount; i++)
            {
                _discardMarkers.Add(false);
            }
        }

        public void RemoveSample(int index)
        {
            SampleStats.RemoveAt(index);
            SampleCalls.RemoveAt(index);
            _calibrationInfo.RemoveAt(index);
            _img.RemoveAt(index);
            _sampleQC.RemoveAt(index);
            _sampleNames.RemoveAt(index);
            CalcGlobalError();
            CalcFlagStatus();
        }

        public void RemoveSamples(List<int> indices)
        {
            for (int i = indices.Count - 1; i >= 0; i += -1)
            {
                RemoveSample(indices[i]);
            }
        }

        public bool AddSamplesToDataset(string[] files)
        {
            try
            {
                foreach (string filename in files)
                {
                    if (!ReadTextFile(filename))
                        throw new Exception(string.Format("Not able to read '{0}'", filename));
                    if (!ReadBitmapFile(filename))
                        throw new Exception(string.Format("Accompanying bitmap file for '{0}' does not exist.", filename));
                    _calibrationInfo.Add(new CalibrationInfo());
                    SampleCalls.Add(new SampleCalls(MarkerCount));
                    _sampleQC.Add(false);
                    //set default value for QC finalization

                    string tmp = filename.Substring(filename.LastIndexOf("\\", StringComparison.Ordinal) + 1);
                    tmp = tmp.Remove(tmp.LastIndexOf(".", StringComparison.Ordinal));
                    SampleNames.Add(tmp);

                    //trigger event to perform step in progress bar
                }
                CalcGlobalError();
                CalcFlagStatus();
                if (_thresholds == null)
                {
                    _thresholds = new MarkerThresholdCollection(this);
                }
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        private bool ReadTextFile(string filename)
        {
            try
            {
                using (var sr = new StreamReader(filename))
                {
                    string line = sr.ReadLine();
                    if (line != null)
                    {
                        string[] headers = line.Split('\t');
                        if (!headers.Contains("Spot") & !headers.Contains("Substance") & !headers.Contains("Signal"))
                            throw new Exception("File format not recognized for " + filename);

                        int indexSignal = Array.IndexOf(headers, "Signal");
                        var samples = new List<ArrayTube>();
                        while (!sr.EndOfStream)
                        {
                            line = sr.ReadLine();
                            if (line == null) continue;
                            string[] tmp = line.Trim().Split('\t');
                            if (tmp.Length < 3) continue;
                            var at = new ArrayTube
                                         {
                                             Spot = int.Parse(tmp[0].Trim()),
                                             ID = Encoding.ASCII.GetString(Misc.StrToByteArray(tmp[1].Trim())),
                                             Signal = double.Parse(tmp[indexSignal].Trim())
                                         };
                            //convert sample ID to byte array then to ASCII string to avoid funny characters that could give problems

                            samples.Add(at);
                        }
                        sr.Close();
                        CalcSampleStats(ref samples);
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        public MemoryStream WriteRawDataFile(int index)
        {
            var outStream = new MemoryStream();
            using (var sw = new StreamWriter(outStream))
            {
                sw.WriteLine("Spot\tSubstance\tSignal");
                List<MarkerInfo> markers = _sampleStats[index];
                for (int i = 0; i < Spots.Count; i++)
                {
                    string marker = _markers[i];
                    List<int> replicates = Spots[i];
                    MarkerInfo markerInfo = markers[i];
                    for (int j = 0; j < replicates.Count; j++)
                    {
                        sw.WriteLine(string.Format("{0}\t{1}\t{2}", replicates[j], marker, markerInfo.Signal[j]));
                    }
                }
            }
            return outStream;
        }

        /// <summary>Bring a Bitmap from a file into memory and convert to JPEG (smaller, less memory usage).</summary>
        /// <param name="filename">Bitmap filename.</param>
        private bool ReadBitmapFile(string filename)
        {
            string bmp = filename.Replace(".txt", ".bmp");
            if (!File.Exists(bmp))
                bmp = filename.Replace(".txt", ".jpg");
            var ms = new MemoryStream(File.ReadAllBytes(bmp));
            var ms2 = new MemoryStream();
            Image pic = Image.FromStream(ms);
            pic.Save(ms2, ImageFormat.Jpeg);
            pic.Dispose();
            _img.Add(Image.FromStream(ms2));
            return true;
        }

        private void GetMarkerNames(ref List<ArrayTube> arrayTubes)
        {
            //store non-redundant gene list in global variable 'gene'
            var tmp = new List<string>();
            //Retrieve non-redundant gene list from array tube info from files
            foreach (ArrayTube arrayTube in arrayTubes)
            {
                string markerName = arrayTube.ID;
                if (!tmp.Contains(markerName))
                {
                    tmp.Add(markerName);
                }
            }
            _markers = tmp;
        }

        /// <summary>Calculate all of the relevant statistics for the array tube sample.</summary>
        /// <param name="arrayTubes">Collection of raw array tube information.</param>
        private void CalcSampleStats(ref List<ArrayTube> arrayTubes)
        {
            //Only get the marker list if this is a new meta file with no other files previously added
            if (_sampleStats.Count == 0)
            {
                GetMarkerNames(ref arrayTubes);
            }

            //Sort out and calculate the rest of the information for the array tube
            var sample = new List<MarkerInfo>();
            _lSpots = new List<List<int>>();
            for (int i = 0; i < MarkerCount; i++)
            {
                sample.Add(CalcMarkerStats(i, arrayTubes));
            }
            SampleStats.Add(sample);
        }

        /// <summary>Calculate all of the statistics for a marker for a sample.</summary>
        /// <param name="index">Marker index.</param>
        /// <param name="arrayTubes">A collection of raw array tube information.</param>
        /// <returns>Calculated and organized array tube information.</returns>
        /// <remarks></remarks>
        private MarkerInfo CalcMarkerStats(int index, List<ArrayTube> arrayTubes)
        {
            var signal = new List<double>();

            //If this is the first file within the metadata file then
            string markerName = GetMarkerName(index);
            //rep holds the number of replicates for each marker
            var spots = new List<int>();

            for (int j = 0; j < arrayTubes.Count; j++)
            {
                if (markerName == arrayTubes[j].ID)
                {
                    spots.Add(arrayTubes[j].Spot);
                    signal.Add(arrayTubes[j].Signal);
                }
            }

            _lSpots.Add(spots);

            return new MarkerInfo(signal);
        }

        /// <summary>Check sample marker data for markers that should be flagged.</summary>
        public void CalcFlagStatus()
        {
            double thresholdLow = _globalError + _globalErrorSD*Settings.LowThresholdSD;
            double thresholdHigh = _globalError + _globalErrorSD*Settings.HighThresholdSD;


            for (int i = 0; i < SampleCount; i++)
            {
                List<MarkerInfo> sample = SampleStats[i];
                foreach (MarkerInfo marker in sample)
                {
                    for (int k = 0; k < marker.Signal.Count; k++)
                    {
                        if (marker.Flag[k] == Flagging.Flagged | marker.Flag[k] == Flagging.KeepAuto)
                        {
                            if (marker.Signal[k] >= Settings.HighSignalThreshold)
                            {
                                if (marker.DeltaSignal[k] > thresholdHigh)
                                {
                                    marker.Flag[k] = Flagging.Flagged;
                                }
                                else
                                {
                                    marker.Flag[k] = Flagging.KeepAuto;
                                }
                            }
                            else
                            {
                                if (marker.DeltaSignal[k] > thresholdLow)
                                {
                                    marker.Flag[k] = Flagging.Flagged;
                                }
                                else
                                {
                                    marker.Flag[k] = Flagging.KeepAuto;
                                }
                            }
                        }
                    }
                }
            }

            //for making sure that flags come in duplicates; if one replicate is flagged then all replicates of that marker are flagged
            for (int i = 0; i < SampleCount; i++)
            {
                List<MarkerInfo> sample = SampleStats[i];
                foreach (MarkerInfo marker in sample)
                {
                    for (int k = 0; k < marker.Signal.Count; k++)
                    {
                        if (marker.Flag[k] == Flagging.Flagged)
                        {
                            for (int l = 0; l < marker.Signal.Count; l++)
                            {
                                marker.Flag[l] = Flagging.Flagged;
                            }
                        }
                    }
                }
            }
        }

        /// <summary>Number of flagged spots for specified sample.</summary>
        /// <param name="index">Sample index.</param>
        public int NumberOfFlaggedSpots(int index)
        {
            int count = 0;
            List<MarkerInfo> sample = SampleStats[index];
            foreach (MarkerInfo marker in sample)
            {
                for (int j = 0; j < marker.Flag.Count; j++)
                {
                    if (marker.Flag[j] == Flagging.Flagged)
                    {
                        count += 1;
                    }
                }
            }
            return count;
        }

        public void RecalculateFinalAverageSignal(SpotMarkerArrayIndices indices)
        {
            MarkerInfo sample = SampleStats[indices.Array][indices.Marker];
            var list = new List<double>();
            for (int i = 0; i < sample.Signal.Count; i++)
            {
                if (sample.Flag[i] != Flagging.Discard)
                {
                    list.Add(sample.Signal[i]);
                }
            }
            sample.FinalAverageSignal = Misc.Average(list);
        }

        public void UpdateCalls()
        {
            // global error x global error factor
            for (int i = 0; i < Thresholds.Markers.Count; i++)
            {
                MarkerThresholdInfo marker = Thresholds.Markers[i];
                for (int j = 0; j < SampleCalls.Count; j++)
                {
                    // Only adjust the gene calls if the AT gene calls have not been finalized
                    if (SampleCalls[j].Finalized == false)
                    {
                        //Leave the user gene calls alone - only touch the automatically determined ones
                        Calls sample = SampleCalls[j].Calls[i];

                        if (sample == Calls.AbsentAuto | sample == Calls.PresentAuto | sample == Calls.AmbiguousAbsent |
                            sample == Calls.AmbiguousPresent)
                        {
                            double avg = SampleStats[j][i].FinalAverageSignal;

                            if (avg > marker.UpperThreshold)
                            {
                                SampleCalls[j].Calls[i] = Calls.PresentAuto;
                            }
                            else if (avg > marker.Threshold)
                            {
                                SampleCalls[j].Calls[i] = Calls.AmbiguousPresent;
                            }
                            else if (avg >= marker.LowerThreshold)
                            {
                                SampleCalls[j].Calls[i] = Calls.AmbiguousAbsent;
                            }
                            else
                            {
                                SampleCalls[j].Calls[i] = Calls.AbsentAuto;
                            }
                        }
                    }
                }
            }
        }

        public void ClusterUPGMA()
        {
            if (Cluster == null)
            {
                Cluster = new Cluster(this);
            }
            Cluster.UPGMACluster();
        }

        #region Reading/Writing Metadata file

        /// <summary>Returns the starting byte positions of the images stored in the metadata file.</summary>
        /// <param name="mdf">Entire metadata file byte array.</param>
        /// <returns>Starting byte positions for images within metadata file.</returns>
        private List<long> DelimiterPositions(ref byte[] mdf)
        {
            List<byte> delimiterPk = PicDelimiter();
            var start = new List<long>();
            //may have trouble here - long should be used because there are a lot of characters/bytes in the metafile
            int j = 0;
            for (int i = 0; i <= mdf.Length - 1; i++)
            {
                if (mdf[i] == delimiterPk[j])
                {
                    j += 1;
                    if (j == 100)
                    {
                        start.Add(i);
                        j = 0;
                    }
                }
                else
                {
                    j = 0;
                }
            }
            return start;
        }

        /// <summary>Create a byte array with the characters "PK" repeating for length=100 to separate/delimit the images stored in the metadata file.</summary>
        /// <returns>"PK" delimiter byte array.</returns>
        private List<byte> PicDelimiter()
        {
            var lb = new List<byte>();
            for (int i = 1; i <= 50; i++)
            {
                lb.Add((byte) 'P');
                lb.Add((byte) 'K');
            }
            return lb;
        }

        /// <summary>Reading of metadata file into memory. Extracts pictures and array tube data.</summary>
        private bool ReadMetaDataFile(string filename)
        {
            //             try
            //             {
            FilePath = filename;
            byte[] entireMdf = File.ReadAllBytes(filename);
            List<long> imageIndices = DelimiterPositions(ref entireMdf);
            GetImages(ref imageIndices, ref entireMdf);
            //delimiter size is 100
            var ms = new MemoryStream(entireMdf, 0, (int) (imageIndices[0] - 100 + 1));
            var mdfData = new byte[Convert.ToInt64(imageIndices[0] - 100 + 1) + 1];
            ms.Read(mdfData, 0, (int) (imageIndices[0] - 100 + 1));

            var enc = new ASCIIEncoding();
            string metaData = enc.GetString(mdfData);

            string[] sampleData = metaData.Split(';');
            int sampleDataCount = 0;
            string[] summary = sampleData[sampleDataCount++].Split('\n');
            int summaryCount = 0;
            //ini settings
            _settings.SetINIValues(summary[summaryCount++]);
            //sample names
            _sampleNames.AddRange(summary[summaryCount++].Split('\t'));
            //sample QC statuses
            foreach (string sampleQC in summary[summaryCount++].Split('\t'))
            {
                _sampleQC.Add(bool.Parse(sampleQC));
            }
            //marker names
            _markers.AddRange(summary[summaryCount++].Split('\t'));
            //spot numbers
            foreach (string spots in summary[summaryCount++].Split('\t'))
            {
                _lSpots.Add(GetSpots(spots.Split(',')));
            }
            //marker comments
            _markerComments.AddRange(summary[summaryCount++].Split('\t'));

            //marker keep/discard status
            foreach (string markerKeepDiscard in summary[summaryCount].Split('\t'))
            {
                _discardMarkers.Add(bool.Parse(markerKeepDiscard));
            }

            sampleDataCount = GetMarkerInfo(sampleData, sampleDataCount);


            sampleDataCount = GetCalibrationInfo(sampleData, sampleDataCount);


            sampleDataCount = GetMarkerThresholdInfo(sampleData, sampleDataCount);


            sampleDataCount = GetSampleCalls(sampleData, sampleDataCount);

            UpdateCalls();
            ClusterUPGMA();
            GetDiagnosticProfiles(sampleData, sampleDataCount);

            //            }
            //              catch (Exception ex)
            //              {
            //                  //use event to show error message
            //                  return false;
            //              }
            return true;
        }

        private void GetDiagnosticProfiles(string[] sampleData, int sampleDataCount)
        {
            //get diagnostic profile information
            string[] tmp = sampleData[sampleDataCount].Split('\n');
            int count = int.Parse(tmp[0]);
            if (count > 0)
            {
                var ldp = new List<DiagnosisProfile>();
                bool b = true;
                for (int i = 1; i < tmp.Length; i++)
                {
                    ldp.Add(new DiagnosisProfile(tmp[i], MarkerCount, ref b));
                }
                _diagnostics = new SampleDiagnosticCollection(this, ldp);
            }
        }

        private int GetMarkerInfo(string[] sampleData, int sampleDataCount)
        {
            //loops for extracting information from metadata file into ATFiles
            string[] tmpSignals = sampleData[sampleDataCount++].Split('\n');
            string[] tmpFlag = sampleData[sampleDataCount++].Split('\n');
            for (int i = 0; i < tmpFlag.Length; i++)
            {
                var tmpATinfo = new List<MarkerInfo>();
                string[] tmpSignalValues = tmpSignals[i].Split('\t');
                string[] tmpFlagValues = tmpFlag[i].Split('\t');
                for (int j = 0; j < tmpFlagValues.Length; j++)
                {
                    tmpATinfo.Add(new MarkerInfo(tmpSignalValues[j].Split(','), tmpFlagValues[j].Split(',')));
                }
                SampleStats.Add(tmpATinfo);
            }
            return sampleDataCount;
        }

        private int GetCalibrationInfo(string[] sampleData, int sampleDataCount)
        {
            string[] tmpCali = sampleData[sampleDataCount++].Split('\n');
            foreach (string sCali in tmpCali)
            {
                string[] tCali = sCali.Split('\t');
                _calibrationInfo.Add(new CalibrationInfo(double.Parse(tCali[0]),
                                                         double.Parse(tCali[1]),
                                                         double.Parse(tCali[2]),
                                                         double.Parse(tCali[3]),
                                                         double.Parse(tCali[4]),
                                                         double.Parse(tCali[5]),
                                                         double.Parse(tCali[6]),
                                                         double.Parse(tCali[7])));
            }
            return sampleDataCount;
        }

        private int GetMarkerThresholdInfo(string[] sampleData, int sampleDataCount)
        {
            string[] tmpGeneInfo = sampleData[sampleDataCount++].Split('\n');
            string[] tmpThresholdType = tmpGeneInfo[0].Split('\t');
            string[] tmpThreshold = tmpGeneInfo[1].Split('\t');
            //Dim tmpThresholdFlagging = tmpGeneInfo(2).Split(vbTab)
            var lThresholds = new List<double>();
            var lThresholdTypes = new List<ThresholdType>();
            for (int i = 0; i < tmpThreshold.Length; i++)
            {
                lThresholds.Add(double.Parse(tmpThreshold[i]));
                lThresholdTypes.Add((ThresholdType) int.Parse(tmpThresholdType[i]));
            }
            _thresholds = new MarkerThresholdCollection(this, lThresholdTypes, lThresholds);
            return sampleDataCount;
        }

        private int GetSampleCalls(string[] sampleData, int sampleDataCount)
        {
            string[] tmpCalls = sampleData[sampleDataCount++].Split('\n');
            string[] tmpCallFinalization = sampleData[sampleDataCount++].Split('\t');
            for (int i = 0; i < tmpCalls.Length; i++)
            {
                var gc = new SampleCalls(tmpCalls[i], tmpCallFinalization[i]);
                SampleCalls.Add(gc);
            }
            return sampleDataCount;
        }

        /// <summary>Write all of the gene array tube data to a metadata file.</summary>
        /// <remarks>Still need to add a way of keeping track of what the user has designated as genes to keep or discard for final analysis.</remarks>
        public void WriteToMetaDataFile()
        {
            var mdfText = new List<string> {Settings.GetINIValues(), "\n", string.Join("\t", _sampleNames), "\n"};
            //ini settings
            //sample names
            //sample QC statuses
            var tmpStr = new List<string>();
            foreach (bool b in _sampleQC)
            {
                tmpStr.Add(b.ToString(CultureInfo.InvariantCulture));
            }
            mdfText.Add(string.Join("\t", tmpStr));
            mdfText.Add("\n");
            //marker names
            mdfText.Add(string.Join("\t", _markers));
            mdfText.Add("\n");
            //print spot values into mdftext string for printing to mdf
            tmpStr.Clear();
            foreach (var lspots in _lSpots)
            {
                var tmpSpotStr = new List<string>();
                foreach (int spot in lspots)
                {
                    tmpSpotStr.Add(spot.ToString(CultureInfo.InvariantCulture));
                }
                tmpStr.Add(string.Join(",", tmpSpotStr));
            }
            mdfText.Add(string.Join("\t", tmpStr));
            mdfText.Add("\n");
            tmpStr.Clear();
            //Marker comments
            foreach (string markerComment in _markerComments)
            {
                tmpStr.Add(markerComment);
            }
            mdfText.Add(string.Join("\t", tmpStr));
            mdfText.Add("\n");
            tmpStr.Clear();
            //Marker keep/discard status
            foreach (bool markerDiscard in _discardMarkers)
            {
                tmpStr.Add(markerDiscard.ToString(CultureInfo.InvariantCulture));
            }
            mdfText.Add(string.Join("\t", tmpStr));
            mdfText.Add(";");
            //';" delimiter for different data in mdf
            //Signals
            tmpStr.Clear();
            foreach (var sample in SampleStats)
            {
                var tmpLineStr = new List<string>();
                foreach (MarkerInfo marker in sample)
                {
                    var tmpRepStr = new List<string>();
                    foreach (double signal in marker.Signal)
                    {
                        tmpRepStr.Add(signal.ToString(CultureInfo.InvariantCulture));
                    }
                    tmpLineStr.Add(string.Join(",", tmpRepStr));
                }
                tmpStr.Add(string.Join("\t", tmpLineStr));
            }
            mdfText.Add(string.Join("\n", tmpStr));
            mdfText.Add(";");
            //flagging info - sample QC
            tmpStr.Clear();
            foreach (var sample in SampleStats)
            {
                var tmpLineStr = new List<string>();
                foreach (MarkerInfo marker in sample)
                {
                    var tmpRepStr = new List<string>();
                    foreach (Flagging f in marker.Flag)
                    {
                        tmpRepStr.Add(((int) f).ToString(CultureInfo.InvariantCulture));
                    }
                    tmpLineStr.Add(string.Join(",", tmpRepStr));
                }
                tmpStr.Add(string.Join("\t", tmpLineStr));
            }
            mdfText.Add(string.Join("\n", tmpStr));
            mdfText.Add(";");
            //calibration information
            tmpStr.Clear();
            for (int i = 0; i < _calibrationInfo.Count; i++)
            {
                CalibrationInfo calibration = _calibrationInfo[i];
                tmpStr.Add(string.Join("\t", new[]
                                                 {
                                                     calibration.TopLeftY.ToString(CultureInfo.InvariantCulture),
                                                     calibration.TopLeftX.ToString(CultureInfo.InvariantCulture),
                                                     calibration.TopRightY.ToString(CultureInfo.InvariantCulture),
                                                     calibration.TopRightX.ToString(CultureInfo.InvariantCulture),
                                                     calibration.BottomRightY.ToString(CultureInfo.InvariantCulture),
                                                     calibration.BottomRightX.ToString(CultureInfo.InvariantCulture),
                                                     calibration.BottomLeftY.ToString(CultureInfo.InvariantCulture),
                                                     calibration.BottomLeftX.ToString(CultureInfo.InvariantCulture)
                                                 }));
            }
            mdfText.Add(string.Join("\n", tmpStr));
            mdfText.Add(";");
            //Marker threshold info portion of the metadata file
            //Marker threshold type:
            tmpStr.Clear();
            foreach (MarkerThresholdInfo marker in Thresholds.Markers)
            {
                tmpStr.Add(((int) (marker.ThresholdType)).ToString(CultureInfo.InvariantCulture));
            }
            mdfText.Add(string.Join("\t", tmpStr));
            mdfText.Add("\n");

            //marker threshold:
            tmpStr.Clear();
            foreach (MarkerThresholdInfo marker in Thresholds.Markers)
            {
                tmpStr.Add(marker.Threshold.ToString(CultureInfo.InvariantCulture));
            }
            mdfText.Add(string.Join("\t", tmpStr));
            mdfText.Add("\n");
            mdfText.Add(";");

            //Gene calling information for each gene within each array - finalized and not
            //Gene by gene calling for each array
            tmpStr.Clear();
            foreach (SampleCalls sample in SampleCalls)
            {
                var tmp = new List<string>();
                foreach (Calls markerCall in sample.Calls)
                {
                    tmp.Add(((int) markerCall).ToString(CultureInfo.InvariantCulture));
                }
                tmpStr.Add(string.Join("\t", tmp));
            }
            mdfText.Add(string.Join("\n", tmpStr));
            mdfText.Add(";");

            //Calling finalization information
            tmpStr.Clear();
            foreach (SampleCalls sample in SampleCalls)
            {
                tmpStr.Add(sample.Finalized.ToString(CultureInfo.InvariantCulture));
            }
            mdfText.Add(string.Join("\t", tmpStr));
            mdfText.Add(";");
            //Sample diagnostics
            if (_diagnostics != null)
            {
                tmpStr.Clear();
                //number of diagnostic profiles written to file
                tmpStr.Add(_diagnostics.Profiles.DiagnosisProfileList.Count.ToString(CultureInfo.InvariantCulture));
                //each diagnostic profile is written to the meta file; one line for each
                foreach (DiagnosisProfile d in _diagnostics.Profiles.DiagnosisProfileList)
                {
                    tmpStr.Add(d.ToFileString());
                }
                mdfText.Add(string.Join("\n", tmpStr));
            }
            else
            {
                //no diagnostic profiles present
                mdfText.Add("0");
            }


            //Convert all info in mdfText to bytes and write to file
            using (var fs = new FileStream(FilePath, FileMode.Create, FileAccess.Write))
            {
                byte[] mdfBytes = Misc.StrToByteArray(string.Join("", mdfText));
                fs.Write(mdfBytes, 0, mdfBytes.Length);
                fs.Close();
            }

            //Write pictures to metadata file
            byte[] delimiter = PicDelimiter().ToArray();
            using (var fs = new FileStream(FilePath, FileMode.Append, FileAccess.Write))
            {
                foreach (Image img in _img)
                {
                    fs.Write(delimiter, 0, delimiter.Length);
                    var ms2 = new MemoryStream();
                    Image pic = new Bitmap(img);
                    pic.Save(ms2, ImageFormat.Jpeg);
                    byte[] b = ms2.ToArray();
                    fs.Write(b, 0, b.Length);
                }
            }
        }

        /// <summary>Get the replicate spot numbers from a string array and add them to an integer list.</summary>
        /// <param name="spots">Spot number string array.</param>
        /// <returns>Integer list containing the spot numbers.</returns>
        private static List<int> GetSpots(IEnumerable<string> spots)
        {
            var tmp = new List<int>();
            foreach (string spot in spots)
            {
                tmp.Add(int.Parse(spot));
            }
            return tmp;
        }

        /// <summary>Get the images from the metadata file using the starting byte positions within the entire metadata file byte array.</summary>
        /// <param name="start">Starting byte positions for images within metadata file.</param>
        /// <param name="b">Entire metadata file byte array.</param>
        private void GetImages(ref List<long> start, ref byte[] b)
        {
            for (int j = 0; j < start.Count; j++)
            {
                long tmp = start[j];
                long startIndex = tmp + 1;
                long endIndex;
                if (j == start.Count - 1)
                {
                    endIndex = b.Length - tmp - 1;
                }
                else
                {
                    endIndex = start[j + 1] - tmp + 1;
                }
                var ms = new MemoryStream(b, (int) startIndex, (int) endIndex);
                Image pic = Image.FromStream(ms);
                _img.Add(pic);
                ms.Close();
            }
        }

        #endregion

        #region Global Error

        private const double EPSILON = 0.00001;
        private double _globalError = -1;
        private double _globalErrorSD = -1;

        public double GetGlobalError()
        {
            if (Math.Abs(_globalError - -1) > EPSILON)
            {
                return _globalError;
            }
            CalcGlobalError();
            return _globalError;
        }

        public double GetGlobalErrorSD()
        {
            if (Math.Abs(_globalErrorSD - -1) > EPSILON)
            {
                return _globalErrorSD;
            }
            CalcGlobalError();
            return _globalErrorSD;
        }

        public void CalcGlobalError()
        {
            var list = new List<double>();
            foreach (var sample in SampleStats)
            {
                foreach (MarkerInfo marker in sample)
                {
                    for (int k = 0; k < marker.DeltaSignal.Count; k++)
                    {
                        list.Add(marker.DeltaSignal[k]);
                    }
                }
            }
            _globalError = Misc.Average(list);
            _globalErrorSD = Misc.StandardDeviation(list, _globalError);
        }

        public double GetThresholdFlagRange()
        {
            return Settings.ThresholdFlaggingFactor*GetGlobalErrorSD();
        }

        #endregion

        #region Nested type: ArrayLayout

        [Serializable]
        public struct ArrayLayout
        {
            private readonly int _columns;

            private readonly int _rows;

            public ArrayLayout(int numColumns, int numRows)
            {
                _columns = numColumns;
                _rows = numRows;
            }

            public int Columns
            {
                get { return _columns; }
            }

            public int Rows
            {
                get { return _rows; }
            }

            public int Spots
            {
                get { return _rows*_columns; }
            }
        }

        #endregion
    }
}