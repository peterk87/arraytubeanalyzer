﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace ArrayTubeAnalyzer
{
    public partial class FrmMarkerThresholdAnalysis : Form
    {

        private readonly ListViewColumnSorter _lvwColumnSorter = new ListViewColumnSorter();

        private readonly SampleData _sd;

        private Series _sGB;
        private Series _sErrorBarGB;
        private Series _sMedian;
        private Series _sFreq;
        private Series _sSTDev;
        private Series _sGBThreshold;
        private Series _sErrorBarGBThreshold;
        private Series _sUserThreshold;
        private Series _sErrorBarUserThreshold;
        private Series _sAvgPositiveSignal;
        private Series _sEBAvgPositiveSignal;
        private Series _sPercentPositive;
        private Series _sEBPercentPositive;

        private bool _allowItemCheck;

        public FrmMarkerThresholdAnalysis(object objSampleData)
        {
            // This call is required by the designer.
            InitializeComponent();

            txtGlobalBackgroundCutoff.LostFocus += TxtLostFocus;
            txtGlobalBackgroundFactor.LostFocus += TxtLostFocus;
            txtBinSize.LostFocus += TxtLostFocus;
            txtWindowSize.LostFocus += TxtLostFocus;
            txtPercentPositiveFactor.LostFocus += TxtLostFocus;
            txtPercentPositiveCutoff.LostFocus += TxtLostFocus;
            txtThresholdFlaggingFactor.LostFocus += TxtLostFocus;

            _sd = objSampleData as SampleData;

            InitializeSettings();

            // Add any initialization after the InitializeComponent() call.
            lvwMarkers.ListViewItemSorter = _lvwColumnSorter;

            cbxThresholdType.Items.AddRange(new[] {
			"User-defined",
			"Global Background",
			"%Positive"
		});

            SetupForm();
            lvwMarkers.Items[0].Selected = true;
            lvwMarkers.Focus();
            tslStatus.Text = "Ready";
        }

        private void FrmMarkerThresholdAnalysisFormClosing(object sender, FormClosingEventArgs e)
        {
            _sd.Forms.MarkerThresholdAnalysis = null;
        }

        private void ArraySpotReviewToolStripMenuItemClick(Object sender, EventArgs e)
        {
            _sd.Forms.SampleQC.Focus();
        }

        /// <summary>When the (-) button is pressed beside the bin size textbox.</summary>
        private void BtnDecrementBinSizeClick(Object sender, EventArgs e)
        {
            double d = double.Parse(txtBinSize.Text);
            d -= 0.005;
            if (d <= 0)
                return;
            txtBinSize.Text = d.ToString(CultureInfo.InvariantCulture);
            int index = GetSelectedMarker();
            if (index == -1)
                return;
            _sd.Settings.BinSize = d;
            _sd.Thresholds.GetBin();
            _sd.Thresholds.CalcFrequencies();
            ShowFrequencies(index);
        }

        /// <summary>Decrement window size with the press of the (-) button beside the window size textbox.</summary>
        private void BtnDecrementWindowSizeClick(Object sender, EventArgs e)
        {
            int i = int.Parse(txtWindowSize.Text);
            i--;
            if (i == 1)
                return;
            txtWindowSize.Text = i.ToString(CultureInfo.InvariantCulture);
            _sd.Settings.WindowSize = i;
            foreach (MarkerThresholdInfo marker in _sd.Thresholds.Markers)
            {
                marker.CalcWindowedStats();
            }
            lvwMarkers.Items[GetSelectedMarker()].Selected = true;
            UpdateChart(GetSelectedMarker());
        }

        /// <summary>When the (+) button is pressed beside the bin size textbox.</summary>
        private void BtnIncrementBinSizeClick(Object sender, EventArgs e)
        {
            double d = double.Parse(txtBinSize.Text);
            d += 0.005;
            if (d <= 0)
                return;
            txtBinSize.Text = d.ToString(CultureInfo.InvariantCulture);
            int index = GetSelectedMarker();
            if (index == -1)
                return;
            _sd.Settings.BinSize = d;
            _sd.Thresholds.GetBin();
            _sd.Thresholds.CalcFrequencies();
            ShowFrequencies(index);
        }

        /// <summary>Increment window size with the press of the (+) button beside the window size textbox.</summary>
        private void BtnIncrementWindowSizeClick(Object sender, EventArgs e)
        {
            int i = int.Parse(txtWindowSize.Text);
            i++;
            txtWindowSize.Text = i.ToString(CultureInfo.InvariantCulture);
            _sd.Settings.WindowSize = i;
            foreach (MarkerThresholdInfo marker in _sd.Thresholds.Markers)
            {
                marker.CalcWindowedStats();
            }
            lvwMarkers.Items[GetSelectedMarker()].Selected = true;
            UpdateChart(GetSelectedMarker());
        }

        private void BtnRecalculateClick(Object sender, EventArgs e)
        {
            UpdateForm();
        }

        private void CbxThresholdTypeSelectedIndexChanged(Object sender, EventArgs e)
        {
            int index = GetSelectedMarker();
            if (index == -1)
                return;
            if (cbxThresholdType.SelectedIndex == (int)_sd.Thresholds.Markers[index].ThresholdType)
                return;

            HideUserDefinedThreshold();
            if (cbxThresholdType.SelectedIndex == (int)ThresholdType.UserDefined)
            {
                ShowUserDefinedThreshold(index);
            }
            else if (cbxThresholdType.SelectedIndex == (int)ThresholdType.GlobalBackground)
            {
                SetToGlobalBackgroundThreshold(index);
                //cbxThresholdType.SelectedIndex = GeneInfo.ThresholdTypeEnum.PercentOfPositiveSignal
            }
            else
            {
                SetToPercentPositiveThreshold(index);
            }

            contextMenuMarkers.Close();
        }

        private void ChangeMarkerCommentToolStripMenuItemClick(object sender, EventArgs e)
        {
            int index = GetSelectedMarker();
            string markerComment = _sd.Thresholds.Markers[index].MarkerComment;
            string marker = _sd.Thresholds.Markers[index].MarkerName;
            if (Misc.InputBox("Change marker comment", "Change comment for '" + marker + "'", ref markerComment) == DialogResult.OK)
            {
                _sd.Thresholds.Markers[index].MarkerComment = markerComment;
                int indexLvw = -1;
                if (lvwMarkers.SelectedIndices.Count == 0)
                {
                    for (int i = 0; i < lvwMarkers.Items.Count; i++)
                    {
                        var tmp = (int)lvwMarkers.Items[i].Tag;
                        if (tmp == index)
                        {
                            indexLvw = i;
                            break;
                        }
                    }
                }
                else
                {
                    indexLvw = lvwMarkers.SelectedIndices[0];
                }
                lvwMarkers.Items[indexLvw].SubItems[6].Text = markerComment;
                lvwMarkers.Items[indexLvw].ToolTipText = markerComment;
            }

        }

        private void ChkFrequenciesCheckedChanged(Object sender, EventArgs e)
        {
            if (lvwMarkers.Items.Count == 0)
                return;
            int index = GetSelectedMarker();
            if (index == -1)
                return;
            if (chkFrequencies.Checked)
            {
                _sFreq.Enabled = true;
                ShowFrequencies(index);
            }
            else
            {
                _sFreq.Enabled = true;
                _sFreq.Points.Clear();
            }
        }

        private void ChkWindowedSTDevCheckedChanged(Object sender, EventArgs e)
        {
            if (lvwMarkers.Items.Count == 0)
                return;
            int index = GetSelectedMarker();
            if (index == -1)
                return;
            if (chkWindowedSTDev.Checked)
            {
                _sSTDev.Enabled = true;
                ShowDeviations(index);
            }
            else
            {
                _sSTDev.Enabled = false;
                _sSTDev.Points.Clear();
            }
        }

        private void ChkWindowedMedianCheckedChanged(Object sender, EventArgs e)
        {
            if (lvwMarkers.Items.Count == 0)
                return;
            int index = GetSelectedMarker();
            if (index == -1)
                return;
            if (chkWindowedMedian.Checked)
            {
                _sMedian.Enabled = true;
                ShowMedians(index);
            }
            else
            {
                _sMedian.Enabled = false;
                _sMedian.Points.Clear();
            }
        }

        /// <summary>When the user clicks on the chart, the position of the user-defined threshold changes.</summary>
        private void ChtGeneCursorPositionChanging(object sender, CursorEventArgs e)
        {
            int index = GetSelectedMarker();
            if (index == -1)
                return;
            MarkerThresholdInfo marker = _sd.Thresholds.Markers[index];
            marker.SetUserDefinedThreshold(e.NewPosition);

            //so the right listview item is updated
            index = lvwMarkers.SelectedIndices[0];
            int flaggedCount = marker.FlaggedCount;
            ListViewItem lvi = lvwMarkers.Items[index];
            lvi.SubItems[1].Text = e.NewPosition.ToString("0.0000");
            lvi.SubItems[2].Text = marker.ThresholdType.ToString();
            lvi.SubItems[4].Text = flaggedCount.ToString(CultureInfo.InvariantCulture);
            lvi.BackColor = flaggedCount > 0 ? Color.Yellow : Color.PaleGreen;
            cht.ChartAreas[0].CursorX.LineColor = Color.Transparent;
            cht.ChartAreas[0].CursorX.SelectionColor = Color.FromArgb(50, 100, 100, 100);
            cht.ChartAreas[0].CursorX.SelectionStart = marker.LowerThreshold;
            cht.ChartAreas[0].CursorX.SelectionEnd = marker.UpperThreshold;
            _sUserThreshold.Enabled = true;
            _sUserThreshold.ChartType = SeriesChartType.Line;
            _sUserThreshold.Points[0].XValue = e.NewPosition;
            _sErrorBarUserThreshold.Enabled = true;
            _sErrorBarUserThreshold.Color = Color.Lime;

            ShowFrequencies(index);
            _sd.UpdateCalls();
            _sd.ClusterUPGMA();
            UpdateParametersListview();
            UpdateForms();
        }

        /// <summary>When the user presses the Close control.</summary>
        private void CloseGeneThresholdAnalysisToolStripMenuItemClick(Object sender, EventArgs e)
        {
            Close();
        }

        private void ClusteringToolStripMenuItemClick(Object sender, EventArgs e)
        {
            _sd.UpdateCalls();
            if (_sd.Forms.Clustering == null)
            {
                _sd.Forms.Clustering = new FrmClustering(_sd);
                _sd.Forms.Clustering.Show();
            }
            _sd.Forms.Clustering.Focus();
        }

        private void ContextMenuGenesOpening(Object sender, System.ComponentModel.CancelEventArgs e)
        {
            int index = GetSelectedMarker();
            if (index == -1)
                return;
            cbxThresholdType.SelectedIndex = (int)_sd.Thresholds.Markers[index].ThresholdType;
        }

        private void DiagnosticsToolStripMenuItemClick(Object sender, EventArgs e)
        {
            _sd.UpdateCalls();
            _sd.ClusterUPGMA();

            if (_sd.Forms.Diagnosis == null)
            {
                _sd.Forms.Diagnosis = new FrmDiagnosis(_sd);
                _sd.Forms.Diagnosis.Show();
            }
            _sd.Forms.Diagnosis.Focus();
        }

        /// <summary>When the user presses the Exit control.</summary>
        private void ExitToolStripMenuItemClick(Object sender, EventArgs e)
        {
            if (_sd.Forms.SampleQC.ExitApplication() == false)
            {
                Environment.Exit(0);
            }
        }

        private void GeneCallReviewToolStripMenuItemClick(Object sender, EventArgs e)
        {
            if (_sd.Forms.ReviewMarkerCalls == null)
            {
                _sd.Forms.ReviewMarkerCalls = new FrmReviewMarkerCalls(_sd);
                _sd.Forms.ReviewMarkerCalls.Show();
            }
            _sd.Forms.ReviewMarkerCalls.Focus();
        }

        /// <summary>Determines which gene is selected in the listview if any gene is selected.</summary>
        private int GetSelectedMarker()
        {
            if (lvwMarkers.SelectedIndices.Count == 0)
            {
                return -1;
            }
            return (int)lvwMarkers.SelectedItems[0].Tag;
        }

        private void HideUserDefinedThreshold()
        {
            _sErrorBarUserThreshold.Enabled = false;
            _sUserThreshold.Enabled = false;
        }

        private void InitializeSettings()
        {
            txtGlobalBackgroundCutoff.Text = (_sd.Settings.GlobalBackgroundCutoff * 100).ToString(CultureInfo.InvariantCulture);
            txtGlobalBackgroundFactor.Text = (_sd.Settings.GlobalBackgroundFactor).ToString(CultureInfo.InvariantCulture);
            txtThresholdFlaggingFactor.Text = _sd.Settings.ThresholdFlaggingFactor.ToString(CultureInfo.InvariantCulture);
            txtPercentPositiveFactor.Text = (_sd.Settings.PositiveSignalFactor * 100).ToString(CultureInfo.InvariantCulture);
            txtPercentPositiveCutoff.Text = (_sd.Settings.PositiveSignalCutoff * 100).ToString(CultureInfo.InvariantCulture);
            txtBinSize.Text = _sd.Settings.BinSize.ToString(CultureInfo.InvariantCulture);
            txtWindowSize.Text = _sd.Settings.WindowSize.ToString(CultureInfo.InvariantCulture);
        }

        private void LvwGenesColumnClick(Object sender, ColumnClickEventArgs e)
        {
            // Determine if the clicked column is already the column that is being sorted.
            if ((e.Column == _lvwColumnSorter.SortColumn))
            {
                // Reverse the current sort direction for this column.
                _lvwColumnSorter.Order = (_lvwColumnSorter.Order == SortOrder.Ascending) ? SortOrder.Descending : SortOrder.Ascending;
            }
            else
            {
                // Set the column number that is to be sorted; default to ascending.
                _lvwColumnSorter.SortColumn = e.Column;
                _lvwColumnSorter.Order = SortOrder.Ascending;
            }
            // Perform the sort with these new sort options.
            lvwMarkers.Sort();
        }

        /// <summary>When the listview selection is changed.</summary>
        private void LvwGenesSelectedIndexChanged(Object sender, EventArgs e)
        {
            int index = GetSelectedMarker();
            if (index == -1)
                return;
            UpdateParametersListview();
            UpdateChart(index);
        }

        private void LvwMarkersItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if (_allowItemCheck)
            {
                var markerIndex = (int)e.Item.Tag;
                _sd.DiscardedMarkers[markerIndex] = !e.Item.Checked;//true to discard marker; false to keep
                e.Item.SubItems[5].Text = e.Item.Checked ? "Yes" : "No";
                if (_sd.Forms.Clustering != null)
                {
                    _sd.ClusterUPGMA();
                    _sd.Forms.Clustering.UpdateForm();
                }
            }

        }

        private void NumberOfTimesAboveGlobalBackgrounddatasetspecificToolStripMenuItemCheckedChanged(Object sender, EventArgs e)
        {
            if (lvwMarkers.Items.Count == 0)
                return;
            var gBmenuItem = sender as ToolStripMenuItem;
            if (gBmenuItem != null && gBmenuItem.Checked)
            {
                SetToGlobalBackgroundThreshold();
            }
            if (gBmenuItem != null) percentPositiveSignalToolStripMenuItem.Checked = !gBmenuItem.Checked;
        }

        private void OfAveragePositiveSignalToolStripMenuItemCheckedChanged(Object sender, EventArgs e)
        {
            if (lvwMarkers.Items.Count == 0)
                return;
            var pPmenuItem = sender as ToolStripMenuItem;
            if (pPmenuItem != null && pPmenuItem.Checked)
            {
                SetToPercentPositiveThreshold();
            }
            if (pPmenuItem != null) globalBackgroundToolStripMenuItem.Checked = !pPmenuItem.Checked;
        }

        private void ResetAllUserDefinedThresholdsToolStripMenuItemClick(Object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to reset all user-defined thresholds?", "Reset thresholds?", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                foreach (MarkerThresholdInfo t in _sd.Thresholds.Markers)
                {
                    if (t.ThresholdType == ThresholdType.UserDefined)
                    {
                        MarkerThresholdInfo gi = t;
                        gi.ThresholdType = ThresholdType.GlobalBackground;
                        gi.GetThreshold();
                    }
                }
            }
            UpdateListview();
            lvwMarkers.Items[0].Selected = true;
            _sd.UpdateCalls();
            UpdateForms();
        }

        /// <summary>When the user presses the Save control.</summary>
        private void SaveToolStripMenuItemClick(Object sender, EventArgs e)
        {
            _sd.WriteToMetaDataFile();
        }

        /// <summary>Change the threshold of all genes except user thresholded genes to global background times user-defined amount.</summary>
        private void SetToGlobalBackgroundThreshold()
        {
            _sd.Thresholds.SetThresholdsToGlobalBackground();

            int markerIndex = GetSelectedMarker();
            if (markerIndex != -1)
            {
                UpdateChart(markerIndex);
            }
            UpdateListview();
            _sd.UpdateCalls();
            UpdateForms();
        }

        private void SetToGlobalBackgroundThreshold(int markerIndex)
        {
            MarkerThresholdInfo marker = _sd.Thresholds.Markers[markerIndex];
            marker.ThresholdType = ThresholdType.GlobalBackground;
            marker.GetThreshold();

            UpdateListview();
            UpdateChart(markerIndex);
            _sd.UpdateCalls();
            UpdateForms();
        }

        /// <summary>Change the threshold of all genes except user thresholded genes to the user-defined % of the average positive signal for each gene.</summary>
        private void SetToPercentPositiveThreshold()
        {
            _sd.Thresholds.SetThresholdsToPercentPositiveSignal();

            int markerIndex = GetSelectedMarker();
            if (markerIndex != -1)
            {
                UpdateChart(markerIndex);
            }
            UpdateListview();
            _sd.UpdateCalls();
            UpdateForms();
        }

        private void SetToPercentPositiveThreshold(int index)
        {
            MarkerThresholdInfo marker = _sd.Thresholds.Markers[index];
            marker.ThresholdType = ThresholdType.PercentOfPositiveSignal;
            marker.GetThreshold();

            UpdateChart(index);
            _sd.UpdateCalls();
            UpdateListview();
            UpdateForms();
        }

        public void SetupForm()
        {
            SetupChart();
            UpdateForm();
        }

        /// <summary>Sets up the chart with all of the necessary series.</summary>
        private void SetupChart()
        {
            Legend l = cht.Legends.Add("Legend");
            l.BackColor = Color.Transparent;

            //.Legends.Item(0).BackColor = Color.Transparent
            cht.ChartAreas[0].Position = new ElementPosition(0, 0, 95, 95);
            cht.ChartAreas[0].InnerPlotPosition = new ElementPosition(6, 6, 85, 85);
            cht.ChartAreas[0].AxisX.Interval = _sd.Settings.BinSize;
            cht.ChartAreas[0].AxisX.Minimum = 0;
            cht.ChartAreas[0].AxisX.Maximum = _sd.MaxSignal;
            cht.ChartAreas[0].AxisX.MinorGrid.Enabled = false;
            cht.ChartAreas[0].AxisX.MajorGrid.Enabled = false;
            cht.ChartAreas[0].AxisX.LabelStyle.Angle = -90;
            cht.ChartAreas[0].AxisX.Title = "Signal Bin";
            cht.ChartAreas[0].AxisY.Minimum = 0;
            cht.ChartAreas[0].AxisY.MinorTickMark.Enabled = true;
            cht.ChartAreas[0].AxisY.MinorTickMark.Interval = 1;
            cht.ChartAreas[0].AxisY.MinorTickMark.Size = 0.25f;
            cht.ChartAreas[0].AxisY.Interval = 5;
            cht.ChartAreas[0].CursorX.IsUserEnabled = true;
            cht.ChartAreas[0].CursorX.Interval = 0;

            cht.ChartAreas[0].AxisY.Title = "Frequency";
            _sFreq = cht.Series.Add("Frequencies");
            _sFreq.Enabled = false;
            _sFreq.Points.Clear();
            _sFreq.ChartType = SeriesChartType.Column;
            _sFreq.XValueType = ChartValueType.Single;
            _sFreq.XAxisType = AxisType.Primary;
            _sMedian = cht.Series.Add("Windowed Median");
            _sMedian.Enabled = false;
            _sMedian.Points.Clear();
            _sMedian.XValueType = ChartValueType.Single;
            _sMedian.XAxisType = AxisType.Primary;
            _sMedian.ChartType = SeriesChartType.Line;
            _sMedian.MarkerSize = 3;
            _sMedian.MarkerStyle = MarkerStyle.Diamond;
            _sSTDev = cht.Series.Add("Windowed Standard Deviation");
            _sSTDev.XValueType = ChartValueType.Single;
            _sSTDev.XAxisType = AxisType.Primary;
            _sSTDev.Enabled = false;
            _sSTDev.Points.Clear();
            _sSTDev.ChartType = SeriesChartType.Line;
            _sSTDev.MarkerSize = 3;
            _sSTDev.MarkerStyle = MarkerStyle.Circle;
            Misc.CreateYAxis(cht, cht.ChartAreas[0], _sMedian, 0, 0);
            Misc.CreateYAxis(cht, cht.ChartAreas[0], _sSTDev, 0, 0);
            cht.ChartAreas[1].AxisY2.Enabled = AxisEnabled.True;
            cht.ChartAreas[1].AxisY2.MajorGrid.Enabled = false;
            cht.ChartAreas[1].AxisY2.MinorGrid.Enabled = false;
            cht.ChartAreas[2].AxisY2.Enabled = AxisEnabled.True;
            cht.ChartAreas[2].AxisY2.MajorGrid.Enabled = false;
            cht.ChartAreas[2].AxisY2.MinorGrid.Enabled = false;

            //Setup Global Background with an error bar line
            _sGB = cht.Series.Add("Global Background");
            _sGB.Enabled = false;
            _sGB.Points.AddXY(0, 0);
            _sGB.Points[0].AxisLabel = "";
            _sGB.Color = Color.LightSlateGray;
            _sGB.ChartType = SeriesChartType.Line;
            _sGB.BorderDashStyle = ChartDashStyle.Dash;
            _sErrorBarGB = cht.Series.Add("EB Global Background");
            _sErrorBarGB.ChartType = SeriesChartType.ErrorBar;
            _sErrorBarGB.Enabled = false;
            _sErrorBarGB.IsVisibleInLegend = false;
            _sErrorBarGB.Color = Color.LightSlateGray;
            _sErrorBarGB.BorderWidth = 2;
            _sErrorBarGB.BorderDashStyle = ChartDashStyle.Dash;
            _sErrorBarGB["ErrorBarSeries"] = "Global Background";
            _sErrorBarGB["ErrorBarType"] = "FixedValue(1000)";
            _sErrorBarGB["ErrorBarStyle"] = "UpperError";

            //Setup Global Background with an error bar line 
            _sGBThreshold = cht.Series.Add("Global Background Threshold");
            _sGBThreshold.Enabled = false;
            _sGBThreshold.Points.AddXY(0, 0);
            _sGBThreshold.IsVisibleInLegend = true;
            _sGBThreshold.Points[0].AxisLabel = "";
            _sGBThreshold.Color = Color.LightSlateGray;
            _sGBThreshold.ChartType = SeriesChartType.Line;
            _sGBThreshold.BorderDashStyle = ChartDashStyle.Dot;
            _sErrorBarGBThreshold = cht.Series.Add("EB Global Background Threshold");
            _sErrorBarGBThreshold.ChartType = SeriesChartType.ErrorBar;
            _sErrorBarGBThreshold.Enabled = false;
            _sErrorBarGBThreshold.IsVisibleInLegend = false;
            _sErrorBarGBThreshold.Color = Color.LightSlateGray;
            _sErrorBarGBThreshold.BorderWidth = 2;
            _sErrorBarGBThreshold.BorderDashStyle = ChartDashStyle.Dot;
            _sErrorBarGBThreshold["ErrorBarSeries"] = "Global Background Threshold";
            _sErrorBarGBThreshold["ErrorBarType"] = "FixedValue(1000)";
            _sErrorBarGBThreshold["ErrorBarStyle"] = "UpperError";

            //Setup User Threshold with a lime green error bar line 
            _sUserThreshold = cht.Series.Add("User Threshold");
            _sUserThreshold.Enabled = false;
            _sUserThreshold.Points.AddXY(0, 0);
            _sUserThreshold.IsVisibleInLegend = true;
            _sUserThreshold.Points[0].AxisLabel = "";
            _sUserThreshold.Color = Color.Lime;
            _sUserThreshold.ChartType = SeriesChartType.Line;
            _sErrorBarUserThreshold = cht.Series.Add("EB User Threshold");
            _sErrorBarUserThreshold.ChartType = SeriesChartType.ErrorBar;
            _sErrorBarUserThreshold.Enabled = false;
            _sErrorBarUserThreshold.IsVisibleInLegend = false;
            _sErrorBarUserThreshold.Color = Color.Lime;
            _sErrorBarUserThreshold.BorderWidth = 2;
            _sErrorBarUserThreshold["ErrorBarSeries"] = "User Threshold";
            _sErrorBarUserThreshold["ErrorBarType"] = "FixedValue(1000)";
            _sErrorBarUserThreshold["ErrorBarStyle"] = "UpperError";


            //Setup Average Positive Signal with a Light Blue error bar line - dashed
            _sAvgPositiveSignal = cht.Series.Add("Avg Signal");
            _sAvgPositiveSignal.Enabled = false;
            _sAvgPositiveSignal.Points.AddXY(0, 0);
            _sAvgPositiveSignal.IsVisibleInLegend = true;
            _sAvgPositiveSignal.Points[0].AxisLabel = "";
            _sAvgPositiveSignal.Color = Color.LightBlue;
            _sAvgPositiveSignal.ChartType = SeriesChartType.Line;
            _sAvgPositiveSignal.BorderDashStyle = ChartDashStyle.Dash;
            _sEBAvgPositiveSignal = cht.Series.Add("EB Avg Signal");
            _sEBAvgPositiveSignal.ChartType = SeriesChartType.ErrorBar;
            _sEBAvgPositiveSignal.Enabled = false;
            _sEBAvgPositiveSignal.IsVisibleInLegend = false;
            _sEBAvgPositiveSignal.Color = Color.LightBlue;
            _sEBAvgPositiveSignal.BorderWidth = 2;
            _sEBAvgPositiveSignal.BorderDashStyle = ChartDashStyle.Dash;
            _sEBAvgPositiveSignal["ErrorBarSeries"] = "Avg Signal";
            _sEBAvgPositiveSignal["ErrorBarType"] = "FixedValue(1000)";
            _sEBAvgPositiveSignal["ErrorBarStyle"] = "UpperError";

            //Setup percent positive signal with a Light Blue error bar line - dotted
            _sPercentPositive = cht.Series.Add("Percent Positive Signal");
            _sPercentPositive.Enabled = false;
            _sPercentPositive.Points.AddXY(0, 0);
            _sPercentPositive.IsVisibleInLegend = true;
            _sPercentPositive.Points[0].AxisLabel = "";
            _sPercentPositive.Color = Color.LightBlue;
            _sPercentPositive.ChartType = SeriesChartType.Line;
            _sPercentPositive.BorderDashStyle = ChartDashStyle.Dot;
            _sEBPercentPositive = cht.Series.Add("EB Percent Positive Signal");
            _sEBPercentPositive.ChartType = SeriesChartType.ErrorBar;
            _sEBPercentPositive.Enabled = false;
            _sEBPercentPositive.IsVisibleInLegend = false;
            _sEBPercentPositive.Color = Color.LightBlue;
            _sEBPercentPositive.BorderWidth = 2;
            _sEBPercentPositive.BorderDashStyle = ChartDashStyle.Dot;
            _sEBPercentPositive["ErrorBarSeries"] = "Percent Positive Signal";
            _sEBPercentPositive["ErrorBarType"] = "FixedValue(1000)";
            _sEBPercentPositive["ErrorBarStyle"] = "UpperError";

        }

        /// <summary>Show the signal frequencies on the chart.</summary>
        /// <param name="index">Index of the selected gene information.</param>
        private void ShowFrequencies(int index)
        {
            MarkerThresholdInfo marker = _sd.Thresholds.Markers[index];
            cht.ChartAreas[0].AxisX.Interval = _sd.Settings.BinSize;
            cht.ChartAreas[0].AxisX.Maximum = _sd.MaxSignal;
            cht.ChartAreas[0].AxisY.Maximum = (int)(marker.Frequencies.Max() * 1.1) + 1;
            cht.ChartAreas[0].AxisY.Title = "Frequency";
            cht.Titles.Clear();
            cht.Titles.Add(marker.MarkerName);


            Color absent = _sd.Settings.C.AbsentColour;
            Color ambiguousAbsent = _sd.Settings.C.AbsentFlaggedColour;
            Color ambiguousPresent = _sd.Settings.C.PresentFlaggedColour;
            Color present = _sd.Settings.C.PresentColour;


            _sFreq.Points.Clear();
            for (int i = 0; i < _sd.Thresholds.Bin.Count; i++)
            {
                double bin = _sd.Thresholds.Bin[i];
                var dp = new DataPoint(bin, marker.Frequencies[i]);

                if (bin >= marker.UpperThreshold)
                {
                    dp.Color = present;
                }
                else if (bin >= marker.Threshold)
                {
                    dp.Color = ambiguousPresent;
                }
                else if (bin >= marker.LowerThreshold)
                {
                    dp.Color = ambiguousAbsent;
                }
                else
                {
                    dp.Color = absent;
                }


                dp.BorderColor = Color.Black;
                dp.BorderWidth = 1;
                _sFreq.Points.Add(dp);
            }
        }

        /// <summary>Show the sliding window standard deviation curve on the chart.</summary>
        /// <param name="index">Index of gene in listview.</param>
        private void ShowDeviations(int index)
        {
            MarkerThresholdInfo marker = _sd.Thresholds.Markers[index];
            cht.Titles.Clear();
            cht.Titles.Add(lvwMarkers.Items[GetSelectedMarker()].SubItems[0].Text);

            _sSTDev.Points.Clear();
            for (int i = 0; i < marker.WindowedDeviations.Count; i++)
            {
                _sSTDev.Points.AddXY(marker.WindowedAverages[i], marker.WindowedDeviations[i]);
            }
        }

        /// <summary>Show the sliding window median variance on the chart.</summary>
        /// <param name="index">Index of the gene in the listview.</param>
        private void ShowMedians(int index)
        {
            MarkerThresholdInfo marker = _sd.Thresholds.Markers[index];
            cht.Titles.Clear();
            cht.Titles.Add(marker.MarkerName);

            _sMedian.Points.Clear();
            for (int i = 0; i < marker.WindowedMedianVariances.Count; i++)
            {
                _sMedian.Points.AddXY(marker.WindowedAverages[i], marker.WindowedMedianVariances[i]);
            }
        }
        private void ShowSpotDistributionToolStripMenuItemClick(Object sender, EventArgs e)
        {
            int index = GetSelectedMarker();
            if (index == -1)
                return;
            var f = new FrmShowMarkerSpots(_sd, index, SpotImageCrop.Sorting.BySignal);
            f.Show();
        }

        private void ShowSpotDistributionClumpedToolStripMenuItemClick(Object sender, EventArgs e)
        {
            int index = GetSelectedMarker();
            if (index == -1)
                return;
            var f = new FrmShowMarkerSpots(_sd, index);
            _sd.Forms.ShowMarkerSpots.Add(f);
            f.Show();
        }

        /// <summary>Threshold type has been changed to UserDefined.</summary>
        /// <param name="index">marker index</param>
        public void ShowUserDefinedThreshold(int index)
        {
            MarkerThresholdInfo marker = _sd.Thresholds.Markers[index];
            marker.ThresholdType = ThresholdType.UserDefined;

            int lvwIndex = index;
            foreach (ListViewItem l in lvwMarkers.Items)
            {
                if (index == (int)l.Tag)
                {
                    lvwIndex = lvwMarkers.Items.IndexOf(l);
                }
            }

            int flaggedCount = marker.FlaggedCount;
            ListViewItem lvi = lvwMarkers.Items[lvwIndex];
            lvi.SubItems[1].Text = marker.Threshold.ToString(CultureInfo.InvariantCulture);
            lvi.SubItems[2].Text = "User-Defined";
            lvi.SubItems[4].Text = flaggedCount.ToString(CultureInfo.InvariantCulture);
            lvi.BackColor = flaggedCount > 0 ? Color.Yellow : Color.PaleGreen;
            cht.ChartAreas[0].CursorX.LineColor = Color.Transparent;
            _sUserThreshold.Enabled = true;
            _sUserThreshold.Points[0].XValue = marker.Threshold;
            _sErrorBarUserThreshold.Enabled = true;
            _sErrorBarUserThreshold.Color = Color.Lime;
            _sd.UpdateCalls();
            UpdateParametersListview();
            UpdateForms();
        }

        private void SpotSignalReviewToolStripMenuItemClick(Object sender, EventArgs e)
        {
            if (_sd.Forms.MarkerThresholdReview == null)
            {
                double min = Math.Floor(_sd.MinSignal * 10) / 10;
                double max = Math.Ceiling(_sd.MaxSignal * 10) / 10;

                _sd.Forms.MarkerThresholdReview = new FrmMarkerThresholdReview(_sd, min, max);
                _sd.Forms.MarkerThresholdReview.Show();
            }
            _sd.Forms.MarkerThresholdReview.Focus();
        }

        /// <summary>When the background cutoff is changed by the user.</summary>
        private void TxtBackgroundCutoffKeyPress(object sender, KeyPressEventArgs e)
        {
            int i;
            if (int.TryParse(e.KeyChar.ToString(CultureInfo.InvariantCulture), out i) | e.KeyChar == '\b')
            {
                e.Handled = false;
            }
            else if (e.KeyChar == '.')
            {
                e.Handled = txtGlobalBackgroundCutoff.Text.Contains(".");
            }
            else if (e.KeyChar == '\t' | e.KeyChar == (char)(Keys.Enter) | e.KeyChar == (char)(Keys.Space))
            {
                double d;
                if (double.TryParse(txtGlobalBackgroundCutoff.Text, out d))
                {
                    if (d <= 100 & d > 0)
                    {
                        _sd.Settings.GlobalBackgroundCutoff = d / 100;
                        _sd.Thresholds.GetGlobalBackground();
                        UpdateGlobalBackgroundOnChart();
                        SetToGlobalBackgroundThreshold();
                        percentPositiveSignalToolStripMenuItem.Checked = false;
                        int index = GetSelectedMarker();
                        UpdateListview();
                        if (index == -1)
                            index = 0;
                        lvwMarkers.Items[index].Selected = true;
                    }
                }
            }
        }

        /// <summary>When the user changes the bin size.</summary>
        private void TxtBinSizeKeyPress(object sender, KeyPressEventArgs e)
        {
            tslStatus.Text = "Ready";
            int i;
            if (int.TryParse(e.KeyChar.ToString(CultureInfo.InvariantCulture), out i) | e.KeyChar == '\b')
            {
                e.Handled = false;
            }
            else if (e.KeyChar == '.')
            {
                e.Handled = txtBinSize.Text.Contains(".");
            }
            else if (e.KeyChar == (char)(Keys.Enter) | e.KeyChar == (char)(Keys.Space) | e.KeyChar == '\t')
            {
                double d;
                if (double.TryParse(txtBinSize.Text, out d))
                {
                    const double epsilon = 0.0001;
                    if (Math.Abs(d - 0) < epsilon)
                    {
                        d = 0.02;
                        txtBinSize.Text = d.ToString(CultureInfo.InvariantCulture);
                    }
                    _sd.Settings.BinSize = d;
                    _sd.Thresholds.GetBin();
                    _sd.Thresholds.CalcFrequencies();
                    ShowFrequencies(GetSelectedMarker());
                }
            }
            else
            {
                e.Handled = true;
            }
        }

        /// <summary>When the "Times above global background" textbox text is changed.</summary>
        private void TxtGlobalBackgroundFactorKeyPress(object sender, KeyPressEventArgs e)
        {
            int i;
            if (int.TryParse(e.KeyChar.ToString(CultureInfo.InvariantCulture), out i) | e.KeyChar == '\b')
            {
                e.Handled = false;
            }
            else if (e.KeyChar == '.')
            {
                e.Handled = txtGlobalBackgroundFactor.Text.Contains(".");
            }
            else if (e.KeyChar == (char)(Keys.Enter) | e.KeyChar == '\t' | e.KeyChar == (char)(Keys.Space))
            {
                double d;
                if (double.TryParse(txtGlobalBackgroundFactor.Text, out d))
                {
                    _sd.Settings.GlobalBackgroundFactor = d;
                    _sd.Thresholds.GetGlobalBackground();
                    UpdateGlobalBackgroundOnChart();
                    SetToGlobalBackgroundThreshold();
                    percentPositiveSignalToolStripMenuItem.Checked = false;
                    int index = GetSelectedMarker();
                    if (index == -1)
                        index = 0;
                    UpdateListview();
                    lvwMarkers.Items[index].Selected = true;
                }
            }
            else
            {
                e.Handled = true;
            }
        }

        private void TxtLostFocus(object sender, EventArgs e)
        {
            var keyPressEvent = new KeyPressEventArgs((char)(Keys.Enter));
            if (ReferenceEquals(sender, txtGlobalBackgroundCutoff))
            {
                TxtBackgroundCutoffKeyPress(txtGlobalBackgroundCutoff, keyPressEvent);
            }
            if (ReferenceEquals(sender, txtGlobalBackgroundFactor))
            {
                TxtGlobalBackgroundFactorKeyPress(txtGlobalBackgroundFactor, keyPressEvent);
            }
            if (ReferenceEquals(sender, txtBinSize))
            {
                TxtBinSizeKeyPress(txtBinSize, keyPressEvent);
            }
            if (ReferenceEquals(sender, txtWindowSize))
            {
                TxtWindowSizeKeyPress(txtWindowSize, keyPressEvent);
            }
            if (ReferenceEquals(sender, txtPercentPositiveFactor))
            {
                TxtPercentPositiveFactorKeyPress(txtPercentPositiveFactor, keyPressEvent);
            }
            if (ReferenceEquals(sender, txtPercentPositiveCutoff))
            {
                TxtPercentPositiveCutOffKeyPress(txtPercentPositiveCutoff, keyPressEvent);
            }
            if (ReferenceEquals(sender, txtThresholdFlaggingFactor))
            {
                TxtThresholdFlaggingFactorKeyPress(txtThresholdFlaggingFactor, keyPressEvent);
            }

        }

        private void TxtPercentPositiveCutOffKeyPress(object sender, KeyPressEventArgs e)
        {
            int i;
            if (int.TryParse(e.KeyChar.ToString(CultureInfo.InvariantCulture), out i) | e.KeyChar == '\b')
            {
                e.Handled = false;
            }
            else if (e.KeyChar == '.')
            {
                e.Handled = txtPercentPositiveCutoff.Text.Contains(".");
            }
            else if (e.KeyChar == (char)(Keys.Enter) | e.KeyChar == (char)(Keys.Space) | e.KeyChar == '\t')
            {
                double d;
                if (double.TryParse(txtPercentPositiveCutoff.Text, out d))
                {
                    if (d <= 100 & d >= 0)
                    {
                        _sd.Settings.PositiveSignalCutoff = d / 100;
                        _sd.Thresholds.CalcAveragePositiveSignals();
                        UpdatePercentPositiveSignalLineOnGraph();
                        UpdateParametersListview();
                    }
                }
            }
            else
            {
                e.Handled = true;
            }
        }

        private void TxtPercentPositiveFactorKeyPress(Object sender, KeyPressEventArgs e)
        {
            int i;
            if (int.TryParse(e.KeyChar.ToString(CultureInfo.InvariantCulture), out i) | e.KeyChar == '\b')
            {
                e.Handled = false;
            }
            else if (e.KeyChar == '.')
            {
                e.Handled = txtPercentPositiveFactor.Text.Contains(".");
            }
            else if (e.KeyChar == (char)(Keys.Enter) | e.KeyChar == (char)(Keys.Space) | e.KeyChar == '\t')
            {
                double d;
                if (double.TryParse(txtPercentPositiveFactor.Text, out d))
                {
                    if (d <= 100 & d >= 0)
                    {
                        _sd.Settings.PositiveSignalFactor = d / 100;
                        _sd.Thresholds.CalcAveragePositiveSignals();
                        UpdatePercentPositiveSignalLineOnGraph();
                        UpdateParametersListview();
                    }
                }
            }
            else
            {
                e.Handled = true;
            }
        }

        /// <summary>If the number of times global error threshold flagging value is changed by the user.</summary>
        private void TxtThresholdFlaggingFactorKeyPress(object sender, KeyPressEventArgs e)
        {
            int i;
            if (int.TryParse(e.KeyChar.ToString(CultureInfo.InvariantCulture), out i) | e.KeyChar == '\b')
            {
                e.Handled = false;
            }
            else if (e.KeyChar == '.')
            {
                e.Handled = txtThresholdFlaggingFactor.Text.Contains(".");
            }
            else if (e.KeyChar == (char)(Keys.Enter) | e.KeyChar == (char)(Keys.Space) | e.KeyChar == '\t')
            {
                double d;
                if (double.TryParse(txtThresholdFlaggingFactor.Text, out d))
                {
                    _sd.Settings.ThresholdFlaggingFactor = d;
                    UpdateListview();
                    int index = GetSelectedMarker();
                    if (index == -1)
                        index = 0;
                    lvwMarkers.Items[index].Selected = true;
                }
            }
            else
            {
                e.Handled = true;
            }
        }

        /// <summary>When the user inputs a number value into the window size textbox, input is validated.</summary>
        private void TxtWindowSizeKeyPress(object sender, KeyPressEventArgs e)
        {
            int i;
            if (int.TryParse(e.KeyChar.ToString(CultureInfo.InvariantCulture), out i) | e.KeyChar == '\b')
            {
                e.Handled = false;
            }
            else if (e.KeyChar == (char)(Keys.Enter) | e.KeyChar == (char)(Keys.Space))
            {
                int windowSize;
                if (int.TryParse(txtWindowSize.Text, out windowSize))
                {
                    if (windowSize <= 0)
                    {
                        windowSize = 5;
                        txtWindowSize.Text = windowSize.ToString(CultureInfo.InvariantCulture);
                        tslStatus.Text = "Numbers above 0 only for the window size of the median variance and standard deviation.";
                    }
                    _sd.Settings.WindowSize = windowSize;
                    _sd.Thresholds.CalcWindowedStats();
                    lvwMarkers.Items[GetSelectedMarker()].Selected = true;
                    UpdateChart(GetSelectedMarker());
                }

            }
            else
            {
                e.Handled = true;
            }
        }

        /// <summary>When the About control is clicked.</summary>
        private void TsbAboutClick(Object sender, EventArgs e)
        {
            var f = new AboutBox1(_sd.Forms);
            f.ShowDialog();
        }

        /// <summary>Update the gene information chart on the form for the selected listview item.</summary>
        /// <param name="index">Index of the selected gene in the listview.</param>
        private void UpdateChart(int index)
        {
            if (chkFrequencies.Checked)
            {
                _sFreq.Enabled = true;
                ShowFrequencies(index);
            }
            else
            {
                _sFreq.Enabled = false;
            }

            if (chkWindowedMedian.Checked)
            {
                _sMedian.Enabled = true;
                ShowMedians(index);
            }
            else
            {
                _sMedian.Enabled = false;
            }

            if (chkWindowedSTDev.Checked)
            {
                _sSTDev.Enabled = true;
                ShowDeviations(index);
            }
            else
            {
                _sSTDev.Enabled = false;
            }

            MarkerThresholdInfo marker = _sd.Thresholds.Markers[index];

            if (marker.ThresholdType == ThresholdType.GlobalBackground)
            {
                _sGB.Enabled = true;
                _sGB.ChartType = SeriesChartType.Line;
                _sGB.Points[0].XValue = _sd.Thresholds.GlobalBackground;
                _sErrorBarGB.Enabled = true;
                _sErrorBarGBThreshold.Enabled = true;
            }
            else
            {
                _sGB.Enabled = false;
                _sErrorBarGB.Enabled = false;
                _sErrorBarGBThreshold.Enabled = false;
            }

            _sErrorBarUserThreshold.Enabled = true;
            _sUserThreshold.Points[0].XValue = _sd.Thresholds.Markers[index].Threshold;
            _sErrorBarUserThreshold.Color = marker.ThresholdType == ThresholdType.UserDefined ? Color.Lime : Color.Transparent;

            if (marker.ThresholdType == ThresholdType.PercentOfPositiveSignal)
            {
                _sAvgPositiveSignal.Enabled = true;
                _sAvgPositiveSignal.Points[0].XValue = _sd.Thresholds.Markers[index].AveragePositiveSignal;
                _sEBAvgPositiveSignal.Enabled = true;
                _sPercentPositive.Enabled = true;
                _sPercentPositive.Points[0].XValue = _sd.Settings.PositiveSignalFactor * _sd.Thresholds.Markers[index].AveragePositiveSignal;
                _sEBPercentPositive.Enabled = true;
            }
            else
            {
                _sAvgPositiveSignal.Enabled = false;
                _sEBAvgPositiveSignal.Enabled = false;
                _sPercentPositive.Enabled = false;
                _sEBPercentPositive.Enabled = false;
            }

            cht.ChartAreas[0].CursorX.Position = marker.Threshold;
            cht.ChartAreas[0].CursorX.LineColor = Color.Transparent;
            cht.ChartAreas[0].CursorX.SelectionColor = Color.FromArgb(50, 100, 100, 100);
            cht.ChartAreas[0].CursorX.SelectionStart = marker.LowerThreshold;
            cht.ChartAreas[0].CursorX.SelectionEnd = marker.UpperThreshold;
        }

        public void UpdateForm()
        {
            lblGlobalBackground.Text = _sd.Thresholds.GetGlobalBackground().ToString("0.00000");
            lblGlobalBackgroundThreshold.Text = (_sd.Thresholds.GetGlobalBackground() * _sd.Settings.GlobalBackgroundFactor).ToString("0.00000");
            txtGlobalErrorSD.Text = _sd.GetGlobalErrorSD().ToString("0.00000");

            UpdateListview();
        }

        private void UpdateForms()
        {
            if (_sd.Forms.MarkerThresholdReview != null)
            {
                _sd.Forms.MarkerThresholdReview.UpdateForm();
            }
            if (_sd.Forms.ReviewMarkerCalls != null)
            {
                _sd.Forms.ReviewMarkerCalls.UpdateForm(0);
            }
            if (_sd.Forms.Clustering != null)
            {
                _sd.Forms.Clustering.UpdateForm();
            }
            if (_sd.Forms.Diagnosis != null)
            {
                _sd.Forms.Diagnosis.UpdateListview();
            }
        }

        /// <summary>Update the global background values in the form</summary>
        private void UpdateGlobalBackgroundOnChart()
        {
            lblGlobalBackground.Text = _sd.Thresholds.GlobalBackground.ToString("0.00000");
            lblGlobalBackgroundThreshold.Text = (_sd.Thresholds.GlobalBackground * _sd.Settings.GlobalBackgroundFactor).ToString("0.00000");
            if (Visible)
            {
                _sGB.Enabled = true;
                _sGB.Points[0].XValue = _sd.Thresholds.GlobalBackground;
                _sGBThreshold.Enabled = true;
                _sGBThreshold.Points[0].XValue = _sd.Thresholds.GlobalBackground * _sd.Settings.GlobalBackgroundFactor;
            }
        }

        /// <summary>Update the information in the listview with each change to the underlying information.</summary>
        private void UpdateListview()
        {
            _allowItemCheck = false;
            lvwMarkers.Items.Clear();

            _sd.Thresholds.GetBin();
            _sd.Thresholds.CalcWindowedStats();
            _sd.Thresholds.CalcFrequencies();

            var lvList = new List<ListViewItem>();
            foreach (MarkerThresholdInfo marker in _sd.Thresholds.Markers)
            {
                int markerIndex = _sd.Thresholds.Markers.IndexOf(marker);
                Color itemColor = Color.LightGreen;
                switch (marker.ThresholdType)
                {
                    case ThresholdType.UserDefined:
                        //user-defined
                        itemColor = Color.LightGreen;
                        break;
                    case ThresholdType.GlobalBackground:
                        //x above background
                        itemColor = Color.LightSlateGray;
                        break;
                    case ThresholdType.PercentOfPositiveSignal:
                        // % of average positive signal
                        itemColor = Color.LightBlue;
                        break;
                }
                int flag = marker.FlaggedCount;
                if (flag > 0)
                    itemColor = Color.Yellow;
                var lvItem = new ListViewItem(new[]
                                                  {
                                                      marker.MarkerName,
                                                      marker.Threshold.ToString("0.0000"),
                                                      marker.ThresholdType.ToString(),
                                                      marker.Signals.Max().ToString("0.0000"),
                                                      flag.ToString(CultureInfo.InvariantCulture),
                                                      _sd.DiscardedMarkers[markerIndex] ? "No" : "Yes",
                                                      marker.MarkerComment
                                                  })
                                 {
                                     BackColor = itemColor,
                                     Tag = _sd.Thresholds.Markers.IndexOf(marker),
                                     Checked = !_sd.DiscardedMarkers[markerIndex]
                                 };
                lvList.Add(lvItem);
            }
            lvwMarkers.Items.AddRange(lvList.ToArray());
            _allowItemCheck = true;
        }
        private void UpdateParametersListview()
        {
            if (lvwMarkers.Items.Count == 0)
                return;
            //Marker
            //Threshold
            //Threshold Type
            //Global Background
            //Global Background Factor
            //Avg Positive Signal
            //% Avg Positive Signal
            //Global Error
            //Global Error Factor
            //Flagged Genes
            int i = GetSelectedMarker();
            if (i == -1)
                return;
            MarkerThresholdInfo marker = _sd.Thresholds.Markers[i];

            var lvList = new List<ListViewItem>
                             {
                                 new ListViewItem(new[] {"Marker Name", marker.MarkerName}),
                                 new ListViewItem(new[] {"Threshold Type", marker.ThresholdType.ToString()}),
                                 new ListViewItem(new[] {"Marker Threshold", marker.Threshold.ToString("0.00000")}),
                                 new ListViewItem(new[]
                                                      {
                                                          "Threshold Flagging Lowerbound",
                                                          marker.LowerThreshold.ToString("0.00000")
                                                      }),
                                 new ListViewItem(new[]
                                                      {
                                                          "Threshold Flagging Upperbound",
                                                          marker.UpperThreshold.ToString("0.00000")
                                                      })
                             };

            if (marker.ThresholdType == ThresholdType.GlobalBackground)
            {
                lvList.Add(new ListViewItem(new[] { "Global Background", _sd.Thresholds.GlobalBackground.ToString("0.00000") }));
                lvList.Add(new ListViewItem(new[] { "Global Background Factor", _sd.Settings.GlobalBackgroundFactor.ToString(CultureInfo.InvariantCulture) }));
            }
            else if (marker.ThresholdType == ThresholdType.PercentOfPositiveSignal)
            {
                lvList.Add(new ListViewItem(new[] { "Percent Positive Signal", (_sd.Settings.PositiveSignalFactor * marker.AveragePositiveSignal).ToString("0.00000") }));
                lvList.Add(new ListViewItem(new[] { "Percent Positive Signal Factor", _sd.Settings.PositiveSignalFactor.ToString("p") }));
            }
            lvList.Add(new ListViewItem(new[] { "Global Error", _sd.GetGlobalError().ToString("0.00000") }));
            lvList.Add(new ListViewItem(new[] { "Global Error SD", _sd.GetGlobalErrorSD().ToString("0.00000") }));
            lvList.Add(new ListViewItem(new[] { "Global Error Factor (Threshold Flagging Factor)", _sd.Settings.ThresholdFlaggingFactor.ToString(CultureInfo.InvariantCulture) }));
            lvList.Add(new ListViewItem(new[] { "Threshold Flagging Value (GE SD x GE Factor)", _sd.GetThresholdFlagRange().ToString("0.00000") }));
            lvList.Add(new ListViewItem(new[] { "Number of Ambiguous Markers", marker.FlaggedCount.ToString(CultureInfo.InvariantCulture) }));
            lvList.Add(new ListViewItem(new[] { "Marker Discarded?", _sd.DiscardedMarkers[i] ? "No" : "Yes" }));
            lvList.Add(new ListViewItem(new[] { "Marker Comment", _sd.GetMarkerComment(i) }));
            lvList.Add(new ListViewItem(new[] { "Marker Max Signal", marker.Signals.Max().ToString("0.0000") }));
            lvwParam.Items.Clear();
            lvwParam.Items.AddRange(lvList.ToArray());
        }

        private void UpdatePercentPositiveSignalLineOnGraph()
        {
            int index = GetSelectedMarker();
            if (index == -1)
                return;

            //Setup Average Positive Signal with a Light Blue error bar line - dashed
            _sAvgPositiveSignal.ChartType = SeriesChartType.Line;
            _sAvgPositiveSignal.Enabled = true;
            _sAvgPositiveSignal.Points[0].XValue = _sd.Thresholds.Markers[index].AveragePositiveSignal;
            _sAvgPositiveSignal.MarkerStyle = MarkerStyle.Square;
            _sAvgPositiveSignal.MarkerSize = 3;
            _sEBAvgPositiveSignal.Enabled = true;

            //Setup Half Average Positive Signal with a Light Blue error bar line - dotted
            _sPercentPositive.ChartType = SeriesChartType.Line;
            _sPercentPositive.Enabled = true;
            _sPercentPositive.Points[0].XValue = _sd.Settings.PositiveSignalFactor * _sd.Thresholds.Markers[index].AveragePositiveSignal;
            _sPercentPositive.MarkerStyle = MarkerStyle.Square;
            _sPercentPositive.MarkerSize = 3;
            _sEBPercentPositive.Enabled = true;

        }

        private void SetMarkerThresholdToolStripMenuItemClick(object sender, EventArgs e)
        {
            //get the index of the selected marker
            int index = GetSelectedMarker();
            if (index == -1)
                return;
            //get the threshold info for the selected marker
            MarkerThresholdInfo thresholdInfo = _sd.Thresholds.Markers[index];
            //get the current upper and lower thresholds for the selected marker
            double lowerThreshold = thresholdInfo.LowerThreshold;
            double upperThreshold = thresholdInfo.UpperThreshold;
            //if the user clicks OK then return the new lower and upper thresholds for the current marker
            if (ThresholdInputBox(_sd.Markers[index], ref lowerThreshold, ref upperThreshold) != DialogResult.OK) return;
            //set the threshold to be the average of the upper and lower thresholds
            thresholdInfo.SetUserDefinedThreshold((upperThreshold + lowerThreshold) / 2);
            thresholdInfo.LowerThreshold = lowerThreshold;
            thresholdInfo.UpperThreshold = upperThreshold;
            //set the threshold type to user-defined
            thresholdInfo.ThresholdType = ThresholdType.UserDefined;
            UpdateChart(index);
            _sd.UpdateCalls();
            UpdateListview();
            UpdateForms();
        }

        public static DialogResult ThresholdInputBox(string markerName, ref double lowerThreshold, ref double upperThreshold)
        {
            var form = new Form();
            var label1 = new Label();
            var label2 = new Label();
            var textBoxLowerThreshold = new TextBox();
            var textBoxUpperThreshold = new TextBox();
            var buttonOk = new Button();
            var buttonCancel = new Button();

            form.Text = string.Format("Set upper and lower thresholds for {0}", markerName);
            label1.Text = string.Format("Set lower threshold for {0}", markerName);
            label2.Text = string.Format("Set upper threshold for {0}", markerName);
            textBoxLowerThreshold.Text = lowerThreshold.ToString(CultureInfo.InvariantCulture);
            textBoxUpperThreshold.Text = upperThreshold.ToString(CultureInfo.InvariantCulture);

            Action<object, KeyPressEventArgs> textBoxKeyPress = (sender, e) =>
            {
                var textBox = sender as TextBox;
                if (textBox == null) return;

                int i;
                if (int.TryParse(e.KeyChar.ToString(CultureInfo.InvariantCulture), out i) || e.KeyChar == '\b')
                {
                    e.Handled = false;
                }
                else if (e.KeyChar == '.')
                {
                    e.Handled = textBox.Text.Contains(".");
                }
                else if (e.KeyChar == (char)(Keys.Enter) || e.KeyChar == (char)(Keys.Space) || e.KeyChar == '\t')
                {
                    double d;
                    if (double.TryParse(textBox.Text, out d))
                    {
                        if (textBox == textBoxLowerThreshold)
                        {
                            textBoxLowerThreshold.Text = d.ToString(CultureInfo.InvariantCulture);

                        }
                        else if (textBox == textBoxUpperThreshold)
                        {
                            textBoxUpperThreshold.Text = d.ToString(CultureInfo.InvariantCulture);
                        }
                        else
                        {
                            return;
                        }
                    }
                }
                else
                {
                    e.Handled = true;
                }
            };

            textBoxLowerThreshold.KeyPress += (sender, e) => textBoxKeyPress(sender, e);
            textBoxUpperThreshold.KeyPress += (sender, e) => textBoxKeyPress(sender, e);

            //             Action<object, EventArgs> textBoxLostFocus = (sender, e) =>
            //             {
            //                 var textBox = sender as TextBox;
            //                 if (textBox == null) return;
            // 
            //                 double d;
            //                 if (double.TryParse(textBox.Text, out d))
            //                 {
            //                     if (textBox == textBoxLowerThreshold)
            //                     {
            //                         textBoxLowerThreshold.Text = d.ToString(CultureInfo.InvariantCulture);
            //                     }
            //                     else if (textBox == textBoxUpperThreshold)
            //                     {
            //                         textBoxUpperThreshold.Text = d.ToString(CultureInfo.InvariantCulture);
            //                     }
            //                     else
            //                     {
            //                         return;
            //                     }
            //                 }
            // 
            //             };
            // 
            //             textBoxLowerThreshold.KeyPress += (sender, e) => textBoxLostFocus(sender, e);
            //             textBoxUpperThreshold.KeyPress += (sender, e) => textBoxLostFocus(sender, e);


            buttonOk.Text = "OK";
            buttonCancel.Text = "Cancel";
            buttonOk.DialogResult = DialogResult.OK;
            buttonCancel.DialogResult = DialogResult.Cancel;

            label1.SetBounds(9, 20, 372, 13);
            textBoxLowerThreshold.SetBounds(12, 36, 372, 20);
            label2.SetBounds(9, 60, 372, 13);
            textBoxUpperThreshold.SetBounds(12, 76, 372, 20);
            buttonOk.SetBounds(228, 112, 75, 23);
            buttonCancel.SetBounds(309, 112, 75, 23);

            label1.AutoSize = true;
            label2.AutoSize = true;
            textBoxLowerThreshold.Anchor = textBoxLowerThreshold.Anchor | AnchorStyles.Right;
            textBoxUpperThreshold.Anchor = textBoxUpperThreshold.Anchor | AnchorStyles.Right;
            buttonOk.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            buttonCancel.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;


            form.ClientSize = new Size(396, 143);
            form.Controls.AddRange(new Control[] {
			label1,
            label2,
			textBoxLowerThreshold,
            textBoxUpperThreshold,
			buttonOk,
			buttonCancel
		});
            form.ClientSize = new Size(Math.Max(300, label1.Right + 10), form.ClientSize.Height);
            form.FormBorderStyle = FormBorderStyle.FixedDialog;
            form.StartPosition = FormStartPosition.CenterScreen;
            form.MinimizeBox = false;
            form.MaximizeBox = false;
            form.AcceptButton = buttonOk;
            form.CancelButton = buttonCancel;

            DialogResult dialogResult1 = form.ShowDialog();
            lowerThreshold = Convert.ToDouble(textBoxLowerThreshold.Text);
            upperThreshold = Convert.ToDouble(textBoxUpperThreshold.Text);
            return dialogResult1;
        }

        private void SetUpperAndLowerThresholdsAcrossDatasetToolStripMenuItemClick(object sender, EventArgs e)
        {
            double lowerThreshold = _sd.Thresholds.GlobalBackground - _sd.GetGlobalError();
            double upperThreshold = _sd.Thresholds.GlobalBackground + _sd.GetGlobalError();

            if (ThresholdInputBox("entire dataset", ref lowerThreshold, ref upperThreshold) != DialogResult.OK) return;
            foreach (MarkerThresholdInfo thresholdInfo in _sd.Thresholds.Markers)
            {
                //set the threshold to be the average of the upper and lower thresholds
                thresholdInfo.SetUserDefinedThreshold((upperThreshold + lowerThreshold) / 2);
                thresholdInfo.LowerThreshold = lowerThreshold;
                thresholdInfo.UpperThreshold = upperThreshold;
                //set the threshold type to user-defined
                thresholdInfo.ThresholdType = ThresholdType.UserDefined;
            }
            int index = GetSelectedMarker();
            UpdateChart(index == -1 ? 0 : index);
            _sd.UpdateCalls();
            UpdateListview();
            UpdateForms();
        }

    }


}
