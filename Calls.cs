namespace ArrayTubeAnalyzer
{
    public enum Calls
    {
        AbsentAuto = 0,
        PresentAuto = 1,
        Absent = 2,
        Present = 3,
        AmbiguousAbsent = 4,
        AmbiguousPresent = 5,
        AmbiguousManual = 6
    }
}