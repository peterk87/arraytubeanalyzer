﻿using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace ArrayTubeAnalyzer
{
    public partial class FrmArrayStats : Form
    {
	/// <summary>All signals within the AT Analysis Report for all array tube samples.</summary>
	private List<double> _allSignals = new List<double>();
	/// <summary>Signals for the selected array in the AT Analysis Window.</summary>
	private List<double> _signalsForArray = new List<double>();
	/// <summary>List of bin sizes, e.g. 0, 0.5, 1.0, 1.5...</summary>
	private List<double> _bin = new List<double>();
	/// <summary>Binned frequencies for all signals within dataset for all array tube samples.</summary>
	private List<int> _freqAll = new List<int>();
	/// <summary>Binned frequencies for all signal values for selected array within AT visualization form.</summary>
	private List<int> _freqArray = new List<int>();
	/// <summary>Flagging status counts for all spots within selected array.</summary>
	private FlaggedStats _flagCountsArray;
	/// <summary>Flagging status counts for all spots within all arrays.</summary>
	private FlaggedStats _flagCountsAll;

        private readonly SampleData _sd;

	public FrmArrayStats(object objSampleData)
	{
		FormClosed += FrmArrayStatsFormClosed;
		Load += FrmArrayStatsLoad;
		// This call is required by the designer.
		InitializeComponent();

		// Add any initialization after the InitializeComponent() call.
		_sd = objSampleData as SampleData;
	}

	/// <summary>Update the data presented on the form.</summary>
	public void UpdateForm()
	{
		_sd.GetGlobalError();
		if (chtStats.ChartAreas.Count > 1) {
			for (int i = chtStats.ChartAreas.Count - 1; i >= 1; i--) {
				chtStats.ChartAreas.RemoveAt(i);
			}
		}

		Text = "Array Statistics - " + _sd.SampleNames[FrmSampleQC.SelectedArrayIndex];
		_allSignals = new List<double>();
		GetAllSignals();
		_signalsForArray = new List<double>();
		GetSelectedArraySignals();

		_flagCountsAll = new FlaggedStats();
		_flagCountsArray = new FlaggedStats();
		GetFlaggingStatusCountsForAll();
		GetFlaggingStatusCountsForSelectedArray();
		PopulateListviewWithStats();
		_bin = new List<double>();
		GetBinSizes();
		_freqAll = new List<int>();
		_freqArray = new List<int>();
		_freqAll.AddRange(GetFrequencies(_allSignals));
		_freqArray.AddRange(GetFrequencies(_signalsForArray));
		PlotFrequencies();
	}

	private void FrmArrayStatsLoad(object sender, System.EventArgs e)
	{
		Text = "Array Statistics - " + _sd.SampleNames[FrmSampleQC.SelectedArrayIndex];
		GetAllSignals();
		GetSelectedArraySignals();
		GetFlaggingStatusCountsForAll();
		GetFlaggingStatusCountsForSelectedArray();
		PopulateListviewWithStats();

		GetBinSizes();
		_freqAll.AddRange(GetFrequencies(_allSignals));
		_freqArray.AddRange(GetFrequencies(_signalsForArray));
		PlotFrequencies();

	}

	/// <summary>Plot the histograms for the selected array and the entire dataset in the chart on the AT Stats form.</summary>
	private void PlotFrequencies()
	{
		chtStats.ChartAreas[0].Position = new ElementPosition(0, 0, 95, 95);
		chtStats.ChartAreas[0].InnerPlotPosition = new ElementPosition(6, 6, 85, 85);
		chtStats.ChartAreas[0].Visible = false;
		chtStats.Series.Clear();
		var with2 = chtStats.Series.Add("All AT");
		with2.Enabled = false;
		with2.ChartType = SeriesChartType.Column;
		for (int i = 0; i <= _freqAll.Count - 1; i++) {
			with2.Points.AddXY(_bin[i], _freqAll[i]);
		}
		var with3 = chtStats.ChartAreas[0].AxisX;
		with3.Interval = 0.05;
		with3.Minimum = 0;
		with3.Maximum = _allSignals[_allSignals.Count - 1];
		with3.MinorGrid.Enabled = false;
		with3.MajorGrid.Enabled = false;
		with3.LabelStyle.Angle = -90;
		with3.Title = "Signal Bin";

		var with4 = chtStats.ChartAreas[0].AxisY;
		with4.Minimum = 0;
		with4.MinorGrid.Enabled = false;
		with4.MajorGrid.Enabled = false;

		var with5 = chtStats.Series.Add(_sd.SampleNames[FrmSampleQC.SelectedArrayIndex]);
		with5.Enabled = false;
		//.XValueType = DataVisualization.Charting.ChartValueType.Single
		//.XAxisType = DataVisualization.Charting.AxisType.Primary
		with5.ChartType = SeriesChartType.Column;

		Misc.CreateYAxis(chtStats, chtStats.ChartAreas[0], chtStats.Series[_sd.SampleNames[FrmSampleQC.SelectedArrayIndex]], 0, 0);

		var with6 = chtStats.Series[1];
		for (int i = 0; i < _freqArray.Count; i++) {
			with6.Points.AddXY(_bin[i], _freqArray[i]);
		}

		var with7 = chtStats.ChartAreas[1].AxisY2;
		with7.Enabled = AxisEnabled.True;
		with7.MajorGrid.Enabled = false;
		with7.MinorGrid.Enabled = false;

		var with8 = chtStats.ChartAreas[1].AxisX;
		with8.Interval = 0.05;
		with8.Minimum = 0;
		with8.Maximum = _sd.MaxSignal;

		chtStats.Series[0].Enabled = true;
		chtStats.Series[1].Enabled = true;
		chtStats.ChartAreas[0].Visible = true;
		//.ChartAreas(1).Visible = True

	}

	/// <summary>Get the bin sizes for the histogram with 0.05 increments from 0 to the maximum signal value in the dataset.</summary>
	private void GetBinSizes()
	{
		double dMax = _sd.MaxSignal;
		double dTmp = 0;
		do {
			_bin.Add(dTmp);
			dTmp += 0.05;
		} while (!(dTmp > dMax));
	}

	/// <summary>Get the frequencies for the histogram using the signal values and the bin sizes.</summary>
	/// <param name="l">Signal values to be binned up.</param>
	/// <returns>Frequencies for the different signal value inputs.</returns>
	private IEnumerable<int> GetFrequencies(IEnumerable<double> l)
	{
		var iFreq = new int[_bin.Count];
		foreach (double dTmp in l)
		{
		    for (int j = _bin.Count - 1; j >= 0; j--) {
		        if (dTmp > _bin[j]) {
		            iFreq[j] += 1;
		            break; // TODO: might not be correct. Was : Exit For
		        }
		    }
		}
		return iFreq;
	}

	/// <summary>Retrieve all of the signals for the dataset and store in a list and sort.</summary>
	private void GetAllSignals()
	{
	    foreach (List<MarkerInfo> lsInfo in _sd.SampleStats)
	    {
	        foreach (MarkerInfo sInfo in lsInfo)
	        {
	            for (int k = 0; k <= sInfo.Signal.Count - 1; k++) {
	                _allSignals.Add(sInfo.Signal[k]);
	            }
	        }
	    }
	    _allSignals.Sort();
	}

        /// <summary>Retrieve all of the signals for the selected array tube sample and store in a list and sort.</summary>
	private void GetSelectedArraySignals()
	{
		for (int i = 0; i < _sd.SampleStats.Count; i++)
		{
		    MarkerInfo sInfo = _sd.SampleStats[FrmSampleQC.SelectedArrayIndex][i];
		    foreach (double t in sInfo.Signal)
		    {
		        _signalsForArray.Add(t);
		    }
		}
            _signalsForArray.Sort();
	}

	/// <summary>Fill in the listview with relevant stats for the dataset and the selected array tube sample.</summary>
	private void PopulateListviewWithStats()
	{
		lvwStats.Items.Clear();

		var lvList = new List<ListViewItem>();

	    var lvItem = new ListViewItem {Text = "Name of Selected AT Sample"};
	    lvItem.SubItems.Add(_sd.SampleNames[FrmSampleQC.SelectedArrayIndex]);
		lvList.Add(lvItem);

	    lvItem = new ListViewItem {Text = "Number of AT Samples"};
	    lvItem.SubItems.Add(_sd.SampleCount.ToString(CultureInfo.InvariantCulture));
		lvList.Add(lvItem);

	    lvItem = new ListViewItem {Text = "Number of Markers in Each AT"};
	    lvItem.SubItems.Add(_sd.MarkerCount.ToString(CultureInfo.InvariantCulture));
		lvList.Add(lvItem);

	    lvItem = new ListViewItem {Text = "Global Error"};
	    lvItem.SubItems.Add(_sd.GetGlobalError().ToString("0.00000"));
		lvList.Add(lvItem);

	    lvItem = new ListViewItem {Text = "Selected AT Error"};
	    lvItem.SubItems.Add(ArrayError().ToString("0.00000"));
		lvList.Add(lvItem);

	    lvItem = new ListViewItem {Text = "Global Signal Average"};
	    lvItem.SubItems.Add(Misc.Average(_sd.AllSignals).ToString("0.00000"));
		lvList.Add(lvItem);

	    lvItem = new ListViewItem {Text = "Selected AT Signal Average"};
	    lvItem.SubItems.Add(Misc.Average(_signalsForArray).ToString("0.00000"));
		lvList.Add(lvItem);

	    lvItem = new ListViewItem {Text = "No. of Flagged Spots - All"};
	    lvItem.SubItems.Add(_flagCountsAll.Flagged.ToString(CultureInfo.InvariantCulture));
		lvList.Add(lvItem);

	    lvItem = new ListViewItem {Text = "No. of Kept (auto) Spots - All"};
	    lvItem.SubItems.Add(_flagCountsAll.KeepAuto.ToString(CultureInfo.InvariantCulture));
		lvList.Add(lvItem);

	    lvItem = new ListViewItem {Text = "No. of Kept Spots - All"};
	    lvItem.SubItems.Add(_flagCountsAll.Keep.ToString(CultureInfo.InvariantCulture));
		lvList.Add(lvItem);

	    lvItem = new ListViewItem {Text = "No. of Discarded Spots - All"};
	    lvItem.SubItems.Add(_flagCountsAll.Discard.ToString(CultureInfo.InvariantCulture));
		lvList.Add(lvItem);

	    lvItem = new ListViewItem {Text = "Total No. of Spots - All"};
	    lvItem.SubItems.Add(_flagCountsAll.Total().ToString(CultureInfo.InvariantCulture));
		lvList.Add(lvItem);

	    lvItem = new ListViewItem {Text = "No. of Flagged Spots - Selected Array"};
	    lvItem.SubItems.Add(_flagCountsArray.Flagged.ToString(CultureInfo.InvariantCulture));
		lvList.Add(lvItem);

	    lvItem = new ListViewItem {Text = "No. of Kept (auto) Spots - Selected Array"};
	    lvItem.SubItems.Add(_flagCountsArray.KeepAuto.ToString(CultureInfo.InvariantCulture));
		lvList.Add(lvItem);

	    lvItem = new ListViewItem {Text = "No. of Kept Spots - Selected Array"};
	    lvItem.SubItems.Add(_flagCountsArray.Keep.ToString(CultureInfo.InvariantCulture));
		lvList.Add(lvItem);

	    lvItem = new ListViewItem {Text = "No. of Discarded Spots - Selected Array"};
	    lvItem.SubItems.Add(_flagCountsArray.Discard.ToString(CultureInfo.InvariantCulture));
		lvList.Add(lvItem);

	    lvItem = new ListViewItem {Text = "Total No. of Spots - Selected Array"};
	    lvItem.SubItems.Add(_flagCountsArray.Total().ToString(CultureInfo.InvariantCulture));
		lvList.Add(lvItem);

		for (int i = 0; i < lvList.Count; i++) {
			lvList[i].BackColor = i % 2 == 0 ? Color.White : Color.LightBlue;
		}

		lvwStats.Items.AddRange(lvList.ToArray());

	}

	/// <summary>Calculates the global error decimal value for the selected array tube sample in the AT visualization form.</summary>
	private double ArrayError()
	{
		var ds = new List<double>();
		List<MarkerInfo> lat = _sd.SampleStats[FrmSampleQC.SelectedArrayIndex];
		foreach (MarkerInfo at in lat)
		{
		    for (int j = 0; j <at.DeltaSignal.Count; j++) {
		        ds.Add(at.DeltaSignal[j]);
		    }
		}
		return Misc.Average(ds);
	}

	/// <summary>Get the flagging status counts for the selected array.</summary>
	private void GetFlaggingStatusCountsForSelectedArray()
	{
	    List<MarkerInfo> lat = _sd.SampleStats[FrmSampleQC.SelectedArrayIndex];
	    foreach (MarkerInfo at in lat)
	    {
	        foreach (Flagging t in at.Flag)
	        {
	            switch (t) {
	                case Flagging.Flagged:
	                    _flagCountsArray.Flagged++;
	                    break;
	                case Flagging.KeepAuto:
	                    _flagCountsArray.KeepAuto++;
	                    break;
	                case Flagging.KeepManual:
	                    _flagCountsArray.Keep++;
	                    break;
	                case Flagging.Discard:
	                    _flagCountsArray.Discard++;
	                    break;
	            }
	        }
	    }
	}

        /// <summary>Get the flagging status counts for the entire dataset.</summary>
	private void GetFlaggingStatusCountsForAll()
	{
		for (int i = 0; i < _sd.SampleCount; i++)
		{
		    List<MarkerInfo> lat = _sd.SampleStats[i];
		    foreach (MarkerInfo at in lat)
		    {
		        foreach (Flagging t in at.Flag)
		        {
		            switch (t) {
		                case Flagging.Flagged:
		                    _flagCountsAll.Flagged++;
		                    break;
		                case Flagging.KeepAuto:
		                    _flagCountsAll.KeepAuto++;
		                    break;
		                case Flagging.KeepManual:
		                    _flagCountsAll.Keep++;
		                    break;
		                case Flagging.Discard:
		                    _flagCountsAll.Discard++;
		                    break;
		            }
		        }
		    }
		}
	}

	/// <summary>Spot flagging status count for spots.</summary>
	private struct FlaggedStats
	{
		/// <summary>Count of flagged spots.</summary>
		public int Flagged;
		/// <summary>Count of automatically kept spots.</summary>
		public int KeepAuto;
		/// <summary>Count of manually kept spots.</summary>
		public int Keep;
		/// <summary>Count of discarded spots.</summary>
		public int Discard;
		/// <summary>Total count of kept, discarded and flagged spots.</summary>
		public int Total()
		{
			return Flagged + Keep + KeepAuto + Discard;
		}
	}

	private void FrmArrayStatsFormClosed(System.Object sender, FormClosedEventArgs e)
	{
		Dispose();
	}

    private void FrmArrayStatsFormClosing(object sender, FormClosingEventArgs e)
    {
        _sd.Forms.ArrayStats = null;
    }
}


}
