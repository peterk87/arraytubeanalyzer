namespace ArrayTubeAnalyzer
{
    public enum Flagging
    {
        Flagged = 0,
        KeepAuto = 1,
        KeepManual = 2,
        Discard = 3
    }
}